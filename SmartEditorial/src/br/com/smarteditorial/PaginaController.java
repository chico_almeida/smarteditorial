package br.com.smarteditorial;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import br.com.smarteditorial.model.bo.CategoriaBO;
import br.com.smarteditorial.model.bo.EdicaoBO;
import br.com.smarteditorial.model.bo.FileUploadBO;
import br.com.smarteditorial.model.bo.FluxoDeTrabalhoBO;
import br.com.smarteditorial.model.bo.PaginaBO;
import br.com.smarteditorial.model.bo.PaginaFluxoBO;
import br.com.smarteditorial.model.bo.PublicacaoBO;
import br.com.smarteditorial.model.bo.TagsBO;
import br.com.smarteditorial.model.bo.UsuarioBO;
import br.com.smarteditorial.model.entities.Categoria;
import br.com.smarteditorial.model.entities.Edicao;
import br.com.smarteditorial.model.entities.FluxoDeTrabalho;
import br.com.smarteditorial.model.entities.Pagina;
import br.com.smarteditorial.model.entities.PaginaFluxo;
import br.com.smarteditorial.model.entities.Usuario;
import br.com.smarteditorial.util.SystemConfiguration;

@Controller
public class PaginaController {
	
	@Autowired
	private FileUploadBO fileUploadBO;
	@Autowired
	private PaginaBO paginaBO;
	@Autowired
	private PublicacaoBO publicacaoBO;
	@Autowired
	private EdicaoBO edicaoBO;
	@Autowired
	private UsuarioBO usuarioBO;
	@Autowired
	private CategoriaBO categoriaBO;
	@Autowired
	private TagsBO tagsBO;
	@Autowired
	private FluxoDeTrabalhoBO fluxoDeTrabalhoBO;
	@Autowired
	private PaginaFluxoBO paginaFluxoBO;
	@Autowired
	SystemConfiguration systemConfiguration;
	
	@RequestMapping("/paginas")
	public String anuncios(Model model) {
		model.addAttribute("pagina", new Pagina());
		model.addAttribute("pchaves", new String());
		model.addAttribute("idPublicacao", new String());
		model.addAttribute("idEdicao", new String());
		model.addAttribute("listaDePublicacoes", this.publicacaoBO.pegaPublicacoes());
		return "/acervo/pagina/cadastro";
	}
	
	public Usuario pegaUsuarioLogado() {
		UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return this.usuarioBO.pegaUsuarioPorEmail(userDetails.getUsername());
	}
	
	/*@RequestMapping(value="/pagina/iniciar-fluxo", method = RequestMethod.POST)
	public String iniciarFluxo(@ModelAttribute("idPagina")String idPagina, @ModelAttribute("comentario")String comentario) {
		PaginaFluxo pagf = new PaginaFluxo();
		List<FluxoDeTrabalho> fxs = this.fluxoDeTrabalhoBO.pegaFluxosPorElemento("pagina", this.paginaBO.pegaPaginaPorId(Integer.parseInt(idPagina)).getEdicao().getPublicacao().getIdPublicacao());
		pagf.setPagina(this.paginaBO.pegaPaginaPorId(Integer.parseInt(idPagina)));
		pagf.setFluxoDeTrabalho(fxs.get(0));
		pagf.setComentario(comentario);
		
		try {
			this.paginaFluxoBO.salvar(pagf);
			return "redirect:/paginas/minhaspaginas";
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| SQLException e) {
			e.printStackTrace();
			return "redirect:/paginas/minhaspaginas?q=erro";
		}
	}*/
	
	@RequestMapping(value = "/paginas/cadastro/add", method = RequestMethod.POST)
	public String salvarImagem(@ModelAttribute("pagina") Pagina pagina, 
			@ModelAttribute("idEdicao") String idEdicao, 
			@ModelAttribute("idCategoria") String idCategoria,
			@RequestParam("PDFarquivo") MultipartFile PDFarquivo, 
			@ModelAttribute("pchaves") String tags) {
		Edicao e = this.edicaoBO.pegaEdicaoId(Integer.parseInt(idEdicao));
		Categoria c = this.categoriaBO.pegaCategoriaPorId(Integer.parseInt(idCategoria));
		String caminho = fileUploadBO.imageUpload(PDFarquivo, e.getPublicacao().getTitulo()+"/"+e.getTitulo(), pagina.getTitulo());
		pagina.setEdicao(e);
		pagina.setCategoria(c);
		pagina.setTags(this.tagsBO.splitTags(tags));
		pagina.setCaminho(caminho);
		pagina.setEstado("Aguardando");
		pagina.setUsuario(pegaUsuarioLogado());
		this.paginaBO.salvarPagina(pagina);
		return "redirect:/paginas/minhaspaginas";
	}
	
	@RequestMapping("/paginas/minhaspaginas")
	public String listaDePaginas(Model model) {
		model.addAttribute("paginasFinalizadas", this.paginaBO.pegaPaginasFinalizadas(pegaUsuarioLogado()));
		model.addAttribute("paginasEmFluxo", this.paginaBO.pegaPaginasDoUsuarioEmFluxo(pegaUsuarioLogado()));
		model.addAttribute("paginasForaDoFluxo", this.paginaBO.pegaPaginasDoUsuarioForaDoFluxo(pegaUsuarioLogado()));
		model.addAttribute("contextPath","http://localhost/");
		return "/acervo/pagina/minhaspaginas";
	}
	
	@RequestMapping(value="/paginas/minhaspaginas/remover", method = RequestMethod.POST)
	public @ResponseBody String removerPagina(@ModelAttribute("id") String idPagina) {
		try {
			this.paginaBO.excluirPagina(this.paginaBO.pegaPaginaPorId(Integer.parseInt(idPagina)));
			return "sucesso";
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| NumberFormatException | SQLException e) {
			e.printStackTrace();
			return "falha";
		}
	}
	
	@RequestMapping(value="/pagina/iniciar-fluxo", method = RequestMethod.POST)
	public @ResponseBody String inicipaFluxo(
			@ModelAttribute("idPagina")String idPagina, 
			@ModelAttribute("comentario")String comentario) throws IndexOutOfBoundsException {
		PaginaFluxo paf = new PaginaFluxo();
		
		List<FluxoDeTrabalho> fxs;
		try {
			fxs = this.fluxoDeTrabalhoBO.pegaFluxosPorElemento("pagina", this.paginaBO.pegaPaginaPorId(Integer.parseInt(idPagina)).getEdicao().getPublicacao().getIdPublicacao());
		} catch (IndexOutOfBoundsException e1) {
			return "vazio";
		}
		
		paf.setPagina(this.paginaBO.pegaPaginaPorId(Integer.parseInt(idPagina)));
		try {
			paf.setFluxoDeTrabalho(fxs.get(0));
		} catch (IndexOutOfBoundsException e1) {
			return "vazio";
		}
		paf.setComentario(comentario);
		
		try {
			this.paginaFluxoBO.salvar(paf);
			return "sucesso";
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| SQLException e) {
			e.printStackTrace();
			return "falha";
		}
		
	}
	
	@RequestMapping(value="/pagina/proximo-fluxo", method = RequestMethod.POST)
	public @ResponseBody String proximoFluxo(@ModelAttribute("idElemento")String idElemento, 
			@ModelAttribute("comentario")String comentario, 
			@ModelAttribute("ordemFluxo") String PosicaoAtual, 
			@ModelAttribute("idPublicacao") String idPublicacao,
			@ModelAttribute("tipoElemento") String tipoElemento,
			@ModelAttribute("idFluxo") String idFluxo) {

		PaginaFluxo paf = new PaginaFluxo();
		Integer i = Integer.parseInt(PosicaoAtual);
		
		FluxoDeTrabalho fx = this.fluxoDeTrabalhoBO.pegaFluxo(++i, Integer.parseInt(idPublicacao), tipoElemento);
		
		if (fx == null) {
			return "ultimo";
		} else {
			paf.setPagina(this.paginaBO.pegaPaginaPorId(Integer.parseInt(idElemento)));
			paf.setFluxoDeTrabalho(fx);
			paf.setComentario(comentario);

			try {
				this.paginaFluxoBO.salvar(paf);
				this.paginaFluxoBO.excluir(this.paginaFluxoBO.pegaPaginaFluxoPorIds(Integer.parseInt(idFluxo), Integer.parseInt(idElemento)));
				return "sucesso";
			} catch (DataIntegrityViolationException | ConstraintViolationException
					| SQLException e) {
				e.printStackTrace();
				return "falha";
			}
		}
	}
	
	@RequestMapping(value="/pagina/retroceder-fluxo", method = RequestMethod.POST)
	public @ResponseBody String retrocerFluxo(@ModelAttribute("idElemento")String idElemento, 
			@ModelAttribute("comentario")String comentario, 
			@ModelAttribute("ordemFluxo") String PosicaoAtual, 
			@ModelAttribute("idPublicacao") String idPublicacao,
			@ModelAttribute("tipoElemento") String tipoElemento,
			@ModelAttribute("idFluxo") String idFluxo) {
		
		if (Integer.parseInt(PosicaoAtual) == 1) {
			try {
				this.paginaFluxoBO.excluir(this.paginaFluxoBO.pegaPaginaFluxoPorIds(Integer.parseInt(idFluxo), Integer.parseInt(idElemento)));
				return "sucesso";
			} catch (DataIntegrityViolationException
					| ConstraintViolationException | NumberFormatException
					| SQLException e) {
				e.printStackTrace();
				return "falha";
			}
		}
		PaginaFluxo paf = new PaginaFluxo();
		Integer i = Integer.parseInt(PosicaoAtual);
		
		FluxoDeTrabalho fx = this.fluxoDeTrabalhoBO.pegaFluxo(--i, Integer.parseInt(idPublicacao), tipoElemento);
		
		paf.setPagina(this.paginaBO.pegaPaginaPorId(Integer.parseInt(idElemento)));
		paf.setFluxoDeTrabalho(fx);
		paf.setComentario(comentario);

		try {
			this.paginaFluxoBO.salvar(paf);
			this.paginaFluxoBO.excluir(this.paginaFluxoBO.pegaPaginaFluxoPorIds(Integer.parseInt(idFluxo), Integer.parseInt(idElemento)));
			return "sucesso";
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| SQLException e) {
			e.printStackTrace();
			return "falha";
		}
		
	}
	
	@RequestMapping(value="/pagina/pegar-elemento", method = RequestMethod.POST)
	public @ResponseBody String pegarElemento(@ModelAttribute("idElemento") Integer idElemento,
			@ModelAttribute("idFluxo") Integer idFluxo) {
		Pagina p = this.paginaBO.pegaPaginaPorId(idElemento);
		p.setEstado("Editando");
		PaginaFluxo paf =  this.paginaFluxoBO.pegaPaginaFluxoPorIds(idFluxo, idElemento);
		paf.setUsuario(pegaUsuarioLogado());
		
		try {
			this.paginaFluxoBO.atualizar(paf);
			this.paginaBO.atualizarPagina(p);
			return "sucesso";
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| SQLException e) {
			e.printStackTrace();
			return "falha";
		}
	}
	
	@RequestMapping(value="/paginas/interromper-fluxo", method = RequestMethod.POST)
	public @ResponseBody String interromperFluxo(@ModelAttribute("idElemento") Integer idElemento,
			@ModelAttribute("idFluxo") Integer idFluxo) {
		Pagina p = this.paginaBO.pegaPaginaPorId(idElemento);
		p.setEstado("Aguardando");
		try {
			this.paginaFluxoBO.excluir(this.paginaFluxoBO.pegaPaginaFluxoPorIds(idFluxo, idElemento));
			this.paginaBO.atualizarPagina(p);
			return "sucesso";
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| SQLException e) {
			e.printStackTrace();
			return "falha";
		}
		
	}
	@RequestMapping(value="/pagina/devolver-elemento", method = RequestMethod.POST)
	public @ResponseBody String devolverElementoParaOGrupo(@ModelAttribute("idElemento") Integer idElemento,
			@ModelAttribute("idFluxo") Integer idFluxo) {
		Pagina p = this.paginaBO.pegaPaginaPorId(idElemento);
		p.setEstado("Aguardando");
		
		PaginaFluxo paf =  this.paginaFluxoBO.pegaPaginaFluxoPorIds(idFluxo, idElemento);
		paf.setUsuario(null);
		
		try {
			this.paginaFluxoBO.atualizar(paf);
			this.paginaBO.atualizarPagina(p);
			return "sucesso";
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| SQLException e) {
			e.printStackTrace();
			return "falha";
		}
	}
	
	@RequestMapping(value="/pagina/preview", method = RequestMethod.POST)
	public String previewAnuncio(@ModelAttribute("id") Integer id, Model model) {
		model.addAttribute("contextPath", this.systemConfiguration.getUrlAcervo());
		model.addAttribute("pagina", this.paginaBO.pegaPaginaPorId(id));
		return "/restrito/paginaPreview";
	}
}
