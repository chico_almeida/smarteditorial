package br.com.smarteditorial.service;

import java.sql.SQLException;

import javax.transaction.Transactional;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import br.com.smarteditorial.dao.ImagemFluxoDAO;
import br.com.smarteditorial.model.entities.ImagemFluxo;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

@Service

public class ImagemFluxoServiceImpl implements ImagemFluxoService {
	
	private ImagemFluxoDAO imagemFluxoDAO;
	
	public void setImagemFluxoDAO(ImagemFluxoDAO imagemFluxoDAO) {
		this.imagemFluxoDAO = imagemFluxoDAO;
	}
		
	@Override
	@Transactional
	public void salvar(ImagemFluxo imgf)
			throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.imagemFluxoDAO.salvar(imgf);
	}

	@Override
	@Transactional
	public void atualizar(ImagemFluxo imgf)
			throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.imagemFluxoDAO.atualizar(imgf);
	}

	@Override
	@Transactional
	public void merge(ImagemFluxo imgf) throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.imagemFluxoDAO.merge(imgf);
	}

	@Override
	@Transactional
	public void excluir(ImagemFluxo imgf)
			throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.imagemFluxoDAO.excluir(imgf);
	}

	@Override
	@Transactional
	public ImagemFluxo imagemFluxoPorIds(Integer idFluxo, Integer idImagem) {
		return this.imagemFluxoDAO.imagemFluxoPorIds(idFluxo, idImagem);
	}

}
