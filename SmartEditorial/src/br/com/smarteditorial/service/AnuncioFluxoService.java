package br.com.smarteditorial.service;

import java.sql.SQLException;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.smarteditorial.model.entities.AnuncioFluxo;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

public interface AnuncioFluxoService {
	public void salvar(AnuncioFluxo anunf) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException;
	public void atualizar(AnuncioFluxo anunf) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException;
	public void merge(AnuncioFluxo anunf) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException;
	public void excluir(AnuncioFluxo anunf) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException;
	public AnuncioFluxo anuncioFluxoPorIds(Integer idFluxo, Integer idAnuncio);
}
