package br.com.smarteditorial.service;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.smarteditorial.model.entities.Categoria;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

public interface CategoriaService {
	public void salvarCategoria(Categoria c) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException;
	public void atualizarCategoria(Categoria c) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException;
	public void mergeCategoria(Categoria c) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException;
	public List<Categoria> listarCategorias() throws SQLException;
	public void excluirCategoria(Categoria c) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException;
	public Categoria categoriaPorId(Integer id) throws SQLException;
	public Categoria categoriaPorTitulo(String titulo) throws SQLException;
	public List<Categoria> categoriaPorPublicacao(Integer id) throws SQLException;

}
