package br.com.smarteditorial.service;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.smarteditorial.dao.EmpresaDAO;
import br.com.smarteditorial.model.entities.Empresa;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

@Service
public class EmpresaServiceImpl implements EmpresaService  {
	
	private EmpresaDAO empresaDAO;
	
	public void setEmpresaDAO(EmpresaDAO empresaDAO) {
		this.empresaDAO = empresaDAO;
	}

	@Override
	@Transactional
	public void salvarEmpresa(Empresa e)
			throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.empresaDAO.salvar(e);
	}

	@Override
	@Transactional
	public void atualizarEmpresa(Empresa e)
			throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.empresaDAO.atualizar(e);
	}

	@Override
	@Transactional
	public void mergeEmpresa(Empresa e) throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.empresaDAO.merge(e);
	}

	@Override
	@Transactional
	public void excluirEmpresa(Empresa e)
			throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.empresaDAO.excluir(e);
	}

	@Override
	@Transactional
	public List<Empresa> listarEmpresas() throws SQLException {
		return this.empresaDAO.listarEmpresas();
	}

	@Override
	@Transactional
	public Empresa empresaPorNomeEmpresa(String empresa) throws SQLException {
		return this.empresaDAO.empresaPorNomeEmpresa(empresa);
	}

	@Override
	@Transactional
	public Empresa empresaPorId(Integer id) throws SQLException {
		return this.empresaDAO.empresaPorId(id);
	}

	@Override
	@Transactional
	public Empresa empresaPorContato(String contato) throws SQLException {
		return this.empresaPorContato(contato);
	}

}
