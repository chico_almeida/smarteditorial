package br.com.smarteditorial.service;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.smarteditorial.dao.PermissaoDAO;
import br.com.smarteditorial.model.entities.Permissao;

@Service
public class PermissaoServiceImpl implements PermissaoService{
	
	private PermissaoDAO permissaoDAO;
	
	public void setPermissaoDAO(PermissaoDAO permissaoDAO) {
        this.permissaoDAO = permissaoDAO;
    }

	@Override
	@Transactional
	public void salvarPermissao(Permissao p)throws DataIntegrityViolationException, ConstraintViolationException, SQLException {
		this.permissaoDAO.salvar(p);
	}

	@Override
	@Transactional
	public void atualizarPermissao(Permissao p) throws DataIntegrityViolationException, ConstraintViolationException, SQLException {
		this.permissaoDAO.atualizar(p);
		
	}

	@Override
	@Transactional
	public List<Permissao> listarPermissao() throws SQLException {
		return this.permissaoDAO.listarPermissao();
	}

	@Override
	@Transactional
	public Permissao permissaoPorTipo(String tipo) throws SQLException {
		return this.permissaoDAO.permissaoPorTipo(tipo);
	}

	@Override
	@Transactional
	public void excluirPermissao(Permissao p) throws SQLException {
		this.permissaoDAO.excluir(p);
	}

	@Override
	@Transactional
	public void mergePermissao(Permissao p) throws DataIntegrityViolationException, ConstraintViolationException, SQLException {
		this.permissaoDAO.merge(p);
		
	}

	@Override
	@Transactional
	public Permissao permissaoPorId(Integer id) throws SQLException {
		return this.permissaoDAO.permissaoPorId(id);
	}


}
