package br.com.smarteditorial.service;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

import br.com.smarteditorial.dao.PublicacaoDAO;
import br.com.smarteditorial.model.entities.Publicacao;

@Service
public class PublicacaoServiceImpl implements PublicacaoService {
	
	private PublicacaoDAO publicacaoDAO;
	
	public void setPublicacaoDAO(PublicacaoDAO publicacaoDAO) {
		this.publicacaoDAO = publicacaoDAO;
	}

	@Override
	@Transactional
	public void salvarPublicacao(Publicacao p)
			throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException {
		this.publicacaoDAO.salvar(p);
	}

	@Override
	@Transactional
	public void atualizarPublicacao(Publicacao p)
			throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException {
		this.publicacaoDAO.atualizar(p);
	}

	@Override
	@Transactional
	public void mergePublicacao(Publicacao p)
			throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException {
		this.publicacaoDAO.merge(p);
	}

	@Override
	@Transactional
	public List<Publicacao> listarPublicacoes() throws SQLException {
		return this.publicacaoDAO.listarPublicacoes();
	}

	@Override
	@Transactional
	public void excluirPublicacao(Publicacao p) throws DataIntegrityViolationException, ConstraintViolationException, MySQLIntegrityConstraintViolationException, SQLException {
		this.publicacaoDAO.excluir(p);
	}

	@Override
	@Transactional
	public Publicacao publicacaoPorTitulo(String titulo) throws SQLException {
		return this.publicacaoDAO.publicacaoPorTitulo(titulo);
	}

	@Override
	@Transactional
	public Publicacao publicacaoPorId(Integer id) throws SQLException {
		return this.publicacaoDAO.publicacaoPorId(id);
	}


}
