package br.com.smarteditorial.service;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.smarteditorial.dao.EspelhoDAO;
import br.com.smarteditorial.model.entities.Espelho;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

@Service
public class EspelhoServiceImpl implements EspelhoService {
	
	private EspelhoDAO espelhoDAO;
	public void setEspelhoDAO(EspelhoDAO espelhoDAO) {
		this.espelhoDAO = espelhoDAO;
	}
	
	@Override
	@Transactional
	public void salvarEspelho(Espelho e)
			throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.espelhoDAO.salvar(e);
	}

	@Override
	@Transactional
	public void atualizarEspelho(Espelho e)
			throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.espelhoDAO.atualizar(e);
	}

	@Override
	@Transactional
	public void mergeEspelho(Espelho e) throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.espelhoDAO.merge(e);
	}

	@Override
	@Transactional
	public void excluirEspelho(Espelho e)
			throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.espelhoDAO.excluir(e);
	}

	@Override
	@Transactional
	public List<Espelho> Espelhos() throws SQLException {
		return this.espelhoDAO.Espelhos();
	}

	@Override
	@Transactional
	public Espelho espelhoDaEdicao(Integer idEdicao) throws SQLException {
		return this.espelhoDAO.espelhoDaEdicao(idEdicao);
	}

	@Override
	@Transactional
	public Espelho espelhoPorId(Integer id) throws SQLException {
		return this.espelhoDAO.espelhoPorId(id);
	}

}
