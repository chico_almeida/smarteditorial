package br.com.smarteditorial.service;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.smarteditorial.model.entities.Grupo;

public interface GrupoService {
	public void salvarGrupo(Grupo g) throws DataIntegrityViolationException, ConstraintViolationException, SQLException;
	public void atualizarGrupo(Grupo g) throws DataIntegrityViolationException, ConstraintViolationException, SQLException;
	public void mergeGrupo(Grupo g) throws DataIntegrityViolationException, ConstraintViolationException, SQLException;
	public List<Grupo> listarGrupos() throws SQLException;
	public Grupo grupoPorNome(String nome) throws SQLException;
	public void excluirGrupo(Grupo g) throws SQLException;
	public Grupo grupoPorId(Integer id) throws SQLException;
	public void desvicularPermissao(Integer idU, Integer idP) throws SQLException;
	public void desvicularUsuario(Integer idG, Integer idU) throws SQLException;
}
