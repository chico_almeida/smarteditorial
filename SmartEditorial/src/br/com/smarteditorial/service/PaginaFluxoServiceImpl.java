package br.com.smarteditorial.service;

import java.sql.SQLException;

import javax.transaction.Transactional;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import br.com.smarteditorial.dao.PaginaFluxoDAO;
import br.com.smarteditorial.model.entities.PaginaFluxo;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

@Service

public class PaginaFluxoServiceImpl implements PaginaFluxoService {
	
	private PaginaFluxoDAO paginaFluxoDAO;
	
	public void setPaginaFluxoDAO(PaginaFluxoDAO paginaFluxoDAO) {
		this.paginaFluxoDAO = paginaFluxoDAO;
	}
		
	@Override
	@Transactional
	public void salvar(PaginaFluxo pagf)
			throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.paginaFluxoDAO.salvar(pagf);
	}

	@Override
	@Transactional
	public void atualizar(PaginaFluxo pagf)
			throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.paginaFluxoDAO.atualizar(pagf);
	}

	@Override
	@Transactional
	public void merge(PaginaFluxo pagf) throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.paginaFluxoDAO.merge(pagf);
	}

	@Override
	@Transactional
	public void excluir(PaginaFluxo pagf)
			throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.paginaFluxoDAO.excluir(pagf);
	}

	@Override
	@Transactional
	public PaginaFluxo paginaFluxoPorIds(Integer idFluxo, Integer idPagina) {
		return this.paginaFluxoDAO.paginaFluxoPorIds(idFluxo, idPagina);
	}

}
