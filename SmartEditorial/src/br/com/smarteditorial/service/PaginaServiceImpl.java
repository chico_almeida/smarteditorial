package br.com.smarteditorial.service;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.smarteditorial.dao.PaginaDAO;
import br.com.smarteditorial.model.entities.Pagina;
import br.com.smarteditorial.model.entities.PaginaFluxo;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
@Service
public class PaginaServiceImpl implements PaginaService {
	
	private  PaginaDAO paginaDAO;
	
	public void setPaginaDAO(PaginaDAO paginaDAO) {
		this.paginaDAO = paginaDAO;
	}

	@Override
	@Transactional
	public void salvarPagina(Pagina p) throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.paginaDAO.salvar(p);
	}

	@Override
	@Transactional
	public void atualizarPagina(Pagina p)
			throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.paginaDAO.atualizar(p);
	}

	@Override
	@Transactional
	public void mergePagina(Pagina p) throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.paginaDAO.merge(p);
	}

	@Override
	@Transactional
	public void excluirPagina(Pagina p) throws SQLException {
		this.paginaDAO.excluir(p);
	}

	@Override
	@Transactional
	public List<Pagina> listarPaginas() throws SQLException {
		return this.paginaDAO.listarPaginas();
	}

	@Override
	@Transactional
	public List<Pagina> listarPaginasPorUsuario(Integer idUsuario)
			throws SQLException {
		return paginaDAO.listarPaginasPorUsuario(idUsuario);
	}

	@Override
	@Transactional
	public Pagina paginaPorTitulo(String titulo) throws SQLException {
		return paginaDAO.paginaPorTitulo(titulo);
	}

	@Override
	@Transactional
	public Pagina paginaPorId(Integer id) throws SQLException {
		return paginaDAO.paginaPorId(id);
	}

	@Override
	@Transactional
	public List<PaginaFluxo> listarPaginasDoUsuarioEmFluxo(Integer idUsuario) {
		return this.paginaDAO.listarPaginasDoUsuarioEmFluxo(idUsuario);
	}

	@Override
	@Transactional
	public List<Pagina> listarPaginasPorUsuarioForaDoFluxo(Integer idUsuario)
			throws SQLException {
		return this.paginaDAO.listarPaginasPorUsuarioForaDoFluxo(idUsuario);
	}

	@Override
	@Transactional
	public List<PaginaFluxo> paginasDoGrupoAtualNoFluxo() {
		return this.paginaDAO.paginasDoGrupoAtualNoFluxo();
	}

	@Override
	@Transactional
	public List<Pagina> listarPaginasDoUsuarioFinalizadas(Integer idUsuario) {
		return this.paginaDAO.listarPaginasDoUsuarioFinalizadas(idUsuario);
	}

	@Override
	@Transactional
	public List<Pagina> listarPaginasFinalizadasDaEdicao(Integer idEdicao) {
		return this.paginaDAO.listarPaginasFinalizadasDaEdicao(idEdicao);
	}

}
