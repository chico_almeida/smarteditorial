package br.com.smarteditorial.service;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.smarteditorial.dao.UsuarioAlteraImagemDAO;
import br.com.smarteditorial.model.entities.Edicao;
import br.com.smarteditorial.model.entities.Imagem;
import br.com.smarteditorial.model.entities.Usuario;
import br.com.smarteditorial.model.entities.UsuarioAlteraImagem;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

public interface UsuarioAlteraImagemService {
	public void salvar(UsuarioAlteraImagem uai) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException;
	public void excluir(UsuarioAlteraImagem uai) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException;
	public void atualizar(UsuarioAlteraImagem uai) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException;
	public void merge(UsuarioAlteraImagem uai) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException;
	public List<UsuarioAlteraImagemDAO> imagensModificadasPorUsuarioNaEdicao(Usuario u, Edicao e);
	public List<UsuarioAlteraImagemDAO> imagensModificadasPorUsuario(Usuario u);
	public UsuarioAlteraImagem alteracaoPorImagem(Imagem i);
}
