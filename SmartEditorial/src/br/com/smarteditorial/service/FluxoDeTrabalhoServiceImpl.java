package br.com.smarteditorial.service;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.smarteditorial.dao.FluxoDeTrabalhoDAO;
import br.com.smarteditorial.model.entities.FluxoDeTrabalho;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

@Service
public class FluxoDeTrabalhoServiceImpl implements FluxoDeTrabalhoService {
	
	private FluxoDeTrabalhoDAO fluxoDeTrabalhoDAO;
	
	public void setFluxoDeTrabalhoDAO (FluxoDeTrabalhoDAO fluxoDeTrabalhoDAO) {
		this.fluxoDeTrabalhoDAO = fluxoDeTrabalhoDAO;
	}

	@Override
	@Transactional
	public void salvarFluxoDeTrabalho(FluxoDeTrabalho f)
			throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.fluxoDeTrabalhoDAO.salvar(f);
	}

	@Override
	@Transactional
	public void atualizarFluxoDeTrabalho(FluxoDeTrabalho f)
			throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.fluxoDeTrabalhoDAO.atualizar(f);
	}

	@Override
	@Transactional
	public void mergeFluxoDeTrabalho(FluxoDeTrabalho f)
			throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.fluxoDeTrabalhoDAO.merge(f);
	}

	@Override
	@Transactional
	public void excluirFluxoDeTrabalho(FluxoDeTrabalho f)
			throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.fluxoDeTrabalhoDAO.excluir(f);
	}

	@Override
	@Transactional
	public List<FluxoDeTrabalho> listarTrabalhos() throws SQLException {
		return this.fluxoDeTrabalhoDAO.listarTrabalhos();
	}

	@Override
	@Transactional
	public FluxoDeTrabalho fluxoPorId(Integer id)
			throws SQLException {
		return this.fluxoDeTrabalhoDAO.fluxoPorId(id);
	}

	@Override
	@Transactional
	public List<FluxoDeTrabalho> listaFluxosPorElementoDaPublicacao(String elemento, Integer idPublicacao)
			throws SQLException {
		return this.fluxoDeTrabalhoDAO.listaFluxosPorElementoDaPublicacao(elemento, idPublicacao);
	}

	@Override
	@Transactional
	public FluxoDeTrabalho pegaFluxo(Integer ordem, Integer idPublicacao, String tipoDeElemetno) {
		return this.fluxoDeTrabalhoDAO.pegaFluxo(ordem, idPublicacao, tipoDeElemetno);
	}
	
}
