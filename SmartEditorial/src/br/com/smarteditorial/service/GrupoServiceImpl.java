package br.com.smarteditorial.service;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.smarteditorial.dao.GrupoDAO;
import br.com.smarteditorial.model.entities.Grupo;

@Service
public class GrupoServiceImpl implements GrupoService{
	
	private GrupoDAO grupoDAO;
	
	public void setGrupoDAO(GrupoDAO grupoDAO) {
        this.grupoDAO = grupoDAO;
    }

	@Override
	@Transactional
	public void salvarGrupo(Grupo g) throws DataIntegrityViolationException, ConstraintViolationException, SQLException {
		this.grupoDAO.salvar(g);
	}
	
	@Override
	@Transactional
	public void atualizarGrupo(Grupo g) throws DataIntegrityViolationException, ConstraintViolationException, SQLException {
		this.grupoDAO.atualizar(g);
	}
	@Override
	@Transactional
	public List<Grupo> listarGrupos() throws SQLException {
		return this.grupoDAO.listarGrupos();
	}
	@Override
	@Transactional
	public Grupo grupoPorNome(String nome) throws SQLException {
		return this.grupoDAO.grupoPorNome(nome);
	}
	@Override
	@Transactional
	public void excluirGrupo(Grupo g) throws SQLException {
		this.grupoDAO.excluir(g);
	}

	@Override
	@Transactional
	public Grupo grupoPorId(Integer id) throws SQLException {
		return this.grupoDAO.grupoPorId(id);
	}

	@Override
	@Transactional
	public void mergeGrupo(Grupo g) throws DataIntegrityViolationException, ConstraintViolationException, SQLException {
		this.grupoDAO.merge(g);
		
	}
	@Override
	@Transactional
	public void desvicularPermissao(Integer idU, Integer idP) {
		this.grupoDAO.desvicularPermissao(idU, idP);
	}

	@Override
	@Transactional
	public void desvicularUsuario(Integer idG, Integer idU) throws SQLException {
		this.grupoDAO.desvicularUsuario(idG, idU);
	}
}
