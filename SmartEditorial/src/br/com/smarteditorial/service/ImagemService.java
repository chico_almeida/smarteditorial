package br.com.smarteditorial.service;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.smarteditorial.model.entities.Imagem;
import br.com.smarteditorial.model.entities.ImagemFluxo;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

public interface ImagemService {
	public void salvarImagem(Imagem i) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException;
	public void atualizarImagem(Imagem i) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException;
	public void mergeImagem(Imagem i) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException;
	public List<Imagem> listarImagem() throws SQLException;
	public List<Imagem> listarImagensPorUsuario(Integer idUsuario) throws SQLException;
	public void excluirImagem(Imagem i) throws SQLException;
	public Imagem imagemPorId(Integer id) throws SQLException;
	public Imagem imagemPorTitulo(String titulo) throws SQLException;
	public List<Imagem> listarImagensDoUsuarioFinalizadas(Integer idUsuario);
	public List<ImagemFluxo> listarImagensDoUsuarioEmFluxo(Integer idUsuario);
	public List<Imagem> listarImagensPorUsuarioForaDoFluxo(Integer idUsuario) throws SQLException;
	public List<ImagemFluxo> imagensDoGrupoAtualNoFluxo();
	public List<Imagem> listarImagensFinalizadasDaEdicao(Integer idEdicao);
}
