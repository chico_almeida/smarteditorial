package br.com.smarteditorial.service;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.smarteditorial.model.entities.Empresa;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

public interface EmpresaService {
	
	public void salvarEmpresa(Empresa e) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException;
	public void atualizarEmpresa(Empresa e) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException;
	public void mergeEmpresa(Empresa e) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException;
	public void excluirEmpresa(Empresa e) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException;
	public List<Empresa> listarEmpresas () throws SQLException;
	public Empresa empresaPorNomeEmpresa(String empresa) throws SQLException;
	public Empresa empresaPorId(Integer id) throws SQLException;
	public Empresa empresaPorContato(String contato) throws SQLException;

}
