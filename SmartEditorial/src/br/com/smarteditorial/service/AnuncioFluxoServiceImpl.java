package br.com.smarteditorial.service;

import java.sql.SQLException;

import javax.transaction.Transactional;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import br.com.smarteditorial.dao.AnuncioFluxoDAO;
import br.com.smarteditorial.model.entities.AnuncioFluxo;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

@Service
public class AnuncioFluxoServiceImpl implements AnuncioFluxoService {
	
	private AnuncioFluxoDAO anuncioFluxoDAO;
	
	public void setAnuncioFluxoDAO(AnuncioFluxoDAO anuncioFluxoDAO) {
		this.anuncioFluxoDAO = anuncioFluxoDAO;
	}
		
	@Override
	@Transactional
	public void salvar(AnuncioFluxo anunf)
			throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.anuncioFluxoDAO.salvar(anunf);
	}

	@Override
	@Transactional
	public void atualizar(AnuncioFluxo anunf)
			throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.anuncioFluxoDAO.atualizar(anunf);
	}

	@Override
	@Transactional
	public void merge(AnuncioFluxo anunf) throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.anuncioFluxoDAO.merge(anunf);
	}

	@Override
	@Transactional
	public void excluir(AnuncioFluxo anunf)
			throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.anuncioFluxoDAO.excluir(anunf);
	}

	@Override
	@Transactional
	public AnuncioFluxo anuncioFluxoPorIds(Integer idFluxo, Integer idAnuncio) {
		return this.anuncioFluxoDAO.anuncioFluxoPorIds(idFluxo, idAnuncio);
	}

}
