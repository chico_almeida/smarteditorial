package br.com.smarteditorial.service;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

import br.com.smarteditorial.model.entities.Edicao;

public interface EdicaoService {
	public void salvarEdicao(Edicao e) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException;
	public void atualizarEdicao(Edicao e) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException;
	public void mergeEdicao(Edicao e) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException;
	public List<Edicao> listarEdicoes() throws SQLException;
	public void excluirEdicao(Edicao e) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException;
	public Edicao edicaoPorId(Integer id) throws SQLException;
	public Edicao edicaoPorTitulo(String titulo) throws SQLException;
	public List<Edicao> edicaoPorPublicacao(Integer id) throws SQLException;
}
