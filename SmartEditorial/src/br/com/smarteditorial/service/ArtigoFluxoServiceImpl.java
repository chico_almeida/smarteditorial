package br.com.smarteditorial.service;

import java.sql.SQLException;

import javax.transaction.Transactional;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import br.com.smarteditorial.dao.ArtigoFluxoDAO;
import br.com.smarteditorial.model.entities.ArtigoFluxo;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

@Service

public class ArtigoFluxoServiceImpl implements ArtigoFluxoService {
	
	private ArtigoFluxoDAO artigoFluxoDAO;
	
	public void setArtigoFluxoDAO(ArtigoFluxoDAO artigoFluxoDAO) {
		this.artigoFluxoDAO = artigoFluxoDAO;
	}
		
	@Override
	@Transactional
	public void salvar(ArtigoFluxo artf)
			throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.artigoFluxoDAO.salvar(artf);
	}

	@Override
	@Transactional
	public void atualizar(ArtigoFluxo artf)
			throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.artigoFluxoDAO.atualizar(artf);
	}

	@Override
	@Transactional
	public void merge(ArtigoFluxo artf) throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.artigoFluxoDAO.merge(artf);
	}

	@Override
	@Transactional
	public void excluir(ArtigoFluxo artf)
			throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.artigoFluxoDAO.excluir(artf);
	}

	@Override
	@Transactional
	public ArtigoFluxo artigoFluxoPorIds(Integer idFluxo, Integer idArtigo) {
		return this.artigoFluxoDAO.artigoFluxoPorIds(idFluxo, idArtigo);
	}

}
