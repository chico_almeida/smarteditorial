package br.com.smarteditorial.service;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.smarteditorial.model.entities.Permissao;

public interface PermissaoService {

	public void salvarPermissao(Permissao p) throws DataIntegrityViolationException, ConstraintViolationException, SQLException;
	public void mergePermissao(Permissao p) throws DataIntegrityViolationException, ConstraintViolationException, SQLException;
	public void atualizarPermissao(Permissao p) throws DataIntegrityViolationException, ConstraintViolationException, SQLException;
	public List<Permissao> listarPermissao() throws SQLException;
	public Permissao permissaoPorTipo(String tipo) throws SQLException;
	public Permissao permissaoPorId(Integer id) throws SQLException;
	public void excluirPermissao(Permissao p) throws SQLException;
	
}
