package br.com.smarteditorial.service;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.smarteditorial.dao.UsuarioDAO;
import br.com.smarteditorial.model.entities.MapElementos;
import br.com.smarteditorial.model.entities.Usuario;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

@Service
public class UsuarioServiceImpl implements UsuarioService{
	UsuarioDAO usuarioDAO;

	public void setUsuarioDAO(UsuarioDAO usuarioDAO) {
		this.usuarioDAO = usuarioDAO;
	}

	@Override
	@Transactional
	public void salvarUsuario(Usuario u) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException {
		this.usuarioDAO.salvar(u);
	}

	@Override
	@Transactional
	public void atualizarUsuario(Usuario u) throws DataIntegrityViolationException, 
	ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException {
		this.usuarioDAO.atualizar(u);
	}
	@Override
	@Transactional
	public List<Usuario> listarUsuarios() throws SQLException {
		return this.usuarioDAO.listarUsuarios();
	}

	@Override
	@Transactional
	public Usuario usuarioPorEmail(String email) throws SQLException {
		return this.usuarioDAO.usuarioPorEmail(email);
	}

	@Override
	@Transactional
	public Usuario usuarioPorId(int id) throws SQLException {
		return this.usuarioDAO.usuarioPorId(id);
	}

	@Override
	@Transactional
	public void excluirUsuario(Usuario u) throws SQLException {
		this.usuarioDAO.excluir(u);

	}

	@Override
	@Transactional
	public void mergeUsuario(Usuario u) throws DataIntegrityViolationException,
	ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException {
		this.usuarioDAO.merge(u);
	}

	@Override
	@Transactional
	public List<Integer> gruposDoUsuario(Integer idUsuario) {
		return this.usuarioDAO.gruposDoUsuario(idUsuario);
	}

	@Override
	@Transactional
	public List<MapElementos> listarElementosEmFluxoParaOGrupo(String grupos) {
		return this.usuarioDAO.listarElementosEmFluxoParaOGrupo(grupos);
	}
	@Override
	@Transactional
	public List<MapElementos> listarElementosEmFluxoParaOUsuario(String grupos, Integer idUsuario) {
		return this.usuarioDAO.listarElementosEmFluxoParaOUsuario(grupos, idUsuario);
	}

}
