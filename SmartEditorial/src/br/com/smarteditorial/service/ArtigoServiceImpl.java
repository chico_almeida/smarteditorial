package br.com.smarteditorial.service;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.smarteditorial.dao.ArtigoDAO;
import br.com.smarteditorial.model.entities.Artigo;
import br.com.smarteditorial.model.entities.ArtigoFluxo;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

@Service
public class ArtigoServiceImpl implements ArtigoService {

	private ArtigoDAO artigoDAO;
	
	public void setArtigoDAO(ArtigoDAO artigoDAO) {
		this.artigoDAO = artigoDAO;
	}
	
	@Override
	@Transactional
	public void salvarArtigo(Artigo a)
			throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.artigoDAO.salvar(a);
	}

	@Override
	@Transactional
	public void atualizarArtigo(Artigo a)
			throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.artigoDAO.atualizar(a);
	}

	@Override
	@Transactional
	public void mergeArtigo(Artigo a) throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.artigoDAO.merge(a);
	}

	@Override
	@Transactional
	public List<Artigo> listarArtigos() throws SQLException {
		return this.artigoDAO.listarArtigos();
	}

	@Override
	@Transactional
	public void excluirArtigo(Artigo a) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException {
		this.artigoDAO.excluir(a);
	}

	@Override
	@Transactional
	public Artigo artigoPorId(Integer id) throws SQLException {
		return this.artigoDAO.artigoPorId(id);
	}

	@Override
	@Transactional
	public Artigo artigoPorTitulo(String titulo) throws SQLException {
		return this.artigoDAO.artigoPorTitulo(titulo);
	}

	@Override
	@Transactional
	public List<Artigo> artigoPorUsuario(Integer idUsuario) throws SQLException {
		return this.artigoDAO.artigoPorUsuario(idUsuario);
	}

	@Override
	@Transactional
	public List<Artigo> listarArtigosDoUsuarioAguardando(Integer idUsuario) {
		return this.artigoDAO.listarArtigosDoUsuarioAguardando(idUsuario);
	}

	@Override
	@Transactional
	public List<Artigo> listarArtigosDoUsuarioFinalizadas(Integer idUsuario) {
		return this.artigoDAO.listarArtigosDoUsuarioFinalizadas(idUsuario);
	}

	@Override
	@Transactional
	public List<Artigo> listarArtigosDoUsuarioAprovado(Integer idUsuario) {
		return this.artigoDAO.listarArtigosDoUsuarioAprovado(idUsuario);
	}

	@Override
	@Transactional
	public List<ArtigoFluxo> listarArtigosDoUsuarioEmFluxo(Integer idUsuario) {
		return this.artigoDAO.listarArtigosDoUsuarioEmFluxo(idUsuario);
	}

	@Override
	@Transactional
	public List<Artigo> listarArtigosPorUsuarioForaDoFluxo(Integer idUsuario)
			throws SQLException {
		return this.artigoDAO.listarArtigosPorUsuarioForaDoFluxo(idUsuario);
	}

	@Override
	@Transactional
	public List<ArtigoFluxo> artigosDoGrupoAtualNoFluxo() {
		return this.artigoDAO.artigosDoGrupoAtualNoFluxo();
	}

	@Override
	@Transactional
	public List<Artigo> listarArtigosFinalizadasDaEdicao(Integer idEdicao) {
		return this.artigoDAO.listarArtigosFinalizadasDaEdicao(idEdicao);
	}

}
