package br.com.smarteditorial.service;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.smarteditorial.dao.ImagemDAO;
import br.com.smarteditorial.model.entities.Imagem;
import br.com.smarteditorial.model.entities.ImagemFluxo;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

@Service
public class ImagemServiceImpl implements ImagemService {

	private ImagemDAO imagemDAO;
	
	public void setImagemDAO(ImagemDAO imagemDAO) {
		this.imagemDAO = imagemDAO;
	}
	
	@Override
	@Transactional
	public void salvarImagem(Imagem i)
			throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.imagemDAO.salvar(i);
	}

	@Override
	@Transactional
	public void atualizarImagem(Imagem i)
			throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.imagemDAO.atualizar(i);
	}

	@Override
	@Transactional
	public void mergeImagem(Imagem i) throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.imagemDAO.merge(i);
	}

	@Override
	@Transactional
	public List<Imagem> listarImagem() throws SQLException {
		return this.imagemDAO.listarImagens();
	}

	@Override
	@Transactional
	public void excluirImagem(Imagem i) throws SQLException {
		this.imagemDAO.excluir(i);
	}

	@Override
	@Transactional
	public Imagem imagemPorId(Integer id) throws SQLException {
		return this.imagemDAO.imagemPorId(id);
	}

	@Override
	@Transactional
	public Imagem imagemPorTitulo(String titulo) throws SQLException {
		return this.imagemDAO.imagemPorTitulo(titulo);
	}

	@Override
	@Transactional
	public List<Imagem> listarImagensPorUsuario(Integer idUsuario)
			throws SQLException {
		return this.imagemDAO.listarImagensPorUsuario(idUsuario);
	}

	@Override
	@Transactional
	public List<Imagem> listarImagensDoUsuarioFinalizadas(Integer idUsuario) {
		return this.imagemDAO.listarImagensDoUsuarioFinalizadas(idUsuario);
	}
	
	@Override
	@Transactional
	public List<ImagemFluxo> listarImagensDoUsuarioEmFluxo(Integer idUsuario) {
		return this.imagemDAO.listarImagensDoUsuarioEmFluxo(idUsuario);
	}

	@Override
	@Transactional
	public List<Imagem> listarImagensPorUsuarioForaDoFluxo(Integer idUsuario)
			throws SQLException {
		return this.imagemDAO.listarImagensPorUsuarioForaDoFluxo(idUsuario);
	}

	@Override
	@Transactional
	public List<ImagemFluxo> imagensDoGrupoAtualNoFluxo() {
		return this.imagemDAO.imagensDoGrupoAtualNoFluxo();
	}

	@Override
	@Transactional
	public List<Imagem> listarImagensFinalizadasDaEdicao(Integer idEdicao) {
		return this.imagemDAO.listarImagensFinalizadasDaEdicao(idEdicao);
	}

}
