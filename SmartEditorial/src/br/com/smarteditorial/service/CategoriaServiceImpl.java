package br.com.smarteditorial.service;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.smarteditorial.dao.CategoriaDAO;
import br.com.smarteditorial.model.entities.Categoria;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

@Service
public class CategoriaServiceImpl implements CategoriaService {
	
	private CategoriaDAO categoriaDAO;
	
	public void setCategoriaDAO(CategoriaDAO categoriaDAO) {
		this.categoriaDAO = categoriaDAO;
	}
	
	@Override
	@Transactional
	public void salvarCategoria(Categoria c)
			throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.categoriaDAO.salvar(c);
	}

	@Override
	@Transactional
	public void atualizarCategoria(Categoria c)
			throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.categoriaDAO.atualizar(c);
	}

	@Override
	@Transactional
	public void mergeCategoria(Categoria c)
			throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.categoriaDAO.merge(c);
	}

	@Override
	@Transactional
	public List<Categoria> listarCategorias() throws SQLException {
		return this.categoriaDAO.listarCategorias();
	}

	@Override
	@Transactional
	public void excluirCategoria(Categoria c)
			throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.categoriaDAO.excluir(c);
	}

	@Override
	@Transactional
	public Categoria categoriaPorId(Integer id) throws SQLException {
		return this.categoriaDAO.categoriaPorId(id);
	}

	@Override
	@Transactional
	public Categoria categoriaPorTitulo(String titulo) throws SQLException {
		return this.categoriaDAO.categoriaPorTitulo(titulo);
	}
	
	@Override
	@Transactional
	public List<Categoria> categoriaPorPublicacao(Integer id) throws SQLException {
		return this.categoriaDAO.categoriaPorPublicacao(id);
	}
}
