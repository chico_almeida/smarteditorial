package br.com.smarteditorial.service;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.smarteditorial.dao.AnuncioDAO;
import br.com.smarteditorial.model.entities.Anuncio;
import br.com.smarteditorial.model.entities.AnuncioFluxo;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

@Service
public class AnuncioServiceImpl implements AnuncioService {

	private AnuncioDAO anuncioDAO;
	
	public void setAnuncioDAO(AnuncioDAO anuncioDAO) {
		this.anuncioDAO = anuncioDAO;
	}
	
	@Override
	@Transactional
	public void salvarAnuncio(Anuncio a)
			throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.anuncioDAO.salvar(a);
	}

	@Override
	@Transactional
	public void atualizarAnuncio(Anuncio a)
			throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.anuncioDAO.atualizar(a);
	}

	@Override
	@Transactional
	public void mergeAnuncio(Anuncio a) throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.anuncioDAO.merge(a);
	}

	@Override
	@Transactional
	public List<Anuncio> listarAnuncios() throws SQLException {
		return this.anuncioDAO.listarAnuncios();
	}

	@Override
	@Transactional
	public void excluirAnuncio(Anuncio a) throws SQLException {
		this.anuncioDAO.excluir(a);
	}

	@Override
	@Transactional
	public Anuncio anuncioPorId(Integer id) throws SQLException {
		return this.anuncioDAO.anuncioPorId(id);
	}

	@Override
	@Transactional
	public Anuncio anuncioPorTitulo(String titulo) throws SQLException {
		return this.anuncioDAO.anuncioPorTitulo(titulo);
	}

	@Override
	@Transactional
	public List<Anuncio> listarAnunciosPorUsuario(Integer idUsuario)
			throws SQLException {
		return this.anuncioDAO.listarAnunciosPorUsuario(idUsuario);
	}

	@Override
	@Transactional
	public List<Anuncio> listarAnunciosDoUsuarioAguardando(Integer idUsuario) {
		return this.anuncioDAO.listarAnunciosDoUsuarioAguardando(idUsuario);
	}

	@Override
	@Transactional
	public List<Anuncio> listarAnunciosDoUsuarioFinalizadas(Integer idUsuario) {
		return this.anuncioDAO.listarAnunciosDoUsuarioFinalizadas(idUsuario);
	}

	@Override
	@Transactional
	public List<Anuncio> listarAnunciosDoUsuarioAprovado(Integer idUsuario) {
		return this.anuncioDAO.listarAnunciosDoUsuarioAprovado(idUsuario);
	}

	@Override
	@Transactional
	public List<AnuncioFluxo> listarAnunciosDoUsuarioEmFluxo(Integer idUsuario) {
		return this.anuncioDAO.listarAnunciosDoUsuarioEmFluxo(idUsuario);
	}

	@Override
	@Transactional
	public List<Anuncio> listarAnunciosPorUsuarioForaDoFluxo(Integer idUsuario)
			throws SQLException {
		return this.anuncioDAO.listarAnunciosPorUsuarioForaDoFluxo(idUsuario);
	}

	@Override
	@Transactional
	public List<AnuncioFluxo> anunciosDoGrupoAtualNoFluxo() {
		return this.anuncioDAO.anunciosDoGrupoAtualNoFluxo();
	}

	@Override
	@Transactional
	public List<Anuncio> listarAnunciosFinalizadasDaEdicao(Integer idEdicao) {
		return this.anuncioDAO.listarAnunciosFinalizadasDaEdicao(idEdicao);
	}

}
