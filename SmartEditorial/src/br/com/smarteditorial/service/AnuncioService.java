package br.com.smarteditorial.service;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.smarteditorial.model.entities.Anuncio;
import br.com.smarteditorial.model.entities.AnuncioFluxo;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

public interface AnuncioService {
	public void salvarAnuncio(Anuncio a) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException;
	public void atualizarAnuncio(Anuncio a) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException;
	public void mergeAnuncio(Anuncio a) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException;
	public List<Anuncio> listarAnuncios() throws SQLException;
	public List<Anuncio> listarAnunciosPorUsuario(Integer idUsuario) throws SQLException;
	public void excluirAnuncio(Anuncio a) throws SQLException;
	public Anuncio anuncioPorId(Integer id) throws SQLException;
	public Anuncio anuncioPorTitulo(String titulo) throws SQLException;
	public List<Anuncio> listarAnunciosDoUsuarioAguardando(Integer idUsuario);
	public List<Anuncio> listarAnunciosDoUsuarioFinalizadas(Integer idUsuario);
	public List<Anuncio> listarAnunciosDoUsuarioAprovado(Integer idUsuario);
	public List<AnuncioFluxo> listarAnunciosDoUsuarioEmFluxo(Integer idUsuario);
	public List<Anuncio> listarAnunciosPorUsuarioForaDoFluxo(Integer idUsuario) throws SQLException;
	public List<AnuncioFluxo> anunciosDoGrupoAtualNoFluxo();
	public List<Anuncio> listarAnunciosFinalizadasDaEdicao(Integer idEdicao);
}
