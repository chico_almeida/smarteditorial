package br.com.smarteditorial.service;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.annotation.Transactional;

import br.com.smarteditorial.dao.UsuarioAlteraImagemDAO;
import br.com.smarteditorial.model.entities.Edicao;
import br.com.smarteditorial.model.entities.Imagem;
import br.com.smarteditorial.model.entities.Usuario;
import br.com.smarteditorial.model.entities.UsuarioAlteraImagem;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

public class UsuarioAlteraImagemServiceImpl implements
		UsuarioAlteraImagemService {
	
	private UsuarioAlteraImagemDAO usuarioAlteraImagemDAO;
	
	public void setUsuarioAlteraImagemDAO (UsuarioAlteraImagemDAO usuarioAlteraImagemDAO) {
		this.usuarioAlteraImagemDAO = usuarioAlteraImagemDAO;
	}

	@Override
	@Transactional
	public void salvar(UsuarioAlteraImagem uai)
			throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.usuarioAlteraImagemDAO.salvar(uai);
	}

	@Override
	@Transactional
	public void excluir(UsuarioAlteraImagem uai)
			throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.usuarioAlteraImagemDAO.excluir(uai);
	}

	@Override
	@Transactional
	public void atualizar(UsuarioAlteraImagem uai)
			throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.usuarioAlteraImagemDAO.atualizar(uai);
	}

	@Override
	@Transactional
	public void merge(UsuarioAlteraImagem uai)
			throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.usuarioAlteraImagemDAO.merge(uai);
	}

	@Override
	@Transactional
	public List<UsuarioAlteraImagemDAO> imagensModificadasPorUsuarioNaEdicao(
			Usuario u, Edicao e) {
		return this.usuarioAlteraImagemDAO.imagensModificadasPorUsuarioNaEdicao(u, e);
	}

	@Override
	@Transactional
	public List<UsuarioAlteraImagemDAO> imagensModificadasPorUsuario(Usuario u) {
		return this.usuarioAlteraImagemDAO.imagensModificadasPorUsuario(u);
	}

	@Override
	@Transactional
	public UsuarioAlteraImagem alteracaoPorImagem(Imagem i) {
		return this.usuarioAlteraImagemDAO.alteracaoPorImagem(i);
	}

}
