package br.com.smarteditorial.service;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.smarteditorial.model.entities.FluxoDeTrabalho;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

public interface FluxoDeTrabalhoService {
	public void salvarFluxoDeTrabalho(FluxoDeTrabalho f) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException;
	public void atualizarFluxoDeTrabalho(FluxoDeTrabalho f) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException;
	public void mergeFluxoDeTrabalho(FluxoDeTrabalho f) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException;
	public void excluirFluxoDeTrabalho(FluxoDeTrabalho f) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException;
	public List<FluxoDeTrabalho> listarTrabalhos() throws SQLException;
	public FluxoDeTrabalho fluxoPorId(Integer id) throws SQLException;
	public List<FluxoDeTrabalho> listaFluxosPorElementoDaPublicacao(String elemento, Integer idPublicacao) throws SQLException;
	public FluxoDeTrabalho pegaFluxo(Integer ordem, Integer idPublicacao, String tipoDeElemento);
}
