package br.com.smarteditorial.service;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

import br.com.smarteditorial.model.entities.Publicacao;

public interface PublicacaoService {
	public void salvarPublicacao(Publicacao p) throws DataIntegrityViolationException, ConstraintViolationException, SQLException;
	public void atualizarPublicacao(Publicacao p) throws DataIntegrityViolationException, ConstraintViolationException, SQLException;
	public void mergePublicacao(Publicacao p) throws DataIntegrityViolationException, ConstraintViolationException, SQLException;
	public List<Publicacao> listarPublicacoes() throws SQLException;
	public void excluirPublicacao(Publicacao p) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException;
	public Publicacao publicacaoPorId(Integer id) throws SQLException;
	public Publicacao publicacaoPorTitulo(String titulo) throws SQLException;
}
