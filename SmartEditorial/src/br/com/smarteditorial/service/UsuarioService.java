package br.com.smarteditorial.service;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.smarteditorial.model.entities.MapElementos;
import br.com.smarteditorial.model.entities.Usuario;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

public interface UsuarioService {
	public void salvarUsuario(Usuario u) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException;
	public void atualizarUsuario(Usuario u) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException;
	public void mergeUsuario(Usuario u) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException;
	public List<Usuario> listarUsuarios() throws SQLException;
	public Usuario usuarioPorId(int id)  throws SQLException;
	public Usuario usuarioPorEmail(String email) throws SQLException;
	public void excluirUsuario(Usuario u) throws SQLException;
	public List<Integer> gruposDoUsuario(Integer idUsuario);
	public List<MapElementos> listarElementosEmFluxoParaOUsuario(String grupos, Integer idUsuario);
	public List<MapElementos> listarElementosEmFluxoParaOGrupo(String grupos);
}

