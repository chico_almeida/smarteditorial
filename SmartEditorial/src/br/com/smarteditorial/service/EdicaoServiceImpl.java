package br.com.smarteditorial.service;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

import br.com.smarteditorial.dao.EdicaoDAO;
import br.com.smarteditorial.model.entities.Edicao;

@Service
public class EdicaoServiceImpl implements EdicaoService {
	
	private EdicaoDAO edicaoDAO;
	
	public void setEdicaoDAO (EdicaoDAO edicaoDAO) {
		this.edicaoDAO = edicaoDAO;
	}

	@Override
	@Transactional
	public void salvarEdicao(Edicao e) throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException {
		this.edicaoDAO.salvar(e);
	}

	@Override
	@Transactional
	public void atualizarEdicao(Edicao e)
			throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException {
		this.edicaoDAO.atualizar(e);
	}

	@Override
	@Transactional
	public void mergeEdicao(Edicao e) throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException {
		this.edicaoDAO.merge(e);
	}

	@Override
	@Transactional
	public List<Edicao> listarEdicoes() throws SQLException {
		return this.edicaoDAO.listarEdicao();
	}

	@Override
	@Transactional
	public void excluirEdicao(Edicao e) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException {
		this.edicaoDAO.excluir(e);
	}

	@Override
	@Transactional
	public Edicao edicaoPorId(Integer id) throws SQLException {
		return this.edicaoDAO.edicaoPorId(id);
	}

	@Override
	@Transactional
	public Edicao edicaoPorTitulo(String titulo) throws SQLException {
		return this.edicaoDAO.edicaoPorTitulo(titulo);
	}

	@Override
	@Transactional
	public List<Edicao> edicaoPorPublicacao(Integer id) throws SQLException {
		return this.edicaoDAO.edicaoPorPublicacao(id);
	}

}
