package br.com.smarteditorial.service;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.smarteditorial.model.entities.Pagina;
import br.com.smarteditorial.model.entities.PaginaFluxo;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

public interface PaginaService {
	public void salvarPagina(Pagina p) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException;
	public void atualizarPagina(Pagina p) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException;
	public void mergePagina(Pagina p) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException;
	public void excluirPagina(Pagina p) throws SQLException;
	public List<Pagina> listarPaginas() throws SQLException;
	public List<Pagina> listarPaginasPorUsuario(Integer idUsuario) throws SQLException;
	public Pagina paginaPorTitulo(String titulo) throws SQLException;
	public Pagina paginaPorId(Integer id) throws SQLException;
	public List<PaginaFluxo> listarPaginasDoUsuarioEmFluxo(Integer idUsuario);
	public List<Pagina> listarPaginasPorUsuarioForaDoFluxo(Integer idUsuario) throws SQLException;
	public List<PaginaFluxo> paginasDoGrupoAtualNoFluxo();
	public List<Pagina> listarPaginasDoUsuarioFinalizadas(Integer idUsuario);
	public List<Pagina> listarPaginasFinalizadasDaEdicao(Integer idEdicao);
}
