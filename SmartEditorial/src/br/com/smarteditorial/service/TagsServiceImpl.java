package br.com.smarteditorial.service;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.smarteditorial.dao.TagsDAO;
import br.com.smarteditorial.model.entities.Tags;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

@Service
public class TagsServiceImpl implements TagsService {
	
	private TagsDAO tagsDAO;
	
	public void setTagsDAO(TagsDAO tagsDAO) {
        this.tagsDAO = tagsDAO;
    }
	
	@Override
	@Transactional
	public void salvarTags(Tags t) throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.tagsDAO.salvar(t);

	}

	@Override
	@Transactional
	public void atualizarTag(Tags t) throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.tagsDAO.atualizar(t);
	}

	@Override
	@Transactional
	public void mergeTag(Tags t) throws DataIntegrityViolationException,
			ConstraintViolationException, SQLException,
			MySQLIntegrityConstraintViolationException {
		this.tagsDAO.merge(t);
	}

	@Override
	@Transactional
	public List<Tags> listarTags() throws SQLException {
		return this.tagsDAO.listarTags();
	}

	@Override
	@Transactional
	public void excluirTag(Tags t) throws SQLException {
		this.tagsDAO.excluir(t);
	}

	@Override
	@Transactional
	public Tags tagsPorId(Integer id) throws SQLException {
		return this.tagsDAO.tagsPorId(id);
	}

	@Override
	@Transactional
	public Tags tagsPorTitulo(String titulo) throws SQLException {
		return this.tagsDAO.tagsPorTitulo(titulo);
	}

}
