package br.com.smarteditorial.service;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.smarteditorial.model.entities.Artigo;
import br.com.smarteditorial.model.entities.ArtigoFluxo;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

public interface ArtigoService {
	public void salvarArtigo(Artigo a) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException;
	public void atualizarArtigo(Artigo a) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException;
	public void mergeArtigo(Artigo a) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException;
	public List<Artigo> listarArtigos() throws SQLException;
	public void excluirArtigo(Artigo a) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException;
	public Artigo artigoPorId(Integer id) throws SQLException;
	public Artigo artigoPorTitulo(String titulo) throws SQLException;
	public List<Artigo> artigoPorUsuario(Integer idUsuario) throws SQLException;
	public List<Artigo> listarArtigosDoUsuarioAguardando(Integer idUsuario);
	public List<Artigo> listarArtigosDoUsuarioFinalizadas(Integer idUsuario);
	public List<Artigo> listarArtigosDoUsuarioAprovado(Integer idUsuario);
	public List<ArtigoFluxo> listarArtigosDoUsuarioEmFluxo(Integer idUsuario);
	public List<Artigo> listarArtigosPorUsuarioForaDoFluxo(Integer idUsuario) throws SQLException;
	public List<ArtigoFluxo> artigosDoGrupoAtualNoFluxo();
	public List<Artigo> listarArtigosFinalizadasDaEdicao(Integer idEdicao);
}
