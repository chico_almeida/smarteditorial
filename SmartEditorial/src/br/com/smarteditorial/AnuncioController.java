package br.com.smarteditorial;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import br.com.smarteditorial.model.bo.AnuncioBO;
import br.com.smarteditorial.model.bo.AnuncioFluxoBO;
import br.com.smarteditorial.model.bo.CategoriaBO;
import br.com.smarteditorial.model.bo.EdicaoBO;
import br.com.smarteditorial.model.bo.EmpresaBO;
import br.com.smarteditorial.model.bo.FileUploadBO;
import br.com.smarteditorial.model.bo.FluxoDeTrabalhoBO;
import br.com.smarteditorial.model.bo.PublicacaoBO;
import br.com.smarteditorial.model.bo.TagsBO;
import br.com.smarteditorial.model.bo.UsuarioBO;
import br.com.smarteditorial.model.entities.Anuncio;
import br.com.smarteditorial.model.entities.AnuncioFluxo;
import br.com.smarteditorial.model.entities.Edicao;
import br.com.smarteditorial.model.entities.Empresa;
import br.com.smarteditorial.model.entities.FluxoDeTrabalho;
import br.com.smarteditorial.model.entities.Publicacao;
import br.com.smarteditorial.model.entities.Usuario;
import br.com.smarteditorial.util.SystemConfiguration;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

@Controller
public class AnuncioController {
	
	@Autowired
	FileUploadBO fileUploadBO;
	@Autowired
	private AnuncioBO anuncioBO;
	@Autowired
	private PublicacaoBO publicacaoBO;
	@Autowired
	private EdicaoBO edicaoBO;
	@Autowired
	private TagsBO tagsBO;
	@Autowired
	private UsuarioBO usuarioBO;
	@Autowired
	private EmpresaBO empresaBO;
	@Autowired
	private CategoriaBO categoriaBO;
	@Autowired
	SystemConfiguration systemConfiguration;
	@Autowired
	AnuncioFluxoBO anuncioFluxoBO;
	@Autowired
	FluxoDeTrabalhoBO fluxoDeTrabalhoBO;
	
	@RequestMapping("/anuncios")
	public String cadastroAnuncio(Model model) {
		model.addAttribute("anuncio", new Anuncio());
		model.addAttribute("email", new String());
		model.addAttribute("tags", new String());
		model.addAttribute("idPublicacao", new String());
		model.addAttribute("idEdicao", new String());
		model.addAttribute("listaDePublicacoes", publicacaoBO.pegaPublicacoes());
		return "/acervo/anuncio/cadastro";
	}
	
	@RequestMapping(value = "/anuncios/cadastro/add", method = RequestMethod.POST)
	public String salvarAnuncio(@ModelAttribute("anuncio") Anuncio anuncio, @ModelAttribute("idEdicao") String idEdicao, 
			@RequestParam("arquivo") MultipartFile arquivo, 
			@ModelAttribute("pchaves") String tags,
			@ModelAttribute("nomeEmpresa") String nomeEmpresa) {
		Edicao e = this.edicaoBO.pegaEdicaoId(Integer.parseInt(idEdicao));
		String caminho = fileUploadBO.imageUpload(arquivo, e.getPublicacao().getTitulo()+"/"+e.getTitulo(), anuncio.getTitulo());
		anuncio.setEdicao(e);
		anuncio.setTags(this.tagsBO.splitTags(tags));
		anuncio.setCaminho(caminho);
		anuncio.setAltura(fileUploadBO.getHeight(arquivo));
		anuncio.setLargura(fileUploadBO.getWidth(arquivo));
		anuncio.setEstado("Aguardando");
		anuncio.setUsuario(this.usuarioBO.pegaUsuarioPorId(pegaUsuarioLogado().getIdUsuario()));
		anuncio.setEmpresa(this.empresaBO.empresaPorNome(nomeEmpresa));;
		this.anuncioBO.salvarAnuncio(anuncio);
		return "redirect:/anuncios/meusanuncios";
	}
	
	@RequestMapping("/anuncios/meusanuncios")
	public String listaDeAnuncios(Model model) {
		model.addAttribute("anuncios", this.anuncioBO.pegaAnunciosDoUsuario(pegaUsuarioLogado().getIdUsuario()));
		model.addAttribute("anunciosFinalizadas", this.anuncioBO.pegaAnunciosFinalizadas(pegaUsuarioLogado()));
		model.addAttribute("anunciosEmFluxo", this.anuncioBO.pegaAnunciosDoUsuarioEmFluxo(pegaUsuarioLogado()));
		model.addAttribute("anunciosForaDoFluxo", this.anuncioBO.pegaAnunciosDoUsuarioForaDoFluxo(pegaUsuarioLogado()));
		model.addAttribute("contextPath","http://localhost/");
		return "/acervo/anuncio/anuncios";
	}
	
	public Usuario pegaUsuarioLogado() {
		UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return this.usuarioBO.pegaUsuarioPorEmail(userDetails.getUsername());
	}
	
	@RequestMapping(value="/anuncios/empresas", method=RequestMethod.POST)
	public String pegaListaDeEmpresas(Model model) {
		model.addAttribute("empresa", new Empresa());
		model.addAttribute("listaDeEmpresas", this.empresaBO.empresas());
		return  "/acervo/anuncio/empresas";
	}
	
	@RequestMapping(value="/anuncios/empresas/criar", method=RequestMethod.POST)
	public @ResponseBody String salvarEmpresa(@ModelAttribute("empresa") Empresa e){
		try {
			this.empresaBO.salvar(e);
			return "true";
		} catch (DataIntegrityViolationException |ConstraintViolationException | MySQLIntegrityConstraintViolationException e1) {
			return "false";
		} catch (SQLException e1) {
			return "falha";
		}
		
	}
	
	@RequestMapping("/anuncios/meusanuncios/editar")
	public String editarArtigo(@RequestParam("id") String id, 
			@RequestParam("publicacao")String idPublicacao, Model model){
		Publicacao p = this.publicacaoBO.pegaPublicacaoID(Integer.parseInt(idPublicacao));
		model.addAttribute("listaDePublicacoes", publicacaoBO.pegaPublicacoes());
		model.addAttribute("listaDeEdicoes", p.getEdicoes());
		model.addAttribute("listaDeCategorias", p.getCategorias());
		model.addAttribute("anuncio", this.anuncioBO.pegaAnuncioPorID(Integer.parseInt(id)));
		model.addAttribute("contextPath","http://localhost/");
		return "/acervo/anuncio/cadastro";
	}
	
	@RequestMapping(value="/anuncios/meusanuncios/remover", method= RequestMethod.POST)
	public @ResponseBody String removerArtigo(@ModelAttribute("id") String id) {
		try {
			this.anuncioBO.removerAnuncio(this.anuncioBO.pegaAnuncioPorID(Integer.parseInt(id)));
			return "sucesso";
		} catch (DataIntegrityViolationException | ConstraintViolationException | NumberFormatException e) {
			e.printStackTrace();
			return "vinculo";
		} catch (SQLException e) {
			e.printStackTrace();
			return "sqlError";
		}
	}
	
	@RequestMapping(value = "/anuncios/cadastro/update", method = RequestMethod.POST)
	public String alterarImagem(@ModelAttribute("anuncio") Anuncio anuncio, 
			@ModelAttribute("idEdicao") String idEdicao, 
			@ModelAttribute("idCategoria") String idCategoria , 
			@ModelAttribute("pchaves") String tags,
			@ModelAttribute("nomeEmpresa") String nomeEmpresa) {
		anuncio.setTags(this.tagsBO.splitTags(tags));
		anuncio.setEdicao(this.edicaoBO.pegaEdicaoId(Integer.parseInt(idEdicao)));
		anuncio.setCategoria(this.categoriaBO.pegaCategoriaPorId(Integer.parseInt(idCategoria)));
		anuncio.setUsuario(this.usuarioBO.pegaUsuarioPorId(pegaUsuarioLogado().getIdUsuario()));
		anuncio.setEmpresa(this.empresaBO.empresaPorNome(nomeEmpresa));;
		this.anuncioBO.atualizar(anuncio);
		return "redirect:/anuncios/meusanuncios";
	}
	
	@RequestMapping(value="/anuncio/preview", method = RequestMethod.POST)
	public String previewAnuncio(@ModelAttribute("id") Integer id, Model model) {
		model.addAttribute("contextPath", this.systemConfiguration.getUrlAcervo());
		model.addAttribute("anuncio", this.anuncioBO.pegaAnuncioPorID(id));
		return "/restrito/anuncioPreview";
	}
	
	@RequestMapping(value="/anuncio/pegar-elemento", method = RequestMethod.POST)
	public @ResponseBody String pegarElemento(@ModelAttribute("idElemento") Integer idElemento,
			@ModelAttribute("idFluxo") Integer idFluxo) {
		Anuncio a = this.anuncioBO.pegaAnuncioPorID(idElemento);
		a.setEstado("Editando");
		
		AnuncioFluxo anf =  this.anuncioFluxoBO.pegaAnuncioFluxoPorIds(idFluxo, idElemento);
		anf.setUsuario(pegaUsuarioLogado());
		
		try {
			this.anuncioFluxoBO.atualizar(anf);
			this.anuncioBO.atualizar(a);
			return "sucesso";
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| SQLException e) {
			e.printStackTrace();
			return "falha";
		}
	}
	
	@RequestMapping(value="/anuncio/iniciar-fluxo", method = RequestMethod.POST)
	public @ResponseBody String iniciarFluxo(
			@ModelAttribute("idAnuncio")String idAnuncio, 
			@ModelAttribute("comentario")String comentario) throws IndexOutOfBoundsException {
		AnuncioFluxo anf = new AnuncioFluxo();
		
		List<FluxoDeTrabalho> fxs;
		try {
			fxs = this.fluxoDeTrabalhoBO.pegaFluxosPorElemento("anuncio", this.anuncioBO.pegaAnuncioPorID(Integer.parseInt(idAnuncio)).getEdicao().getPublicacao().getIdPublicacao());
		} catch (IndexOutOfBoundsException e1) {
			return "vazio";
		}
		
		anf.setAnuncio(this.anuncioBO.pegaAnuncioPorID(Integer.parseInt(idAnuncio)));
		try {
			anf.setFluxoDeTrabalho(fxs.get(0));
		} catch (IndexOutOfBoundsException e1) {
			return "vazio";
		}
		anf.setComentario(comentario);
		
		try {
			this.anuncioFluxoBO.salvar(anf);
			return "sucesso";
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| SQLException e) {
			e.printStackTrace();
			return "falha";
		}
		
	}
	
	@RequestMapping(value="/anuncio/proximo-fluxo", method = RequestMethod.POST)
	public @ResponseBody String proximoFluxo(@ModelAttribute("idElemento")String idElemento, 
			@ModelAttribute("comentario")String comentario, 
			@ModelAttribute("ordemFluxo") String PosicaoAtual, 
			@ModelAttribute("idPublicacao") String idPublicacao,
			@ModelAttribute("tipoElemento") String tipoElemento,
			@ModelAttribute("idFluxo") String idFluxo) {

		AnuncioFluxo anf = new AnuncioFluxo();
		Integer i = Integer.parseInt(PosicaoAtual);
		
		FluxoDeTrabalho fx = this.fluxoDeTrabalhoBO.pegaFluxo(++i, Integer.parseInt(idPublicacao), tipoElemento);
		
		if (fx == null) {
			return "ultimo";
		} else {
			anf.setAnuncio(this.anuncioBO.pegaAnuncioPorID(Integer.parseInt(idElemento)));
			anf.setFluxoDeTrabalho(fx);
			anf.setComentario(comentario);

			try {
				this.anuncioFluxoBO.salvar(anf);
				this.anuncioFluxoBO.excluir(this.anuncioFluxoBO.pegaAnuncioFluxoPorIds(Integer.parseInt(idFluxo), Integer.parseInt(idElemento)));
				return "sucesso";
			} catch (DataIntegrityViolationException | ConstraintViolationException
					| SQLException e) {
				e.printStackTrace();
				return "falha";
			}
		}
	}
	
	@RequestMapping(value="/anuncio/retroceder-fluxo", method = RequestMethod.POST)
	public @ResponseBody String retrocerFluxo(@ModelAttribute("idElemento")String idElemento, 
			@ModelAttribute("comentario")String comentario, 
			@ModelAttribute("ordemFluxo") String PosicaoAtual, 
			@ModelAttribute("idPublicacao") String idPublicacao,
			@ModelAttribute("tipoElemento") String tipoElemento,
			@ModelAttribute("idFluxo") String idFluxo) {
		
		if (Integer.parseInt(PosicaoAtual) == 1) {
			try {
				this.anuncioFluxoBO.excluir(this.anuncioFluxoBO.pegaAnuncioFluxoPorIds(Integer.parseInt(idFluxo), Integer.parseInt(idElemento)));
				return "sucesso";
			} catch (DataIntegrityViolationException
					| ConstraintViolationException | NumberFormatException
					| SQLException e) {
				e.printStackTrace();
				return "falha";
			}
		}
		AnuncioFluxo anf = new AnuncioFluxo();
		Integer i = Integer.parseInt(PosicaoAtual);
		
		FluxoDeTrabalho fx = this.fluxoDeTrabalhoBO.pegaFluxo(--i, Integer.parseInt(idPublicacao), tipoElemento);
		
		anf.setAnuncio(this.anuncioBO.pegaAnuncioPorID(Integer.parseInt(idElemento)));
		
		anf.setFluxoDeTrabalho(fx);
		anf.setComentario(comentario);

		try {
			this.anuncioFluxoBO.salvar(anf);
			this.anuncioFluxoBO.excluir(this.anuncioFluxoBO.pegaAnuncioFluxoPorIds(Integer.parseInt(idFluxo), Integer.parseInt(idElemento)));
			return "sucesso";
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| SQLException e) {
			e.printStackTrace();
			return "falha";
		}
		
	}
	@RequestMapping(value="/anuncios/interromper-fluxo", method = RequestMethod.POST)
	public @ResponseBody String interromperFluxo(@ModelAttribute("idElemento") Integer idElemento,
			@ModelAttribute("idFluxo") Integer idFluxo) {
		Anuncio a = this.anuncioBO.pegaAnuncioPorID(idElemento);
		a.setEstado("Aguardando");
		try {
			this.anuncioFluxoBO.excluir(this.anuncioFluxoBO.pegaAnuncioFluxoPorIds(idFluxo, idElemento));
			this.anuncioBO.atualizar(a);
			return "sucesso";
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| SQLException e) {
			e.printStackTrace();
			return "falha";
		}
		
	}
	
	@RequestMapping(value="/anuncios/empresas/add", method = RequestMethod.POST)
	public @ResponseBody String formularioCadastrarEmpresa(@ModelAttribute("empresa") Empresa e) {
		try {
			this.empresaBO.salvar(e);
			return "sucesso";
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| SQLException e1) {
			e1.printStackTrace();
			return "falha";
		}
	}
	
	@RequestMapping(value="/anuncio/devolver-elemento", method = RequestMethod.POST)
	public @ResponseBody String devolverElementoParaOGrupo(@ModelAttribute("idElemento") Integer idElemento,
			@ModelAttribute("idFluxo") Integer idFluxo) {
		Anuncio a = this.anuncioBO.pegaAnuncioPorID(idElemento);
		a.setEstado("Aguardando");
		
		AnuncioFluxo anf =  this.anuncioFluxoBO.pegaAnuncioFluxoPorIds(idFluxo, idElemento);
		anf.setUsuario(null);
		
		try {
			this.anuncioFluxoBO.atualizar(anf);
			this.anuncioBO.atualizar(a);
			return "sucesso";
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| SQLException e) {
			e.printStackTrace();
			return "falha";
		}
	}
}
