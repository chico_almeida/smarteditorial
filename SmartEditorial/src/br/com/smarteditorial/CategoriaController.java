package br.com.smarteditorial;

import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.smarteditorial.model.bo.CategoriaBO;
import br.com.smarteditorial.model.bo.EdicaoBO;
import br.com.smarteditorial.model.bo.PublicacaoBO;
import br.com.smarteditorial.model.bo.UsuarioBO;
import br.com.smarteditorial.model.entities.Categoria;

@Controller
public class CategoriaController {
	
	@Autowired
	CategoriaBO categoriaBO;
	@Autowired
	EdicaoBO edicaoBO;
	@Autowired
	PublicacaoBO publicacaoBO;
	@Autowired
	UsuarioBO usuarioBO;
	
	@RequestMapping(value="/categorias/cadastro", method = RequestMethod.POST)
	public String cadastroCategoria(@ModelAttribute("idPublicacao") String idPublicacao, Model model) {
		model.addAttribute("categoria", new Categoria());
		model.addAttribute("idPub", idPublicacao);
		return "/categorias/cadastro";
	}
	
	@RequestMapping(value="/categorias/cadastro/criar",  method = RequestMethod.POST)
	public String criarCategoria(@ModelAttribute("categoria") Categoria c, @ModelAttribute("idPub") String idPublicacao, Model  model) {
		c.setPublicacao(this.publicacaoBO.pegaPublicacaoID(Integer.parseInt(idPublicacao)));
		this.categoriaBO.salvaCategoria(c);
		model.addAttribute("publicacao", this.publicacaoBO.pegaPublicacaoID(Integer.parseInt(idPublicacao
		)));
		return "redirect:/publicacao?idPublicacao="+idPublicacao;
	}
	
	@RequestMapping(value="/categorias/remover", method = RequestMethod.POST)
	public @ResponseBody String removerCategoria(@ModelAttribute("idCategoria") String idCategoria) {
		try {
			this.categoriaBO.excluirCategoria(this.categoriaBO.pegaCategoriaPorId(Integer.parseInt(idCategoria)));
			return "sucesso";
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| NumberFormatException e) {
			return "vinculos";
		} catch (Exception e) {
			return "Falha na remo��o, contate o Administrador do Sistema. Erro :: "+e.getMessage();
		}
	}
	@RequestMapping(value="/categorias/validar", method = RequestMethod.POST)
	public @ResponseBody String existeCategoria(@ModelAttribute("tituloCategoria") String tituloCategoria, @ModelAttribute("publicacao") String idPublicacao) {
		
		List<Categoria> ctgs = categoriaBO.pegaCategoriaPorPublicacao(Integer.parseInt(idPublicacao));
		for (Categoria categoria: ctgs) {
			if (categoria.getTitulo().toLowerCase().equals(tituloCategoria.toLowerCase())) {
				return "true";
			}
		}
		return "false";
	}
	
}
