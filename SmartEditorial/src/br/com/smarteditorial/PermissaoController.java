package br.com.smarteditorial;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.smarteditorial.model.entities.Permissao;
import br.com.smarteditorial.service.PermissaoService;

@Controller
public class PermissaoController {
	
	private static final Logger logger = LoggerFactory.getLogger(PermissaoController.class);

	private PermissaoService permissaoService;

	@Autowired(required=true)
	@Qualifier(value="permissaoService")
	public void setPermissaoService(PermissaoService ps){
		this.permissaoService = ps;
	}

	@RequestMapping(value = "/admin/permissoes", method = RequestMethod.GET)
	public String criaPermissao(Model model) {
		model.addAttribute("permissao", new Permissao());
		return "/admin/permissao";
	}

	@RequestMapping(value= "/admin/permissao/add", method = RequestMethod.POST)
	public String salvarPermissao(@ModelAttribute("permissao") Permissao p){
		try {
			this.permissaoService.salvarPermissao(p);
		} catch (SQLException e) {
			e.printStackTrace();
			logger.info("Erro ao salvar a Permisssao"+e);
		} catch (ConstraintViolationException e) {
			e.printStackTrace();
			logger.info("Persistir Usuario::"+e);
			return "redirect:/admin/permissoes?q=Tipo";
		} catch (DataIntegrityViolationException e) {
			e.printStackTrace();
			logger.info("Persistir Usuario::"+e);
			return "redirect:/admin/permissoes?q=Tipo";
		}
		return "redirect:/admin/listapermissao";
	}
	
	@RequestMapping("/admin/permissao/update")
	public String atualizaPermissao(@ModelAttribute("permissao") Permissao p){
		try {
			this.permissaoService.atualizarPermissao(p);
		} catch (SQLException e) {
			e.printStackTrace();
			logger.info("Erro ao atualizar a Permisssao"+e);
		} catch (ConstraintViolationException e) {
			e.printStackTrace();
			logger.info("Persistir Usuario::"+e);
			return "redirect:/admin/permissoes?q=Tipo";
		}
		return "redirect:/admin/listapermissao";
	}

	@RequestMapping(value= "/admin/listapermissao", method = RequestMethod.GET)
	public ModelAndView listarPermissoes() {
		List<Permissao> permissoes;
		try {
			permissoes = this.permissaoService.listarPermissao();
			ModelAndView mv = new ModelAndView("admin/lista-permissao");
			mv.addObject("permissao", permissoes);
			return mv;
		} catch (SQLException e) {
			e.printStackTrace();
			logger.info("Erro ao listar a Permisssao"+e);
			return null;
		}
		
	}

	@RequestMapping("/admin/removerPermissao/{idPermissao}")
	public String excluirPermissao(@PathVariable("idPermissao") Integer id){
		Permissao p;
		try {
			p = this.permissaoService.permissaoPorId(id);
			this.permissaoService.excluirPermissao(p);
		} catch (SQLException e) {
			e.printStackTrace();
			logger.info("Erro ao excluir a Permisssao"+e);
		}
		return "redirect:/admin/listapermissao";
	}

	@RequestMapping("/admin/editarPermissao/{idPermissao}")
	public String editarPermissao(@PathVariable("idPermissao") Integer id, Model model){
		try {
			model.addAttribute("permissao", this.permissaoService.permissaoPorId(id));
			model.addAttribute("listarPermissoes", this.permissaoService.listarPermissao());
		} catch (SQLException e) {
			e.printStackTrace();
			logger.info("Erro ao editar a Permisssao"+e);
		}
		
		return "/admin/permissao";
	}
	

}
