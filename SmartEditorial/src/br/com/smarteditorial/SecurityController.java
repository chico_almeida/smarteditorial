package br.com.smarteditorial;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class SecurityController {
	
	@RequestMapping("/publico/login")
	public String cadastroEdicao() {
		return "/publico/login";
	}
	
	@RequestMapping("restrito/administracao")
	public String homeAdmin() {
		return "/restrito/administracao";
	}
	
	@RequestMapping("/")
	public String paginaInicial() {
		return "/publico/home";
	}
	
	@RequestMapping(value="/infoUser", method = RequestMethod.POST)
	public @ResponseBody String infoUser() {
		return "HUEHUEUHU";
	}
}	
