package br.com.smarteditorial;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.smarteditorial.model.bo.GrupoBO;
import br.com.smarteditorial.model.bo.PermissaoBO;
import br.com.smarteditorial.model.bo.UsuarioBO;
import br.com.smarteditorial.model.entities.Grupo;
import br.com.smarteditorial.model.entities.Usuario;

@Controller
public class GrupoController {


	@Autowired
	GrupoBO grupoBO;
	@Autowired
	UsuarioBO usuarioBO;
	@Autowired
	PermissaoBO permissaoBO;
	@Autowired
	Usuario u;
	@RequestMapping("/admin/grupos/criar")
	public String listarGrupos(Model model) {
		model.addAttribute("grupo", new Grupo());
		return "/admin/grupo";
	}

	@RequestMapping(value= "/admin/grupo/add", method = RequestMethod.POST)
	public String salvarGrupo(@ModelAttribute("grupo") Grupo g){
		this.grupoBO.salvarGrupo(g);
		return "redirect:/admin/grupos";
	}

	@RequestMapping(value= "/admin/grupo/update", method = RequestMethod.POST)
	public String atualizarGrupo(@ModelAttribute("grupo") Grupo g){
		this.grupoBO.atualizarGrupo(g);
		return "redirect:/admin/grupos";
	}

	@RequestMapping(value= "/admin/grupo/validar", method = RequestMethod.POST)
	public @ResponseBody String existeGrupo(@ModelAttribute("titulo") String tituloGrupo) {
		if (this.grupoBO.pegaGrupoPorNome(tituloGrupo) == null) {
			return "false";
		} else {
			return "true";
		}
	}
	@RequestMapping(value= "/admin/grupo/ehvazio", method = RequestMethod.POST)
	public @ResponseBody String grupoEhVazio(@ModelAttribute("id") String id) {
	
		if(this.grupoBO.pegaGrupoPorId(Integer.parseInt(id)).getPermissoes().isEmpty() || this.grupoBO.pegaGrupoPorId(Integer.parseInt(id)).getUsuarios().isEmpty()) {
			return "false";
		} else {
			return "true";
		}
	}
	
	@RequestMapping("/admin/grupos")
	public ModelAndView listarGrupos() {
		ModelAndView mv = new ModelAndView("admin/lista-grupos");
		mv.addObject("grupos",this.grupoBO.pegaGrupos());
		return mv;
	}

	@RequestMapping(value="/admin/grupo/excluir", method = RequestMethod.POST)
	public String excluirGrupo(@ModelAttribute("id") String id, Model model){
		this.grupoBO.excluirGrupo(this.grupoBO.pegaGrupoPorId(Integer.parseInt(id)));
		model.addAttribute("grupos", this.grupoBO.pegaGrupos());
		return "admin/lista-grupos";
	}

	@RequestMapping(value="/admin/grupo/editar", method = RequestMethod.POST)
	public String editarGrupo(@ModelAttribute("id") String id, Model model){
		model.addAttribute("grupo", this.grupoBO.pegaGrupoPorId(Integer.parseInt(id)));
		return "/admin/grupo";

	}

	@RequestMapping("/admin/grupos/interna")
	public String internarGrupo(@RequestParam("idGrupo") String idGrupo, Model model) {
		model.addAttribute("grupo", this.grupoBO.pegaGrupoPorId(Integer.parseInt(idGrupo)));
		return "/admin/interna-grupo";
	}

	@RequestMapping(value="/admin/grupo/interna/usuario", method = RequestMethod.POST)
	public String listarUsuariosGrupo(@ModelAttribute("idGrupo") Integer idGrupo, Model model){
		model.addAttribute("grupo", this.grupoBO.pegaGrupoPorId(idGrupo));
		model.addAttribute("usuarios", this.usuarioBO.pegaUsuario());
		return "/admin/listaDeUsuarios";
	}

	@RequestMapping(value="/admin/grupo/interna/vincularusuario", method = RequestMethod.POST)
	public @ResponseBody String addUserToAGroup(@ModelAttribute("idGrupo") String idGrupo, 
			@ModelAttribute("idUsuario") String idUsuario,  Model model){
		if (this.grupoBO.vincularUsuario(this.grupoBO.pegaGrupoPorId(Integer.parseInt(idGrupo)), this.usuarioBO.pegaUsuarioPorId(Integer.parseInt(idUsuario))) == true) {
			model.addAttribute("grupo", this.grupoBO.pegaGrupoPorId(Integer.parseInt(idGrupo)));
			model.addAttribute("usuarios", this.usuarioBO.pegaUsuario());
			return "sucesso";
		} else {
			return "falha";
		}
	}
	
	@RequestMapping(value="/admin/grupo/interna/removerusuario", method = RequestMethod.POST)
	public @ResponseBody String delUserFromGroup(@ModelAttribute("idGrupo") String idGrupo, 
			@ModelAttribute("idUsuario") String idUsuario,  Model model){
		if (this.grupoBO.removerUsuarioDoGrupo(Integer.parseInt(idGrupo), Integer.parseInt(idUsuario)) == true) {
			model.addAttribute("grupo", this.grupoBO.pegaGrupoPorId(Integer.parseInt(idGrupo)));
			return "sucesso";
		} else {
			return "falha";
		}
	}

	@RequestMapping(value="/admin/grupo/interna/permissao", method = RequestMethod.POST)
	public String listarPermissoesGrupo(@ModelAttribute("idGrupo") Integer idGrupo, Model model){
		model.addAttribute("permissoes", this.permissaoBO.pegaPermissoes());
		model.addAttribute("grupo", this.grupoBO.pegaGrupoPorId(idGrupo));
		return "/admin/listaDePermissoes";
	}


	@RequestMapping(value="/admin/grupo/interna/vincularpermissao", method = RequestMethod.POST)
	public @ResponseBody String addPermissionToAGroup(@ModelAttribute("idGrupo") String idGrupo, 
			@ModelAttribute("idPermissao") String idPermissao,  Model model){
		if (this.grupoBO.vincularPermissao(this.grupoBO.pegaGrupoPorId(Integer.parseInt(idGrupo)), this.permissaoBO.pegaPermissaoPorId(Integer.parseInt(idPermissao))) == true) {
			model.addAttribute("grupo", this.grupoBO.pegaGrupoPorId(Integer.parseInt(idGrupo)));
			model.addAttribute("permissoes", this.permissaoBO.pegaPermissoes());
			return "sucesso";
		} else {
			return "falha";
		}

	}

	@RequestMapping(value="/admin/grupo/interna/removerpermissao", method = RequestMethod.POST)
	public @ResponseBody String delPermissionFromGroup(@ModelAttribute("idGrupo") String idGrupo, 
			@ModelAttribute("idPermissao") String idPermissao,  Model model){
		if (this.grupoBO.removerPermissaoDoGrupo(Integer.parseInt(idGrupo), Integer.parseInt(idPermissao))==true) {
			model.addAttribute("grupo", this.grupoBO.pegaGrupoPorId(Integer.parseInt(idGrupo)));
			return "sucesso";
		} else {
			return "falha";
		}	

	}

}
