package br.com.smarteditorial.model.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import org.springframework.stereotype.Component;

@Entity
@Component
public class Espelho extends AbstractEntity {
	private static final long serialVersionUID = 6390762414204879465L;
	
	@Id
	@GeneratedValue
	@Column(name="id_espelho")
	private Integer idEspelho;
	private Integer quatidadePaginas;
	private Integer posi��o;
	
	@OneToOne
	private Pagina pagina;

	@OneToOne
	private Edicao edicao;

	public Integer getIdEspelho() {
		return idEspelho;
	}

	public void setIdEspelho(Integer idEspelho) {
		this.idEspelho = idEspelho;
	}

	public Integer getQuatidadePaginas() {
		return quatidadePaginas;
	}

	public void setQuatidadePaginas(Integer quatidadePaginas) {
		this.quatidadePaginas = quatidadePaginas;
	}

	public Integer getPosi��o() {
		return posi��o;
	}

	public void setPosi��o(Integer posi��o) {
		this.posi��o = posi��o;
	}

	public Pagina getPagina() {
		return pagina;
	}

	public void setPagina(Pagina pagina) {
		this.pagina = pagina;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Edicao getEdicao() {
		return edicao;
	}

	public void setEdicao(Edicao edicao) {
		this.edicao = edicao;
	}
}
