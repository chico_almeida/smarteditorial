package br.com.smarteditorial.model.entities;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

@Embeddable
public class PaginaFluxoId implements Serializable {

	private static final long serialVersionUID = -7774133708465438649L;
	private Pagina pagina;
	private FluxoDeTrabalho fluxo;
	
	@ManyToOne
	public Pagina getPagina() {
		return pagina;
	}
	public void setPagina(Pagina pagina) {
		this.pagina = pagina;
	}
	@ManyToOne
	public FluxoDeTrabalho getFluxo() {
		return fluxo;
	}
	public void setFluxo(FluxoDeTrabalho fluxo) {
		this.fluxo = fluxo;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fluxo == null) ? 0 : fluxo.hashCode());
		result = prime * result + ((pagina == null) ? 0 : pagina.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaginaFluxoId other = (PaginaFluxoId) obj;
		if (fluxo == null) {
			if (other.fluxo != null)
				return false;
		} else if (!fluxo.equals(other.fluxo))
			return false;
		if (pagina == null) {
			if (other.pagina != null)
				return false;
		} else if (!pagina.equals(other.pagina))
			return false;
		return true;
	}
	
	
}
