package br.com.smarteditorial.model.entities;

import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@IdClass(UsuarioModificaEdicaoID.class)
public class UsuarioModificaEdicao extends AbstractEntity {

	private static final long serialVersionUID = -653370520455876106L;

	@Id
	@ManyToOne
	@JoinColumn(name="id_usuario")
	private Usuario usuario;
	
	@Id
	@ManyToOne
	@JoinColumn(name="id_edicao")
	private Edicao edicao;

	private Calendar dataModificacao;
	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Edicao getEdicao() {
		return edicao;
	}

	public void setEdicao(Edicao edicao) {
		this.edicao = edicao;
	}

	public Calendar getDataModificacao() {
		return dataModificacao;
	}

	public void setDataModificacao(Calendar dataModificacao) {
		this.dataModificacao = dataModificacao;
	}
	
	
}
