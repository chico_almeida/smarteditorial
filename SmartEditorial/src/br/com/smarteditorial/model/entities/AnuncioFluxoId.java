package br.com.smarteditorial.model.entities;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

@Embeddable
public class AnuncioFluxoId implements Serializable {

	private static final long serialVersionUID = -7774133708465438649L;
	private Anuncio anuncio;
	private FluxoDeTrabalho fluxo;
	
	@ManyToOne
	public Anuncio getAnuncio() {
		return anuncio;
	}
	public void setAnuncio(Anuncio anuncio) {
		this.anuncio = anuncio;
	}
	@ManyToOne
	public FluxoDeTrabalho getFluxo() {
		return fluxo;
	}
	public void setFluxo(FluxoDeTrabalho fluxo) {
		this.fluxo = fluxo;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fluxo == null) ? 0 : fluxo.hashCode());
		result = prime * result + ((anuncio == null) ? 0 : anuncio.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AnuncioFluxoId other = (AnuncioFluxoId) obj;
		if (fluxo == null) {
			if (other.fluxo != null)
				return false;
		} else if (!fluxo.equals(other.fluxo))
			return false;
		if (anuncio == null) {
			if (other.anuncio != null)
				return false;
		} else if (!anuncio.equals(other.anuncio))
			return false;
		return true;
	}
	
	
}
