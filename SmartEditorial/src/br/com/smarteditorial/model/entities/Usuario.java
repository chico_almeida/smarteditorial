package br.com.smarteditorial.model.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import org.springframework.stereotype.Component;

@Entity
@Component
public class Usuario extends AbstractEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3303919650646095754L;
	@Id
	@GeneratedValue
	@Column(name="id_usuario")
	private Integer idUsuario;
	@Column(nullable = false)
	private String nome;
	private String sobrenome;
	private String senha;
	@Column(nullable = false, unique = true)
	private String email;
	private boolean ativo;

	@ManyToMany(mappedBy ="usuarios", fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH, CascadeType.MERGE})
	private Set<Grupo> grupos = new HashSet<Grupo>();
	
	@OneToMany(mappedBy="usuario")
	private Set<Edicao> edicoes= new HashSet<Edicao>();
	
	@OneToMany(mappedBy="usuario")
	private Set<Publicacao> publicacoes = new HashSet<Publicacao>();

	@OneToMany(mappedBy="usuario")
	private Set<Artigo> artigos = new HashSet<Artigo>();
	
	@OneToMany(mappedBy="usuario")
	private Set<Imagem> imagens = new HashSet<Imagem>();
	
	@OneToMany(mappedBy="usuario")
	private Set<Anuncio> anuncios = new HashSet<Anuncio>();
	
	@OneToMany(mappedBy="usuario")
	private Set<Pagina> paginas = new HashSet<Pagina>();
	
	@OneToMany(mappedBy="usuario")
	private Set<ImagemFluxo> imagensFluxo = new HashSet<ImagemFluxo>();
	
	@OneToMany(mappedBy="usuario")
	private Set<AnuncioFluxo> anunciosFluxo = new HashSet<AnuncioFluxo>();
	
	@OneToMany(mappedBy="usuario")
	private Set<ArtigoFluxo> artigosFluxo = new HashSet<ArtigoFluxo>();
	
	@OneToMany(mappedBy="usuario")
	private Set<PaginaFluxo> paginasFluxo = new HashSet<PaginaFluxo>();
	
	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Set<Grupo> getGrupos() {
		return grupos;
	}

	public void setGrupos(Set<Grupo> grupos) {
		this.grupos = grupos;
	}

	public Set<Edicao> getEdicoes() {
		return edicoes;
	}

	public void setEdicoes(Set<Edicao> edicoes) {
		this.edicoes = edicoes;
	}

	public Set<Publicacao> getPublicacoes() {
		return publicacoes;
	}

	public void setPublicacoes(Set<Publicacao> publicacoes) {
		this.publicacoes = publicacoes;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Set<Artigo> getArtigos() {
		return artigos;
	}

	public void setArtigos(Set<Artigo> artigos) {
		this.artigos = artigos;
	}

	public Set<Imagem> getImagens() {
		return imagens;
	}

	public void setImagens(Set<Imagem> imagens) {
		this.imagens = imagens;
	}

	public Set<Anuncio> getAnuncios() {
		return anuncios;
	}

	public void setAnuncios(Set<Anuncio> anuncios) {
		this.anuncios = anuncios;
	}

	public Set<Pagina> getPaginas() {
		return paginas;
	}

	public void setPaginas(Set<Pagina> paginas) {
		this.paginas = paginas;
	}

	public Set<ImagemFluxo> getImagensFluxo() {
		return imagensFluxo;
	}

	public void setImagensFluxo(Set<ImagemFluxo> imagensFluxo) {
		this.imagensFluxo = imagensFluxo;
	}

	public Set<AnuncioFluxo> getAnunciosFluxo() {
		return anunciosFluxo;
	}

	public void setAnunciosFluxo(Set<AnuncioFluxo> anunciosFluxo) {
		this.anunciosFluxo = anunciosFluxo;
	}

	public Set<ArtigoFluxo> getArtigosFluxo() {
		return artigosFluxo;
	}

	public void setArtigosFluxo(Set<ArtigoFluxo> artigosFluxo) {
		this.artigosFluxo = artigosFluxo;
	}

	public Set<PaginaFluxo> getPaginasFluxo() {
		return paginasFluxo;
	}

	public void setPaginasFluxo(Set<PaginaFluxo> paginasFluxo) {
		this.paginasFluxo = paginasFluxo;
	}



}