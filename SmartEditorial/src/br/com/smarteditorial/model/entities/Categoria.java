package br.com.smarteditorial.model.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.springframework.stereotype.Component;

@Entity
@Component
public class Categoria extends AbstractEntity{

	private static final long serialVersionUID = -2866621110494874632L;
	
	@Id
	@GeneratedValue
	@Column(name="id_categoria")
	private Integer idCategoria;
	private String titulo;
	private String descricao;
	
	@ManyToOne
	@JoinColumn(name = "id_publicacao")
	private Publicacao publicacao;

	public Integer getIdCategoria() {
		return idCategoria;
	}

	public void setIdCategoria(Integer idCategoria) {
		this.idCategoria = idCategoria;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Publicacao getPublicacao() {
		return publicacao;
	}

	public void setPublicacao(Publicacao publicacao) {
		this.publicacao = publicacao;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
