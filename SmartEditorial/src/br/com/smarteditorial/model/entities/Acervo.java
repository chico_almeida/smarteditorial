package br.com.smarteditorial.model.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import org.springframework.stereotype.Component;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Component
public class Acervo extends AbstractEntity {

	private static final long serialVersionUID = -1632252856932583714L;
	
	@Id
	@GeneratedValue
	@Column(name="id_acervo")
	private Integer idAcervo;
	private String estado;
	private String caminho;
	private String titulo;
	
	public Integer getIdAcervo() {
		return idAcervo;
	}

	public void setIdAcervo(Integer idAcervo) {
		this.idAcervo = idAcervo;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCaminho() {
		return caminho;
	}

	public void setCaminho(String caminho) {
		this.caminho = caminho;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}