package br.com.smarteditorial.model.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import org.springframework.stereotype.Component;

@Entity
@Component
public class Permissao extends AbstractEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3236573802845565402L;
	@Id
	@GeneratedValue
	@Column(name="id_permissao")
	private Integer idPermissao;
	private String tipo;
	private String descricao;
	
	@ManyToMany(fetch = FetchType.LAZY, mappedBy ="permissoes", cascade = {CascadeType.REFRESH, CascadeType.MERGE})
	private Set<Grupo> grupos = new HashSet<Grupo>();
	
	public Integer getIdPermissao() {
		return idPermissao;
	}
	public void setIdPermissao(Integer idPermissao) {
		this.idPermissao = idPermissao;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Set<Grupo> getGrupos() {
		return grupos;
	}
	public void setGrupos(Set<Grupo> grupos) {
		this.grupos = grupos;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
