package br.com.smarteditorial.model.entities;

import java.io.Serializable;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.stereotype.Component;

@Entity
@Component
@Table(name= "fluxo_pagina")
@AssociationOverrides({
	@AssociationOverride(name = "idPaginaFluxo.pagina", 
		joinColumns = @JoinColumn(name = "id_pagina")),
	@AssociationOverride(name = "idPaginaFluxo.fluxo", 
		joinColumns = @JoinColumn(name = "id_fluxo")),
	@AssociationOverride(name = "usuario", 
		joinColumns = @JoinColumn(name = "id_usuario"))
})
public class PaginaFluxo extends AbstractEntity implements Serializable {
	
	private static final long serialVersionUID = -8603972887713959836L;
	
	private PaginaFluxoId idPaginaFluxo = new PaginaFluxoId();
	private String comentario;
	private Usuario usuario;
	
	@ManyToOne
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public PaginaFluxo() {
		
	}
	
	@EmbeddedId
	public PaginaFluxoId getIdPaginaFluxo() {
		return idPaginaFluxo;
	}

	public void setIdPaginaFluxo(PaginaFluxoId idPaginaFluxo) {
		this.idPaginaFluxo = idPaginaFluxo;
	}
	
	@Transient
	public Pagina getPagina() {
		return getIdPaginaFluxo().getPagina();
	}
 
	public void setPagina(Pagina pagina) {
		getIdPaginaFluxo().setPagina(pagina);
	}
 
	@Transient
	public FluxoDeTrabalho getFluxoDeTrabalho() {
		return getIdPaginaFluxo().getFluxo();
	}
 
	public void setFluxoDeTrabalho(FluxoDeTrabalho fluxo) {
		getIdPaginaFluxo().setFluxo(fluxo);
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((comentario == null) ? 0 : comentario.hashCode());
		result = prime * result
				+ ((idPaginaFluxo == null) ? 0 : idPaginaFluxo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaginaFluxo other = (PaginaFluxo) obj;
		if (comentario == null) {
			if (other.comentario != null)
				return false;
		} else if (!comentario.equals(other.comentario))
			return false;
		if (idPaginaFluxo == null) {
			if (other.idPaginaFluxo != null)
				return false;
		} else if (!idPaginaFluxo.equals(other.idPaginaFluxo))
			return false;
		return true;
	}
	
}
