package br.com.smarteditorial.model.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.springframework.stereotype.Component;

@Entity
@Component
public class Pagina extends AbstractEntity {

	private static final long serialVersionUID = 6027117715712284914L;
	
	@Id
	@GeneratedValue
	@Column(name="id_pagina")
	private Integer idPagina;
	private String titulo;
	private String descricao;
	private String caminho;
	private String estado;
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "idPaginaFluxo.pagina", cascade= CascadeType.REFRESH)
	private Set<PaginaFluxo> paginaFluxos = new HashSet<PaginaFluxo>(0);
	@ManyToOne
	@JoinColumn(name = "id_edicao")
	private Edicao edicao;
	@ManyToOne
	@JoinColumn(name = "id_categoria")
	private Categoria categoria;
	@ManyToOne
	@JoinColumn(name = "id_usuario")
	private Usuario usuario;
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	@JoinTable(name="tags_pagina", 
		joinColumns = { @JoinColumn(name = "id_pagina", nullable = false, updatable = false) }, 
		inverseJoinColumns = { @JoinColumn(name = "id_tag", nullable = false, updatable = false) })  
	private Set<Tags> tags = new HashSet<Tags>();
	
	public Pagina() {
	}
 
	public Pagina(String estado, String caminho, String titulo, String descricao) {
		this.titulo =  titulo;
		this.descricao= descricao;
		this.caminho = caminho;
		this.estado = estado;
	}
 
	public Pagina(String estado, String caminho, String titulo, String descricao, Set<PaginaFluxo> paginaFluxos) {
		this.estado = estado;
		this.caminho = caminho;
		this.titulo =  titulo;
		this.paginaFluxos = paginaFluxos;
	}
	
	public Categoria getCategoria() {
		return categoria;
	}
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	public Integer getIdPagina() {
		return idPagina;
	}
	public void setIdPagina(Integer idPagina) {
		this.idPagina = idPagina;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getCaminho() {
		return caminho;
	}
	public void setCaminho(String caminho) {
		this.caminho = caminho;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public Set<Tags> getTags() {
		return tags;
	}
	public void setTags(Set<Tags> tags) {
		this.tags = tags;
	}
	public Edicao getEdicao() {
		return edicao;
	}
	public void setEdicao(Edicao edicao) {
		this.edicao = edicao;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	

}
