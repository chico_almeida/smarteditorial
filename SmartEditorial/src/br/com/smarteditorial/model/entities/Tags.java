package br.com.smarteditorial.model.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import org.springframework.stereotype.Component;

@Entity
@Component
public class Tags extends AbstractEntity {

	private static final long serialVersionUID = 8433310816853816435L;

	
	@Id
	@GeneratedValue
	@Column(name="id_tags")
	private Integer idTags;
	private String tags;
	
	@ManyToMany(fetch = FetchType.LAZY, mappedBy ="tags")
	private Set<Anuncio> anuncios = new HashSet<Anuncio>();
	
	@ManyToMany(fetch = FetchType.LAZY, mappedBy ="tags")
	private Set<Imagem> imagens = new HashSet<Imagem>();
	
	@ManyToMany(fetch = FetchType.LAZY, mappedBy ="tags")
	private Set<Artigo> artigos = new HashSet<Artigo>();

	@ManyToMany(fetch = FetchType.LAZY, mappedBy ="tags")
	private Set<Pagina> paginas = new HashSet<Pagina>();

	

	public Integer getIdTags() {
		return idTags;
	}

	public void setIdTags(Integer idTags) {
		this.idTags = idTags;
	}

	public Set<Anuncio> getAnuncios() {
		return anuncios;
	}

	public void setAnuncios(Set<Anuncio> anuncios) {
		this.anuncios = anuncios;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Set<Imagem> getImagens() {
		return imagens;
	}

	public void setImagens(Set<Imagem> imagens) {
		this.imagens = imagens;
	}

	public Set<Artigo> getArtigos() {
		return artigos;
	}

	public void setArtigos(Set<Artigo> artigos) {
		this.artigos = artigos;
	}

	public Set<Pagina> getPaginas() {
		return paginas;
	}

	public void setPaginas(Set<Pagina> paginas) {
		this.paginas = paginas;
	}
	
}
