package br.com.smarteditorial.model.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import org.springframework.stereotype.Component;

@Entity
@Component
public class Grupo extends AbstractEntity {
	
	private static final long serialVersionUID = 3239867183377080941L;
	@Id
	@GeneratedValue
	@Column(name="id_grupo")
	private Integer idGrupo;
	@Column(nullable = false, unique = true)
	private String nome;
	private String descricao;
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH, CascadeType.MERGE})
	@JoinTable(name="grupo_usuario", 
		joinColumns = { @JoinColumn(name = "id_grupo", nullable = false, updatable = false) }, 
		inverseJoinColumns = { @JoinColumn(name = "id_usuario", nullable = false, updatable = false) })
	private Set<Usuario> usuarios = new HashSet<Usuario>();
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH, CascadeType.MERGE})
	@JoinTable(name="grupo_permissao", 
		joinColumns = { @JoinColumn(name = "id_grupo", nullable = false, updatable = false) }, 
		inverseJoinColumns = { @JoinColumn(name = "id_permissao", nullable = false, updatable = false) })  
	private Set<Permissao> permissoes = new HashSet<Permissao>();

	public Integer getIdGrupo() {
		return idGrupo;
	}

	public void setIdGrupo(Integer idGrupo) {
		this.idGrupo = idGrupo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Set<Usuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(Set<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	public Set<Permissao> getPermissoes() {
		return permissoes;
	}

	public void setPermissoes(Set<Permissao> permissoes) {
		this.permissoes = permissoes;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}


}
