package br.com.smarteditorial.model.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.springframework.stereotype.Component;

@Entity
@Component
public class Empresa extends AbstractEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7062889461756768280L;
	@Id
	@GeneratedValue
	@Column(name="id_empresa")
	private Integer idEmpresa;
	@Column(nullable = false, unique = true)
	private String empresa;
	private Integer telefone;
	@Column(nullable = false, unique = true)
	private String email;
	@Column(nullable = false)
	private String contatoComercial;
	
	
	public Integer getIdEmpresa() {
		return idEmpresa;
	}
	public void setIdEmpresa(Integer idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
	public String getEmpresa() {
		return empresa;
	}
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	public Integer getTelefone() {
		return telefone;
	}
	public void setTelefone(Integer telefone) {
		this.telefone = telefone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getContatoComercial() {
		return contatoComercial;
	}
	public void setContatoComercial(String contatoComercial) {
		this.contatoComercial = contatoComercial;
	}
	
	
}
