package br.com.smarteditorial.model.entities;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.springframework.stereotype.Component;

@Entity
@Component
public class Edicao extends AbstractEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5726169147434227237L;
	@Id
	@GeneratedValue
	@Column(name="id_edicao")
	private Integer idEdicao;
	private String titulo;
	private String descricao;
	@Column(name="data_publicacao", nullable=true)
	private Date dataPublicacao;
	@Column(name="data_limite_edicao", nullable=true)
	private Date dataLimiteEdicao;
	
	@ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH, CascadeType.MERGE})
	@JoinColumn(name = "id_usuario")
	private Usuario usuario;
	
	@ManyToOne
	@JoinColumn(name = "id_publicacao")
	private Publicacao publicacao;
	
	@OneToMany(mappedBy="edicao", fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH, CascadeType.MERGE})
	private Set<Anuncio> anuncios = new HashSet<Anuncio>(0);
	
	@OneToMany(mappedBy="edicao", fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH, CascadeType.MERGE})
	private Set<Imagem> imagens = new HashSet<Imagem>(0);
	
	@OneToMany(mappedBy="edicao", fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH, CascadeType.MERGE})
	private Set<Artigo> artigos = new HashSet<Artigo>(0);
	
	@OneToMany(mappedBy="edicao", fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH, CascadeType.MERGE})
	private Set<Pagina> paginas = new HashSet<Pagina>(0);

	@OneToOne(mappedBy="edicao")
	@JoinColumn(name="id_espelho")
	private Espelho espelho;
	
	public Integer getIdEdicao() {
		return idEdicao;
	}

	public void setIdEdicao(Integer idEdicao) {
		this.idEdicao = idEdicao;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Date getDataPublicacao() {
		return dataPublicacao;
	}

	public void setDataPublicacao(Date dataPublicacao) {
		this.dataPublicacao = dataPublicacao;
	}

	public Date getDataLimiteEdicao() {
		return dataLimiteEdicao;
	}

	public void setDataLimiteEdicao(Date dataLimiteEdicao) {
		this.dataLimiteEdicao = dataLimiteEdicao;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Publicacao getPublicacao() {
		return publicacao;
	}

	public void setPublicacao(Publicacao publicacao) {
		this.publicacao = publicacao;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Set<Anuncio> getAnuncios() {
		return anuncios;
	}

	public void setAnuncios(Set<Anuncio> anuncios) {
		this.anuncios = anuncios;
	}

	public Set<Imagem> getImagens() {
		return imagens;
	}

	public void setImagens(Set<Imagem> imagens) {
		this.imagens = imagens;
	}

	public Set<Artigo> getArtigos() {
		return artigos;
	}

	public void setArtigos(Set<Artigo> artigos) {
		this.artigos = artigos;
	}

	public Espelho getEspelho() {
		return espelho;
	}

	public void setEspelho(Espelho espelho) {
		this.espelho = espelho;
	}

	public Set<Pagina> getPaginas() {
		return paginas;
	}

	public void setPaginas(Set<Pagina> paginas) {
		this.paginas = paginas;
	}
	

}
