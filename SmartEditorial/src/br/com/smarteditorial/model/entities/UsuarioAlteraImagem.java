package br.com.smarteditorial.model.entities;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.springframework.stereotype.Component;


@Entity
@Component
@IdClass(UsuarioAlteraImagem.class)
public class UsuarioAlteraImagem extends AbstractEntity {

	private static final long serialVersionUID = -5782356236914637883L;

	@Id
	@ManyToOne
	@JoinColumn(name="id_usuario")
	private Usuario usuario;
	
	@Id
	@ManyToOne
	@JoinColumn(name="id_edicao")
	private Edicao edicao;
	
	@Id
	@ManyToOne
	@JoinColumn(name="id_imagem")
	private Imagem imagem;
	
	private Date dataDeModificacao;
	private String comentario;
	
	
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public Edicao getEdicao() {
		return edicao;
	}
	public void setEdicao(Edicao edicao) {
		this.edicao = edicao;
	}
	public Imagem getImagem() {
		return imagem;
	}
	public void setImagem(Imagem imagem) {
		this.imagem = imagem;
	}
	public Date getDataDeModificacao() {
		return dataDeModificacao;
	}
	public void setDataDeModificacao(Date dataDeModificacao) {
		this.dataDeModificacao = dataDeModificacao;
	}
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((comentario == null) ? 0 : comentario.hashCode());
		result = prime
				* result
				+ ((dataDeModificacao == null) ? 0 : dataDeModificacao
						.hashCode());
		result = prime * result + ((edicao == null) ? 0 : edicao.hashCode());
		result = prime * result + ((imagem == null) ? 0 : imagem.hashCode());
		result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UsuarioAlteraImagem other = (UsuarioAlteraImagem) obj;
		if (comentario == null) {
			if (other.comentario != null)
				return false;
		} else if (!comentario.equals(other.comentario))
			return false;
		if (dataDeModificacao == null) {
			if (other.dataDeModificacao != null)
				return false;
		} else if (!dataDeModificacao.equals(other.dataDeModificacao))
			return false;
		if (edicao == null) {
			if (other.edicao != null)
				return false;
		} else if (!edicao.equals(other.edicao))
			return false;
		if (imagem == null) {
			if (other.imagem != null)
				return false;
		} else if (!imagem.equals(other.imagem))
			return false;
		if (usuario == null) {
			if (other.usuario != null)
				return false;
		} else if (!usuario.equals(other.usuario))
			return false;
		return true;
	}
	
}