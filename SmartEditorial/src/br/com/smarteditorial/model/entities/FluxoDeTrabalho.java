package br.com.smarteditorial.model.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Component
@Table(name="fluxo_de_trabalho")
public class FluxoDeTrabalho extends AbstractEntity {

	private static final long serialVersionUID = 4313302786201016636L;
	
	@Id
	@GeneratedValue
	@Column(name="id_fluxo")
	private Integer idFluxo;
	private Integer ordem;
	private String tipoElemento;
	private boolean podeInterromper;
	private boolean podeFinalizar;
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "idImagemFluxo.fluxo", cascade= CascadeType.REFRESH)
	private Set<ImagemFluxo> imagemFluxos = new HashSet<ImagemFluxo>(0);
	
	public FluxoDeTrabalho() {
	}
 
	public FluxoDeTrabalho(Integer ordem, String tipoElemento, boolean podeInterromper, boolean podeFinalizar) {
		this.ordem = ordem;
		this.tipoElemento = tipoElemento;
		this.podeInterromper =  podeInterromper;
		this.podeFinalizar = podeFinalizar;
	}
 
	public FluxoDeTrabalho(Integer ordem, String tipoElemento, boolean podeInterromper, boolean podeFinalizar, Set<ImagemFluxo> imagemFluxos) {
		this.ordem = ordem;
		this.tipoElemento = tipoElemento;
		this.podeInterromper =  podeInterromper;
		this.podeFinalizar = podeFinalizar;
		this.imagemFluxos = imagemFluxos;
	}
	
	@ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH, CascadeType.MERGE})
	@JoinColumn(name = "id_publicacao")
	private Publicacao publicacao;
	
	@ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH, CascadeType.MERGE})
	@JoinColumn(name = "id_grupo_origem")
	private Grupo grupoOrigem;
	
	@ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH, CascadeType.MERGE})
	@JoinColumn(name = "id_grupo_destino")
	private Grupo grupoDestino;

	public Integer getIdFluxo() {
		return idFluxo;
	}

	public void setIdFluxo(Integer idFluxo) {
		this.idFluxo = idFluxo;
	}

	public Integer getOrdem() {
		return ordem;
	}

	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}

	public String getTipoElemento() {
		return tipoElemento;
	}

	public void setTipoElemento(String tipoElemento) {
		this.tipoElemento = tipoElemento;
	}

	public boolean isPodeInterromper() {
		return podeInterromper;
	}

	public void setPodeInterromper(boolean podeInterromper) {
		this.podeInterromper = podeInterromper;
	}

	public boolean isPodeFinalizar() {
		return podeFinalizar;
	}

	public void setPodeFinalizar(boolean podeFinalizar) {
		this.podeFinalizar = podeFinalizar;
	}
	
	public Set<ImagemFluxo> getImagemFluxos() {
		return imagemFluxos;
	}

	public void setImagemFluxos(Set<ImagemFluxo> imagemFluxos) {
		this.imagemFluxos = imagemFluxos;
	}

	public Publicacao getPublicacao() {
		return publicacao;
	}

	public void setPublicacao(Publicacao publicacao) {
		this.publicacao = publicacao;
	}

	public Grupo getGrupoOrigem() {
		return grupoOrigem;
	}

	public void setGrupoOrigem(Grupo grupoOrigem) {
		this.grupoOrigem = grupoOrigem;
	}

	public Grupo getGrupoDestino() {
		return grupoDestino;
	}

	public void setGrupoDestino(Grupo grupoDestino) {
		this.grupoDestino = grupoDestino;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
