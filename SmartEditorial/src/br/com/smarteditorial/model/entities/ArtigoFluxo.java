package br.com.smarteditorial.model.entities;

import java.io.Serializable;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.stereotype.Component;

@Entity
@Component
@Table(name= "fluxo_artigo")
@AssociationOverrides({
	@AssociationOverride(name = "idArtigoFluxo.artigo", 
		joinColumns = @JoinColumn(name = "id_artigo")),
	@AssociationOverride(name = "idArtigoFluxo.fluxo", 
		joinColumns = @JoinColumn(name = "id_fluxo")),
	@AssociationOverride(name = "usuario", 
		joinColumns = @JoinColumn(name = "id_usuario"))
})
public class ArtigoFluxo extends AbstractEntity implements Serializable {
	
	private static final long serialVersionUID = -8603972887713959836L;
	
	private ArtigoFluxoId idArtigoFluxo = new ArtigoFluxoId();
	private String comentario;
	private Usuario usuario;

	@ManyToOne
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public ArtigoFluxo() {
		
	}
	
	@EmbeddedId
	public ArtigoFluxoId getIdArtigoFluxo() {
		return idArtigoFluxo;
	}

	public void setIdArtigoFluxo(ArtigoFluxoId idArtigoFluxo) {
		this.idArtigoFluxo = idArtigoFluxo;
	}
	
	@Transient
	public Artigo getArtigo() {
		return getIdArtigoFluxo().getArtigo();
	}
 
	public void setArtigo(Artigo artigo) {
		getIdArtigoFluxo().setArtigo(artigo);
	}
 
	@Transient
	public FluxoDeTrabalho getFluxoDeTrabalho() {
		return getIdArtigoFluxo().getFluxo();
	}
 
	public void setFluxoDeTrabalho(FluxoDeTrabalho fluxo) {
		getIdArtigoFluxo().setFluxo(fluxo);
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((comentario == null) ? 0 : comentario.hashCode());
		result = prime * result
				+ ((idArtigoFluxo == null) ? 0 : idArtigoFluxo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ArtigoFluxo other = (ArtigoFluxo) obj;
		if (comentario == null) {
			if (other.comentario != null)
				return false;
		} else if (!comentario.equals(other.comentario))
			return false;
		if (idArtigoFluxo == null) {
			if (other.idArtigoFluxo != null)
				return false;
		} else if (!idArtigoFluxo.equals(other.idArtigoFluxo))
			return false;
		return true;
	}
	
}
