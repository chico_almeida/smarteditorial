package br.com.smarteditorial.model.entities;

import java.io.Serializable;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.stereotype.Component;

@Entity
@Component
@Table(name= "fluxo_imagem")
@AssociationOverrides({
	@AssociationOverride(name = "idImagemFluxo.imagem", 
		joinColumns = @JoinColumn(name = "id_imagem")),
	@AssociationOverride(name = "idImagemFluxo.fluxo", 
		joinColumns = @JoinColumn(name = "id_fluxo")),
	@AssociationOverride(name = "usuario", 
		joinColumns = @JoinColumn(name = "id_usuario"))
})
public class ImagemFluxo extends AbstractEntity implements Serializable {
	
	private static final long serialVersionUID = -8603972887713959836L;
	
	private ImagemFluxoId idImagemFluxo = new ImagemFluxoId();
	private String comentario;
	private Usuario usuario;
	

	public ImagemFluxo() {
		
	}
	
	@EmbeddedId
	public ImagemFluxoId getIdImagemFluxo() {
		return idImagemFluxo;
	}

	public void setIdImagemFluxo(ImagemFluxoId idImagemFluxo) {
		this.idImagemFluxo = idImagemFluxo;
	}
	
	@Transient
	public Imagem getImagem() {
		return getIdImagemFluxo().getImagem();
	}
 
	public void setImagem(Imagem imagem) {
		getIdImagemFluxo().setImagem(imagem);
	}
 
	@Transient
	public FluxoDeTrabalho getFluxoDeTrabalho() {
		return getIdImagemFluxo().getFluxo();
	}
 
	public void setFluxoDeTrabalho(FluxoDeTrabalho fluxo) {
		getIdImagemFluxo().setFluxo(fluxo);
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((comentario == null) ? 0 : comentario.hashCode());
		result = prime * result
				+ ((idImagemFluxo == null) ? 0 : idImagemFluxo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ImagemFluxo other = (ImagemFluxo) obj;
		if (comentario == null) {
			if (other.comentario != null)
				return false;
		} else if (!comentario.equals(other.comentario))
			return false;
		if (idImagemFluxo == null) {
			if (other.idImagemFluxo != null)
				return false;
		} else if (!idImagemFluxo.equals(other.idImagemFluxo))
			return false;
		return true;
	}

	@ManyToOne
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
}
