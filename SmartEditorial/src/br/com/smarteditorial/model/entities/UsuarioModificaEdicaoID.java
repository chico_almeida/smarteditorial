package br.com.smarteditorial.model.entities;

import java.io.Serializable;

public class UsuarioModificaEdicaoID implements Serializable{
	private static final long serialVersionUID = 777031735325947088L;
	private Usuario usuario;
	private Edicao edicao;
	
	public UsuarioModificaEdicaoID() {
		
	}
	
	public UsuarioModificaEdicaoID(Usuario usuario, Edicao edicao){
		super();
		this.usuario = usuario;
		this.edicao = edicao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((edicao == null) ? 0 : edicao.hashCode());
		result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UsuarioModificaEdicaoID other = (UsuarioModificaEdicaoID) obj;
		if (edicao == null) {
			if (other.edicao != null)
				return false;
		} else if (!edicao.equals(other.edicao))
			return false;
		if (usuario == null) {
			if (other.usuario != null)
				return false;
		} else if (!usuario.equals(other.usuario))
			return false;
		return true;
	}

	
}