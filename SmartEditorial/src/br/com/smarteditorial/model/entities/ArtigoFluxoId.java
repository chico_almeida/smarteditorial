package br.com.smarteditorial.model.entities;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

@Embeddable
public class ArtigoFluxoId implements Serializable {

	private static final long serialVersionUID = -7774133708465438649L;
	private Artigo artigo;
	private FluxoDeTrabalho fluxo;
	
	@ManyToOne
	public Artigo getArtigo() {
		return artigo;
	}
	public void setArtigo(Artigo artigo) {
		this.artigo = artigo;
	}
	@ManyToOne
	public FluxoDeTrabalho getFluxo() {
		return fluxo;
	}
	public void setFluxo(FluxoDeTrabalho fluxo) {
		this.fluxo = fluxo;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fluxo == null) ? 0 : fluxo.hashCode());
		result = prime * result + ((artigo == null) ? 0 : artigo.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ArtigoFluxoId other = (ArtigoFluxoId) obj;
		if (fluxo == null) {
			if (other.fluxo != null)
				return false;
		} else if (!fluxo.equals(other.fluxo))
			return false;
		if (artigo == null) {
			if (other.artigo != null)
				return false;
		} else if (!artigo.equals(other.artigo))
			return false;
		return true;
	}
	
	
}
