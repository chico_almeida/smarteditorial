package br.com.smarteditorial.model.entities;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@IdClass(UsuarioAlteraPaginaID.class)
public class UsuarioAlteraPagina {
	
	@Id
	@ManyToOne
	@JoinColumn(name="id_usuario")
	private Usuario usuario;
	
	@Id
	@ManyToOne
	@JoinColumn(name="id_edicao")
	private Edicao edicao;
	
	@Id
	@ManyToOne
	@JoinColumn(name="id_pagina")
	private Pagina pagina;
	
	private Date dataDeModificacao;
	private String comentario;
	
	
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public Edicao getEdicao() {
		return edicao;
	}
	public void setEdicao(Edicao edicao) {
		this.edicao = edicao;
	}
	public Pagina getPagina() {
		return pagina;
	}
	public void setPagina(Pagina pagina) {
		this.pagina = pagina;
	}
	public Date getDataDeModificacao() {
		return dataDeModificacao;
	}
	public void setDataDeModificacao(Date dataDeModificacao) {
		this.dataDeModificacao = dataDeModificacao;
	}
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	
}	
