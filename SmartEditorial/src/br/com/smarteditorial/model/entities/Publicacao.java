package br.com.smarteditorial.model.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.springframework.stereotype.Component;

@Entity
@Component
public class Publicacao extends AbstractEntity {
 
	/**
	 * 
	 */
	private static final long serialVersionUID = -3346538692278736950L;
	@Id
	@GeneratedValue
	@Column(name="id_publicacao")
	private Integer idPublicacao;
	private String titulo;
	private String descricao;
	
	@OneToMany(mappedBy="publicacao", fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	private Set<Edicao> edicoes = new HashSet<Edicao>(0);
	
	@OneToMany(mappedBy="publicacao", fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	private Set<Categoria> categorias = new HashSet<Categoria>(0);
	
	
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "id_usuario")
	private Usuario usuario;

	public Integer getIdPublicacao() {
		return idPublicacao;
	}

	public void setIdPublicacao(Integer idPublicacao) {
		this.idPublicacao = idPublicacao;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Set<Edicao> getEdicoes() {
		return edicoes;
	}

	public void setEdicoes(Set<Edicao> edicoes) {
		this.edicoes = edicoes;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Set<Categoria> getCategorias() {
		return categorias;
	}

	public void setCategorias(Set<Categoria> categorias) {
		this.categorias = categorias;
	}

	
}
