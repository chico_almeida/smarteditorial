package br.com.smarteditorial.model.entities;

import java.io.Serializable;

public class UsuarioAlteraImagemID implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5578715113306303286L;
	
	private Usuario usuario;
	private Edicao edicao;
	private Imagem imagem;
	
	public UsuarioAlteraImagemID() {
		
	}
	
	public UsuarioAlteraImagemID(Usuario usuario, Edicao edicao, Imagem imagem){
		super();
		this.usuario = usuario;
		this.edicao = edicao;
		this.imagem = imagem;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((edicao == null) ? 0 : edicao.hashCode());
		result = prime * result + ((imagem == null) ? 0 : imagem.hashCode());
		result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UsuarioAlteraImagemID other = (UsuarioAlteraImagemID) obj;
		if (edicao == null) {
			if (other.edicao != null)
				return false;
		} else if (!edicao.equals(other.edicao))
			return false;
		if (imagem == null) {
			if (other.imagem != null)
				return false;
		} else if (!imagem.equals(other.imagem))
			return false;
		if (usuario == null) {
			if (other.usuario != null)
				return false;
		} else if (!usuario.equals(other.usuario))
			return false;
		return true;
	}

}
