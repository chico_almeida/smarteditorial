package br.com.smarteditorial.model.entities;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

@Embeddable
public class ImagemFluxoId implements Serializable {

	private static final long serialVersionUID = -7774133708465438649L;
	private Imagem imagem;
	private FluxoDeTrabalho fluxo;
	
	@ManyToOne
	public Imagem getImagem() {
		return imagem;
	}
	public void setImagem(Imagem imagem) {
		this.imagem = imagem;
	}
	@ManyToOne
	public FluxoDeTrabalho getFluxo() {
		return fluxo;
	}
	public void setFluxo(FluxoDeTrabalho fluxo) {
		this.fluxo = fluxo;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fluxo == null) ? 0 : fluxo.hashCode());
		result = prime * result + ((imagem == null) ? 0 : imagem.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ImagemFluxoId other = (ImagemFluxoId) obj;
		if (fluxo == null) {
			if (other.fluxo != null)
				return false;
		} else if (!fluxo.equals(other.fluxo))
			return false;
		if (imagem == null) {
			if (other.imagem != null)
				return false;
		} else if (!imagem.equals(other.imagem))
			return false;
		return true;
	}
	
	
}
