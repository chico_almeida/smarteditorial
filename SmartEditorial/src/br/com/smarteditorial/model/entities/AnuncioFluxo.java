package br.com.smarteditorial.model.entities;

import java.io.Serializable;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.stereotype.Component;

@Entity
@Component
@Table(name= "fluxo_anuncio")
@AssociationOverrides({
	@AssociationOverride(name = "idAnuncioFluxo.anuncio", 
		joinColumns = @JoinColumn(name = "id_anuncio")),
	@AssociationOverride(name = "idAnuncioFluxo.fluxo", 
		joinColumns = @JoinColumn(name = "id_fluxo")),
	@AssociationOverride(name = "usuario", 
		joinColumns = @JoinColumn(name = "id_usuario"))
})
public class AnuncioFluxo extends AbstractEntity implements Serializable {
	
	private static final long serialVersionUID = -8603972887713959836L;
	
	private AnuncioFluxoId idAnuncioFluxo = new AnuncioFluxoId();
	private String comentario;
	private Usuario usuario;
	
	@ManyToOne
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public AnuncioFluxo() {
		
	}
	
	@EmbeddedId
	public AnuncioFluxoId getIdAnuncioFluxo() {
		return idAnuncioFluxo;
	}

	public void setIdAnuncioFluxo(AnuncioFluxoId idAnuncioFluxo) {
		this.idAnuncioFluxo = idAnuncioFluxo;
	}
	
	@Transient
	public Anuncio getAnuncio() {
		return getIdAnuncioFluxo().getAnuncio();
	}
 
	public void setAnuncio(Anuncio anuncio) {
		getIdAnuncioFluxo().setAnuncio(anuncio);
	}
 
	@Transient
	public FluxoDeTrabalho getFluxoDeTrabalho() {
		return getIdAnuncioFluxo().getFluxo();
	}
 
	public void setFluxoDeTrabalho(FluxoDeTrabalho fluxo) {
		getIdAnuncioFluxo().setFluxo(fluxo);
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((comentario == null) ? 0 : comentario.hashCode());
		result = prime * result
				+ ((idAnuncioFluxo == null) ? 0 : idAnuncioFluxo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AnuncioFluxo other = (AnuncioFluxo) obj;
		if (comentario == null) {
			if (other.comentario != null)
				return false;
		} else if (!comentario.equals(other.comentario))
			return false;
		if (idAnuncioFluxo == null) {
			if (other.idAnuncioFluxo != null)
				return false;
		} else if (!idAnuncioFluxo.equals(other.idAnuncioFluxo))
			return false;
		return true;
	}
	
}
