package br.com.smarteditorial.model.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class MapElementos {
	@Id
	@Column(name="ID_ELEMENTO")
	private String idElemento;
	private String titulo;
	private String estado;
	private String caminho;
	private String comentario;
	private String idPublicacao;
	private String publicacao;
	private String edicao;
	private String categoria;
	@Column(name="id_fluxo")
	private String idFluxo;
	@Column(name="ordem")
	private String ordemFluxo;
	private String tipoElemento;
	@Column(name="usuario")
	private String usuario;
	
	public String getIdPublicacao() {
		return idPublicacao;
	}
	public void setIdPublicacao(String idPublicacao) {
		this.idPublicacao = idPublicacao;
	}
	public String getIdElemento() {
		return idElemento;
	}
	public void setIdElemento(String idElemento) {
		this.idElemento = idElemento;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getCaminho() {
		return caminho;
	}
	public void setCaminho(String caminho) {
		this.caminho = caminho;
	}
	public String getOrdemFluxo() {
		return ordemFluxo;
	}
	public void setOrdemFluxo(String ordemFluxo) {
		this.ordemFluxo = ordemFluxo;
	}
	public String getTipoElemento() {
		return tipoElemento;
	}
	public void setTipoElemento(String tipoElemento) {
		this.tipoElemento = tipoElemento;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getIdFluxo() {
		return idFluxo;
	}
	public void setIdFluxo(String idFluxo) {
		this.idFluxo = idFluxo;
	}
	public String getPublicacao() {
		return publicacao;
	}
	public void setPublicacao(String publicacao) {
		this.publicacao = publicacao;
	}
	public String getEdicao() {
		return edicao;
	}
	public void setEdicao(String edicao) {
		this.edicao = edicao;
	}
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
}
