package br.com.smarteditorial.model.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Component
@Table(name = "imagem")
public class Imagem extends AbstractEntity {

	private static final long serialVersionUID = -7229218285540965897L;
	
	@Id
	@GeneratedValue
	@Column(name="id_imagem")
	private Integer idImagem;
	private String estado;
	private String caminho;
	private String titulo;
	private String creditos;
	private String fotografo;
	private double altura;
	private double largura;
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "idImagemFluxo.imagem", cascade= CascadeType.REFRESH)
	private Set<ImagemFluxo> imagemFluxos = new HashSet<ImagemFluxo>(0);

	
	public Imagem() {
	}
 
	public Imagem(String estado, String caminho, String titulo, 
			String creditos, String fotografo, double altura, double largura) {
		this.estado = estado;
		this.caminho = caminho;
		this.titulo =  titulo;
		this.creditos= creditos;
		this.fotografo = fotografo;
		this.altura = altura;
		this.largura  = largura;
	}
 
	public Imagem(String estado, String caminho, String titulo, 
			String creditos, String fotografo, double altura, double largura, Set<ImagemFluxo> imagemFluxos) {
		this.estado = estado;
		this.caminho = caminho;
		this.titulo =  titulo;
		this.creditos= creditos;
		this.fotografo = fotografo;
		this.altura = altura;
		this.largura  = largura;
		this.imagemFluxos = imagemFluxos;
	}

	
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	@JoinTable(name="tags_imagem", 
		joinColumns = { @JoinColumn(name = "id_imagem", nullable = false, updatable = false) }, 
		inverseJoinColumns = { @JoinColumn(name = "id_tag", nullable = false, updatable = false) })  
	private Set<Tags> tags = new HashSet<Tags>();
	
	@ManyToOne
	@JoinColumn(name = "id_edicao")
	private Edicao edicao;

	@ManyToOne
	@JoinColumn(name = "id_categoria")
	private Categoria categoria;

	@ManyToOne
	@JoinColumn(name = "id_usuario")
	private Usuario usuario;


	public Integer getIdImagem() {
		return idImagem;
	}

	public void setIdImagem(Integer idImagem) {
		this.idImagem = idImagem;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCaminho() {
		return caminho;
	}

	public void setCaminho(String caminho) {
		this.caminho = caminho;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getCreditos() {
		return creditos;
	}

	public void setCreditos(String creditos) {
		this.creditos = creditos;
	}

	public String getFotografo() {
		return fotografo;
	}

	public void setFotografo(String fotografo) {
		this.fotografo = fotografo;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	public double getLargura() {
		return largura;
	}

	public void setLargura(double largura) {
		this.largura = largura;
	}

	public Set<ImagemFluxo> getImagemFluxos() {
		return this.imagemFluxos;
	}

	public void setImagemFluxos(Set<ImagemFluxo> imagemFluxos) {
		this.imagemFluxos = imagemFluxos;
	}

	public Set<Tags> getTags() {
		return tags;
	}

	public void setTags(Set<Tags> tags) {
		this.tags = tags;
	}

	public Edicao getEdicao() {
		return edicao;
	}

	public void setEdicao(Edicao edicao) {
		this.edicao = edicao;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
