package br.com.smarteditorial.model.bo;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.smarteditorial.model.entities.Tags;
import br.com.smarteditorial.service.TagsService;

public class TagsBO {
	
	private static final Logger logger = LoggerFactory.getLogger(TagsBO.class);
	
	protected TagsService tagsService;
	@Autowired(required=true)
	@Qualifier(value="tagsService")
	public void setTagsService(TagsService ts){
		this.tagsService = ts;
	}
	private Tags tg;
	
	public List<Tags> pegaTags() {
		try {
			return this.tagsService.listarTags();
		} catch (SQLException e) {
			logger.error("Falha na listagem das Tags"+e);
			e.printStackTrace();
			return null;
		}
	}
	
	public Tags pegaTagsId(Integer id) {
		try {
			return this.tagsService.tagsPorId(id);
		} catch (SQLException e) {
			logger.error("Falha ao carregar a Tags por Id"+e);
			e.printStackTrace();
			return null;
		}
	} 
	
	public Tags pegaTagsTitulo(String titulo) {
		try {
			return this.tagsService.tagsPorTitulo(titulo);
		} catch (SQLException e) {
			logger.error("Falha ao carregar a Tag por Titulo"+e);
			e.printStackTrace();
			return null;
		}
	}
	
	public void salvaTag(Tags t) {
		try {
			this.tagsService.salvarTags(t);
		} catch (ConstraintViolationException e1) {
			logger.error("Falha ao salvar Tags"+e1);
			e1.printStackTrace();
		} catch (DataIntegrityViolationException e1) {
			logger.error("Falha ao salvar Tags"+e1);
			e1.printStackTrace();
		} catch (SQLException e1) {
			logger.error("Falha ao salvar Tags"+e1);
			e1.printStackTrace();
		}
	}
	
	public void atualizaTag(Tags t) {
		try {
			this.tagsService.atualizarTag(t);
		} catch (ConstraintViolationException e1) {
			logger.error("Falha ao atualizar Tag"+e1);
			e1.printStackTrace();
		} catch (DataIntegrityViolationException e1) {
			logger.error("Falha ao atualizar Tag"+e1);
			e1.printStackTrace();
		} catch (SQLException e1) {
			logger.error("Falha ao atualizar Tag"+e1);
			e1.printStackTrace();
		}
	}
	
	public void mergeTag(Tags t) {
		try {
			this.tagsService.mergeTag(t);
		} catch (ConstraintViolationException e1) {
			logger.error("Falha ao fazer MERGE com Tag"+e1);
			e1.printStackTrace();
		} catch (DataIntegrityViolationException e1) {
			logger.error("Falha ao fazer MERGE com Tag"+e1);
			e1.printStackTrace();
		} catch (SQLException e1) {
			logger.error("Falha ao fazer MERGE com Tag"+e1);
			e1.printStackTrace();
		}
	}
	
	public void excluirTag(Tags t) {
		try {
			this.tagsService.excluirTag(t);
		} catch (SQLException e1) {
			logger.error("Falha ao remover Tags"+e1);
			e1.printStackTrace();
		}
	}
	
	public Set<Tags> splitTags(String t) {
		String[] args = t.split(",");
		Set<String> set = new HashSet<String>(Arrays.asList(args));
		Set<Tags> tags = new HashSet<Tags>();
		for (String s : set) {
			tg = new Tags();
			tg = this.pegaTagsTitulo(s);
			if (tg == null) {
				System.out.println(s);
				tg = new Tags();
				tg.setTags(s);
				this.salvaTag(tg);
			}
			tg = this.pegaTagsTitulo(s);
			tags.add(tg);
		}
		return tags;
	}	

}
