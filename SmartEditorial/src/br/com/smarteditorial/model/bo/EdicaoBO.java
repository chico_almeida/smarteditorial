package br.com.smarteditorial.model.bo;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

import br.com.smarteditorial.model.entities.Edicao;
import br.com.smarteditorial.service.EdicaoService;

public class EdicaoBO {
	private static final Logger logger = LoggerFactory.getLogger(EdicaoBO.class);
	
	protected EdicaoService edicaoService;
	@Autowired(required=true)
	@Qualifier(value="edicaoService")
	public void setEdicaoService(EdicaoService es){
		this.edicaoService = es;
	}
	@Autowired
	Edicao edicao;
	
	public List<Edicao> pegaEdicoes() {
		try {
			return this.edicaoService.listarEdicoes();
		} catch (SQLException e) {
			logger.error("Falha na listagem das Edi�oes"+e);
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Edicao> pegaEdicaoPorPublicacao(Integer id) {
		try {
			return this.edicaoService.edicaoPorPublicacao(id);
		} catch (SQLException e) {
			logger.error("Falha na listagem das Edi�oes pelo ID da Publicacao"+e);
			e.printStackTrace();
			return null;
		}
	}
	
	public Edicao pegaEdicaoId(Integer id) {
		try {
			return this.edicaoService.edicaoPorId(id);
		} catch (SQLException e) {
			logger.error("Falha ao carregar a Edicao por Id"+e);
			e.printStackTrace();
			return null;
		}
	} 
	
	public Edicao pegaEdicaoTitulo(String titulo) {
		try {
			return this.edicaoService.edicaoPorTitulo(titulo);
		} catch (SQLException e) {
			logger.error("Falha ao carregar a Edicao por Titulo"+e);
			e.printStackTrace();
			return null;
		}
	}
	
	public void salvaEdicao(Edicao e) {
		try {
			this.edicaoService.salvarEdicao(e);
		} catch (ConstraintViolationException e1) {
			logger.error("Falha ao salvar Edicao"+e1);
			e1.printStackTrace();
		} catch (DataIntegrityViolationException e1) {
			logger.error("Falha ao salvar Edicao"+e1);
			e1.printStackTrace();
		} catch (SQLException e1) {
			logger.error("Falha ao salvar Edicao"+e1);
			e1.printStackTrace();
		}
	}
	
	public void atualizaEdicao(Edicao e) {
		try {
			this.edicaoService.atualizarEdicao(e);
		} catch (ConstraintViolationException e1) {
			logger.error("Falha ao atualizar Edicao"+e1);
			e1.printStackTrace();
		} catch (DataIntegrityViolationException e1) {
			logger.error("Falha ao atualizar Edicao"+e1);
			e1.printStackTrace();
		} catch (SQLException e1) {
			logger.error("Falha ao atualizar Edicao"+e1);
			e1.printStackTrace();
		}
	}
	
	public void mergeEdicao(Edicao e) {
		try {
			this.edicaoService.mergeEdicao(e);
		} catch (ConstraintViolationException e1) {
			logger.error("Falha ao fazer MERGE com Edicao"+e1);
			e1.printStackTrace();
		} catch (DataIntegrityViolationException e1) {
			logger.error("Falha ao fazer MERGE com Edicao"+e1);
			e1.printStackTrace();
		} catch (SQLException e1) {
			logger.error("Falha ao fazer MERGE com Edicao"+e1);
			e1.printStackTrace();
		}
	}
	
	public void excluirEdicao(Edicao e) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException {
		try {
			this.edicaoService.excluirEdicao(e);
		} catch (DataIntegrityViolationException
				| ConstraintViolationException e1){
			e1.printStackTrace();
			throw e1;
		} catch (SQLException e1) {
			e1.printStackTrace();
			throw e1;
		}
	}

}
