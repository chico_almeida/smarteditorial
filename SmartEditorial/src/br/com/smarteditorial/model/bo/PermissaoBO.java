package br.com.smarteditorial.model.bo;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.smarteditorial.model.entities.Permissao;
import br.com.smarteditorial.service.PermissaoService;

public class PermissaoBO {
	
private static final Logger logger = LoggerFactory.getLogger(PermissaoBO.class);
	
	protected PermissaoService permissaoService;
	@Autowired(required=true)
	@Qualifier(value="permissaoService")
	public void setPermissaoService(PermissaoService gs){
		this.permissaoService = gs;
	}
	
	@Autowired
	Permissao permissao;
	
	public void salvarPermissao(Permissao p){
		try {
			this.permissaoService.salvarPermissao(p);
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| SQLException e) {
			logger.info("Falha ao salvar o Permissao" + e);
			e.printStackTrace();
		}
	}
	public void atualizarPermissao(Permissao p){
		try {
			this.permissaoService.atualizarPermissao(p);
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| SQLException e) {
			logger.info("Falha ao atualizar o Permissao" + e);
			e.printStackTrace();
		}
	}
	public void excluirPermissao(Permissao p){
		try {
			this.permissaoService.excluirPermissao(p);
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| SQLException e) {
			logger.info("Falha ao excluir o Permissao" + e);
			e.printStackTrace();
		}
	}
	public List<Permissao> pegaPermissoes() {
		try {
			return this.permissaoService.listarPermissao();
		} catch (SQLException e) {
			logger.info("Falha ao carregar a lista de Permissoes" + e);
			e.printStackTrace();
			return null;
		}
	}
	public Permissao pegaPermissaoPorId(Integer id) {
		try {
			return this.permissaoService.permissaoPorId(id);
		} catch (SQLException e) {
			logger.info("Falha ao carregar o Permissao por ID" + e);
			e.printStackTrace();
			return null;
		}
	}
	public Permissao pegaPermissaoPorNome(String tipo) {
		try {
			return this.permissaoService.permissaoPorTipo(tipo);
		} catch (SQLException e) {
			logger.info("Falha ao carregar o Permissao por Nome" + e);
			e.printStackTrace();
			return null;
		}
	}
}
