package br.com.smarteditorial.model.bo;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.smarteditorial.model.entities.Pagina;
import br.com.smarteditorial.model.entities.PaginaFluxo;
import br.com.smarteditorial.model.entities.Usuario;
import br.com.smarteditorial.service.PaginaService;

public class PaginaBO {

	private static final Logger logger = LoggerFactory.getLogger(PaginaBO.class);
	
	protected PaginaService paginaService;
	@Autowired(required=true)
	@Qualifier(value="paginaService")
	public void setPaginaService(PaginaService ps){
		this.paginaService = ps;
	}
	
	public void salvarPagina(Pagina p) {
		try {
			this.paginaService.salvarPagina(p);
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| SQLException e) {
			logger.error("Erro ao tentar salvar a pagina:: "+e.getCause());
			e.printStackTrace();
		}
	}

	public void atualizarPagina(Pagina p) {
		try {
			this.paginaService.atualizarPagina(p);
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| SQLException e) {
			logger.error("Erro ao tentar atualizar a pagina:: "+e.getCause());
			e.printStackTrace();
		}
	}
	
	public void excluirPagina(Pagina p) throws DataIntegrityViolationException, ConstraintViolationException, SQLException {
		try {
			this.paginaService.excluirPagina(p);
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| SQLException e) {
			logger.error("Erro ao tentar excluir a pagina:: "+e.getCause());
			e.printStackTrace();
			throw e;
		}
	}
	
	public Pagina pegaPaginaPorId(Integer i) {
		try {
			return this.paginaService.paginaPorId(i);
		} catch (SQLException e) {
			e.printStackTrace();
			logger.error("Erro ao tentar carregar a pagina:: "+e.getCause());
			return null;
		}
	}
	
	public List<PaginaFluxo> pegaPaginasDoUsuarioEmFluxo(Usuario u) {
		return this.paginaService.listarPaginasDoUsuarioEmFluxo(u.getIdUsuario());
	}

	public  List<Pagina> pegaPaginasFinalizadas(Usuario u) {
		return this.paginaService.listarPaginasDoUsuarioFinalizadas(u.getIdUsuario());
	}

	public List<Pagina> pegaPaginasDoUsuarioForaDoFluxo(Usuario u) {
		try {
			return this.paginaService.listarPaginasPorUsuarioForaDoFluxo(u.getIdUsuario());
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public List<PaginaFluxo> pegaPaginasDoGrupoAtualNoFluxo() {
		return this.paginaService.paginasDoGrupoAtualNoFluxo();
	}
	
	public List<Pagina> pegaPaginasFinalidasDaEdicao(Integer idEdicao) {
		return this.paginaService.listarPaginasFinalizadasDaEdicao(idEdicao);
	}
	
}
