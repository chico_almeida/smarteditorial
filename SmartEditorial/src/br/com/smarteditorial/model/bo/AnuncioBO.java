package br.com.smarteditorial.model.bo;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.smarteditorial.model.entities.Anuncio;
import br.com.smarteditorial.model.entities.AnuncioFluxo;
import br.com.smarteditorial.model.entities.Publicacao;
import br.com.smarteditorial.model.entities.Usuario;
import br.com.smarteditorial.service.AnuncioService;

public class AnuncioBO {
	
	private static final Logger logger = LoggerFactory.getLogger(Publicacao.class);
	
	protected AnuncioService anuncioService;
	@Autowired(required=true)
	@Qualifier(value="anuncioService")
	public void setAnuncioService(AnuncioService as){
		this.anuncioService = as;
	}
	
	public void salvarAnuncio(Anuncio a) {
		try {
			this.anuncioService.salvarAnuncio(a);
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| SQLException e) {
			logger.info("Erro ao tentar salvar o anuncio :: "+e.getCause());
			e.printStackTrace();
		}
	}
	
	public void removerAnuncio(Anuncio a) throws DataIntegrityViolationException, ConstraintViolationException, SQLException {
		this.anuncioService.excluirAnuncio(a);
	}
	public List<Anuncio> pegaAnuncios() {
		try {
			return this.anuncioService.listarAnuncios();
		} catch (SQLException e) {
			logger.error("Falha ao carregar a Lista de Anuncios"+e);
			e.printStackTrace();
			return null;
		}
	}
	public List<Anuncio> pegaAnunciosDoUsuario(Integer idUsuario) {
		try {
			return this.anuncioService.listarAnunciosPorUsuario(idUsuario);
		} catch (SQLException e) {
			logger.error("Falha ao carregar a Lista de Anuncios do Usuario"+e);
			e.printStackTrace();
			return null;

		}
	}
	
	public Anuncio  pegaAnuncioPorID(Integer i) {
		try {
			return this.anuncioService.anuncioPorId(i);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public void atualizar(Anuncio a) {
		try {
			this.anuncioService.atualizarAnuncio(a);
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| SQLException e) {
			e.printStackTrace();
		}
	}
	
	public List<Anuncio> pegaAnunciosAguardando(Usuario u) {
		return this.anuncioService.listarAnunciosDoUsuarioAguardando(u.getIdUsuario());
	}

	public List<Anuncio> pegaAnunciosAprovadas(Usuario u) {
		return this.anuncioService.listarAnunciosDoUsuarioAprovado(u.getIdUsuario());
	}

	public  List<Anuncio> pegaAnunciosFinalizadas(Usuario u) {
		return this.anuncioService.listarAnunciosDoUsuarioFinalizadas(u.getIdUsuario());
	}
	
	public List<AnuncioFluxo> pegaAnunciosDoUsuarioEmFluxo(Usuario u) {
		return this.anuncioService.listarAnunciosDoUsuarioEmFluxo(u.getIdUsuario());
	}

	public List<Anuncio> pegaAnunciosDoUsuarioForaDoFluxo(Usuario u) {
		try {
			return this.anuncioService.listarAnunciosPorUsuarioForaDoFluxo(u.getIdUsuario());
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public List<AnuncioFluxo> pegaAnunciosDoGrupoAtualNoFluxo() {
		return this.anuncioService.anunciosDoGrupoAtualNoFluxo();
	}
	
	public List<Anuncio> penaAnunciosFinalidoDaEdicao(Integer idEdicao) {
		return this.anuncioService.listarAnunciosFinalizadasDaEdicao(idEdicao);
	}
}
