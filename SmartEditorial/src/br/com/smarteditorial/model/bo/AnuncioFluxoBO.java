package br.com.smarteditorial.model.bo;

import java.sql.SQLException;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.smarteditorial.model.entities.AnuncioFluxo;
import br.com.smarteditorial.service.AnuncioFluxoService;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

public class AnuncioFluxoBO {
	
	protected AnuncioFluxoService anuncioFluxoService;
	@Autowired(required=true)
	@Qualifier(value="anuncioFluxoService")
	public void setAnuncioFluxoService(AnuncioFluxoService afs){
		this.anuncioFluxoService = afs;
	}
	
	
	public void salvar(AnuncioFluxo anunf) throws DataIntegrityViolationException, ConstraintViolationException, MySQLIntegrityConstraintViolationException, SQLException {
		this.anuncioFluxoService.salvar(anunf);
	}
	
	public void atualizar(AnuncioFluxo anunf) throws DataIntegrityViolationException, ConstraintViolationException, MySQLIntegrityConstraintViolationException, SQLException {
		this.anuncioFluxoService.atualizar(anunf);
	}
	
	public void excluir(AnuncioFluxo anunf) throws DataIntegrityViolationException, ConstraintViolationException, MySQLIntegrityConstraintViolationException, SQLException {
		this.anuncioFluxoService.excluir(anunf);
	}
	
	public void merge(AnuncioFluxo anunf) throws DataIntegrityViolationException, ConstraintViolationException, MySQLIntegrityConstraintViolationException, SQLException {
		this.anuncioFluxoService.merge(anunf);
	}
	
	public AnuncioFluxo pegaAnuncioFluxoPorIds(Integer idFluxo, Integer idAnuncio) {
		return this.anuncioFluxoService.anuncioFluxoPorIds(idFluxo, idAnuncio);
	}
}
