package br.com.smarteditorial.model.bo;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.smarteditorial.model.entities.FluxoDeTrabalho;
import br.com.smarteditorial.service.FluxoDeTrabalhoService;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

public class FluxoDeTrabalhoBO {
	private static final Logger logger = LoggerFactory.getLogger(FluxoDeTrabalhoBO.class);

	protected FluxoDeTrabalhoService fluxoDeTrabalhoService;
	@Autowired(required=true)
	@Qualifier(value="fluxoDeTrabalhoService")
	public void setFluxoDeTrabalhoService(FluxoDeTrabalhoService fs){
		this.fluxoDeTrabalhoService = fs;
	}

	public List<FluxoDeTrabalho> pegaListaDeFluxos() {
		try {
			return this.fluxoDeTrabalhoService.listarTrabalhos();
		} catch (SQLException e) {
			logger.error("Falha ao carregar a Lista de Trabalhos "+e);
			e.printStackTrace();
			return null;
		}
	}
	
	public List<FluxoDeTrabalho> pegaFluxosPorElemento(String elemento, Integer idPublicacao) {
		try {
			return this.fluxoDeTrabalhoService.listaFluxosPorElementoDaPublicacao(elemento, idPublicacao);
		} catch (SQLException e) {
			logger.error("Falha ao carregar a Lista de Trabalhos por Elemento: "+e);
			e.printStackTrace();
			return null;
		}
	}

	public FluxoDeTrabalho pegaFluxosDaPorId(Integer id) {
		try {
			return this.fluxoDeTrabalhoService.fluxoPorId(id);
		} catch (SQLException e) {
			logger.error("Falha ao carregar Fluxos da  por ID"+e);
			e.printStackTrace();
			return null;
		}
	}
	
	public void salvaFluxo(FluxoDeTrabalho f) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException {
		try {
			this.fluxoDeTrabalhoService.salvarFluxoDeTrabalho(f);
		} catch (ConstraintViolationException e) {
			logger.error("Falha ao salvar Fluxo "+e);
			throw e;
		} catch (DataIntegrityViolationException e) {
			logger.error("Falha ao salvar Fluxo "+e);
			throw e;
		} catch (SQLException e) {
			logger.error("Falha ao salvar Fluxo "+e);
			throw e;
		}
	}

	public void atualizarFluxo (FluxoDeTrabalho f){
		try {
			this.fluxoDeTrabalhoService.atualizarFluxoDeTrabalho(f);
		} catch (ConstraintViolationException e) {
			logger.error("Falha ao atualizar Fluxo "+e);
			e.printStackTrace();
		} catch (DataIntegrityViolationException e) {
			logger.error("Falha ao atualizar Fluxo "+e);
			e.printStackTrace();
		} catch (SQLException e) {
			logger.error("Falha ao atualizar Fluxo "+e);
			e.printStackTrace();
		}
	}

	public void mergeFluxo(FluxoDeTrabalho f) {
		try {
			this.fluxoDeTrabalhoService.mergeFluxoDeTrabalho(f);
		} catch (ConstraintViolationException e) {
			logger.error("Falha ao fazer MERGE com Fluxo "+e);
			e.printStackTrace();
		} catch (DataIntegrityViolationException e) {
			logger.error("Falha ao fazer MERGE com Fluxo "+e);
			e.printStackTrace();
		} catch (SQLException e) {
			logger.error("Falha ao fazer MERGE com Fluxo "+e);
			e.printStackTrace();
		}
	}

	public void excluirFluxo(FluxoDeTrabalho f) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException {
		try {
			this.fluxoDeTrabalhoService.excluirFluxoDeTrabalho(f);
		} catch (DataIntegrityViolationException
				| ConstraintViolationException e) {
			e.printStackTrace();
			logger.error("Falha ao remover a Fluxo "+e);
			throw e;
		} catch (SQLException e) {
			e.printStackTrace();
			logger.error("Falha ao remover a Fluxo "+e);
			throw e;
		}
	}
	
	public FluxoDeTrabalho pegaFluxo(Integer ordem, Integer idPublicacao, String tipoDeElemento) {
		return this.fluxoDeTrabalhoService.pegaFluxo(ordem, idPublicacao, tipoDeElemento);
	}

}
