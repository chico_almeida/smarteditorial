package br.com.smarteditorial.model.bo;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.smarteditorial.model.entities.Artigo;
import br.com.smarteditorial.model.entities.ArtigoFluxo;
import br.com.smarteditorial.model.entities.Usuario;
import br.com.smarteditorial.service.ArtigoService;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

public class ArtigoBO {
	private static final Logger logger = LoggerFactory.getLogger(ArtigoBO.class);

	protected ArtigoService artigoService;
	@Autowired(required=true)
	@Qualifier(value="artigoService")
	public void setArtigoService(ArtigoService as){
		this.artigoService = as;
	}
	
	
	public List<Artigo> pegaArtigos() {
		try {
			return this.artigoService.listarArtigos();
		} catch (SQLException e) {
			logger.error("Falha ao carregar a Lista de Artigos"+e);
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Artigo> pegaArtigosDoUsuario(Integer idUsuario) {
		try {
			return this.artigoService.artigoPorUsuario(idUsuario);
		} catch (SQLException e) {
			logger.error("Falha ao carregar a Lista de Artigos do Usuario"+e);
			e.printStackTrace();
			return null;
		}
	}

	public Artigo pegaArtigoID(Integer id) {
		try {
			return this.artigoService.artigoPorId(id);
		} catch (SQLException e) {
			logger.error("Falha ao carregar Artigo por ID"+e);
			e.printStackTrace();
			return null;
		}
	}

	public Artigo pegaArtigoTitulo(String titulo) {
		try {
			return this.artigoService.artigoPorTitulo(titulo);
		} catch (SQLException e) {
			logger.error("Falha ao carregar Artigo por Titulo"+e);
			e.printStackTrace();
			return null;
		}
	}

	public void salvaArtigo(Artigo a) {
		try {
			this.artigoService.salvarArtigo(a);
		} catch (ConstraintViolationException e) {
			logger.error("Falha ao salvar Artigo"+e);
			e.printStackTrace();
		} catch (DataIntegrityViolationException e) {
			logger.error("Falha ao salvar Artigo"+e);
			e.printStackTrace();
		} catch (SQLException e) {
			logger.error("Falha ao salvar Artigo"+e);
			e.printStackTrace();
		}
	}
	
	public void atualizaArtigo(Artigo a) {
		try {
			this.artigoService.atualizarArtigo(a);
		} catch (ConstraintViolationException e) {
			logger.error("Falha ao atualizar Artigo"+e);
			e.printStackTrace();
		} catch (DataIntegrityViolationException e) {
			logger.error("Falha ao atualizar Artigo"+e);
			e.printStackTrace();
		} catch (SQLException e) {
			logger.error("Falha ao atualizar Artigo"+e);
			e.printStackTrace();
		}
	}
	
	public void mergeArtigo(Artigo a) {
		try {
			this.artigoService.mergeArtigo(a);
		} catch (ConstraintViolationException e) {
			logger.error("Falha ao fazer MERGE com Artigo"+e);
			e.printStackTrace();
		} catch (DataIntegrityViolationException e) {
			logger.error("Falha ao fazer MERGE com Artigo"+e);
			e.printStackTrace();
		} catch (SQLException e) {
			logger.error("Falha ao fazer MERGE com Artigo"+e);
			e.printStackTrace();
		}
	}
	
	public void excluirArtigo(Artigo a) throws DataIntegrityViolationException, ConstraintViolationException, MySQLIntegrityConstraintViolationException, SQLException {
			this.artigoService.excluirArtigo(a);
	}
	
	public List<Artigo> pegaArtigosAguardando(Usuario u) {
		return this.artigoService.listarArtigosDoUsuarioAguardando(u.getIdUsuario());
	}

	public List<Artigo> pegaArtigosAprovadas(Usuario u) {
		return this.artigoService.listarArtigosDoUsuarioAprovado(u.getIdUsuario());
	}

	public  List<Artigo> pegaArtigosFinalizadas(Usuario u) {
		return this.artigoService.listarArtigosDoUsuarioFinalizadas(u.getIdUsuario());
	}
	
	public List<ArtigoFluxo> pegaArtigosDoUsuarioEmFluxo(Usuario u) {
		return this.artigoService.listarArtigosDoUsuarioEmFluxo(u.getIdUsuario());
	}

	public List<Artigo> pegaArtigosDoUsuarioForaDoFluxo(Usuario u) {
		try {
			return this.artigoService.listarArtigosPorUsuarioForaDoFluxo(u.getIdUsuario());
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public List<ArtigoFluxo> pegaArtigosDoGrupoAtualNoFluxo() {
		return this.artigoService.artigosDoGrupoAtualNoFluxo();
	}
	
	public List<Artigo> pegaArtigosFinalidosDaEdicao(Integer idEdicao) {
		return this.artigoService.listarArtigosFinalizadasDaEdicao(idEdicao);
	}
	
}
