package br.com.smarteditorial.model.bo;

import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import br.com.smarteditorial.util.SystemConfiguration;

public class FileUploadBO {

	@Autowired
	SystemConfiguration systemConfiguration;

	@Autowired
	private ServletContext servletContext;

	private static final Logger logger = LoggerFactory.getLogger(FileUploadBO.class);

	public String imageUpload(MultipartFile file, String path, String titulo) {
		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();
				File dir = new File(systemConfiguration.getPathAcervo()+File.separator+path+File.separator);
				if (!dir.exists())
					dir.mkdirs();

				File serverFile = new File(dir.getAbsolutePath()
						+ File.separator + file.getOriginalFilename());
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();

				logger.info("Caminho do Arquivo no Servidor="
						+ serverFile.getAbsolutePath());				
				return path+"/"+file.getOriginalFilename();
			} catch (Exception e) {
				logger.info("Falha ao carregar o arquivo " + titulo + " => " + e.getCause());
				e.printStackTrace();
			}
		} else {
			logger.info("Falha ao carregar o arquivo" + titulo + " arquivo vazio.");
		}
		return null;
	}

	public double getWidth(MultipartFile file) {
		BufferedImage image;
		try {
			image = ImageIO.read(file.getInputStream());
			return image.getWidth();
		} catch (IOException e) {
			e.printStackTrace();
			return 0;
		}
	}

	public double getHeight(MultipartFile file) {
		BufferedImage image;
		try {
			image = ImageIO.read(file.getInputStream());
			return image.getHeight();
		} catch (IOException e) {
			e.printStackTrace();
			return 0;
		}
	}

	/*public void convertPdfToJpg(MultipartFile file, String path, String titulo) throws IOException {
		File pdfFile = new File(systemConfiguration.getPathAcervo()+File.separator+path+File.separator);
		@SuppressWarnings("resource")
		RandomAccessFile raf = new RandomAccessFile(pdfFile, "r");
		FileChannel channel = raf.getChannel();
		ByteBuffer buf = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());
		PDFFile pdf = new PDFFile(buf);
		PDFPage page = pdf.getPage(0);

		// create the image
		Rectangle rect = new Rectangle(0, 0, (int) page.getBBox().getWidth(),
				(int) page.getBBox().getHeight());
		BufferedImage bufferedImage = new BufferedImage(rect.width, rect.height,
				BufferedImage.TYPE_INT_RGB);

		Image image = page.getImage(rect.width, rect.height,    // width & height
				rect,                       // clip rect
				null,                       // null for the ImageObserver
				true,                       // fill background with white
				true                        // block until drawing is done
				);
		Graphics2D bufImageGraphics = bufferedImage.createGraphics();
		bufImageGraphics.drawImage(image, 0, 0, null);
		ImageIO.write(bufferedImage, "JPG", new File(systemConfiguration.getPathAcervo()+File.separator+path+File.separator+file.getOriginalFilename()+".jpg" ));
	}*/
}
