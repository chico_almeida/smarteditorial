package br.com.smarteditorial.model.bo;

import java.sql.SQLException;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.smarteditorial.model.entities.ArtigoFluxo;
import br.com.smarteditorial.service.ArtigoFluxoService;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

public class ArtigoFluxoBO {
	
	protected ArtigoFluxoService artigoFluxoService;
	@Autowired(required=true)
	@Qualifier(value="artigoFluxoService")
	public void setArtigoFluxoService(ArtigoFluxoService afs){
		this.artigoFluxoService = afs;
	}
	
	
	public void salvar(ArtigoFluxo artf) throws DataIntegrityViolationException, ConstraintViolationException, MySQLIntegrityConstraintViolationException, SQLException {
		this.artigoFluxoService.salvar(artf);
	}
	
	public void atualizar(ArtigoFluxo artf) throws DataIntegrityViolationException, ConstraintViolationException, MySQLIntegrityConstraintViolationException, SQLException {
		this.artigoFluxoService.atualizar(artf);
	}
	
	public void excluir(ArtigoFluxo artf) throws DataIntegrityViolationException, ConstraintViolationException, MySQLIntegrityConstraintViolationException, SQLException {
		this.artigoFluxoService.excluir(artf);
	}
	
	public void merge(ArtigoFluxo artf) throws DataIntegrityViolationException, ConstraintViolationException, MySQLIntegrityConstraintViolationException, SQLException {
		this.artigoFluxoService.merge(artf);
	}
	
	public ArtigoFluxo pegaArtigoFluxoPorIds(Integer idFluxo, Integer idArtigo) {
		return this.artigoFluxoService.artigoFluxoPorIds(idFluxo, idArtigo);
	}
}
