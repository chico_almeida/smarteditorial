package br.com.smarteditorial.model.bo;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.smarteditorial.model.entities.Imagem;
import br.com.smarteditorial.model.entities.ImagemFluxo;
import br.com.smarteditorial.model.entities.Usuario;
import br.com.smarteditorial.model.entities.UsuarioAlteraImagem;
import br.com.smarteditorial.service.ImagemService;
import br.com.smarteditorial.service.UsuarioAlteraImagemService;

public class ImagemBO {

	private static final Logger logger = LoggerFactory.getLogger(ImagemBO.class);

	protected ImagemService imagemService;
	@Autowired(required=true)
	@Qualifier(value="imagemService")
	public void setImagemService(ImagemService is){
		this.imagemService = is;
	}

	protected UsuarioAlteraImagemService usuarioAlteraImagemService;
	@Autowired(required=true)
	@Qualifier(value="usuarioAlteraImagemService")
	public void setUsuarioAlteraImagemService(UsuarioAlteraImagemService uais){
		this.usuarioAlteraImagemService = uais;
	}

	public void salvarImagem(Imagem i) {
		try {
			this.imagemService.salvarImagem(i);
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| SQLException e) {
			logger.info("Erro ao tentar salvar a imagem:: "+e.getCause());
			e.printStackTrace();
		}
	}

	public void atualizarImagem(Imagem i) {
		try {
			this.imagemService.atualizarImagem(i);
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| SQLException e) {
			logger.info("Erro ao tentar atualizar a imagem:: "+e.getCause());
			e.printStackTrace();
		}
	}
	
	public void excluirImagem(Imagem i) throws DataIntegrityViolationException, ConstraintViolationException, SQLException {
		try {
			this.imagemService.excluirImagem(i);
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| SQLException e) {
			logger.info("Erro ao tentar excluir a imagem:: "+e.getCause());
			e.printStackTrace();
			throw e;
		}
	}

	public List<Imagem> pegaImagem() {
		try {
			return this.imagemService.listarImagem();
		} catch (SQLException e) {
			logger.error("Falha ao carregar a Lista de Imagens"+e);
			e.printStackTrace();
			return null;
		}
	}

	public List<Imagem> pegaImagensDoUsuario(Integer idUsuario) {
		try {
			return this.imagemService.listarImagensPorUsuario(idUsuario);
		} catch (SQLException e) {
			logger.error("Falha ao carregar a Lista de Imagens do Usuario"+e);
			e.printStackTrace();
			return null;
		}
	}

	public Imagem pegaImagemId(Integer id) {
		try {
			return this.imagemService.imagemPorId(id);
		} catch (SQLException e) {
			logger.error("Falha ao carregar a Imagem por ID"+e);
			e.printStackTrace();
			return null;
		}
	}

	public void alterarImagemFluxo(UsuarioAlteraImagem uai) throws DataIntegrityViolationException, ConstraintViolationException, SQLException {
		try {
			this.usuarioAlteraImagemService.alteracaoPorImagem(uai.getImagem());
			try {
				this.usuarioAlteraImagemService.atualizar(uai);
			} catch (DataIntegrityViolationException | ConstraintViolationException
					| SQLException e) {
				e.printStackTrace();
			}
		} catch (DataIntegrityViolationException | ConstraintViolationException e) {
			try {
				this.usuarioAlteraImagemService.salvar(uai);
			} catch (DataIntegrityViolationException | ConstraintViolationException
					| SQLException ei) {
				e.printStackTrace();
				throw ei;
			}
		}
	}

	public List<ImagemFluxo> pegaImagensDoUsuarioEmFluxo(Usuario u) {
		return this.imagemService.listarImagensDoUsuarioEmFluxo(u.getIdUsuario());
	}

	public  List<Imagem> pegaImagensFinalizadas(Usuario u) {
		return this.imagemService.listarImagensDoUsuarioFinalizadas(u.getIdUsuario());
	}

	public List<Imagem> pegaImagensDoUsuarioForaDoFluxo(Usuario u) {
		try {
			return this.imagemService.listarImagensPorUsuarioForaDoFluxo(u.getIdUsuario());
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public List<ImagemFluxo> pegaImagensDoGrupoAtualNoFluxo() {
		return this.imagemService.imagensDoGrupoAtualNoFluxo();
	}
	
	public List<Imagem> pegaImagensFinalidasDaEdicao(Integer idEdicao) {
		return this.imagemService.listarImagensFinalizadasDaEdicao(idEdicao);
	}
}
