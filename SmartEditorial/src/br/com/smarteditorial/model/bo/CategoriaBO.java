package br.com.smarteditorial.model.bo;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.smarteditorial.model.entities.Categoria;
import br.com.smarteditorial.service.CategoriaService;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

public class CategoriaBO {

	private static final Logger logger = LoggerFactory.getLogger(CategoriaBO.class);

	protected CategoriaService categoriaService;
	@Autowired(required=true)
	@Qualifier(value="categoriaService")
	public void setCategoriaService(CategoriaService cs){
		this.categoriaService = cs;
	}

	public List<Categoria> pegaCategorias() {
		try {
			return this.categoriaService.listarCategorias();
		} catch (SQLException e) {
			logger.error("Falha ao carregar a Lista de Categorias"+e);
			e.printStackTrace();
			return null;
		}
	}

	public Categoria pegaCategoriaPorId(Integer id) {
		try {
			return this.categoriaService.categoriaPorId(id);
		} catch (SQLException e) {
			logger.error("Falha ao carregar Categoria por ID"+e);
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Categoria> pegaCategoriaPorPublicacao(Integer id) {
		try {
			return this.categoriaService.categoriaPorPublicacao(id);
		} catch (SQLException e) {
			logger.error("Falha na listagem das Categorias pelo ID da Publicacao"+e);
			e.printStackTrace();
			return null;
		}
	}

	public Categoria pegaCategoriaTitulo(String titulo) {
		try {
			return this.categoriaService.categoriaPorTitulo(titulo);
		} catch (SQLException e) {
			logger.error("Falha ao carregar Categoria por Titulo"+e);
			e.printStackTrace();
			return null;
		}
	}

	public void salvaCategoria(Categoria c) {
		try {
			this.categoriaService.salvarCategoria(c);
		} catch (ConstraintViolationException e) {
			logger.error("Falha ao salvar Categoria"+e);
			e.printStackTrace();
		} catch (DataIntegrityViolationException e) {
			logger.error("Falha ao salvar Categoria"+e);
			e.printStackTrace();
		} catch (SQLException e) {
			logger.error("Falha ao salvar Categoria"+e);
			e.printStackTrace();
		}
	}

	public void atualizaCategoria(Categoria c) {
		try {
			this.categoriaService.atualizarCategoria(c);
		} catch (ConstraintViolationException e) {
			logger.error("Falha ao atualizar Categoria"+e);
			e.printStackTrace();
		} catch (DataIntegrityViolationException e) {
			logger.error("Falha ao atualizar Categoria"+e);
			e.printStackTrace();
		} catch (SQLException e) {
			logger.error("Falha ao atualizar Categoria"+e);
			e.printStackTrace();
		}
	}

	public void mergeCategoria(Categoria c) {
		try {
			this.categoriaService.mergeCategoria(c);
		} catch (ConstraintViolationException e) {
			logger.error("Falha ao fazer MERGE com Categoria"+e);
			e.printStackTrace();
		} catch (DataIntegrityViolationException e) {
			logger.error("Falha ao fazer MERGE com Categoria"+e);
			e.printStackTrace();
		} catch (SQLException e) {
			logger.error("Falha ao fazer MERGE com Categoria"+e);
			e.printStackTrace();
		}
	}

	public void excluirCategoria(Categoria c) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException {
		try {
			this.categoriaService.excluirCategoria(c);
		} catch (DataIntegrityViolationException
				| ConstraintViolationException e) {
			e.printStackTrace();
			logger.error("Falha ao remover a Categoria"+e);
			throw e;
		} catch (SQLException e) {
			e.printStackTrace();
			logger.error("Falha ao remover a Categoria"+e);
			throw e;
		}
	}


}
