package br.com.smarteditorial.model.bo;

import java.sql.SQLException;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

import br.com.smarteditorial.model.entities.ImagemFluxo;
import br.com.smarteditorial.service.ImagemFluxoService;

public class ImagemFluxoBO {
	
	protected ImagemFluxoService imagemFluxoService;
	@Autowired(required=true)
	@Qualifier(value="imagemFluxoService")
	public void setImagemFluxoService(ImagemFluxoService ifs){
		this.imagemFluxoService = ifs;
	}
	
	
	public void salvar(ImagemFluxo imgF) throws DataIntegrityViolationException, ConstraintViolationException, MySQLIntegrityConstraintViolationException, SQLException {
		this.imagemFluxoService.salvar(imgF);
	}
	
	public void atualizar(ImagemFluxo imgF) throws DataIntegrityViolationException, ConstraintViolationException, MySQLIntegrityConstraintViolationException, SQLException {
		this.imagemFluxoService.atualizar(imgF);
	}
	
	public void excluir(ImagemFluxo imgF) throws DataIntegrityViolationException, ConstraintViolationException, MySQLIntegrityConstraintViolationException, SQLException {
		this.imagemFluxoService.excluir(imgF);
	}
	
	public void merge(ImagemFluxo imgF) throws DataIntegrityViolationException, ConstraintViolationException, MySQLIntegrityConstraintViolationException, SQLException {
		this.imagemFluxoService.merge(imgF);
	}
	
	public ImagemFluxo pegaImagemFluxoPorIds(Integer idFluxo, Integer idImagem) {
		return this.imagemFluxoService.imagemFluxoPorIds(idFluxo, idImagem);
	}
}
