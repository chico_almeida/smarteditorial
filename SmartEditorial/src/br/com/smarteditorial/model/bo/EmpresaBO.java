package br.com.smarteditorial.model.bo;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.smarteditorial.model.entities.Empresa;
import br.com.smarteditorial.service.EmpresaService;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

public class EmpresaBO {
	
private static final Logger logger = LoggerFactory.getLogger(EmpresaBO.class);
	
	protected EmpresaService empresaService;
	@Autowired(required=true)
	@Qualifier(value="empresaService")
	public void setEmpresaService(EmpresaService es){
		this.empresaService = es;
	}
	@Autowired
	Empresa empresa;
	
	public void salvar(Empresa e) throws DataIntegrityViolationException, ConstraintViolationException, SQLException, MySQLIntegrityConstraintViolationException {
		try {
			this.empresaService.salvarEmpresa(e);
			logger.info("Empresa Salva com sucesso");
		} catch (DataIntegrityViolationException e1) {
			logger.error("Falha ao salvar a empresa. Erro ::" + e1.getMessage());
			e1.printStackTrace();
			throw e1; 
		} catch (ConstraintViolationException e1) {
			logger.error("Falha ao salvar a empresa. Erro ::" + e1.getMessage());
			e1.printStackTrace();
			throw e1; 
		} catch (MySQLIntegrityConstraintViolationException e1) {
			logger.error("Falha ao salvar a empresa. Erro ::" + e1.getMessage());
			e1.printStackTrace();
			throw e1; 
		} catch (SQLException e1) {
			logger.error("Falha ao salvar a empresa. Erro ::" + e1.getMessage());
			e1.printStackTrace();
			throw e1; 
		}
	}
	
	public void atualizar(Empresa e) {
		try {
			this.empresaService.atualizarEmpresa(e);
			logger.info("Empresa atualizada com sucesso");
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| SQLException e1) {
			logger.error("Falha ao atualizar a empresa. Erro ::" + e1.getMessage());
			e1.printStackTrace();
		}
	}
	
	public void merge(Empresa e) {
		try {
			this.empresaService.mergeEmpresa(e);
			logger.info("Merge da emrpesa efetuado com sucesso");
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| SQLException e1) {
			logger.error("Falhar ao fazer o Merge da Empresa. Erro ::" + e1.getMessage());
			e1.printStackTrace();
		}
	}
	
	public void excluir(Empresa e) {
		try {
			this.empresaService.excluirEmpresa(e);
			logger.info("Empresa Removida com Sucesso");
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| SQLException e1) {
			logger.error("Falhar ao exluir a empresa. Erro ::" + e1.getMessage());
			e1.printStackTrace();
		}
	}
	
	public List<Empresa> empresas() {
		try {
			return this.empresaService.listarEmpresas();
		} catch (SQLException e) {
			logger.error("Erro ao tentar listar as empresas. Erro ::" + e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public Empresa empresaPorContato(String contato) {
		try {
			return this.empresaService.empresaPorContato(contato);
		} catch (SQLException e) {
			logger.error("Falha ao buscar a empresa por Contato. Erro ::" + e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public Empresa empresaPorId(Integer id) {
		try {
			return this.empresaService.empresaPorId(id);
		} catch (SQLException e) {
			logger.error("Erro na busca do contato por ID. Erro ::" + e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public Empresa empresaPorNome(String nome) {
		try {
			return this.empresaService.empresaPorNomeEmpresa(nome);
		} catch (SQLException e) {
			logger.info("Erro na busca da empresa por nome. Erro ::" + e.getMessage());
			e.printStackTrace();
			return  null;
		}
	}
}
