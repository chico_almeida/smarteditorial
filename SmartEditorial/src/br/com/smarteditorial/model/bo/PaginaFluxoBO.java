package br.com.smarteditorial.model.bo;

import java.sql.SQLException;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.smarteditorial.model.entities.PaginaFluxo;
import br.com.smarteditorial.service.PaginaFluxoService;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

public class PaginaFluxoBO {
	
	protected PaginaFluxoService paginaFluxoService;
	@Autowired(required=true)
	@Qualifier(value="paginaFluxoService")
	public void setPaginaFluxoService(PaginaFluxoService pfs){
		this.paginaFluxoService = pfs;
	}
	
	
	public void salvar(PaginaFluxo pagf) throws DataIntegrityViolationException, ConstraintViolationException, MySQLIntegrityConstraintViolationException, SQLException {
		this.paginaFluxoService.salvar(pagf);
	}
	
	public void atualizar(PaginaFluxo pagf) throws DataIntegrityViolationException, ConstraintViolationException, MySQLIntegrityConstraintViolationException, SQLException {
		this.paginaFluxoService.atualizar(pagf);
	}
	
	public void excluir(PaginaFluxo pagf) throws DataIntegrityViolationException, ConstraintViolationException, MySQLIntegrityConstraintViolationException, SQLException {
		this.paginaFluxoService.excluir(pagf);
	}
	
	public void merge(PaginaFluxo pagf) throws DataIntegrityViolationException, ConstraintViolationException, MySQLIntegrityConstraintViolationException, SQLException {
		this.paginaFluxoService.merge(pagf);
	}
	
	public PaginaFluxo pegaPaginaFluxoPorIds(Integer idFluxo, Integer idPagina) {
		return this.paginaFluxoService.paginaFluxoPorIds(idFluxo, idPagina);
	}
}
