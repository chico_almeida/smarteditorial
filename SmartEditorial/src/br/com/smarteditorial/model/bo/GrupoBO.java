package br.com.smarteditorial.model.bo;

import java.sql.SQLException;
import java.util.List;
import java.util.Set;

import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.smarteditorial.model.entities.Grupo;
import br.com.smarteditorial.model.entities.Permissao;
import br.com.smarteditorial.model.entities.Usuario;
import br.com.smarteditorial.service.GrupoService;

public class GrupoBO {
	
private static final Logger logger = LoggerFactory.getLogger(GrupoBO.class);
	
	protected GrupoService grupoService;
	@Autowired(required=true)
	@Qualifier(value="grupoService")
	public void setGrupoService(GrupoService gs){
		this.grupoService = gs;
	}
	
	@Autowired
	Grupo grupo;
	
	public void salvarGrupo(Grupo g){
		try {
			this.grupoService.salvarGrupo(g);
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| SQLException e) {
			logger.info("Falha ao salvar o Grupo" + e);
			e.printStackTrace();
		}
	}
	public void atualizarGrupo(Grupo g){
		try {
			g.setPermissoes(this.pegaGrupoPorId(g.getIdGrupo()).getPermissoes());
			g.setUsuarios(this.pegaGrupoPorId(g.getIdGrupo()).getUsuarios());
			this.grupoService.atualizarGrupo(g);
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| SQLException e) {
			logger.info("Falha ao atualizar o Grupo" + e);
			e.printStackTrace();
		}
	}
	public void excluirGrupo(Grupo g){
		try {
			Set<Permissao> ps = g.getPermissoes();
			Set<Usuario> us = g.getUsuarios();
			for (Permissao p : ps) {
				this.removerPermissaoDoGrupo(g.getIdGrupo(), p.getIdPermissao());
			}
			for (Usuario u : us) {
				this.removerUsuarioDoGrupo(g.getIdGrupo(), u.getIdUsuario());
			}
			
			this.grupoService.excluirGrupo(g);
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| SQLException e) {
			logger.info("Falha ao excluir o Grupo" + e);
			e.printStackTrace();
		}
	}
	public List<Grupo> pegaGrupos() {
		try {
			return this.grupoService.listarGrupos();
		} catch (SQLException e) {
			logger.info("Falha ao carregar a lista de Grupos" + e);
			e.printStackTrace();
			return null;
		}
	}
	public Grupo pegaGrupoPorId(Integer id) {
		try {
			return this.grupoService.grupoPorId(id);
		} catch (SQLException e) {
			logger.info("Falha ao carregar o Grupo por ID" + e);
			e.printStackTrace();
			return null;
		}
	}
	public Grupo pegaGrupoPorNome(String nome) {
		try {
			return this.grupoService.grupoPorNome(nome);
		} catch (SQLException e) {
			logger.info("Falha ao carregar o Grupo por Nome" + e);
			e.printStackTrace();
			return null;
		}
	}
	public Boolean vincularUsuario(Grupo g, Usuario u) {
		try {
			Set<Usuario> usuarios = g.getUsuarios();
			usuarios.add(u);
			this.grupoService.atualizarGrupo(g);
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			logger.info("Falha ao ::"+e);
		} catch (Throwable e) {
			e.printStackTrace();
			logger.info("Erro ao atualizar o usuario:: " + e);
		}
		return false;
	}
	
	public Boolean vincularPermissao(Grupo g, Permissao p) {
		try {
			Set<Permissao> permissoes = g.getPermissoes();
			permissoes.add(p);
			this.grupoService.atualizarGrupo(g);
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			logger.info("Falha ao inserir permissao ao grupo::"+e);
		} catch (Throwable e) {
			e.printStackTrace();
			logger.info("Falha ao inserir permissao ao grupo:: " + e);
		}
		return false;
	}
	public Boolean removerPermissaoDoGrupo(Integer idG, Integer idP) {
		try {
			this.grupoService.desvicularPermissao(idG, idP);
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			logger.info("Erro ao atualizar o usuario::"+e);
		} catch (Throwable e) {
			e.printStackTrace();
			logger.info("Erro ao atualizar o usuario:: " + e);
		}
		return false;
	}

	public Boolean removerUsuarioDoGrupo(Integer idG, Integer idU) {
		try {
			this.grupoService.desvicularUsuario(idG, idU);
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			logger.info("Erro ao atualizar o usuario::"+e);
		} catch (Throwable e) {
			e.printStackTrace();
			logger.info("Erro ao atualizar o usuario:: " + e);
		}
		return false;
	}
}
