package br.com.smarteditorial.model.bo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import br.com.smarteditorial.service.EdicaoService;
import br.com.smarteditorial.service.GrupoService;
import br.com.smarteditorial.service.PermissaoService;
import br.com.smarteditorial.service.PublicacaoService;
import br.com.smarteditorial.service.UsuarioService;

public abstract class AbstractService {
	
	protected GrupoService grupoService;
	@Autowired(required=true)
	@Qualifier(value="grupoService")
	public void setGrupoService(GrupoService gs){
		this.grupoService = gs;
	}
	protected UsuarioService usuarioService;
	@Autowired(required=true)
	@Qualifier(value="usuarioService")
	public void setUsuarioService(UsuarioService us){
		this.usuarioService = us;
	}
	protected PermissaoService permissaoService;
	@Autowired(required=true)
	@Qualifier(value="permissaoService")
	public void setPermissaoService(PermissaoService ps){
		this.permissaoService = ps;
	}
	protected PublicacaoService publicacaoService;
	@Autowired(required=true)
	@Qualifier(value="publicacaoService")
	public void setPublicacaoService(PublicacaoService ps){
		this.publicacaoService = ps;
	}
	protected EdicaoService edicaoService;
	@Autowired(required=true)
	@Qualifier(value="edicaoService")
	public void setEdicaoService(EdicaoService es){
		this.edicaoService = es;
	}
}
