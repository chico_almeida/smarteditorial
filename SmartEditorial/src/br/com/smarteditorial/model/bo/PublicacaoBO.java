package br.com.smarteditorial.model.bo;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

import br.com.smarteditorial.model.entities.Publicacao;
import br.com.smarteditorial.service.PublicacaoService;

public class PublicacaoBO {

	private static final Logger logger = LoggerFactory.getLogger(Publicacao.class);

	protected PublicacaoService publicacaoService;
	@Autowired(required=true)
	@Qualifier(value="publicacaoService")
	public void setPublicacaoService(PublicacaoService ps){
		this.publicacaoService = ps;
	}
	
	
	public List<Publicacao> pegaPublicacoes() {
		try {
			return this.publicacaoService.listarPublicacoes();
		} catch (SQLException e) {
			logger.error("Falha ao carregar a Lista de Publicacoes"+e);
			e.printStackTrace();
			return null;
		}
	}

	public Publicacao pegaPublicacaoID(Integer id) {
		try {
			return this.publicacaoService.publicacaoPorId(id);
		} catch (SQLException e) {
			logger.error("Falha ao carregar Publicacao por ID"+e);
			e.printStackTrace();
			return null;
		}
	}

	public Publicacao pegaPublicacaoTitulo(String titulo) {
		try {
			return this.publicacaoService.publicacaoPorTitulo(titulo);
		} catch (SQLException e) {
			logger.error("Falha ao carregar Publicacao por Titulo"+e);
			e.printStackTrace();
			return null;
		}
	}

	public void salvaPublicacao(Publicacao p) {
		System.out.println(p.getTitulo());
		try {
			this.publicacaoService.salvarPublicacao(p);
		} catch (ConstraintViolationException e) {
			logger.error("Falha ao salvar Publicacao"+e);
			e.printStackTrace();
		} catch (DataIntegrityViolationException e) {
			logger.error("Falha ao salvar Publicacao"+e);
			e.printStackTrace();
		} catch (SQLException e) {
			logger.error("Falha ao salvar Publicacao"+e);
			e.printStackTrace();
		}
	}
	
	public void atualizaPublicacao(Publicacao p) {
		try {
			this.publicacaoService.atualizarPublicacao(p);
		} catch (ConstraintViolationException e) {
			logger.error("Falha ao atualizar Publicacao"+e);
			e.printStackTrace();
		} catch (DataIntegrityViolationException e) {
			logger.error("Falha ao atualizar Publicacao"+e);
			e.printStackTrace();
		} catch (SQLException e) {
			logger.error("Falha ao atualizar Publicacao"+e);
			e.printStackTrace();
		}
	}
	
	public void mergePublicacao(Publicacao p) {
		try {
			this.publicacaoService.mergePublicacao(p);
		} catch (ConstraintViolationException e) {
			logger.error("Falha ao fazer MERGE com Publicacao"+e);
			e.printStackTrace();
		} catch (DataIntegrityViolationException e) {
			logger.error("Falha ao fazer MERGE com Publicacao"+e);
			e.printStackTrace();
		} catch (SQLException e) {
			logger.error("Falha ao fazer MERGE com Publicacao"+e);
			e.printStackTrace();
		}
	}
	
	public void excluirPublicacao(Publicacao p)  throws DataIntegrityViolationException, ConstraintViolationException, MySQLIntegrityConstraintViolationException, SQLException {
			try {
				this.publicacaoService.excluirPublicacao(p);
			} catch (DataIntegrityViolationException
					| ConstraintViolationException e){
				e.printStackTrace();
				throw e;
			} catch (SQLException e) {
				e.printStackTrace();
				throw e;
			}
	}
}
