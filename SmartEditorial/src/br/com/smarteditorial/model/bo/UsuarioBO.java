package br.com.smarteditorial.model.bo;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.smarteditorial.model.entities.MapElementos;
import br.com.smarteditorial.model.entities.Usuario;
import br.com.smarteditorial.service.UsuarioService;

public class UsuarioBO {
	
private static final Logger logger = LoggerFactory.getLogger(UsuarioBO.class);
	
	protected UsuarioService usuarioService;
	@Autowired(required=true)
	@Qualifier(value="usuarioService")
	public void setUsuarioService(UsuarioService us){
		this.usuarioService = us;
	}
	
	@Autowired
	Usuario usuario;
	
	public void salvarUsuario(Usuario u){
		try {
			this.usuarioService.salvarUsuario(u);
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| SQLException e) {
			logger.info("Falha ao salvar o Usuario" + e);
			e.printStackTrace();
		}
	}
	public void atualizarUsuario(Usuario u){
		try {
			u.setEdicoes(this.pegaUsuarioPorId(u.getIdUsuario()).getEdicoes());
			u.setGrupos(this.pegaUsuarioPorId(u.getIdUsuario()).getGrupos());
			u.setPublicacoes(this.pegaUsuarioPorId(u.getIdUsuario()).getPublicacoes());
			this.usuarioService.atualizarUsuario(u);
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| SQLException e) {
			logger.info("Falha ao atualizar o Usuario" + e);
			e.printStackTrace();
		}
	}
	public void excluirUsuario(Usuario u){
		try {
			this.usuarioService.excluirUsuario(u);
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| SQLException e) {
			logger.info("Falha ao excluir o Usuario" + e);
			e.printStackTrace();
		}
	}
	public List<Usuario> pegaUsuario() {
		try {
			return this.usuarioService.listarUsuarios();
		} catch (SQLException e) {
			logger.info("Falha ao carregar a lista de Usuarios" + e);
			e.printStackTrace();
			return null;
		}
	}
	public Usuario pegaUsuarioPorId(Integer id) {
		try {
			return this.usuarioService.usuarioPorId(id);
		} catch (SQLException e) {
			logger.info("Falha ao carregar o Usuario por ID" + e);
			e.printStackTrace();
			return null;
		}
	}
	public Usuario pegaUsuarioPorEmail(String email) {
		try {
			return this.usuarioService.usuarioPorEmail(email);
		} catch (SQLException e) {
			logger.info("Falha ao carregar o Usuario por Email" + e);
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Integer> pegaGruposDoUsuario(Usuario u) {
		return this.usuarioService.gruposDoUsuario(u.getIdUsuario());
	}
	
	public List<MapElementos> pegarElementosDoGrupoDoGrupo(String grupos) {
		return this.usuarioService.listarElementosEmFluxoParaOGrupo(grupos);
	}
	
	public List<MapElementos> pegarElementosDoGrupoDoUsuario(String grupos, Usuario u) {
		return this.usuarioService.listarElementosEmFluxoParaOUsuario(grupos, u.getIdUsuario());
	}
}
