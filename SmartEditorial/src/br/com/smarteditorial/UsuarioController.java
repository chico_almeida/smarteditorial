package br.com.smarteditorial;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.smarteditorial.model.bo.UsuarioBO;
import br.com.smarteditorial.model.entities.Usuario;

@Controller
public class UsuarioController {
	
	@Autowired
	UsuarioBO usuarioBO;
	@Autowired
	Usuario usuario;

	@RequestMapping(value="/admin/usuario/criar", method = RequestMethod.POST)
	public String novoUsuarios(Model model) {
		model.addAttribute("usuario", new Usuario());
		return "/admin/usuario";
	}

	@RequestMapping(value= "/admin/usuario/add", method = RequestMethod.POST)
	public String salvarUsuario(@ModelAttribute("usuario") Usuario u){
		this.usuarioBO.salvarUsuario(u);
		return "redirect:/admin/usuarios";
	}

	@RequestMapping(value= "/admin/usuario/update", method = RequestMethod.POST)
	public String atualizarUsuario(@ModelAttribute("usuario") Usuario u, Model model){
		this.usuarioBO.atualizarUsuario(u);
		return "redirect:/admin/usuarios";
	}


	@RequestMapping("/admin/usuarios")
	public String listarUsuarios(Model model) {
		model.addAttribute("listarUsuarios", this.usuarioBO.pegaUsuario());
		return "/admin/lista-usuarios";
	}

	@RequestMapping(value="/admin/usuario/excluir", method = RequestMethod.POST)
	public @ResponseBody String excluirUsuario(@ModelAttribute("id") Integer id, Model model){
		if (this.usuarioBO.pegaUsuarioPorId(id).getGrupos().isEmpty()) {
			this.usuarioBO.excluirUsuario(this.usuarioBO.pegaUsuarioPorId(id));
			model.addAttribute("listarUsuarios", this.usuarioBO.pegaUsuario());
			return "sucesso";
		} else {
			model.addAttribute("listarUsuarios", this.usuarioBO.pegaUsuario());
			model.addAttribute("vinculo", "true");
			return "falha";
		}
		
	}

	@RequestMapping(value= "/admin/usuario/editar", method = RequestMethod.POST)
	public String editarUsuario(@ModelAttribute("id") String id, Model model){
		model.addAttribute("usuario", this.usuarioBO.pegaUsuarioPorId(Integer.parseInt(id)));
		return "/admin/usuario";
	}
	
	@RequestMapping(value= "/admin/usuario/validar", method = RequestMethod.POST)
	public @ResponseBody String existeEmail(@ModelAttribute("email") String email) {
		if (this.usuarioBO.pegaUsuarioPorEmail(email) == null) {
			return "false";
		} else {
			return "true";
		}
	}
	@RequestMapping(value="/admin/usuario/ativo", method = RequestMethod.POST)
	public String usuarioAtivo(@ModelAttribute("idUsuario") String idUsuario) {
		if (this.usuarioBO.pegaUsuarioPorId(Integer.parseInt(idUsuario)).isAtivo() == true) {
			this.usuario = this.usuarioBO.pegaUsuarioPorId(Integer.parseInt(idUsuario));
			this.usuario.setAtivo(false);
			this.usuarioBO.atualizarUsuario(this.usuario);
		} else {
			this.usuario = this.usuarioBO.pegaUsuarioPorId(Integer.parseInt(idUsuario));
			this.usuario.setAtivo(true);
			this.usuarioBO.atualizarUsuario(this.usuario);
		}
		return "redirect:/admin/usuarios";
	}
	
	
}
