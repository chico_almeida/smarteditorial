package br.com.smarteditorial;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import br.com.smarteditorial.model.bo.CategoriaBO;
import br.com.smarteditorial.model.bo.EdicaoBO;
import br.com.smarteditorial.model.bo.FileUploadBO;
import br.com.smarteditorial.model.bo.FluxoDeTrabalhoBO;
import br.com.smarteditorial.model.bo.ImagemBO;
import br.com.smarteditorial.model.bo.ImagemFluxoBO;
import br.com.smarteditorial.model.bo.PublicacaoBO;
import br.com.smarteditorial.model.bo.TagsBO;
import br.com.smarteditorial.model.bo.UsuarioBO;
import br.com.smarteditorial.model.entities.Categoria;
import br.com.smarteditorial.model.entities.Edicao;
import br.com.smarteditorial.model.entities.FluxoDeTrabalho;
import br.com.smarteditorial.model.entities.Imagem;
import br.com.smarteditorial.model.entities.ImagemFluxo;
import br.com.smarteditorial.model.entities.Publicacao;
import br.com.smarteditorial.model.entities.Usuario;
import br.com.smarteditorial.util.SystemConfiguration;

@Controller
public class ImagemController {
	@Autowired
	private FileUploadBO fileUploadBO;
	@Autowired
	private ImagemBO imagemBO;
	@Autowired
	private PublicacaoBO publicacaoBO;
	@Autowired
	private EdicaoBO edicaoBO;
	@Autowired
	private TagsBO tagsBO;
	@Autowired
	private UsuarioBO usuarioBO;
	@Autowired
	private CategoriaBO categoriaBO;
	@Autowired
	private ImagemFluxoBO imagemFluxoBO;
	@Autowired
	private ImagemFluxo imagemFluxo;
	@Autowired
	private FluxoDeTrabalhoBO fluxoDeTrabalhoBO;
	@Autowired
	SystemConfiguration systemConfiguration;

	@RequestMapping("/imagens")
	public String cadastroImagem(Model model) {
		model.addAttribute("Imagem", new Imagem());
		model.addAttribute("tags", new String());
		model.addAttribute("idPublicacao", new String());
		model.addAttribute("idEdicao", new String());
		model.addAttribute("listaDePublicacoes", publicacaoBO.pegaPublicacoes());
		return "/acervo/imagem/cadastro";
	}
	
	@RequestMapping(value = "/imagens/update-file", method = RequestMethod.POST)
	public String versaoImagem(@RequestParam("arquivo") MultipartFile arquivo, 
			@RequestParam("id") String id) {
		Imagem i = this.imagemBO.pegaImagemId(Integer.parseInt(id));
		String caminho = fileUploadBO.imageUpload(arquivo, i.getEdicao().getPublicacao().getTitulo()+"/"+i.getEdicao().getTitulo(), i.getTitulo());
		i.setCaminho(caminho);
		this.imagemBO.atualizarImagem(i);
		return "redirect:/home";
	}
	
	@RequestMapping(value = "/imagens/cadastro/add", method = RequestMethod.POST)
	public String salvarImagem(@ModelAttribute("imagem") Imagem imagem, 
			@ModelAttribute("idEdicao") String idEdicao,
			@ModelAttribute("idCategoria") String idCategoria,
			@RequestParam("arquivo") MultipartFile arquivo, 
			@ModelAttribute("pchaves") String tags) {
		UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Integer idUsuario = this.usuarioBO.pegaUsuarioPorEmail(userDetails.getUsername()).getIdUsuario();
		Edicao e = this.edicaoBO.pegaEdicaoId(Integer.parseInt(idEdicao));
		Categoria c = this.categoriaBO.pegaCategoriaPorId(Integer.parseInt(idCategoria));
		String caminho = fileUploadBO.imageUpload(arquivo, e.getPublicacao().getTitulo()+"/"+e.getTitulo(), imagem.getTitulo());
		imagem.setEdicao(e);
		imagem.setCategoria(c);
		imagem.setTags(this.tagsBO.splitTags(tags));
		imagem.setCaminho(caminho);
		imagem.setAltura(this.fileUploadBO.getHeight(arquivo));
		imagem.setLargura(this.fileUploadBO.getWidth(arquivo));
		imagem.setEstado("Aguardando");
		imagem.setUsuario(this.usuarioBO.pegaUsuarioPorId(idUsuario));
		this.imagemBO.salvarImagem(imagem);
		return "redirect:/imagens/minhasimagens";
	}
	
	@RequestMapping("/imagens/minhasimagens")
	public String listaDeImagens(Model model) {
		
		UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Usuario u = this.usuarioBO.pegaUsuarioPorEmail(userDetails.getUsername());
		
		model.addAttribute("imagensFinalizadas", this.imagemBO.pegaImagensFinalizadas(u));
		model.addAttribute("imagensEmFluxo", this.imagemBO.pegaImagensDoUsuarioEmFluxo(u));
		model.addAttribute("imagensForaDoFluxo", this.imagemBO.pegaImagensDoUsuarioForaDoFluxo(u));
		model.addAttribute("contextPath", this.systemConfiguration.getUrlAcervo());
		return "/acervo/imagem/imagens";
	}

	@RequestMapping("/imagens/minhasimagens/editar")
	public String editarImagem(@RequestParam("id") String id, 
			@RequestParam("publicacao")String idPublicacao, Model model){
		Publicacao p = this.publicacaoBO.pegaPublicacaoID(Integer.parseInt(idPublicacao));
		model.addAttribute("listaDePublicacoes", publicacaoBO.pegaPublicacoes());
		model.addAttribute("listaDeEdicoes", p.getEdicoes());
		model.addAttribute("listaDeCategorias", p.getCategorias());
		model.addAttribute("imagem", this.imagemBO.pegaImagemId(Integer.parseInt(id)));
		model.addAttribute("contextPath", this.systemConfiguration.getUrlAcervo());
		return "/acervo/imagem/cadastro";
	}
	
	@RequestMapping(value="/imagens/minhasimagens/remover", method = RequestMethod.POST)
	public @ResponseBody String removerImagem(@ModelAttribute("id") String id){
		System.out.println(id);
		try {
			this.imagemBO.excluirImagem(this.imagemBO.pegaImagemId(Integer.parseInt(id)));
			return "sucesso";
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| NumberFormatException | SQLException e) {
			return "falha";
		}
	}

	@RequestMapping(value = "/imagens/cadastro/update", method = RequestMethod.POST)
	public String alterarImagem(@ModelAttribute("imagem") Imagem imagem, 
			@ModelAttribute("idEdicao") String idEdicao, 
			@ModelAttribute("idCategoria") String idCategoria , 
			@ModelAttribute("pchaves") String tags) {
		imagem.setTags(this.tagsBO.splitTags(tags));
		imagem.setEdicao(this.edicaoBO.pegaEdicaoId(Integer.parseInt(idEdicao)));
		imagem.setCategoria(this.categoriaBO.pegaCategoriaPorId(Integer.parseInt(idCategoria)));
		imagem.setUsuario(this.usuarioBO.pegaUsuarioPorId(pegaUsuarioLogado().getIdUsuario()));
		this.imagemBO.atualizarImagem(imagem);
		return "redirect:/imagens/minhasimagens";
	}
	
	public Usuario pegaUsuarioLogado() {
		UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return this.usuarioBO.pegaUsuarioPorEmail(userDetails.getUsername());
	}
	
	@RequestMapping(value="/imagem/iniciar-fluxo", method = RequestMethod.POST)
	public @ResponseBody String iniciarFluxo(
			@ModelAttribute("idImagem")String idImagem, 
			@ModelAttribute("comentario")String comentario) throws IndexOutOfBoundsException {
		ImagemFluxo imf = new ImagemFluxo();
		
		List<FluxoDeTrabalho> fxs;
		try {
			fxs = this.fluxoDeTrabalhoBO.pegaFluxosPorElemento("imagem", this.imagemBO.pegaImagemId(Integer.parseInt(idImagem)).getEdicao().getPublicacao().getIdPublicacao());
		} catch (IndexOutOfBoundsException e1) {
			return "vazio";
		}
		
		imf.setImagem(this.imagemBO.pegaImagemId(Integer.parseInt(idImagem)));
		try {
			imf.setFluxoDeTrabalho(fxs.get(0));
		} catch (IndexOutOfBoundsException e1) {
			return "vazio";
		}
		imf.setComentario(comentario);
		
		try {
			this.imagemFluxoBO.salvar(imf);
			return "sucesso";
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| SQLException e) {
			e.printStackTrace();
			return "falha";
		}
		
	}
	
	@RequestMapping(value="/imagem/proximo-fluxo", method = RequestMethod.POST)
	public @ResponseBody String proximoFluxo(@ModelAttribute("idElemento")String idElemento, 
			@ModelAttribute("comentario")String comentario, 
			@ModelAttribute("ordemFluxo") String PosicaoAtual, 
			@ModelAttribute("idPublicacao") String idPublicacao,
			@ModelAttribute("tipoElemento") String tipoElemento,
			@ModelAttribute("idFluxo") String idFluxo) {

		ImagemFluxo imf = new ImagemFluxo();
		Integer i = Integer.parseInt(PosicaoAtual);
		
		FluxoDeTrabalho fx = this.fluxoDeTrabalhoBO.pegaFluxo(++i, Integer.parseInt(idPublicacao), tipoElemento);
		
		if (fx == null) {
			return "ultimo";
		} else {
			imf.setImagem(this.imagemBO.pegaImagemId(Integer.parseInt(idElemento)));
			imf.setFluxoDeTrabalho(fx);
			imf.setComentario(comentario);

			try {
				this.imagemFluxoBO.salvar(imf);
				this.imagemFluxoBO.excluir(this.imagemFluxoBO.pegaImagemFluxoPorIds(Integer.parseInt(idFluxo), Integer.parseInt(idElemento)));
				return "sucesso";
			} catch (DataIntegrityViolationException | ConstraintViolationException
					| SQLException e) {
				e.printStackTrace();
				return "falha";
			}
		}
	}
	
	@RequestMapping(value="/imagem/retroceder-fluxo", method = RequestMethod.POST)
	public @ResponseBody String retrocerFluxo(@ModelAttribute("idElemento")String idElemento, 
			@ModelAttribute("comentario")String comentario, 
			@ModelAttribute("ordemFluxo") String PosicaoAtual, 
			@ModelAttribute("idPublicacao") String idPublicacao,
			@ModelAttribute("tipoElemento") String tipoElemento,
			@ModelAttribute("idFluxo") String idFluxo) {
		
		if (Integer.parseInt(PosicaoAtual) == 1) {
			try {
				this.imagemFluxoBO.excluir(this.imagemFluxoBO.pegaImagemFluxoPorIds(Integer.parseInt(idFluxo), Integer.parseInt(idElemento)));
				return "sucesso";
			} catch (DataIntegrityViolationException
					| ConstraintViolationException | NumberFormatException
					| SQLException e) {
				e.printStackTrace();
				return "falha";
			}
		}
		ImagemFluxo imf = new ImagemFluxo();
		Integer i = Integer.parseInt(PosicaoAtual);
		
		FluxoDeTrabalho fx = this.fluxoDeTrabalhoBO.pegaFluxo(--i, Integer.parseInt(idPublicacao), tipoElemento);
		
		imf.setImagem(this.imagemBO.pegaImagemId(Integer.parseInt(idElemento)));
		imf.setFluxoDeTrabalho(fx);
		imf.setComentario(comentario);

		try {
			this.imagemFluxoBO.salvar(imf);
			this.imagemFluxoBO.excluir(this.imagemFluxoBO.pegaImagemFluxoPorIds(Integer.parseInt(idFluxo), Integer.parseInt(idElemento)));
			return "sucesso";
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| SQLException e) {
			e.printStackTrace();
			return "falha";
		}
		
	}
	
	@RequestMapping(value="/imagem/preview", method = RequestMethod.POST)
	public String previewImagem(@ModelAttribute("id") Integer id, Model model) {
		model.addAttribute("contextPath", this.systemConfiguration.getUrlAcervo());
		model.addAttribute("imagem", this.imagemBO.pegaImagemId(id));
		return "/restrito/imagemPreview";
	}
	
	@RequestMapping(value="/imagem/pegar-elemento", method = RequestMethod.POST)
	public @ResponseBody String pegarElemento(@ModelAttribute("idElemento") Integer idElemento,
			@ModelAttribute("idFluxo") Integer idFluxo) {
		
		Imagem i = this.imagemBO.pegaImagemId(idElemento);
		i.setEstado("Editando");
		ImagemFluxo imf =  this.imagemFluxoBO.pegaImagemFluxoPorIds(idFluxo, idElemento);
		imf.setUsuario(pegaUsuarioLogado());
		
		try {
			this.imagemFluxoBO.atualizar(imf);
			this.imagemBO.atualizarImagem(i);
			return "sucesso";
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| SQLException e) {
			e.printStackTrace();
			return "falha";
		}
	}
	
	@RequestMapping(value="/imagens/interromper-fluxo", method = RequestMethod.POST)
	public @ResponseBody String interromperFluxo(@ModelAttribute("idElemento") Integer idElemento,
			@ModelAttribute("idFluxo") Integer idFluxo) {
		Imagem i = this.imagemBO.pegaImagemId(idElemento);
		i.setEstado("Aguardando");
		try {
			this.imagemFluxoBO.excluir(this.imagemFluxoBO.pegaImagemFluxoPorIds(idFluxo, idElemento));
			this.imagemBO.atualizarImagem(i);
			return "sucesso";
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| SQLException e) {
			e.printStackTrace();
			return "falha";
		}
		
	}
	
	@RequestMapping(value="/imagem/devolver-elemento", method = RequestMethod.POST)
	public @ResponseBody String devolverElementoParaOGrupo(@ModelAttribute("idElemento") Integer idElemento,
			@ModelAttribute("idFluxo") Integer idFluxo) {
		Imagem i = this.imagemBO.pegaImagemId(idElemento);
		i.setEstado("Aguardando");
		
		ImagemFluxo imf =  this.imagemFluxoBO.pegaImagemFluxoPorIds(idFluxo, idElemento);
		imf.setUsuario(null);
		
		try {
			this.imagemFluxoBO.atualizar(imf);
			this.imagemBO.atualizarImagem(i);
			return "sucesso";
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| SQLException e) {
			e.printStackTrace();
			return "falha";
		}
	}
}
