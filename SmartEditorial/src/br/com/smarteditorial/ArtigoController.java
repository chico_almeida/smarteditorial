package br.com.smarteditorial;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.smarteditorial.model.bo.ArtigoBO;
import br.com.smarteditorial.model.bo.ArtigoFluxoBO;
import br.com.smarteditorial.model.bo.CategoriaBO;
import br.com.smarteditorial.model.bo.EdicaoBO;
import br.com.smarteditorial.model.bo.FluxoDeTrabalhoBO;
import br.com.smarteditorial.model.bo.PublicacaoBO;
import br.com.smarteditorial.model.bo.TagsBO;
import br.com.smarteditorial.model.bo.UsuarioBO;
import br.com.smarteditorial.model.entities.Artigo;
import br.com.smarteditorial.model.entities.ArtigoFluxo;
import br.com.smarteditorial.model.entities.FluxoDeTrabalho;
import br.com.smarteditorial.model.entities.Publicacao;
import br.com.smarteditorial.model.entities.Usuario;

@Controller
public class ArtigoController {
	
	@Autowired
	ArtigoBO artigoBO;
	@Autowired
	PublicacaoBO publicacaoBO;
	@Autowired
	EdicaoBO edicaoBO;
	@Autowired
	UsuarioBO usuarioBO;
	@Autowired
	CategoriaBO categoriaBO;
	@Autowired
	TagsBO tagsBO;
	@Autowired
	FluxoDeTrabalhoBO fluxoDeTrabalhoBO;
	@Autowired
	ArtigoFluxoBO artigoFluxoBO;
	
	
	@RequestMapping("/artigos")
	public String anuncios(Model model) {
		model.addAttribute("artigo", new Artigo());
		model.addAttribute("pchaves", new String());
		model.addAttribute("idPublicacao", new String());
		model.addAttribute("idEdicao", new String());
		model.addAttribute("listaDePublicacoes", this.publicacaoBO.pegaPublicacoes());
		return "/acervo/artigo/artigos";
	}
	
	@RequestMapping(value="/artigo/criar", method = RequestMethod.POST)
	public String salvarArtigo(@ModelAttribute("artigo") Artigo a, 
			@ModelAttribute("idEdicao") String idEdicao, 
			@ModelAttribute("idCategoria") String idCategoria, 
			@ModelAttribute("pchaves") String	 tags) {
		UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		a.setUsuario(this.usuarioBO.pegaUsuarioPorEmail(userDetails.getUsername()));
		a.setTags(this.tagsBO.splitTags(tags));
		a.setEdicao(this.edicaoBO.pegaEdicaoId(Integer.parseInt(idEdicao)));
		a.setCategoria(this.categoriaBO.pegaCategoriaPorId(Integer.parseInt(idCategoria)));
		a.setUsuario(pegaUsuarioLogado());
		a.setEstado("Aguardando");
		a.setDataCriacao(getCurrentDate().getTime());
		a.setUltimaAlteracao(getCurrentDate().getTime());
		artigoBO.salvaArtigo(a);
		
		return "redirect:/artigos/meusartigos";
	}
	
	@RequestMapping("/artigos/meusartigos")
	public String meusArtigos(Model model) {
		UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Usuario u = this.usuarioBO.pegaUsuarioPorEmail(userDetails.getUsername());
		model.addAttribute("meusArtigos", this.artigoBO.pegaArtigosDoUsuario(u.getIdUsuario()));
		model.addAttribute("artigosFinalizadas", this.artigoBO.pegaArtigosFinalizadas(u));
		model.addAttribute("artigosEmFluxo", this.artigoBO.pegaArtigosDoUsuarioEmFluxo(u));
		model.addAttribute("artigosForaDoFluxo", this.artigoBO.pegaArtigosDoUsuarioForaDoFluxo(u));
		
		return "/acervo/artigo/meusartigos";
	}
	
	@RequestMapping(value="/artigos/excluir", method = RequestMethod.POST)
	public @ResponseBody String excluirArtigo(@ModelAttribute("id") String id, Model model){
		try {
			this.artigoBO.excluirArtigo(this.artigoBO.pegaArtigoID(Integer.parseInt(id)));
			return "sucesso";
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| NumberFormatException e) {
			e.printStackTrace();
			return "falha";
		} catch (SQLException  e) {
			e.printStackTrace();
			return "interno";
		}
	}
	
	@RequestMapping("/artigos/editar")
	public String editarArtigo(@RequestParam("artigo") String id, Model model){
		Publicacao p  = this.publicacaoBO.pegaPublicacaoID(this.artigoBO.pegaArtigoID(Integer.parseInt(id)).getEdicao().getPublicacao().getIdPublicacao());
		model.addAttribute("artigo", this.artigoBO.pegaArtigoID(Integer.parseInt(id)));
		model.addAttribute("listaDePublicacoes", this.publicacaoBO.pegaPublicacoes());
		model.addAttribute("listaDeEdicoes", this.edicaoBO.pegaEdicaoPorPublicacao(p.getIdPublicacao()));
		model.addAttribute("listaDeCategorias", this.categoriaBO.pegaCategoriaPorPublicacao(p.getIdPublicacao()));
		return "/acervo/artigo/artigos";

	}
	
	@RequestMapping(value="/artigo/atualizar", method = RequestMethod.POST)
	public String alterarImagem(@ModelAttribute("artigo") Artigo artigo, 
			@ModelAttribute("idEdicao") String idEdicao,
			@ModelAttribute("idCategoria") String idCategoria,
			@ModelAttribute("pchaves") String tags,
			@ModelAttribute("criacao") String date) throws ParseException {
	
		artigo.setEdicao(this.edicaoBO.pegaEdicaoId(Integer.parseInt(idEdicao)));
		artigo.setCategoria(this.categoriaBO.pegaCategoriaPorId(Integer.parseInt(idCategoria)));
		artigo.setUsuario(pegaUsuarioLogado());
		artigo.setTags(this.tagsBO.splitTags(tags));
		artigo.setUltimaAlteracao(new Date());
		artigo.setDataCriacao(formatDate(date));
		this.artigoBO.atualizaArtigo(artigo);
		return "redirect:/artigos/meusartigos";
	}
	
	public Usuario pegaUsuarioLogado() {
		UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return this.usuarioBO.pegaUsuarioPorEmail(userDetails.getUsername());
	}
	
	public Calendar getCurrentDate() {
		Calendar calendar = new GregorianCalendar();
		return calendar;
	}
	
	public Date formatDate(String date) throws ParseException {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(date);
	}
	
	@RequestMapping(value="/artigo/preview", method = RequestMethod.POST)
	public String previewArtigo(@ModelAttribute("id") Integer id, Model model) {
		model.addAttribute("artigo", this.artigoBO.pegaArtigoID(id));
		return "/restrito/artigoPreview";
	}
	
	@RequestMapping(value="/artigo/iniciar-fluxo", method = RequestMethod.POST)
	public @ResponseBody String iniciarFluxo(
			@ModelAttribute("idArtigo")String idArtigo, 
			@ModelAttribute("comentario")String comentario) throws IndexOutOfBoundsException {
		ArtigoFluxo arf = new ArtigoFluxo();
		
		List<FluxoDeTrabalho> fxs;
		try {
			fxs = this.fluxoDeTrabalhoBO.pegaFluxosPorElemento("artigo", this.artigoBO.pegaArtigoID(Integer.parseInt(idArtigo)).getEdicao().getPublicacao().getIdPublicacao());
		} catch (IndexOutOfBoundsException e1) {
			return "vazio";
		}
		
		arf.setArtigo(this.artigoBO.pegaArtigoID(Integer.parseInt(idArtigo)));
		try {
			arf.setFluxoDeTrabalho(fxs.get(0));
		} catch (IndexOutOfBoundsException e1) {
			return "vazio";
		}
		arf.setComentario(comentario);
		
		try {
			this.artigoFluxoBO.salvar(arf);
			return "sucesso";
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| SQLException e) {
			e.printStackTrace();
			return "falha";
		}
		
	}
	
	@RequestMapping(value="/artigo/proximo-fluxo", method = RequestMethod.POST)
	public @ResponseBody String proximoFluxo(@ModelAttribute("idElemento")String idElemento, 
			@ModelAttribute("comentario")String comentario, 
			@ModelAttribute("ordemFluxo") String PosicaoAtual, 
			@ModelAttribute("idPublicacao") String idPublicacao,
			@ModelAttribute("tipoElemento") String tipoElemento,
			@ModelAttribute("idFluxo") String idFluxo) {

		ArtigoFluxo arf = new ArtigoFluxo();
		Integer i = Integer.parseInt(PosicaoAtual);
		
		FluxoDeTrabalho fx = this.fluxoDeTrabalhoBO.pegaFluxo(++i, Integer.parseInt(idPublicacao), tipoElemento);
		
		if (fx == null) {
			return "ultimo";
		} else {
			arf.setArtigo(this.artigoBO.pegaArtigoID(Integer.parseInt(idElemento)));
			arf.setFluxoDeTrabalho(fx);
			arf.setComentario(comentario);

			try {
				this.artigoFluxoBO.salvar(arf);
				this.artigoFluxoBO.excluir(this.artigoFluxoBO.pegaArtigoFluxoPorIds(Integer.parseInt(idFluxo), Integer.parseInt(idElemento)));
				return "sucesso";
			} catch (DataIntegrityViolationException | ConstraintViolationException
					| SQLException e) {
				e.printStackTrace();
				return "falha";
			}
		}
	}
	
	@RequestMapping(value="/artigo/retroceder-fluxo", method = RequestMethod.POST)
	public @ResponseBody String retrocerFluxo(@ModelAttribute("idElemento")String idElemento, 
			@ModelAttribute("comentario")String comentario, 
			@ModelAttribute("ordemFluxo") String PosicaoAtual, 
			@ModelAttribute("idPublicacao") String idPublicacao,
			@ModelAttribute("tipoElemento") String tipoElemento,
			@ModelAttribute("idFluxo") String idFluxo) {
		
		if (Integer.parseInt(PosicaoAtual) == 1) {
			try {
				this.artigoFluxoBO.excluir(this.artigoFluxoBO.pegaArtigoFluxoPorIds(Integer.parseInt(idFluxo), Integer.parseInt(idElemento)));
				return "sucesso";
			} catch (DataIntegrityViolationException
					| ConstraintViolationException | NumberFormatException
					| SQLException e) {
				e.printStackTrace();
				return "falha";
			}
		}
		ArtigoFluxo arf = new ArtigoFluxo();
		Integer i = Integer.parseInt(PosicaoAtual);
		
		FluxoDeTrabalho fx = this.fluxoDeTrabalhoBO.pegaFluxo(--i, Integer.parseInt(idPublicacao), tipoElemento);
		
		arf.setArtigo(this.artigoBO.pegaArtigoID(Integer.parseInt(idElemento)));
		arf.setFluxoDeTrabalho(fx);
		arf.setComentario(comentario);

		try {
			this.artigoFluxoBO.salvar(arf);
			this.artigoFluxoBO.excluir(this.artigoFluxoBO.pegaArtigoFluxoPorIds(Integer.parseInt(idFluxo), Integer.parseInt(idElemento)));
			return "sucesso";
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| SQLException e) {
			e.printStackTrace();
			return "falha";
		}
		
	}
	
	@RequestMapping(value="/artigo/pegar-elemento", method = RequestMethod.POST)
	public @ResponseBody String pegarElemento(@ModelAttribute("idElemento") Integer idElemento,
			@ModelAttribute("idFluxo") Integer idFluxo) {
		Artigo a = this.artigoBO.pegaArtigoID(idElemento);
		a.setEstado("Editando");
		ArtigoFluxo arf =  this.artigoFluxoBO.pegaArtigoFluxoPorIds(idFluxo, idElemento);
		arf.setUsuario(pegaUsuarioLogado());
		
		try {
			this.artigoFluxoBO.atualizar(arf);
			this.artigoBO.atualizaArtigo(a);
			return "sucesso";
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| SQLException e) {
			e.printStackTrace();
			return "falha";
		}
	}
	
	@RequestMapping(value="/artigos/interromper-fluxo", method = RequestMethod.POST)
	public @ResponseBody String interromperFluxo(@ModelAttribute("idElemento") Integer idElemento,
			@ModelAttribute("idFluxo") Integer idFluxo) {
		Artigo a = this.artigoBO.pegaArtigoID(idElemento);
		a.setEstado("Aguardando");
		try {
			this.artigoFluxoBO.excluir(this.artigoFluxoBO.pegaArtigoFluxoPorIds(idFluxo, idElemento));
			this.artigoBO.atualizaArtigo(a);
			return "sucesso";
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| SQLException e) {
			e.printStackTrace();
			return "falha";
		}
		
	}
	
	@RequestMapping(value="/artigo/devolver-elemento", method = RequestMethod.POST)
	public @ResponseBody String devolverElementoParaOGrupo(@ModelAttribute("idElemento") Integer idElemento,
			@ModelAttribute("idFluxo") Integer idFluxo) {
		Artigo a = this.artigoBO.pegaArtigoID(idElemento);
		a.setEstado("Aguardando");
		
		ArtigoFluxo arf =  this.artigoFluxoBO.pegaArtigoFluxoPorIds(idFluxo, idElemento);
		arf.setUsuario(null);
		
		try {
			this.artigoFluxoBO.atualizar(arf);
			this.artigoBO.atualizaArtigo(a);
			return "sucesso";
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| SQLException e) {
			e.printStackTrace();
			return "falha";
		}
	}
}	
