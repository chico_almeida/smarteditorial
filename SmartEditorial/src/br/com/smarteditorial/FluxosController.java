package br.com.smarteditorial;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.smarteditorial.model.bo.FluxoDeTrabalhoBO;
import br.com.smarteditorial.model.bo.GrupoBO;
import br.com.smarteditorial.model.bo.PublicacaoBO;
import br.com.smarteditorial.model.entities.FluxoDeTrabalho;
import br.com.smarteditorial.model.entities.Grupo;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

@Controller
public class FluxosController {

	@Autowired
	private FluxoDeTrabalhoBO fluxoDeTrabalhoBO;
	@Autowired
	private GrupoBO grupoBO;
	@Autowired
	private PublicacaoBO publicacaoBO;
	@Autowired
	private FluxoDeTrabalho fluxoDeTrabalho;
	@Autowired
	private Grupo grupo;

	@RequestMapping(value="/fluxos/workflow", method = RequestMethod.POST)
	public String gerenciarWorkflow(@ModelAttribute("tipoElemento") String tipoElemento, 
			@ModelAttribute("idPublicacao") String idPublicacao,
			Model model) {
		List<FluxoDeTrabalho> flxs = this.fluxoDeTrabalhoBO.pegaFluxosPorElemento(tipoElemento, Integer.parseInt(idPublicacao));
		if (flxs != null && !flxs.isEmpty()) {

			this.fluxoDeTrabalho = flxs.get(flxs.size()-1);
			this.grupo = this.fluxoDeTrabalho.getGrupoDestino();

			model.addAttribute("lastValue", this.fluxoDeTrabalho.getOrdem());
			model.addAttribute("grupo", this.grupo);
		}
		model.addAttribute("listaGrupo", this.grupoBO.pegaGrupos());
		model.addAttribute("elemento", tipoElemento);
		model.addAttribute("idPublicacao", idPublicacao);
		model.addAttribute("fluxoDeTrabalho", new FluxoDeTrabalho());
		model.addAttribute("listaFluxoImagem", this.fluxoDeTrabalhoBO.pegaFluxosPorElemento(tipoElemento, Integer.parseInt(idPublicacao)));


		return "/publicacao/workflow";
	}


	@RequestMapping(value="/fluxos/criar", method = RequestMethod.POST)
	public @ResponseBody String criarFluxo(@ModelAttribute("fluxoDeTrabalho") FluxoDeTrabalho fluxo, 
			@ModelAttribute("idPublicacao") String idPublicacao,
			@ModelAttribute("origem") String idGrupoOrigem, 
			@ModelAttribute("destino") String idGrupoDestino) {
		fluxo.setGrupoOrigem(this.grupoBO.pegaGrupoPorId(Integer.parseInt(idGrupoOrigem)));
		fluxo.setGrupoDestino(this.grupoBO.pegaGrupoPorId(Integer.parseInt(idGrupoDestino)));
		fluxo.setPublicacao(this.publicacaoBO.pegaPublicacaoID(Integer.parseInt(idPublicacao)));

		try {
			this.fluxoDeTrabalhoBO.salvaFluxo(fluxo);
			return "sucesso";
		} catch (DataIntegrityViolationException | ConstraintViolationException | MySQLIntegrityConstraintViolationException e) {
			e.printStackTrace();
			return "Fluxo j� cadastrador";
		} catch (SQLException e) {
			e.printStackTrace();
			return "Erro interno, contate o Administrador";
		}
	}

	@RequestMapping(value="/fluxos/recarregarLista", method = RequestMethod.POST)
	public String devolveLista(@ModelAttribute("elemento") String elemento, @ModelAttribute("idPublicacao") String idPublicacao, Model model) {
		List<FluxoDeTrabalho> flxs = this.fluxoDeTrabalhoBO.pegaFluxosPorElemento(elemento, Integer.parseInt(idPublicacao));
		if (flxs != null && !flxs.isEmpty()) {
			this.fluxoDeTrabalho = flxs.get(flxs.size()-1);
			this.grupo = this.fluxoDeTrabalho.getGrupoDestino();
			model.addAttribute("lastValue", this.fluxoDeTrabalho.getOrdem());
			model.addAttribute("grupo", this.grupo);
		}
		model.addAttribute("listaFluxoImagem", this.fluxoDeTrabalhoBO.pegaFluxosPorElemento(elemento, Integer.parseInt(idPublicacao)));
		return "/publicacao/workflowLista";
	}

	@RequestMapping(value="/fluxos/remover", method = RequestMethod.POST)
	public @ResponseBody String removerFluxo(@ModelAttribute("idFluxo") String idFluxo) {
		System.out.println(idFluxo);
		try {
			this.fluxoDeTrabalhoBO.excluirFluxo(this.fluxoDeTrabalhoBO.pegaFluxosDaPorId(Integer.parseInt(idFluxo)));
			return "sucesso";
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| NumberFormatException e) {
			return "vinculos";
		} catch (Exception e) {
			return "Falha na remo��o, contate o Administrador do Sistema. Erro :: "+e.getMessage();
		}
	}
	
}
