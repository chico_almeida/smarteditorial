package br.com.smarteditorial.dao;

import java.util.List;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import br.com.smarteditorial.model.entities.Edicao;
import br.com.smarteditorial.model.entities.Imagem;
import br.com.smarteditorial.model.entities.Usuario;
import br.com.smarteditorial.model.entities.UsuarioAlteraImagem;

@Repository
public class UsuarioAlteraImagemDAO extends AbstractDAO<UsuarioAlteraImagem> {
	
	@SuppressWarnings("unchecked")
	public List<UsuarioAlteraImagemDAO> imagensModificadasPorUsuario(Usuario u) {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from UsuarioAlteraImagem where id_usuario = :idUsuario").setParameter("idUsuario", u.getIdUsuario()).list();
				
	}
	
	@SuppressWarnings("unchecked")
	public List<UsuarioAlteraImagemDAO> imagensModificadasPorUsuarioNaEdicao(Usuario u, Edicao e) {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from UsuarioAlteraImagem where id_usuario = :idUsuario and id_edicao = :idEdicao"
				).setParameter("idUsuario", u.getIdUsuario()).setParameter("idEdicao", e.getIdEdicao()).list();
	}
	
	public UsuarioAlteraImagem alteracaoPorImagem(Imagem i) {
		Session session = this.sessionFactory.getCurrentSession();
		return (UsuarioAlteraImagem) session.createQuery("from UsuarioAlteraImagem where id_imagem = :idImagem").setParameter("idImagem", i.getIdImagem()).list();
	}
	
}
