package br.com.smarteditorial.dao;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import br.com.smarteditorial.model.entities.FluxoDeTrabalho;

@Repository
public class FluxoDeTrabalhoDAO extends AbstractDAO<FluxoDeTrabalho> {

	@SuppressWarnings("unchecked")
	public List<FluxoDeTrabalho> listarTrabalhos() throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from FluxoDeTrabalho").list();
	}
	
	public FluxoDeTrabalho fluxoPorId(Integer id) throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();  
		FluxoDeTrabalho f = (FluxoDeTrabalho) session.get(FluxoDeTrabalho.class, new Integer(id));
		return f;
	}
	
	@SuppressWarnings("unchecked")
	public List<FluxoDeTrabalho> listaFluxosPorElementoDaPublicacao(String elemento, Integer idPublicacao) throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from FluxoDeTrabalho where tipoElemento= :elemento and id_publicacao =:idPublicacao ORDER BY ordem").setParameter("elemento", elemento).setParameter("idPublicacao", idPublicacao).list();
	}
	
	public FluxoDeTrabalho pegaFluxo(Integer ordem, Integer idPublicacao, String tipoDeElemento) {
		Session session = this.sessionFactory.getCurrentSession();
		return (FluxoDeTrabalho) session.createQuery(""
				+ "FROM FluxoDeTrabalho where id_publicacao = "
				+ ":publicacao and tipoElemento = :elemento "
				+ "and ordem = :ordem").setParameter("publicacao", idPublicacao)
				.setParameter("ordem", ordem)
				.setParameter("elemento", tipoDeElemento).uniqueResult();
	}
}
