package br.com.smarteditorial.dao;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import br.com.smarteditorial.model.entities.Tags;

@Repository
public class TagsDAO extends AbstractDAO<Tags> {
	private static final Logger logger = LoggerFactory.getLogger(TagsDAO.class);
	
	@SuppressWarnings("unchecked")
	public List<Tags> listarTags() throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from Tags").list();
	}
	
	public Tags tagsPorTitulo(String titulo) throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();  
		Tags t = (Tags) session.createQuery("from Tags where tags= :Titulo").setParameter("Titulo", titulo).uniqueResult();
		logger.info("Tags carregado com sucesso");
		return t;
	}
	
	public Tags tagsPorId(Integer id) throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();  
		Tags t = (Tags) session.get(Tags.class, new Integer(id));
		logger.info("Tag carregado com sucesso");
		return t;
	}
	

}
