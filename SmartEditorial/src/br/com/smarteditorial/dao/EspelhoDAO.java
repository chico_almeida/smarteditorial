package br.com.smarteditorial.dao;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import br.com.smarteditorial.model.entities.Espelho;

@Repository
public class EspelhoDAO extends AbstractDAO<Espelho> {
	
	private static final Logger logger = LoggerFactory.getLogger(EspelhoDAO.class);
	
	@SuppressWarnings("unchecked")
	public List<Espelho> Espelhos() throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from Espelho").list();
	}
	
	public Espelho espelhoDaEdicao(Integer idEdicao) throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();  
		Espelho e = (Espelho) session.createQuery("from Espelho where id_edicao= :idEdicao").setParameter("idEdicao", idEdicao).uniqueResult();
		logger.info("Edicao carregada com sucesso");
		return e;
	}
	
	public Espelho espelhoPorId(Integer id) throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();  
		Espelho e = (Espelho) session.get(Espelho.class, new Integer(id));
		logger.info("Esoelho carregada com sucesso");
		return e;
	}
}
