package br.com.smarteditorial.dao;

import java.util.List;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import br.com.smarteditorial.model.entities.PaginaFluxo;

@Repository
public class PaginaFluxoDAO extends AbstractDAO<PaginaFluxo> {
	
	@SuppressWarnings("unchecked")
	public List<PaginaFluxo> fluxosPagina() {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("FROM PaginaFluxo").list();
	}
	public PaginaFluxo paginaFluxoPorIds(Integer idFluxo, Integer idPagina) {
		Session session= this.sessionFactory.getCurrentSession();
		return (PaginaFluxo) session.createQuery("FROM PaginaFluxo "
				+ "where idPaginaFluxo.pagina.idPagina = :idPagina "
				+ "and idPaginaFluxo.fluxo.idFluxo= :idFluxo")
				.setParameter("idPagina", idPagina)
				.setParameter("idFluxo", idFluxo).uniqueResult();
	}
}
