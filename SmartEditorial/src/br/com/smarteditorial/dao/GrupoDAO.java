package br.com.smarteditorial.dao;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import br.com.smarteditorial.model.entities.Grupo;

@Repository
public class GrupoDAO extends AbstractDAO<Grupo> {

	private static final Logger logger = LoggerFactory.getLogger(GrupoDAO.class);

	@SuppressWarnings("unchecked")
	public List<Grupo> listarGrupos() throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();
		List<Grupo> listaGrupos = session.createQuery("from Grupo").list();
		for(Grupo g : listaGrupos){
			logger.info("Grupo Lista::"+g);
		}
		return listaGrupos;

	}
	
	public Grupo grupoPorNome(String nome) throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();  
		Grupo g = (Grupo) session.createQuery("from Grupo where nome= :Nome").setParameter("Nome", nome).uniqueResult();
		logger.info("Grupo carregado com sucesso");
		return g;
	}

	public Grupo grupoPorId(Integer id) throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();
		Grupo g = (Grupo) session.get(Grupo.class, new Integer(id));
		return g;
	}
	
	public void desvicularPermissao(Integer idU, Integer idP) {
		Session session = this.sessionFactory.getCurrentSession();
		session.createSQLQuery("DELETE FROM grupo_permissao WHERE id_grupo = "+idU+" and id_permissao = '"+idP+"'").executeUpdate();
	}
	
	public void desvicularUsuario(Integer idG, Integer idU) {
		Session session = this.sessionFactory.getCurrentSession();
		session.createSQLQuery("DELETE FROM grupo_usuario WHERE id_grupo = "+idG+" and id_usuario= "+idU).executeUpdate();
	}

}
