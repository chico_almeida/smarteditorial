package br.com.smarteditorial.dao;

import java.util.List;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import br.com.smarteditorial.model.entities.ArtigoFluxo;

@Repository
public class ArtigoFluxoDAO extends AbstractDAO<ArtigoFluxo> {
	
	@SuppressWarnings("unchecked")
	public List<ArtigoFluxo> fluxosArtigo() {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("FROM ArtigoFluxo").list();
	}
	public ArtigoFluxo artigoFluxoPorIds(Integer idFluxo, Integer idArtigo) {
		Session session= this.sessionFactory.getCurrentSession();
		return (ArtigoFluxo) session.createQuery("FROM ArtigoFluxo "
				+ "where idArtigoFluxo.artigo.idArtigo = :idArtigo "
				+ "and idArtigoFluxo.fluxo.idFluxo= :idFluxo")
				.setParameter("idArtigo", idArtigo)
				.setParameter("idFluxo", idFluxo).uniqueResult();
	} 
}
