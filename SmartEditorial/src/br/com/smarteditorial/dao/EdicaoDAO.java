package br.com.smarteditorial.dao;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import br.com.smarteditorial.model.entities.Edicao;

@Repository
public class EdicaoDAO extends AbstractDAO<Edicao> {
	
	private static final Logger logger = LoggerFactory.getLogger(EdicaoDAO.class);

	@SuppressWarnings("unchecked")
	public List<Edicao> listarEdicao() throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from Edicao").list();
	}
	
	public Edicao edicaoPorTitulo(String titulo) throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();  
		Edicao e = (Edicao) session.createQuery("from Edicao where titulo= :Titulo").setParameter("Titulo", titulo).uniqueResult();
		logger.info("Edi��o carregado com sucesso");
		return e;
	}
	
	public Edicao edicaoPorId(Integer id) throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();  
		Edicao e = (Edicao) session.get(Edicao.class, new Integer(id));
		logger.info("Edi��o carregada com sucesso");
		return e;
	}
	
	@SuppressWarnings("unchecked")
	public List<Edicao> edicaoPorPublicacao(Integer id) throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();  
		return session.createQuery("from Edicao where id_publicacao= :id").setParameter("id", id).list();
	}
	
}
