package br.com.smarteditorial.dao;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import br.com.smarteditorial.model.entities.Edicao;
import br.com.smarteditorial.model.entities.UsuarioModificaEdicao;

@Repository
public class UsuarioModificaEdicaoDAO extends AbstractDAO<UsuarioModificaEdicao> {
	
//	private static final Logger logger = LoggerFactory.getLogger(UsuarioModificaEdicaoDAO.class);
	
	@SuppressWarnings("unchecked")
	public List<Edicao> edicoesModificadasPorUsuario(Integer idUsuario) throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createSQLQuery("select * from edicao E"
				+ "inner join smarteditorial.usuariomodificaedicao UME on (E.id_edicao = UME.id_edicao)"
				+ "inner join usuario U on (U.id_usuario = UME.id_usuario)"
				+ "where U.id_usuario = :idUsuario").setParameter("idUsuario", idUsuario).list();
	}
}
