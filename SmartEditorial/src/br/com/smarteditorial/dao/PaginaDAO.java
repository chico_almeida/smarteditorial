package br.com.smarteditorial.dao;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import br.com.smarteditorial.model.entities.Pagina;
import br.com.smarteditorial.model.entities.PaginaFluxo;

@Repository
public class PaginaDAO extends AbstractDAO<Pagina> {
	
	private static final Logger logger = LoggerFactory.getLogger(PaginaDAO.class);
	
	@SuppressWarnings("unchecked")
	public List<Pagina> listarPaginas() throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from Pagina").list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Pagina> listarPaginasPorUsuario(Integer idUsuario) throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from Pagina where id_usuario = :idUsuario").setParameter("idUsuario", idUsuario).list();
	}
	
	
	public Pagina paginaPorTitulo(String titulo) throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();  
		Pagina p = (Pagina) session.createQuery("from Pagina where titulo= :Titulo").setParameter("Titulo", titulo).uniqueResult();
		logger.info("Pagina carregada com sucesso");
		return p;
	}
	
	public Pagina paginaPorId(Integer id) throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();  
		Pagina p = (Pagina) session.get(Pagina.class, new Integer(id));
		logger.info("Pagina carregada com sucesso");
		return p;
	}
	@SuppressWarnings("unchecked")
	public List<Pagina> listarPaginasPorUsuarioForaDoFluxo(Integer idUsuario) throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from Pagina as pag "
				+ "where pag.idPagina not in "
				+ "(select pagf.idPaginaFluxo.pagina.idPagina "
				+ "from PaginaFluxo as pagf) "
				+ "and estado = 'aguardando' "
				+ "and id_usuario = :idUsuario").setParameter("idUsuario", idUsuario).list();
	}

	@SuppressWarnings("unchecked")
	public List<PaginaFluxo> paginasDoGrupoAtualNoFluxo() {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from PaginaFluxo as pagf "
				+ "where pagf.idPaginaFluxo.fluxo.idFluxo in "
				+ "(select flx.idFluxo "
				+ "from FluxoDeTrabalho as flx "
				+ "where flx.grupoDestino.idGrupo in "
				+ "(select grp.idGrupo "
				+ "from Grupo as grp))").list();
	}

	@SuppressWarnings("unchecked")
	public List<PaginaFluxo> listarPaginasDoUsuarioEmFluxo(Integer idUsuario) {
		Session session = this.sessionFactory.getCurrentSession();  
		return session.createQuery("from PaginaFluxo as pagf"
				+ " where pagf.idPaginaFluxo.pagina.idPagina in"
				+ "	(select pag.idPagina"
				+ " from Pagina as pag where pag.usuario.idUsuario = :idUsuario1) "
				+ " and pagf.idPaginaFluxo.pagina.usuario.idUsuario = :idUsuario2").setParameter("idUsuario1", idUsuario).setParameter("idUsuario2", idUsuario).list();
	}
	@SuppressWarnings("unchecked")
	public List<Pagina> listarPaginasDoUsuarioFinalizadas(Integer idUsuario) {
		Session session = this.sessionFactory.getCurrentSession();  
		return session.createQuery("from Pagina where estado = 'finalizado' and id_usuario = :idUsuario").setParameter("idUsuario", idUsuario).list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Pagina> listarPaginasFinalizadasDaEdicao(Integer idEdicao) {
		Session session = this.sessionFactory.getCurrentSession();  
		return session.createQuery("from Pagina where estado = 'finalizado' and id_edicao = :idEdicao").setParameter("idEdicao", idEdicao).list();
	}
}
