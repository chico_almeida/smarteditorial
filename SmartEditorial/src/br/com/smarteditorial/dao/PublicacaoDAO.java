package br.com.smarteditorial.dao;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import br.com.smarteditorial.model.entities.Publicacao;

@Repository
public class PublicacaoDAO extends AbstractDAO<Publicacao> {
	
	@SuppressWarnings("unchecked")
	public List<Publicacao> listarPublicacoes () throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from Publicacao").list();
	}
	
	public Publicacao publicacaoPorTitulo(String titulo) throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();
		return (Publicacao) session.createQuery("from Publicacao where titulo = :titulo").setParameter("titulo", titulo).uniqueResult();
	}
	
	public Publicacao publicacaoPorId(Integer id) throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();
		return (Publicacao) session.get(Publicacao.class, new Integer(id));
	}
	
}
