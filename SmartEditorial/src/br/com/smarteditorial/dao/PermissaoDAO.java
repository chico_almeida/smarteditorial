package br.com.smarteditorial.dao;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import br.com.smarteditorial.model.entities.Permissao;

@Repository
public class PermissaoDAO extends AbstractDAO<Permissao> {
		
	public Permissao permissaoPorTipo(String tipo) throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();      
		return (Permissao) session.get(Permissao.class, new String(tipo));
	}
	
	public Permissao permissaoPorId(Integer id) throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();
		return (Permissao) session.get(Permissao.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public List<Permissao> listarPermissao() throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from Permissao").list();
	}

}
