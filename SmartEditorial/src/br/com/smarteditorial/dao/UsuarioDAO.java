package br.com.smarteditorial.dao;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import br.com.smarteditorial.model.entities.MapElementos;
import br.com.smarteditorial.model.entities.Usuario;

@Repository
public class UsuarioDAO extends AbstractDAO<Usuario> {
	
	private static final Logger logger = LoggerFactory.getLogger(UsuarioDAO.class);

	@SuppressWarnings("unchecked")
	public List<Usuario> listarUsuarios() throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();
		List<Usuario> listarUsuarios = session.createQuery("from Usuario").list();
		return listarUsuarios;
	}
	public Usuario usuarioPorId(int idUsuario) throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();      
		Usuario u = (Usuario) session.get(Usuario.class, new Integer(idUsuario));
		logger.info("Usuario carregado com sucesso");
		return u;
	}
	public Usuario usuarioPorEmail(String email) throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();
		Usuario u = (Usuario) session.createQuery("from Usuario where email= :Email").setParameter("Email", email).uniqueResult();
		return u;
	}
	
	@SuppressWarnings("unchecked")
	public List<Integer> gruposDoUsuario(Integer idUsuario) {
		Session session = this.sessionFactory.getCurrentSession();
		return (List<Integer>) session.createSQLQuery("select G.id_grupo from grupo G "
				+ "inner join grupo_usuario GU on (G.id_grupo = GU.id_grupo) "
				+ "inner join usuario U on (GU.id_usuario = U.id_usuario) "
				+ "where U.id_usuario = :idUsuario").setParameter("idUsuario", idUsuario).list();
	}
	
	@SuppressWarnings("unchecked")
	public List<MapElementos> listarElementosEmFluxoParaOUsuario(String grupos, Integer idUsuario) {
		Session session  = this.sessionFactory.getCurrentSession();
		List<MapElementos> list = session.createSQLQuery(""
				+ "SELECT I.id_imagem AS 'ID_ELEMENTO',I.titulo, I.estado, I.caminho, FI.comentario, P.id_publicacao AS 'idPublicacao', P.titulo AS 'publicacao', E.titulo AS 'edicao', C.titulo AS 'categoria', FDT.id_fluxo, FDT.ordem, FDT.tipoElemento, U.nome AS 'usuario' from imagem I "
				+ "inner join fluxo_imagem FI on (I.id_imagem = FI.id_imagem) AND FI.id_usuario = :idUsuario "
				+ "inner join fluxo_de_trabalho FDT on (FI.id_fluxo = FDT.id_fluxo) "
				+ "inner join usuario U on (I.id_usuario = U.id_usuario) "
				+ "inner join publicacao P on(FDT.id_publicacao = P.id_publicacao) "
				+ "inner join edicao E on(I.id_edicao = E.id_edicao) "
				+ "inner join categoria C on (I.id_categoria = C.id_categoria) "
				+ "where id_grupo_destino IN ("+grupos+") "
				+ "union "
				+ "SELECT A.id_anuncio AS 'ID_ELEMENTO', A.titulo, A.estado, A.caminho, FA.comentario, P.id_publicacao AS 'idPublicacao', P.titulo AS 'publicacao', E.titulo AS 'edicao', C.titulo AS 'categoria', FDT.id_fluxo, FDT.ordem, FDT.tipoElemento, U.nome AS 'usuario' from Anuncio A "
				+ "inner join fluxo_anuncio FA ON (A.id_anuncio = FA.id_anuncio) AND FA.id_usuario = :idUsuario "
				+ "inner join fluxo_de_trabalho FDT on (FA.id_fluxo = FDT.id_fluxo) "
				+ "inner join usuario U on (A.id_usuario = U.id_usuario) "
				+ "inner join publicacao P on(FDT.id_publicacao = P.id_publicacao) "
				+ "inner join edicao E on(A.id_edicao = E.id_edicao) "
				+ "inner join categoria C on (A.id_categoria = C.id_categoria) "
				+ "where id_grupo_destino IN ("+grupos+") "
				+ "UNION "
				+ "SELECT AR.id_artigo AS 'ID_ELEMENTO', AR.titulo, AR.estado, null, FAR.comentario, P.id_publicacao AS 'idPublicacao', P.titulo AS 'publicacao', E.titulo AS 'edicao', C.titulo AS 'categoria', FDT.id_fluxo, FDT.ordem, FDT.tipoElemento, U.nome AS 'usuario' from Artigo AR "
				+ "inner join fluxo_artigo FAR ON (AR.id_artigo = FAR.id_artigo) AND FAR.id_usuario = :idUsuario "
				+ "inner join fluxo_de_trabalho FDT on (FAR.id_fluxo = FDT.id_fluxo) "
				+ "inner join usuario U on (AR.id_usuario = U.id_usuario) "
				+ "inner join publicacao P on(FDT.id_publicacao = P.id_publicacao) "
				+ "inner join edicao E on(AR.id_edicao = E.id_edicao) "
				+ "inner join categoria C on (AR.id_categoria = C.id_categoria) "
				+ "where id_grupo_destino IN ("+grupos+") "
				+ "UNION "
				+ "SELECT P.id_pagina AS 'ID_ELEMENTO', P.titulo, P.estado, P.caminho, FP.comentario, PU.id_publicacao AS 'idPublicacao', PU.titulo AS 'publicacao', E.titulo AS 'edicao', C.titulo AS 'categoria', FDT.id_fluxo, FDT.ordem, FDT.tipoElemento, U.nome AS 'usuario' from Pagina P "
				+ "inner join fluxo_pagina FP ON (P.id_pagina = FP.id_pagina) AND FP.id_usuario = :idUsuario "
				+ "inner join fluxo_de_trabalho FDT on (FP.id_fluxo = FDT.id_fluxo) "
				+ "inner join usuario U on (P.id_usuario = U.id_usuario) "
				+ "inner join publicacao PU on(FDT.id_publicacao = PU.id_publicacao) "
				+ "inner join edicao E on(P.id_edicao = E.id_edicao) "
				+ "inner join categoria C on (P.id_categoria = C.id_categoria) "
				+ "where id_grupo_destino IN ("+grupos+")").addEntity(MapElementos.class).setParameter("idUsuario", idUsuario).list();
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<MapElementos> listarElementosEmFluxoParaOGrupo(String grupos) {
		Session session  = this.sessionFactory.getCurrentSession();
		List<MapElementos> list = session.createSQLQuery(""
				+ "SELECT I.id_imagem AS 'ID_ELEMENTO',I.titulo, I.estado, I.caminho, FI.comentario, P.id_publicacao AS 'idPublicacao', P.titulo AS 'publicacao', E.titulo AS 'edicao', C.titulo AS 'categoria', FDT.id_fluxo, FDT.ordem, FDT.tipoElemento, U.nome AS 'usuario' from imagem I "
				+ "inner join fluxo_imagem FI on (I.id_imagem = FI.id_imagem) AND FI.id_usuario IS NULL "
				+ "inner join fluxo_de_trabalho FDT on (FI.id_fluxo = FDT.id_fluxo) "
				+ "inner join usuario U on (I.id_usuario = U.id_usuario) "
				+ "inner join publicacao P on(FDT.id_publicacao = P.id_publicacao) "
				+ "inner join edicao E on(I.id_edicao = E.id_edicao) "
				+ "inner join categoria C on (I.id_categoria = C.id_categoria) "
				+ "where id_grupo_destino IN ("+grupos+") "
				+ "union "
				+ "SELECT A.id_anuncio AS 'ID_ELEMENTO', A.titulo, A.estado, A.caminho, FA.comentario, P.id_publicacao AS 'idPublicacao', P.titulo AS 'publicacao', E.titulo AS 'edicao', C.titulo AS 'categoria', FDT.id_fluxo, FDT.ordem, FDT.tipoElemento, U.nome AS 'usuario' from Anuncio A "
				+ "inner join fluxo_anuncio FA ON (A.id_anuncio = FA.id_anuncio) AND FA.id_usuario IS NULL "
				+ "inner join fluxo_de_trabalho FDT on (FA.id_fluxo = FDT.id_fluxo) "
				+ "inner join usuario U on (A.id_usuario = U.id_usuario) "
				+ "inner join publicacao P on(FDT.id_publicacao = P.id_publicacao) "
				+ "inner join edicao E on(A.id_edicao = E.id_edicao) "
				+ "inner join categoria C on (A.id_categoria = C.id_categoria) "
				+ "where id_grupo_destino IN ("+grupos+") "
				+ "UNION "
				+ "SELECT AR.id_artigo AS 'ID_ELEMENTO', AR.titulo, AR.estado, null, FAR.comentario, P.id_publicacao AS 'idPublicacao', P.titulo AS 'publicacao', E.titulo AS 'edicao', C.titulo AS 'categoria', FDT.id_fluxo, FDT.ordem, FDT.tipoElemento, U.nome AS 'usuario' from Artigo AR "
				+ "inner join fluxo_artigo FAR ON (AR.id_artigo = FAR.id_artigo) AND FAR.id_usuario IS NULL "
				+ "inner join fluxo_de_trabalho FDT on (FAR.id_fluxo = FDT.id_fluxo) "
				+ "inner join usuario U on (AR.id_usuario = U.id_usuario) "
				+ "inner join publicacao P on(FDT.id_publicacao = P.id_publicacao) "
				+ "inner join edicao E on(AR.id_edicao = E.id_edicao) "
				+ "inner join categoria C on (AR.id_categoria = C.id_categoria) "
				+ "where id_grupo_destino IN ("+grupos+") "
				+ "UNION "
				+ "SELECT P.id_pagina AS 'ID_ELEMENTO', P.titulo, P.estado, P.caminho, FP.comentario, PU.id_publicacao AS 'idPublicacao', PU.titulo AS 'publicacao', E.titulo AS 'edicao', C.titulo AS 'categoria', FDT.id_fluxo, FDT.ordem, FDT.tipoElemento, U.nome AS 'usuario' from Pagina P "
				+ "inner join fluxo_pagina FP ON (P.id_pagina = FP.id_pagina) AND FP.id_usuario IS NULL "
				+ "inner join fluxo_de_trabalho FDT on (FP.id_fluxo = FDT.id_fluxo) "
				+ "inner join usuario U on (P.id_usuario = U.id_usuario) "
				+ "inner join publicacao PU on(FDT.id_publicacao = PU.id_publicacao) "
				+ "inner join edicao E on(P.id_edicao = E.id_edicao) "
				+ "inner join categoria C on (P.id_categoria = C.id_categoria) "
				+ "where id_grupo_destino IN ("+grupos+")").addEntity(MapElementos.class).list();
		return list;
	}
	
}
