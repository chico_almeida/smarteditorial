package br.com.smarteditorial.dao;

import java.util.List;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import br.com.smarteditorial.model.entities.ImagemFluxo;

@Repository
public class ImagemFluxoDAO extends AbstractDAO<ImagemFluxo> {
	
	@SuppressWarnings("unchecked")
	public List<ImagemFluxo> fluxosImagem() {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("FROM ImagemFluxo").list();
	}
	
	public ImagemFluxo imagemFluxoPorIds(Integer idFluxo, Integer idImagem) {
		Session session= this.sessionFactory.getCurrentSession();
		return (ImagemFluxo) session.createQuery("FROM ImagemFluxo "
				+ "where idImagemFluxo.imagem.idImagem = :idImagem "
				+ "and idImagemFluxo.fluxo.idFluxo= :idFluxo")
				.setParameter("idImagem", idImagem)
				.setParameter("idFluxo", idFluxo).uniqueResult();
	}
}
