package br.com.smarteditorial.dao;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import br.com.smarteditorial.model.entities.Imagem;
import br.com.smarteditorial.model.entities.ImagemFluxo;

@Repository
public class ImagemDAO extends AbstractDAO<Imagem> {

	private static final Logger logger = LoggerFactory.getLogger(ImagemDAO.class);


	@SuppressWarnings("unchecked")
	public List<Imagem> listarImagens() throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from Imagem").list();
	}

	@SuppressWarnings("unchecked")
	public List<Imagem> listarImagensPorUsuario(Integer idUsuario) throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from Imagem where id_usuario = :idUsuario").setParameter("idUsuario", idUsuario).list();
	}

	public Imagem imagemPorTitulo(String titulo) throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();  
		Imagem i = (Imagem) session.createQuery("from Imagem where titulo= :Titulo").setParameter("Titulo", titulo).uniqueResult();
		logger.info("Imagem carregada com sucesso");
		return i;
	}

	public Imagem imagemPorId(Integer id) throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();  
		Imagem i = (Imagem) session.get(Imagem.class, new Integer(id));
		logger.info("Imagem carregada com sucesso");
		return i;
	}
	@SuppressWarnings("unchecked")
	public List<ImagemFluxo> imagensDoGrupoAtualNoFluxo() {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from ImagemFluxo as imgF "
				+ "where imgF.idImagemFluxo.fluxo.idFluxo in "
				+ "(select flx.idFluxo "
				+ "from FluxoDeTrabalho as flx "
				+ "where flx.grupoDestino.idGrupo in "
				+ "(select grp.idGrupo "
				+ "from Grupo as grp))").list();
	}
	@SuppressWarnings("unchecked")
	public List<Imagem> listarImagensPorUsuarioForaDoFluxo(Integer idUsuario) throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from Imagem as img "
				+ "where img.idImagem not in "
				+ "(select imgf.idImagemFluxo.imagem.idImagem "
				+ "from ImagemFluxo as imgf) "
				+ "and estado = 'aguardando' "
				+ "and id_usuario = :idUsuario").setParameter("idUsuario", idUsuario).list();
	}

	@SuppressWarnings("unchecked")
	public List<ImagemFluxo> listarImagensDoUsuarioEmFluxo(Integer idUsuario) {
		Session session = this.sessionFactory.getCurrentSession();  
		return session.createQuery("from ImagemFluxo as imgF"
				+ " where imgF.idImagemFluxo.imagem.idImagem in"
				+ "	(select img.idImagem"
				+ " from Imagem as img where img.usuario.idUsuario = :idUsuario1) "
				+ " and imgF.idImagemFluxo.imagem.usuario.idUsuario = :idUsuario2").setParameter("idUsuario1", idUsuario).setParameter("idUsuario2", idUsuario).list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Imagem> listarImagensDoUsuarioFinalizadas(Integer idUsuario) {
		Session session = this.sessionFactory.getCurrentSession();  
		return session.createQuery("from Imagem where estado = 'finalizado' and id_usuario = :idUsuario").setParameter("idUsuario", idUsuario).list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Imagem> listarImagensFinalizadasDaEdicao(Integer idEdicao) {
		Session session = this.sessionFactory.getCurrentSession();  
		return session.createQuery("from Imagem where estado = 'finalizado' and id_edicao = :idEdicao").setParameter("idEdicao", idEdicao).list();
	}
}
