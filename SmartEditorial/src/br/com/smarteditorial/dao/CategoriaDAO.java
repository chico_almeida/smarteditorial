package br.com.smarteditorial.dao;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import br.com.smarteditorial.model.entities.Categoria;

@Repository
public class CategoriaDAO extends AbstractDAO<Categoria>{

	@SuppressWarnings("unchecked")
	public List<Categoria> listarCategorias () throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from Categoria").list();
	}
	
	public Categoria categoriaPorTitulo(String titulo) throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();
		return (Categoria) session.createQuery("from Categoria where titulo = :titulo").setParameter("titulo", titulo).uniqueResult();
	}
	
	public Categoria categoriaPorId(Integer id) throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();
		return (Categoria) session.get(Categoria.class, new Integer(id));
	}
	
	@SuppressWarnings("unchecked")
	public List<Categoria> categoriaPorPublicacao(Integer id) throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();  
		return session.createQuery("from Categoria where id_publicacao= :id").setParameter("id", id).list();
	} 

}
