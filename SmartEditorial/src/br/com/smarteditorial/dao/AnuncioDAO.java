package br.com.smarteditorial.dao;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import br.com.smarteditorial.model.entities.Anuncio;
import br.com.smarteditorial.model.entities.AnuncioFluxo;

@Repository
public class AnuncioDAO extends AbstractDAO<Anuncio> {
	
	private static final Logger logger = LoggerFactory.getLogger(AnuncioDAO.class);
	
	@SuppressWarnings("unchecked")
	public List<Anuncio> listarAnuncios() throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from Anuncio").list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Anuncio> listarAnunciosPorUsuario(Integer idUsuario) throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from Anuncio where id_usuario = :idUsuario").setParameter("idUsuario", idUsuario).list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Anuncio> listarAnunciosDoUsuarioAguardando(Integer idUsuario) {
		Session session = this.sessionFactory.getCurrentSession();  
		return session.createQuery("from Anuncio where estado = 'aguardando' and id_usuario = :idUsuario").setParameter("idUsuario", idUsuario).list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Anuncio> listarAnunciosDoUsuarioFinalizadas(Integer idUsuario) {
		Session session = this.sessionFactory.getCurrentSession();  
		return session.createQuery("from Anuncio where estado = 'finalizado' and id_usuario = :idUsuario").setParameter("idUsuario", idUsuario).list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Anuncio> listarAnunciosFinalizadasDaEdicao(Integer idEdicao) {
		Session session = this.sessionFactory.getCurrentSession();  
		return session.createQuery("from Anuncio where estado = 'finalizado' and id_edicao = :idEdicao").setParameter("idEdicao", idEdicao).list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Anuncio> listarAnunciosDoUsuarioAprovado(Integer idUsuario) {
		Session session = this.sessionFactory.getCurrentSession();  
		return session.createQuery("from Anuncio where estado = 'aprovado' and id_usuario = :idUsuario").setParameter("idUsuario", idUsuario).list();
	}
	
	
	public Anuncio anuncioPorTitulo(String titulo) throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();  
		Anuncio a = (Anuncio) session.createQuery("from Anuncio where titulo= :Titulo").setParameter("Titulo", titulo).uniqueResult();
		logger.info("Anuncio carregado com sucesso");
		return a;
	}
	
	public Anuncio anuncioPorId(Integer id) throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();  
		Anuncio a = (Anuncio) session.get(Anuncio.class, new Integer(id));
		logger.info("Anuncio carregado com sucesso");
		return a;
	}
	
	@SuppressWarnings("unchecked")
	public List<AnuncioFluxo> anunciosDoGrupoAtualNoFluxo() {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from AnuncioFluxo as anuF "
				+ "where anuF.idAnuncioFluxo.fluxo.idFluxo in "
				+ "(select flx.idFluxo "
				+ "from FluxoDeTrabalho as flx "
				+ "where flx.grupoDestino.idGrupo in "
				+ "(select grp.idGrupo "
				+ "from Grupo as grp))").list();
	}
	@SuppressWarnings("unchecked")
	public List<Anuncio> listarAnunciosPorUsuarioForaDoFluxo(Integer idUsuario) throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from Anuncio as anu "
				+ "where anu.idAnuncio not in "
				+ "(select anuf.idAnuncioFluxo.anuncio.idAnuncio "
				+ "from AnuncioFluxo as anuf) "
				+ "and estado = 'aguardando' "
				+ "and id_usuario = :idUsuario").setParameter("idUsuario", idUsuario).list();
	}

	@SuppressWarnings("unchecked")
	public List<AnuncioFluxo> listarAnunciosDoUsuarioEmFluxo(Integer idUsuario) {
		Session session = this.sessionFactory.getCurrentSession();  
		return session.createQuery("from AnuncioFluxo as anuf"
				+ " where anuf.idAnuncioFluxo.anuncio.idAnuncio in"
				+ "	(select anu.idAnuncio"
				+ " from Anuncio as anu where anu.usuario.idUsuario = :idUsuario1) "
				+ " and anuf.idAnuncioFluxo.anuncio.usuario.idUsuario = :idUsuario2").setParameter("idUsuario1", idUsuario).setParameter("idUsuario2", idUsuario).list();
	}

}
