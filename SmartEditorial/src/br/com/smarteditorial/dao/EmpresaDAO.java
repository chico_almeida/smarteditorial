package br.com.smarteditorial.dao;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import br.com.smarteditorial.model.entities.Empresa;

@Repository
public class EmpresaDAO extends AbstractDAO<Empresa>  {

	@SuppressWarnings("unchecked")
	public List<Empresa> listarEmpresas () throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from Empresa").list();
	}
	
	public Empresa empresaPorNomeEmpresa(String empresa) throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();
		return (Empresa) session.createQuery("from Empresa where empresa = :empresa").setParameter("empresa", empresa).uniqueResult();
	}
	
	public Empresa empresaPorId(Integer id) throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();
		return (Empresa) session.get(Empresa.class, new Integer(id));
	}
	
	public Empresa empresaPorContato(String contato) throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();
		return (Empresa) session.createQuery("from Empresa where contatoComercial = :contato").setParameter("contato", contato).uniqueResult();
	}
	
}
