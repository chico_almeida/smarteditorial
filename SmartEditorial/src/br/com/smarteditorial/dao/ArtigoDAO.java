package br.com.smarteditorial.dao;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import br.com.smarteditorial.model.entities.Artigo;
import br.com.smarteditorial.model.entities.ArtigoFluxo;

@Repository
public class ArtigoDAO extends AbstractDAO<Artigo> {
	
	private static final Logger logger = LoggerFactory.getLogger(ArtigoDAO.class);
	
	@SuppressWarnings("unchecked")
	public List<Artigo> listarArtigos() throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from Artigo").list();
	}
	
	public Artigo artigoPorTitulo(String titulo) throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();  
		Artigo a = (Artigo) session.createQuery("from Artigo where titulo= :Titulo").setParameter("Titulo", titulo).uniqueResult();
		logger.info("Artigo carregado com sucesso");
		return a;
	}
	
	public Artigo artigoPorId(Integer id) throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();  
		Artigo a = (Artigo) session.get(Artigo.class, new Integer(id));
		logger.info("Artigo carregado com sucesso");
		return a;
	}
	
	@SuppressWarnings("unchecked")
	public List<Artigo> artigoPorUsuario(Integer idUsuario) throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();
		logger.info("Artigos por usuario carregados com sucesso");
		return session.createQuery("from Artigo where id_usuario = :id").setParameter("id", idUsuario).list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Artigo> listarArtigosDoUsuarioAguardando(Integer idUsuario) {
		Session session = this.sessionFactory.getCurrentSession();  
		return session.createQuery("from Artigo where estado = 'aguardando' and id_usuario = :idUsuario").setParameter("idUsuario", idUsuario).list();
	}
	@SuppressWarnings("unchecked")
	public List<Artigo> listarArtigosDoUsuarioFinalizadas(Integer idUsuario) {
		Session session = this.sessionFactory.getCurrentSession();  
		return session.createQuery("from Artigo where estado = 'finalizado' and id_usuario = :idUsuario").setParameter("idUsuario", idUsuario).list();
	}
	@SuppressWarnings("unchecked")
	public List<Artigo> listarArtigosDoUsuarioAprovado(Integer idUsuario) {
		Session session = this.sessionFactory.getCurrentSession();  
		return session.createQuery("from Artigo where estado = 'aprovado' and id_usuario = :idUsuario").setParameter("idUsuario", idUsuario).list();
	}
	
	@SuppressWarnings("unchecked")
	public List<ArtigoFluxo> artigosDoGrupoAtualNoFluxo() {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from ArtigoFluxo as artf "
				+ "where artf.idArtigoFluxo.fluxo.idFluxo in "
				+ "(select flx.idFluxo "
				+ "from FluxoDeTrabalho as flx "
				+ "where flx.grupoDestino.idGrupo in "
				+ "(select grp.idGrupo "
				+ "from Grupo as grp))").list();
	}
	@SuppressWarnings("unchecked")
	public List<Artigo> listarArtigosPorUsuarioForaDoFluxo(Integer idUsuario) throws SQLException {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from Artigo as art "
				+ "where art.idArtigo not in "
				+ "(select artf.idArtigoFluxo.artigo.idArtigo "
				+ "from ArtigoFluxo as artf) "
				+ "and estado = 'aguardando' "
				+ "and id_usuario = :idUsuario").setParameter("idUsuario", idUsuario).list();
	}

	@SuppressWarnings("unchecked")
	public List<ArtigoFluxo> listarArtigosDoUsuarioEmFluxo(Integer idUsuario) {
		Session session = this.sessionFactory.getCurrentSession();  
		return session.createQuery("from ArtigoFluxo as artf"
				+ " where artf.idArtigoFluxo.artigo.idArtigo in"
				+ "	(select art.idArtigo"
				+ " from Artigo as art where art.usuario.idUsuario = :idUsuario1) "
				+ " and artf.idArtigoFluxo.artigo.usuario.idUsuario = :idUsuario2").setParameter("idUsuario1", idUsuario).setParameter("idUsuario2", idUsuario).list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Artigo> listarArtigosFinalizadasDaEdicao(Integer idEdicao) {
		Session session = this.sessionFactory.getCurrentSession();  
		return session.createQuery("from Artigo where estado = 'finalizado' and id_edicao = :idEdicao").setParameter("idEdicao", idEdicao).list();
	}
	
}
