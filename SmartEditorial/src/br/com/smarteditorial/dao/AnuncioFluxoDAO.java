package br.com.smarteditorial.dao;

import java.util.List;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import br.com.smarteditorial.model.entities.AnuncioFluxo;

@Repository
public class AnuncioFluxoDAO extends AbstractDAO<AnuncioFluxo> {
	
	@SuppressWarnings("unchecked")
	public List<AnuncioFluxo> fluxosAnuncio() {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("FROM AnuncioFluxo").list();
	}
	public AnuncioFluxo anuncioFluxoPorIds(Integer idFluxo, Integer idAnuncio) {
		Session session= this.sessionFactory.getCurrentSession();
		return (AnuncioFluxo) session.createQuery("FROM AnuncioFluxo "
				+ "where idAnuncioFluxo.anuncio.idAnuncio = :idAnuncio "
				+ "and idAnuncioFluxo.fluxo.idFluxo= :idFluxo")
				.setParameter("idAnuncio", idAnuncio)
				.setParameter("idFluxo", idFluxo).uniqueResult();
	}
}
