package br.com.smarteditorial;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.smarteditorial.model.bo.ImagemBO;
import br.com.smarteditorial.model.bo.UsuarioBO;
import br.com.smarteditorial.model.entities.Usuario;
import br.com.smarteditorial.util.SystemConfiguration;

@Controller
public class HomeUsuarioController {
	
	@Autowired
	ImagemBO imagemBO;
	
	@Autowired
	UsuarioBO usuarioBO;
	
	@Autowired
	SystemConfiguration systemConfiguration;
	
	/*@RequestMapping("/inicio")
	public String homeInterna() {
		return "/restrito/home";
	}*/
	
	@RequestMapping("/inicio")
	public String carregarFeedElementosDoUsuario(Model model) {
		model.addAttribute("caixaDoGrupo", this.usuarioBO.pegarElementosDoGrupoDoGrupo(gruposDoUsuario(pegaUsuarioLogado())));
		model.addAttribute("caixaDoUsuario", this.usuarioBO.pegarElementosDoGrupoDoUsuario(gruposDoUsuario(pegaUsuarioLogado()), pegaUsuarioLogado()));
		model.addAttribute("contextPath", this.systemConfiguration.getUrlAcervo());
		return "/restrito/home";
	}
	
	public Usuario pegaUsuarioLogado() {
		UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return this.usuarioBO.pegaUsuarioPorEmail(userDetails.getUsername());
	}
	
	public String gruposDoUsuario(Usuario u) {
		List<Integer> gruposLista =  this.usuarioBO.pegaGruposDoUsuario(u);
		String grupos = new String();
		for (Integer idGrupo : gruposLista) {
			if (gruposLista.get(gruposLista.size() - 1) == idGrupo) {
				grupos += idGrupo.toString();
			} else {
				grupos += idGrupo.toString()+", ";
			}
		}
		return grupos;
	}
}
