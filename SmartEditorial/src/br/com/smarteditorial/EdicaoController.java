package br.com.smarteditorial;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.smarteditorial.model.bo.AnuncioBO;
import br.com.smarteditorial.model.bo.ArtigoBO;
import br.com.smarteditorial.model.bo.EdicaoBO;
import br.com.smarteditorial.model.bo.ImagemBO;
import br.com.smarteditorial.model.bo.PaginaBO;
import br.com.smarteditorial.model.bo.PublicacaoBO;
import br.com.smarteditorial.model.bo.UsuarioBO;
import br.com.smarteditorial.model.entities.Edicao;
import br.com.smarteditorial.util.SystemConfiguration;

@Controller
public class EdicaoController {

	@Autowired
	private EdicaoBO edicaoBO;
	@Autowired
	private PublicacaoBO publicacaoBO;
	@Autowired
	private UsuarioBO usuarioBO;
	@Autowired
	private ImagemBO imagemBO;
	@Autowired
	private PaginaBO paginaBO;
	@Autowired
	private ArtigoBO artigoBO;
	@Autowired
	private AnuncioBO anuncioBO;
	@Autowired
	SystemConfiguration systemConfiguration;

	@RequestMapping(value="/edicoes/cadastro", method = RequestMethod.POST)
	public String cadastroEdicao(@ModelAttribute("idPublicacao") String idPublicacao, Model model) {
		model.addAttribute("edicao", new Edicao());
		model.addAttribute("idPub", idPublicacao);
		return "/edicoes/cadastro";
	}

	@RequestMapping(value="/edicoes/cadastro/criar",  method = RequestMethod.POST)
	public String criarEdicao(@ModelAttribute("edicao") Edicao e, @ModelAttribute("idPub") String idPub) {
		UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		e.setUsuario(this.usuarioBO.pegaUsuarioPorEmail(userDetails.getUsername()));
		e.setPublicacao(this.publicacaoBO.pegaPublicacaoID(Integer.parseInt(idPub)));
		this.edicaoBO.salvaEdicao(e);
		return "redirect:/publicacao?idPublicacao="+idPub;
	}

	@RequestMapping(value="/edicoes/cadastro/update",  method = RequestMethod.POST)
	public String atualizarEdicao(@ModelAttribute("edicao") Edicao e, @ModelAttribute("idPublicacao") String idPublicacao) {
		UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		e.setUsuario(this.usuarioBO.pegaUsuarioPorEmail(userDetails.getUsername()));
		e.setPublicacao(this.publicacaoBO.pegaPublicacaoID(Integer.parseInt(idPublicacao)));
		
		this.edicaoBO.atualizaEdicao(e);
		
		return "redirect:/publicacao?idPublicacao="+idPublicacao;
	}

	@RequestMapping(value= "/edicoes", method = RequestMethod.POST)
	public String listarEdicoes(Model model) {
		model.addAttribute("listaDeEdicoes", this.edicaoBO.pegaEdicoes());
		return "/edicoes/edicoes";
	}

	@RequestMapping(value="/edicoes/remover", method = RequestMethod.POST)
	public @ResponseBody String removerEdicao(@ModelAttribute("idEdicao") String id, Model model) {
		
		try {
			this.edicaoBO.excluirEdicao(this.edicaoBO.pegaEdicaoId(Integer.parseInt(id)));
			return "sucesso";
		} catch (DataIntegrityViolationException | ConstraintViolationException
				| NumberFormatException e) {
			return "vinculo";
		} catch (SQLException e) {
			return "vinculo";
		}
	}

	@RequestMapping(value="/edicoes/editar", method = RequestMethod.POST)
	public String editarEdicao(@ModelAttribute("idEdicao") String idEdicao, 
			@ModelAttribute("idPublicacao") String idPublicacao, 
			Model model) {
		model.addAttribute("edicao", this.edicaoBO.pegaEdicaoId(Integer.parseInt(idEdicao)));
		model.addAttribute("idPub", idPublicacao);
		return "/edicoes/cadastro";
	}

	@RequestMapping(value="/edicoes/validar", method = RequestMethod.POST)
	public @ResponseBody String existePublicacao(@ModelAttribute("titulo") String tituloEdicao, @ModelAttribute("publicacao") String idPublicacao) {
		System.out.println("TITULO"+tituloEdicao);
		System.out.println("IDP"+idPublicacao);
		List<Edicao> eds = edicaoBO.pegaEdicaoPorPublicacao(Integer.parseInt(idPublicacao));
		for (Edicao edicao : eds) {
			if (edicao.getTitulo().toLowerCase().equals(tituloEdicao.toLowerCase())) {
				return "true";
			}
		}
		return "false";
	}
	
	@RequestMapping(value="/publicacao/edicao", method=RequestMethod.GET)
	public String internaEdicao(@RequestParam("idEdicao") String idEdicao, Model model) {
		model.addAttribute("contextPath", this.systemConfiguration.getUrlAcervo());
		model.addAttribute("listaImagensFinalizadas", this.imagemBO.pegaImagensFinalidasDaEdicao(Integer.parseInt(idEdicao)));
		model.addAttribute("listaPaginasFinalizadas", this.paginaBO.pegaPaginasFinalidasDaEdicao(Integer.parseInt(idEdicao)));
		model.addAttribute("listaArtigosFinalizadas", this.artigoBO.pegaArtigosFinalidosDaEdicao(Integer.parseInt(idEdicao)));
		model.addAttribute("listaAnunciosFinalizadas", this.anuncioBO.penaAnunciosFinalidoDaEdicao(Integer.parseInt(idEdicao)));
		return "/edicoes/interna";
	}
}
