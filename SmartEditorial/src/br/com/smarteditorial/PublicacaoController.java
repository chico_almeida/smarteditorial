package br.com.smarteditorial;

import java.sql.SQLException;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.smarteditorial.model.bo.CategoriaBO;
import br.com.smarteditorial.model.bo.EdicaoBO;
import br.com.smarteditorial.model.bo.FluxoDeTrabalhoBO;
import br.com.smarteditorial.model.bo.GrupoBO;
import br.com.smarteditorial.model.bo.PublicacaoBO;
import br.com.smarteditorial.model.entities.Publicacao;
import br.com.smarteditorial.util.SystemConfiguration;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

@Controller
public class PublicacaoController {

	@Autowired
	private PublicacaoBO publicacaoBO;
	@Autowired
	private GrupoBO grupoBO;
	@Autowired
	private SystemConfiguration sysConf;
	@Autowired
	private FluxoDeTrabalhoBO fluxoDeTrabalhoBO;
	@Autowired
	private EdicaoBO edicaoBO;
	@Autowired
	private CategoriaBO categoriaBO;
	
	
	@RequestMapping(value="/publicacao/cadastro", method = RequestMethod.POST)
	public String cadastrarPublicacao(Model model) {
		model.addAttribute("publicacao", new Publicacao());
		return "/publicacao/cadastro";
	}

	@RequestMapping(value="/publicacao/cadastro/criar", method = RequestMethod.POST)
	public String salvarPublicacao(@ModelAttribute("publicacao") Publicacao p) {
		this.publicacaoBO.salvaPublicacao(p);
		return "redirect:/publicacoes";
	}

	@RequestMapping(value="/publicacao/cadastro/update", method = RequestMethod.POST)
	public String atualizarPublicacao(@ModelAttribute("publicacao") Publicacao p) {
		this.publicacaoBO.atualizaPublicacao(p);
		return "redirect:/publicacoes";
	}
	@RequestMapping("/publicacoes")
	public String listaDePublicacoes(Model model) {
		model.addAttribute("listaDePublicacoes",this.publicacaoBO.pegaPublicacoes());
		return "/publicacao/publicacoes";
	}

	@RequestMapping(value="/publicacao/remover", method = RequestMethod.POST)
	public @ResponseBody String excluirPublicacao(@ModelAttribute("idPublicacao") String id, Model model) {
		try {
			this.publicacaoBO.excluirPublicacao(this.publicacaoBO.pegaPublicacaoID(Integer.parseInt(id)));
			return "sucesso";
		} catch (NumberFormatException e) {
			e.printStackTrace();
			return "Falha ao reconhecer a Edi��o, entre em contato com o Administrador. Erro ::"+e;
		} catch (ConstraintViolationException | DataIntegrityViolationException | MySQLIntegrityConstraintViolationException e) {
			e.printStackTrace();
			return "vinculo";
		} catch (SQLException e) {
			e.printStackTrace();
			return "Problemas com o Banco de Dados "+ e.getCause();
		}
	}

	@RequestMapping(value="/publicacao/editarPublicacao", method = RequestMethod.POST)
	public String editarPublicacao(@ModelAttribute("id") Integer id, Model model) {
		model.addAttribute("publicacao", this.publicacaoBO.pegaPublicacaoID(id));
		return "/publicacao/cadastro";
	}

	@RequestMapping(value="/publicacao/validar", method = RequestMethod.POST)
	public @ResponseBody String existePublicacao(@ModelAttribute("titulo") String tituloPublicacao) {
		System.out.println(tituloPublicacao);
		if (this.publicacaoBO.pegaPublicacaoTitulo(tituloPublicacao) == null) {
			return "false";
		} else {
			return "true";
		}
	}
	
	@RequestMapping("/publicacao")
	public String internaPublicacao(@RequestParam("idPublicacao") String id, Model model) {
		model.addAttribute("publicacao", this.publicacaoBO.pegaPublicacaoID(Integer.parseInt(id)));
		return "/publicacao/interna-publicacao";
	}
	
	@RequestMapping(value="/publicacao/pegaedicao", method = RequestMethod.POST)
	public String listaDeEdicoes(@ModelAttribute("idPublicacao")String idPublicacao, Model model) {
		model.addAttribute("listaEdicoes", this.edicaoBO.pegaEdicaoPorPublicacao(Integer.parseInt(idPublicacao)));
		return "/publicacao/edicao";
	}
	
	@RequestMapping(value="/publicacao/pegacategorias", method = RequestMethod.POST)
	public String listaDeCategorias(@ModelAttribute("idPublicacao")String idPublicacao, Model model) {
		model.addAttribute("listaCategorias", this.categoriaBO.pegaCategoriaPorPublicacao(Integer.parseInt(idPublicacao)));
		return "/publicacao/categorias";
	}

}