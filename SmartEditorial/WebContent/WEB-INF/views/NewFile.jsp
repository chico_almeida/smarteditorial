<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ page session="false" %>
<!DOCTYPE html>
<html>
<head>
<title>Inicio</title>
<%@ include file="componentes/header/head.jsp" %>
</head>
<body class="page-body">
	<div class="page-container horizontal-menu">
		<%@ include file="componentes/header/menu.jsp" %>
		<div class="main-content" id="main-content">
			<div class="container">
				<div class="row">
				
				</div>
				<%@ include file="componentes/footer/footer.jsp" %>
			</div>
		</div>
	</div>
</body>
</html>