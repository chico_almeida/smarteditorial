<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ page session="false" %>
<script>

	function existeCategoria(tituloCategoria, publicacao) {
		var methodURL = "/SmartEditorial/categorias/validar"
		$.ajax({
			type : "POST",
			url : methodURL,
			data : "tituloCategoria=" + tituloCategoria + "&publicacao=" + publicacao,
			success : function(response) {
				if (response == 'true') {
				
					var opts = {
							"closeButton": true,
							"debug": false,
							"positionClass": "toast-top-right",
							"onclick": null,
							"showDuration": "300",
							"hideDuration": "1000",
							"timeOut": "10000",
							"extendedTimeOut": "1000",
							"showEasing": "swing",
							"hideEasing": "linear",
							"showMethod": "fadeIn",
							"hideMethod": "fadeOut"
						};
						
						toastr.error("Uma categoria com este titulo j� existe nesta Publica��o.", "Puxa! Achamos um erro:", opts);
					document.getElementById("mySubmit").disabled = true;

				} else {
					document.getElementById("mySubmit").disabled = false;
					$("#retorno").html("");
				}
			},
			error : function(e) {
				alert("Falha ao validar titulo");
			}
		});
	}
	
	function ehDiferente(tituloCategoria, publicacao) {
		var titulo = document.getElementById("tituloUpdate").value;
		if (titulo != titulo|Categoria) {
			existeCategoria(titulo, publicacao);
		} else {
			return true;
		}
	}

</script>

<c:url var="addAction" value="/categorias/cadastro/criar"></c:url>
<c:url var="updateAction" value="/categorias/cadastro/update"></c:url>
	<c:if test="${empty categoria.titulo}">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header">
					Criar Categoria
				</h2>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="table-responsive">
							<form action="${addAction}" method="POST" class="form-horizontal" role="form">
								<input type="hidden" name="idPub" value="${idPub}" />
								
								<div class="form-group">
									<label for="titulo" class="col-sm-3 control-label">Titulo</label>
									<div class="col-sm-8">
										<input type="text" required="required" class="form-control" placeholder="Titulo da Categoria" id="tit" name="titulo" onchange="existeCategoria(this.value,'${idPub}')">
									</div>
								</div>
								<div class="form-group">
									<label for="titulo" class="col-sm-3 control-label">Descri��o</label>
									<div class="col-sm-8">
										<textarea class="form-control" rows="3" cols="25" name="descricao"></textarea>
									</div>
								</div>
								<div class="col-md-12">
									<p class="bs-example">
										<button type="submit" class="btn btn-green btn-icon icon-left" onsubmit="this.disabled=true;" id="mySubmit">
									    	Adicionar
									    	<i class="fa fa-floppy-o"></i>
									    </button>
									</p>
								</div>	
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</c:if>
	
	<c:if test="${!empty categoria.titulo}">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header">
					Atualizar Categoria
				</h2>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						Dados da Categoria
					</div>
					<!-- /.panel-heading -->
					<div class="panel-body">
						<div class="table-responsive">
							<form action="${updateAction}" method="POST" >
								<input type="hidden" name="idPub" value="${idPub}" />
								<input type="hidden" value="${categoria.idCategoria}" name="idCategoria" />
								<label class="control-label" for="formGroupInputLarge">
									Titulo
								</label>
								<br>
								<input type="text" class="form-control" value="${categoria.titulo}" name="titulo" id="tituloUpdate" onchange="ehDiferente(${categoria.titulo}, ${idPub})">
								<div id="retorno"></div>
								<br>
								<label class="control-label" for="formGroupInputLarge">
									Descri��o
								</label>
								<br>
								<textarea class="form-control" rows="3" cols="25" name="descricao">${categoria.descricao}</textarea>
								<br>
								<div id="mensagem" style="color: red;" class="col-lg-12"> </div>
								<div class="col-md-12">
									<input type="submit" onsubmit="this.disabled=true;" id="mySubmit"  value="Atualizar"/>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</c:if>
