<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page session="false" %>

<table class="table table-bordered table-striped datatable" id="table-2" style="width: 60% !important;">
	<thead>
		<tr>
			<th>Publica��o</th>
			<th>Edi��o</th>
			<th>Descri��o</th>
			<th>A��es</th>
		</tr>
	</thead>
	
	<tbody>
		<c:forEach items="${listaDeEdicoes}" var="edicao">
		<tr>
			<td>${edicao.publicacao.titulo}</td>
			<td>${edicao.titulo}</td>
			<td>${edicao.descricao}</td>
			<td>
				<a href="<c:url value='/edicoes/editarEdicao/${edicao.idEdicao}' />" class="btn btn-default btn-sm btn-icon icon-left">
					<i class="entypo-pencil"></i>
					Editar
				</a>
				
				<a href="<c:url value='/edicoes/removerEdicao/${edicao.idEdicao}' />" class="btn btn-danger btn-sm btn-icon icon-left">
					<i class="entypo-cancel"></i>
					Deletar
				</a>
			</td>
		</tr>
		</c:forEach>
		
	</tbody>
</table>
