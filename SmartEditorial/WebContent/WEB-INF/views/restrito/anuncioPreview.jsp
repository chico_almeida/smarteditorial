<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ page session="false" %>

<%@ include file="../componentes/header/head.jsp" %>
<div class="row">
	<form class="form-horizontal form-groups-bordered" role="form">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-body">
					<div class="table-responsive">
						<div class="form-group">
							<label class="col-sm-3 control-label">T�tulo</label>
							<div class="col-sm-8">
								<input class="form-control" type="text" name="titulo" readonly="readonly" value="${anuncio.titulo}"> 
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Empresa</label>
							<div class="col-sm-8">
								<input class="form-control" type="text" name="titulo" readonly="readonly" value="${anuncio.empresa.empresa}"> 
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Autor</label>
							
							<div class="col-sm-5">
								<input class="form-control" type="text" name="titulo" readonly="readonly" value="${anuncio.usuario.nome} ${anuncio.usuario.sobrenome}">
							</div>
							<div class="col-sm-4">
								<button class="btn btn-info btn-white" 
									data-toggle="popover" 
									data-trigger="hover" 
									data-placement="top" 
									data-html="true"
									data-content="<b>Publica��o:</b> ${anuncio.edicao.publicacao.titulo} <br>
												  <b>Edi��o:</b> ${anuncio.edicao.titulo} <br> 
												  <b>Categoria:</b> ${anuncio.categoria.titulo}"
									data-original-title="Informa��es do Artigo">
									
										<i class="entypo-info"></i>
								</button>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">An�ncio</label>
							<div class="col-sm-8">
								<img class="col-md-12" src="${contextPath}${anuncio.caminho}">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>