<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ page session="false" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %> 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
		<title>Inicio</title>
		<%@ include file="../componentes/header/head.jsp" %>
		<script type="text/javascript">
		function chamarModalRetrocederFluxo(e, t, n, r, i) {
			$("#botaoSim").attr("onclick", "this.disabled=true, retrocederFluxo(" + e + ", " + t + ', "' + n + '", "' + r + '", ' + i + ")")
		}
		function retrocederFluxo(e, t, n, r, i) {
			if (r == "imagem") {
				var s = "/SmartEditorial/imagem/retroceder-fluxo";
				var o = document.getElementById("comentarioFluxo").value;
				if (o == "") {
					o = " "
				}
				$.ajax({
					type: "POST",
					url: s,
					data: "idElemento=" + e + "&ordemFluxo=" + t + "&idPublicacao=" + n + "&tipoElemento=" + r + "&comentario=" + o + "&idFluxo=" + i,
					success: function (e) {
						if (e == "sucesso") {
							var t = {
								closeButton: true,
								debug: false,
								positionClass: "toast-top-right",
								onclick: null,
								showDuration: "300",
								hideDuration: "1000",
								timeOut: "10000",
								extendedTimeOut: "1000",
								showEasing: "swing",
								hideEasing: "linear",
								showMethod: "fadeIn",
								hideMethod: "fadeOut"
							};
							toastr.success("Elemento enviado para o nivel anterior do Fluxo", "Deu tudo certo", t)
						}
					},
					error: function (e) {
						alert("Falha ao carregar os Dados")
					}
				})
			} else if (r == "artigo") {
				var s = "/SmartEditorial/artigo/retroceder-fluxo";
				var o = document.getElementById("comentarioFluxo").value;
				if (o == "") {
					o = " "
				}
				$.ajax({
					type: "POST",
					url: s,
					data: "idElemento=" + e + "&ordemFluxo=" + t + "&idPublicacao=" + n + "&tipoElemento=" + r + "&comentario=" + o + "&idFluxo=" + i,
					success: function (e) {
						if (e == "sucesso") {
							var t = {
								closeButton: true,
								debug: false,
								positionClass: "toast-top-right",
								onclick: null,
								showDuration: "300",
								hideDuration: "1000",
								timeOut: "10000",
								extendedTimeOut: "1000",
								showEasing: "swing",
								hideEasing: "linear",
								showMethod: "fadeIn",
								hideMethod: "fadeOut"
							};
							toastr.success("Elemento enviado para o nivel anterior do Fluxo", "Deu tudo certo", t)
						}
					},
					error: function (e) {
						alert("Falha ao carregar os Dados")
					}
				})
			} else if (r == "anuncio") {
				var s = "/SmartEditorial/anuncio/retroceder-fluxo";
				var o = document.getElementById("comentarioFluxo").value;
				if (o == "") {
					o = " "
				}
				$.ajax({
					type: "POST",
					url: s,
					data: "idElemento=" + e + "&ordemFluxo=" + t + "&idPublicacao=" + n + "&tipoElemento=" + r + "&comentario=" + o + "&idFluxo=" + i,
					success: function (e) {
						if (e == "sucesso") {
							var t = {
								closeButton: true,
								debug: false,
								positionClass: "toast-top-right",
								onclick: null,
								showDuration: "300",
								hideDuration: "1000",
								timeOut: "10000",
								extendedTimeOut: "1000",
								showEasing: "swing",
								hideEasing: "linear",
								showMethod: "fadeIn",
								hideMethod: "fadeOut"
							};
							toastr.success("Elemento enviado para o nivel seguinte do Fluxo", "Deu tudo certo", t)
						}
					},
					error: function (e) {
						alert("Falha ao carregar os Dados")
					}
				})
			} else if (r == "pagina") {
				var s = "/SmartEditorial/pagina/retroceder-fluxo";
				var o = document.getElementById("comentarioFluxo").value;
				if (o == "") {
					o = " "
				}
				$.ajax({
					type: "POST",
					url: s,
					data: "idElemento=" + e + "&ordemFluxo=" + t + "&idPublicacao=" + n + "&tipoElemento=" + r + "&comentario=" + o + "&idFluxo=" + i,
					success: function (e) {
						if (e == "sucesso") {
							var t = {
								closeButton: true,
								debug: false,
								positionClass: "toast-top-right",
								onclick: null,
								showDuration: "300",
								hideDuration: "1000",
								timeOut: "10000",
								extendedTimeOut: "1000",
								showEasing: "swing",
								hideEasing: "linear",
								showMethod: "fadeIn",
								hideMethod: "fadeOut"
							};
							toastr.success("Elemento enviado para o nivel seguinte do Fluxo", "Deu tudo certo", t)
						}
					},
					error: function (e) {
						alert("Falha ao carregar os Dados")
					}
				})
			}
		}
		function chamarModalProsseguirFluxo(e, t, n, r, i) {
			$("#botaoSim").attr("onclick", "this.disabled=true, prosseguirFluxo(" + e + ", " + t + ', "' + n + '", "' + r + '", ' + i + ")")
		}
		function prosseguirFluxo(e, t, n, r, i) {
			if (r == "imagem") {
				var s = "/SmartEditorial/imagem/proximo-fluxo";
				var o = document.getElementById("comentarioFluxo").value;
				if (o == "") {
					o = " "
				}
				$.ajax({
					type: "POST",
					url: s,
					data: "idElemento=" + e + "&ordemFluxo=" + t + "&idPublicacao=" + n + "&tipoElemento=" + r + "&comentario=" + o + "&idFluxo=" + i,
					success: function (e) {
						if (e == "sucesso") {
							var t = {
								closeButton: true,
								debug: false,
								positionClass: "toast-top-right",
								onclick: null,
								showDuration: "300",
								hideDuration: "1000",
								timeOut: "10000",
								extendedTimeOut: "1000",
								showEasing: "swing",
								hideEasing: "linear",
								showMethod: "fadeIn",
								hideMethod: "fadeOut"
							};
							toastr.success("Elemento enviado para o nivel seguinte do Fluxo", "Deu tudo certo", t)
						} else if (e == "ultimo") {
							var t = {
								closeButton: true,
								debug: false,
								positionClass: "toast-top-right",
								onclick: null,
								showDuration: "300",
								hideDuration: "1000",
								timeOut: "10000",
								extendedTimeOut: "1000",
								showEasing: "swing",
								hideEasing: "linear",
								showMethod: "fadeIn",
								hideMethod: "fadeOut"
							};
							toastr.warning("Parece que voc� o ultimo est�gio do Fluxo. Finalize o Tratamento", "N�o temos para onde enviar", t)
						}
					},
					error: function (e) {
						alert("Falha ao carregar os Dados")
					}
				})
			} else if (r == "artigo") {
				var s = "/SmartEditorial/artigo/proximo-fluxo";
				var o = document.getElementById("comentarioFluxo").value;
				if (o == "") {
					o = " "
				}
				$.ajax({
					type: "POST",
					url: s,
					data: "idElemento=" + e + "&ordemFluxo=" + t + "&idPublicacao=" + n + "&tipoElemento=" + r + "&comentario=" + o + "&idFluxo=" + i,
					success: function (e) {
						if (e == "sucesso") {
							var t = {
								closeButton: true,
								debug: false,
								positionClass: "toast-top-right",
								onclick: null,
								showDuration: "300",
								hideDuration: "1000",
								timeOut: "10000",
								extendedTimeOut: "1000",
								showEasing: "swing",
								hideEasing: "linear",
								showMethod: "fadeIn",
								hideMethod: "fadeOut"
							};
							toastr.success("Elemento enviado para o nivel seguinte do Fluxo", "Deu tudo certo", t)
						} else if (e == "ultimo") {
							var t = {
								closeButton: true,
								debug: false,
								positionClass: "toast-top-right",
								onclick: null,
								showDuration: "300",
								hideDuration: "1000",
								timeOut: "10000",
								extendedTimeOut: "1000",
								showEasing: "swing",
								hideEasing: "linear",
								showMethod: "fadeIn",
								hideMethod: "fadeOut"
							};
							toastr.warning("Parece que voc� o ultimo est�gio do Fluxo. Finalize o Tratamento", "N�o temos para onde enviar", t)
						}
					},
					error: function (e) {
						alert("Falha ao carregar os Dados")
					}
				})
			} else if (r == "anuncio") {
				var s = "/SmartEditorial/anuncio/proximo-fluxo";
				var o = document.getElementById("comentarioFluxo").value;
				if (o == "") {
					o = " "
				}
				$.ajax({
					type: "POST",
					url: s,
					data: "idElemento=" + e + "&ordemFluxo=" + t + "&idPublicacao=" + n + "&tipoElemento=" + r + "&comentario=" + o + "&idFluxo=" + i,
					success: function (e) {
						if (e == "sucesso") {
							var t = {
								closeButton: true,
								debug: false,
								positionClass: "toast-top-right",
								onclick: null,
								showDuration: "300",
								hideDuration: "1000",
								timeOut: "10000",
								extendedTimeOut: "1000",
								showEasing: "swing",
								hideEasing: "linear",
								showMethod: "fadeIn",
								hideMethod: "fadeOut"
							};
							toastr.success("Elemento enviado para o nivel seguinte do Fluxo", "Deu tudo certo", t)
						} else if (e == "ultimo") {
							var t = {
								closeButton: true,
								debug: false,
								positionClass: "toast-top-right",
								onclick: null,
								showDuration: "300",
								hideDuration: "1000",
								timeOut: "10000",
								extendedTimeOut: "1000",
								showEasing: "swing",
								hideEasing: "linear",
								showMethod: "fadeIn",
								hideMethod: "fadeOut"
							};
							toastr.warning("Parece que voc� o ultimo est�gio do Fluxo. Finalize o Tratamento", "N�o temos para onde enviar", t)
						}
					},
					error: function (e) {
						alert("Falha ao carregar os Dados")
					}
				})
			} else if (r == "pagina") {
				var s = "/SmartEditorial/pagina/proximo-fluxo";
				var o = document.getElementById("comentarioFluxo").value;
				if (o == "") {
					o = " "
				}
				$.ajax({
					type: "POST",
					url: s,
					data: "idElemento=" + e + "&ordemFluxo=" + t + "&idPublicacao=" + n + "&tipoElemento=" + r + "&comentario=" + o + "&idFluxo=" + i,
					success: function (e) {
						if (e == "sucesso") {
							var t = {
								closeButton: true,
								debug: false,
								positionClass: "toast-top-right",
								onclick: null,
								showDuration: "300",
								hideDuration: "1000",
								timeOut: "10000",
								extendedTimeOut: "1000",
								showEasing: "swing",
								hideEasing: "linear",
								showMethod: "fadeIn",
								hideMethod: "fadeOut"
							};
							toastr.success("Elemento enviado para o nivel seguinte do Fluxo", "Deu tudo certo", t)
						} else if (e == "ultimo") {
							var t = {
								closeButton: true,
								debug: false,
								positionClass: "toast-top-right",
								onclick: null,
								showDuration: "300",
								hideDuration: "1000",
								timeOut: "10000",
								extendedTimeOut: "1000",
								showEasing: "swing",
								hideEasing: "linear",
								showMethod: "fadeIn",
								hideMethod: "fadeOut"
							};
							toastr.warning("Parece que voc� o ultimo est�gio do Fluxo. Finalize o Tratamento", "N�o temos para onde enviar", t)
						}
					},
					error: function (e) {
						alert("Falha ao carregar os Dados")
					}
				})
			}
		}
		function chamarModalTratarElemento(e, t, n, r) {
			$("#nomeElemento").html(n);
			$("#botaoSimTratar").attr("onclick", "this.disabled=true, tratarElemento(" + e + ", " + t + ', "' + r + '")')
		}
		function tratarElemento(e, t, n) {
			if (n == "imagem") {
				var r = "/SmartEditorial/imagem/pegar-elemento";
				$.ajax({
					type: "POST",
					url: r,
					data: "idElemento=" + e + "&idFluxo=" + t,
					success: function (e) {
						if (e == "sucesso") {
							var e = {
								closeButton: true,
								debug: false,
								positionClass: "toast-top-right",
								onclick: null,
								showDuration: "300",
								hideDuration: "1000",
								timeOut: "10000",
								extendedTimeOut: "1000",
								showEasing: "swing",
								hideEasing: "linear",
								showMethod: "fadeIn",
								hideMethod: "fadeOut"
							};
							toastr.success("Elemento enviado para o sua caixa de entrada", "Deu tudo certo", e)
						}
					},
					error: function (e) {
						alert("Falha ao carregar os Dados")
					}
				})
			} else if (n == "artigo") {
				var r = "/SmartEditorial/artigo/pegar-elemento";
				$.ajax({
					type: "POST",
					url: r,
					data: "idElemento=" + e + "&idFluxo=" + t,
					success: function (e) {
						if (e == "sucesso") {
							var t = {
								closeButton: true,
								debug: false,
								positionClass: "toast-top-right",
								onclick: null,
								showDuration: "300",
								hideDuration: "1000",
								timeOut: "10000",
								extendedTimeOut: "1000",
								showEasing: "swing",
								hideEasing: "linear",
								showMethod: "fadeIn",
								hideMethod: "fadeOut"
							};
							toastr.success("Elemento enviado para o sua caixa de entrada", "Deu tudo certo", t)
						}
					},
					error: function (e) {
						alert("Falha ao carregar os Dados")
					}
				})
			} else if (n == "anuncio") {
				var r = "/SmartEditorial/anuncio/pegar-elemento";
				$.ajax({
					type: "POST",
					url: r,
					data: "idElemento=" + e + "&idFluxo=" + t,
					success: function (e) {
						if (e == "sucesso") {
							var t = {
								closeButton: true,
								debug: false,
								positionClass: "toast-top-right",
								onclick: null,
								showDuration: "300",
								hideDuration: "1000",
								timeOut: "10000",
								extendedTimeOut: "1000",
								showEasing: "swing",
								hideEasing: "linear",
								showMethod: "fadeIn",
								hideMethod: "fadeOut"
							};
							toastr.success("Elemento enviado para o sua caixa de entrada", "Deu tudo certo", t)
						}
					},
					error: function (e) {
						alert("Falha ao carregar os Dados")
					}
				})
			} else if (n == "pagina") {
				var r = "/SmartEditorial/pagina/pegar-elemento";
				$.ajax({
					type: "POST",
					url: r,
					data: "idElemento=" + e + "&idFluxo=" + t,
					success: function (e) {
						if (e == "sucesso") {
							var t = {
								closeButton: true,
								debug: false,
								positionClass: "toast-top-right",
								onclick: null,
								showDuration: "300",
								hideDuration: "1000",
								timeOut: "10000",
								extendedTimeOut: "1000",
								showEasing: "swing",
								hideEasing: "linear",
								showMethod: "fadeIn",
								hideMethod: "fadeOut"
							};
							toastr.success("Elemento enviado para o sua caixa de entrada", "Deu tudo certo", t)
						}
					},
					error: function (e) {
						alert("Falha ao carregar os Dados")
					}
				})
			}
		}
		function loadModalPreview(e, t, n) {
			if (t == "artigo") {
				var r = "/SmartEditorial/artigo/preview";
				$.ajax({
					type: "POST",
					url: r,
					data: "id=" + e,
					success: function (e) {
						$("#divRetornoPreview").html(e)
					},
					error: function (e) {
						alert("Falha ao carregar os Dados")
					}
				})
			} else if (t == "imagem") {
				var r = "/SmartEditorial/imagem/preview";
				$.ajax({
					type: "POST",
					url: r,
					data: "id=" + e,
					success: function (e) {
						$("#divRetornoPreview").html(e)
					},
					error: function (e) {
						alert("Falha ao carregar os Dados")
					}
				})
			} else if (t == "anuncio") {
				var r = "/SmartEditorial/anuncio/preview";
				$.ajax({
					type: "POST",
					url: r,
					data: "id=" + e,
					success: function (e) {
						$("#divRetornoPreview").html(e)
					},
					error: function (e) {
						alert("Falha ao carregar os Dados")
					}
				})
			} else if (t == "pagina") {
				var r = "/SmartEditorial/pagina/preview";
				$.ajax({
					type: "POST",
					url: r,
					data: "id=" + e,
					success: function (e) {
						$("#divRetornoPreview").html(e)
					},
					error: function (e) {
						alert("Falha ao carregar os Dados")
					}
				})
			}
		}
		window.onload = function () {
			$(document).ready(function () {
				$("#dataTables-homeDoUsuario").dataTable();
				$("#dataTables-homeDoGrupo").dataTable()
			})
		};
		$(document).ready(function () {
			$("body").tooltip({
				selector: "[data-tooltip=tooltip]",
				container: "body"
			})
		});
		$(document).ready(function () {
			$("body").tooltip({
				selector: "[data-tooltip=tooltip]",
				container: "body"
			})
		})
		
		function chamarModelDevolverParaOGrupo(e, f, t) {
			$("#botaoSimDevolver").attr("onclick", "this.disabled=true, devolverElementoInbox(" + e + " ," + f + ", '" + t + "')")
		}
		
		function devolverElementoInbox(e, t, n) {
			if (n == "imagem") {
				var r = "/SmartEditorial/imagem/devolver-elemento";
				$.ajax({
					type: "POST",
					url: r,
					data: "idElemento=" + e + "&idFluxo=" + t,
					success: function (e) {
						if (e == "sucesso") {
							var e = {
								closeButton: true,
								debug: false,
								positionClass: "toast-top-right",
								onclick: null,
								showDuration: "300",
								hideDuration: "1000",
								timeOut: "10000",
								extendedTimeOut: "1000",
								showEasing: "swing",
								hideEasing: "linear",
								showMethod: "fadeIn",
								hideMethod: "fadeOut"
							};
							toastr.success("Elemento devolvido para caixa de entrada do grupo", "Deu tudo certo", e)
						}
					},
					error: function (e) {
						alert("Falha ao carregar os Dados")
					}
				})
			} else if (n == "artigo") {
				var r = "/SmartEditorial/artigo/devolver-elemento";
				$.ajax({
					type: "POST",
					url: r,
					data: "idElemento=" + e + "&idFluxo=" + t,
					success: function (e) {
						if (e == "sucesso") {
							var t = {
								closeButton: true,
								debug: false,
								positionClass: "toast-top-right",
								onclick: null,
								showDuration: "300",
								hideDuration: "1000",
								timeOut: "10000",
								extendedTimeOut: "1000",
								showEasing: "swing",
								hideEasing: "linear",
								showMethod: "fadeIn",
								hideMethod: "fadeOut"
							};
							toastr.success("Elemento devolvido para caixa de entrada do grupo", "Deu tudo certo", t)
						}
					},
					error: function (e) {
						alert("Falha ao carregar os Dados")
					}
				})
			} else if (n == "anuncio") {
				var r = "/SmartEditorial/anuncio/devolver-elemento";
				$.ajax({
					type: "POST",
					url: r,
					data: "idElemento=" + e + "&idFluxo=" + t,
					success: function (e) {
						if (e == "sucesso") {
							var t = {
								closeButton: true,
								debug: false,
								positionClass: "toast-top-right",
								onclick: null,
								showDuration: "300",
								hideDuration: "1000",
								timeOut: "10000",
								extendedTimeOut: "1000",
								showEasing: "swing",
								hideEasing: "linear",
								showMethod: "fadeIn",
								hideMethod: "fadeOut"
							};
							toastr.success("Elemento devolvido para caixa de entrada do grupo", "Deu tudo certo", t)
						}
					},
					error: function (e) {
						alert("Falha ao carregar os Dados")
					}
				})
			} else if (n == "pagina") {
				var r = "/SmartEditorial/pagina/devolver-elemento";
				$.ajax({
					type: "POST",
					url: r,
					data: "idElemento=" + e + "&idFluxo=" + t,
					success: function (e) {
						if (e == "sucesso") {
							var t = {
								closeButton: true,
								debug: false,
								positionClass: "toast-top-right",
								onclick: null,
								showDuration: "300",
								hideDuration: "1000",
								timeOut: "10000",
								extendedTimeOut: "1000",
								showEasing: "swing",
								hideEasing: "linear",
								showMethod: "fadeIn",
								hideMethod: "fadeOut"
							};
							toastr.success("Elemento devolvido para caixa de entrada do grupo", "Deu tudo certo", t)
						}
					},
					error: function (e) {
						alert("Falha ao carregar os Dados")
					}
				})
			}
		}

		 function loadModalUpload(elemento, id) {
			 
		 	if (elemento == "pagina") {
		 		$("#formIdElemento").attr("value", id)
		 		$("#myform").attr("action", "/SmartEditorial/paginas/update-file")
			} else if (elemento =="anuncio") {
				$("#formIdElemento").attr("value", id)
				$("#myform").attr("action", "/SmartEditorial/anuncios/update-file")
			} else if (elemento =="imagem") {
				$("#formIdElemento").attr("value", id)
				$("#myform").attr("action", "/SmartEditorial/imagens/update-file")
			} 
		 }
		</script>
	</head>
	<body class="page-body">
		<div class="page-container horizontal-menu">
			<%@ include file="../componentes/header/menu.jsp" %>
			<div class="main-content" id="main-content">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<div class="row">
								<div class="col-lg-12">
									<h2 class="page-header">
										Caixa de entrada <i class="fa fa-inbox"></i>
									</h2>
								</div>
								<div class="panel-body">
									<div id="mensagem" class="col-lg-12"></div>
								</div>
								<!-- /.col-lg-12 -->
							</div>
							<!-- /.row -->
							<div class="row">
								<div class="col-lg-12">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">
												<label class="control-label"><i class="fa fa-pencil-square-o"></i> Elementos do Fluxo <small>Estes elementos chegaram para edi��o</small></label>
											</h4>
											<hr />
										</div>
										<!-- /.panel-heading -->
										<div class="panel-body">
											<div class="panel-body">
												<ul class="nav nav-tabs" role="tablist" id="myTab">
													<li role="presentation" class="active">
														<a href="#caidaDoGrupo" role="tab" data-toggle="tab">Caixa do Grupo</a>
													</li>
													<li role="presentation">
														<a href="#minhaCaixa" role="tab" data-toggle="tab">Minha Caixa</a>
													</li>
												</ul>
												<br />
												<div class="tab-content">
													<div role="tabpanel" class="tab-pane active" id="caidaDoGrupo">
														<div class="table-responsive">
															<table class="table table-bordered" id="dataTables-homeDoGrupo">
																<thead>
																	<tr>
																		<th>Titulo</th>
																		<th>Estado</th>
																		<th>Dono do Elemento</th>
																		<th>Tipo de Elemento</th>
																		<th>Comentario</th>
																		<th>A��es</th>
																	</tr>
																</thead>
																<tbody>
																	<c:forEach items="${caixaDoGrupo}" var="item">
																		<tr>
																			<td>
																				<a data-toggle="modal" data-target="#modalPreviewElemento" 
																					onclick="loadModalPreview(${item.idElemento}, '${item.tipoElemento}', '${contextPath}${item.caminho}')"  
																					data-keyboard="false" data-backdrop="false">
																				<i class="fa fa-folder-open-o"></i>
																				${item.titulo}
																				</a>
																			</td>
																			<td>${item.estado}</td>
																			<td>${item.usuario}</td>
																			<td>${item.tipoElemento}</td>
																			<td>${item.comentario}</td>
																			<td>
																				<button type="button" class="btn btn-success"
																					data-toggle="modal" data-tooltip="tooltip" data-placement="top"
																					data-tooltip="tooltip" 
																					title="" data-original-title="Tratar Elemento"
																					data-target="#modalTratarElemento" data-backdrop="false"
																					onclick="chamarModalTratarElemento('${item.idElemento}', '${item.idFluxo}', '${item.titulo}', '${item.tipoElemento}')">
																				<i class="entypo-check"></i>
																				</button>
																			</td>
																		</tr>
																	</c:forEach>
																</tbody>
															</table>
														</div>
													</div>
													<div role="tabpanel" class="tab-pane" id="minhaCaixa">
														<div class="table-responsive">
															<table class="table table-bordered" id="dataTables-homeDoUsuario">
																<thead>
																	<tr>
																		<th>Titulo</th>
																		<th>Estado</th>
																		<th>Dono do Elemento</th>
																		<th>Tipo de Elemento</th>
																		<th>Comentario</th>
																		<th>A��es</th>
																	</tr>
																</thead>
																<tbody>
																	<c:forEach items="${caixaDoUsuario}" var="item">
																		<tr>
																			<td>
																				<a data-toggle="modal" data-target="#modalPreviewElemento" 
																					onclick="loadModalPreview(${item.idElemento}, '${item.tipoElemento}', '${contextPath}${item.caminho}')"  
																					data-keyboard="false" data-backdrop="false">
																				<i class="fa fa-folder-open-o"></i>
																				${item.titulo}
																				</a>
																			</td>
																			<td>${item.estado}</td>
																			<td>${item.usuario}</td>
																			<td>${item.tipoElemento}</td>
																			<td>${item.comentario}</td>
																			<td>
																				<button type="button" class="btn btn-red" 
																					data-toggle="modal" data-tooltip="tooltip" data-placement="top" 
																					title="" data-original-title="Rejeitar Elemento"
																					data-target="#modalFluxo" data-backdrop="false"
																					onclick="chamarModalRetrocederFluxo('${item.idElemento}', '${item.ordemFluxo}', '${item.idPublicacao}', '${item.tipoElemento}', '${item.idFluxo}')">
																				<i class="fa fa-arrow-left"></i>
																				</button>
																				<button type="button" class="btn btn-info"
																					data-toggle="modal" data-tooltip="tooltip" data-placement="top" 
																					title="" data-original-title="Devolver para o Grupo"
																					data-target="#modalFluxoDevolver" data-backdrop="false"
																					onclick="chamarModelDevolverParaOGrupo('${item.idElemento}', '${item.idFluxo}', '${item.tipoElemento}')">
																				<i class="fa fa-arrow-up"></i>
																				</button>
																				
																				<a  href="${contextPath}${item.caminho}" class="btn btn-black"
																					data-tooltip="tooltip" data-placement="top" 
																					data-original-title="Baixar Elemento" download>
																				<i class="fa fa-download"></i>
																				</a>
																				<c:if test="${item.tipoElemento != 'artigo'}">
																					<a  href="#" onclick="loadModalUpload('${item.tipoElemento}', '${item.idElemento}')" class="btn btn-black"
																						data-toggle="modal" data-tooltip="tooltip" data-placement="top" 
																						data-original-title="Baixar Elemento" download
																						data-target="#modalUpload" data-backdrop="false">
																					<i class="fa fa-upload"></i>
																					</a>
																				</c:if>
																				<c:if test="${item.tipoElemento == 'artigo'}">
																					<a href="/SmartEditorial/artigos/editar?artigo=${item.idElemento}" type="submit" class="btn btn-default" data-tooltip="tooltip" data-placement="top" title="" data-original-title="Editar Elemento">
																						<i class="entypo-pencil"></i>
																					</a>
																				</c:if>
																				
																				
																				<button type="button" class="btn btn-green"
																					data-toggle="modal" data-tooltip="tooltip" data-placement="top" 
																					title="" data-original-title="Enviar Elemento"
																					data-target="#modalFluxo" data-backdrop="false"
																					onclick="chamarModalProsseguirFluxo('${item.idElemento}', '${item.ordemFluxo}', '${item.idPublicacao}', '${item.tipoElemento}', '${item.idFluxo}')">
																				<i class="fa fa-arrow-right"></i>
																				</button>
																			</td>
																		</tr>
																	</c:forEach>
																</tbody>
															</table>
														</div>
													</div> 
												</div>
												<script>
													$(function() {
														$('#myTab a:first').tab('show')
													})
												</script>
											</div>
											<div id="modalFluxo" class="modal fade" tabindex="-1" role="dialog" 
											aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
												<div class="modal-dialog">
													<div class="modal-content">
														<div class="modal-header">
															<button type="button" class="close" onclick="location.reload()" data-dismiss="modal" aria-hidden="true">�</button>
															<h4 class="modal-title" id="myModalLabel">Iniciar Fluxo</h4>
														</div>
														<div class="modal-body">
															<label for="comentario" class="col-sm-3 control-label">Comentario</label>
															<div class="col-sm-8">
																<textarea class="form-control autogrow" id="comentarioFluxo" required="required"></textarea>
															</div>
														</div>
														<div class="modal-footer">
															<button type="button" class="btn btn-default" onclick="location.reload()" data-dismiss="modal">N�o</button>
															<input type="submit" class="btn btn-primary" onclick="" id="botaoSim" value="Sim">
														</div>
													</div>
													<!-- /.modal-content -->
												</div>
												<!-- /.modal-dialog -->
											</div>
											<div id="modalUpload" class="modal fade" tabindex="-1" role="dialog" 
											aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
												<div class="modal-dialog">
												<form action="" method="POST" enctype="multipart/form-data"
															id="myform" class="form-horizontal form-groups-bordered" role="form">
															<input type="hidden" class="form-control" id="formIdElemento" name="id" value=""/>
													<div class="modal-content">
														<div class="modal-header">
															<button type="button" class="close" onclick="location.reload()" data-dismiss="modal" aria-hidden="true">�</button>
															<h4 class="modal-title" id="myModalLabel">Carregar Novar Vers�o</h4>
														</div>
														<div class="modal-body">
															
																<div class="form-group">
																	<label class="col-sm-3 control-label">Arquivo</label>
																	
																	<div class="col-sm-8">
																	
																		<div class="fileinput fileinput-new" data-provides="fileinput">
																			<div class="input-group">
																				<div class="form-control uneditable-input" data-trigger="fileinput">
																					<i class="glyphicon glyphicon-file fileinput-exists"></i>
																					<span class="fileinput-filename"></span>
																				</div>
																				<span class="input-group-addon btn btn-default btn-file">
																					<span class="fileinput-new">Selecione</span>
																					<span class="fileinput-exists">Mudar</span>
																					<input type="file" name="arquivo" accept="image/x-png, image/gif, image/jpeg" required="required"onchange="showimagepreview(this)">
																				</span>
																				<a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remover</a>
																			</div>
																		</div>
																		
																	</div>
																</div>
															
														</div>
														<div class="modal-footer">
															<button type="button" class="btn btn-default" onclick="location.reload()" data-dismiss="modal">Cancelar</button>
															<input type="submit" class="btn btn-primary" onclick="" id="botaoSimUpload" value="Salvar">
														</div>
													</div>
													</form>
													<!-- /.modal-content -->
												</div>
												<!-- /.modal-dialog -->
											</div>
											<div id="modalFluxoDevolver" class="modal fade" tabindex="-1" role="dialog" 
											aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
												<div class="modal-dialog">
													<div class="modal-content">
														<div class="modal-header">
															<button type="button" class="close" onclick="location.reload()" data-dismiss="modal" aria-hidden="true">�</button>
															<h4 class="modal-title" id="myModalLabel">Devolver para o Grupo</h4>
														</div>
														<div class="modal-body">
															Deseja devolver o elemento para o Grupo.
														</div>
														<div class="modal-footer">
															<button type="button" class="btn btn-default" onclick="location.reload()" data-dismiss="modal">N�o</button>
															<input type="submit" class="btn btn-primary" onclick="" id="botaoSimDevolver" value="Sim">
														</div>
													</div>
													<!-- /.modal-content -->
												</div>
												<!-- /.modal-dialog -->
											</div>
											<div id="modalPreviewElemento" style="" class="modal fade" tabindex="-1" role="dialog" 
												onclick="location.reload()" aria-labelledby="myModalLabel" aria-hidden="true" >
												<div class="modal-dialog" id="modelPreview">
													<div class="modal-content">
														<div class="modal-header">
															<button type="button" onclick="location.reload()" class="close" data-dismiss="modal">
															<span aria-hidden="true"><i class="fa fa-times fa-2x"></i></span>
															</button>
														</div>
														<div class="modal-body" id="divRetornoPreview">
															<img id="elemento" class="col-md-12" src="">
														</div>
													</div>
												</div>
											</div>
											<div id="modalTratarElemento" class="modal fade" tabindex="-1" role="dialog" 
												aria-labelledby="myModalLabel" aria-hidden="true">
												<div class="modal-dialog">
													<div class="modal-content">
														<div class="modal-header">
															<button type="button" class="close" onclick="location.reload()" data-dismiss="modal" aria-hidden="true">�</button>
															<h4 class="modal-title" id="myModalLabel">Tratar Elemento</h4>
														</div>
														<div class="modal-body">
															<p>Deseja tratar este elemento: <b id="nomeElemento"> </b></p>
														</div>
														<div class="modal-footer">
															<button type="button" class="btn btn-default" onclick="location.reload()" data-dismiss="modal">N�o</button>
															<input type="submit" class="btn btn-primary" onclick="" id="botaoSimTratar" value="Sim">
														</div>
													</div>
												</div>
											</div>
											
											
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<%@ include file="../componentes/footer/footer.jsp" %>
			</div>
		</div>
	</body>
</html>