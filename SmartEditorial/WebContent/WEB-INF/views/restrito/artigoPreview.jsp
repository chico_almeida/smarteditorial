<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ page session="false" %>
<c:set var="req" value="${pageContext.request}" />
<c:set var="baseURL" value="${fn:replace(req.requestURL, fn:substring(req.requestURI, 1, fn:length(req.requestURI)), req.contextPath)}" />
<c:set var="string1" value="${baseURL}/${MyID}"/>
<c:set var="string2" value="${fn:replace(string1, '8080//', '8080/')}" />
<c:url var="myUrl" value="${string2}" />
<script src="${myUrl}/resources/js/nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">bkLib.onDomLoaded(nicEditors.allTextAreas);</script>
<script type="text/javascript">

document.onload = function() {
	nicEditors.allTextAreas;
};

</script>
<%@ include file="../componentes/header/head.jsp" %>
<div class="row">
	<form class="form-horizontal form-groups-bordered" role="form">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-body">
					<div class="table-responsive">
						<div class="form-group">
							<label class="col-sm-3 control-label">T�tulo</label>
							<div class="col-sm-8">
								<input class="form-control" type="text" name="titulo" readonly="readonly" value="${artigo.titulo}"> 
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">Autor</label>
							
							<div class="col-sm-5">
								<input class="form-control" type="text" name="titulo" readonly="readonly" value="${artigo.usuario.nome} ${artigo.usuario.sobrenome}">
							</div>
							<div class="col-sm-4">
								<button class="btn btn-info btn-white" 
									data-toggle="popover" 
									data-trigger="hover" 
									data-placement="top" 
									data-html="true"
									data-content="<b>Publica��o:</b> ${artigo.edicao.publicacao.titulo} <br> <b>Edi��o:</b> ${artigo.edicao.titulo} <br> <b>Categoria:</b> ${artigo.categoria.titulo}" 
									data-original-title="Informa��es do Artigo">
									
										<i class="entypo-info"></i>
								</button>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Artigo</label>
							<div class="col-sm-8" style="height:250px; overflow-y: scroll;">
								${artigo.artigo}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>