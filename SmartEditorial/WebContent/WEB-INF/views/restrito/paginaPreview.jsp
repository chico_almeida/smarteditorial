<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ page session="false" %>

<%@ include file="../componentes/header/head.jsp" %>
<div class="row">
	<form class="form-horizontal form-groups-bordered" role="form">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-body">
					<div class="table-responsive">
						<div class="form-group">
							<label class="col-sm-3 control-label">T�tulo</label>
							<div class="col-sm-5">
								<input class="form-control" type="text" name="titulo" readonly="readonly" value="${pagina.titulo}"> 
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Autor</label>
							
							<div class="col-sm-5">
								<input class="form-control" type="text" name="titulo" readonly="readonly" value="${pagina.usuario.nome} ${pagina.usuario.sobrenome}">
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">Publica��o</label>
							
							<div class="col-sm-5">
								<input class="form-control" type="text" name="titulo" readonly="readonly" value="${pagina.edicao.publicacao.titulo}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Edi��o</label>
							
							<div class="col-sm-5">
								<input class="form-control" type="text" name="titulo" readonly="readonly" value="${pagina.edicao.titulo}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Categoria</label>
							
							<div class="col-sm-5">
								<input class="form-control" type="text" name="titulo" readonly="readonly" value="${pagina.categoria.titulo}">
							</div>
						</div>	
						<div class="form-group">
							<label class="col-sm-3 control-label">Preview</label>
							<div class="col-sm-5">
								<a data-toggle="modal" data-target="#myModal" 
								onclick="loadModalImage('${contextPath}${pagina.caminho}')"  
								data-keyboard="false" draggable="true">
									<i class="fa fa-folder-open-o"></i>
									Clique Aqui
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
<div id="myModal" class="modal fade" draggable="true" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" style="width: 100%!important; height: 100% !important">
		<div class="modal-content" style="width: 100%!important; height: 100% !important">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true"><i class="fa fa-times fa-2x"></i></span>
				</button>
			</div>
			<div class="modal-body"style="width: 100%!important; height: 100%!important">
				<object id="pdfView" data="" type="application/pdf" width="100%" height="100%"> </object>
			</div>
		</div>
	</div>
</div>
<script>
function loadModalImage(url) {
	document.getElementById("pdfView").data=url;
}

</script>