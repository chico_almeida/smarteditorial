
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %> 
<%@ page session="false" %>
<!DOCTYPE html>
<html>
<head>
<sec:authorize access="isAnonymous()">
	<script type="text/javascript">
		window.location = "/SmartEditorial/"
	</script>
</sec:authorize>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Publica��es</title>
<%@ include file="../componentes/header/head.jsp" %>
<script type="text/javascript">
 window.onload = function(){
	$(document).ready(function() {
        $('#dataTables-ListaDePublicacao').dataTable();
    });
	if(getQueryVariable("q") == 'edicao') {
		listarEdicoes();
	}
	if(getQueryVariable("idPublicacao")) {
		carregarInternaPublicacao(getQueryVariable("idPublicacao"));
	}
};
function existePublicacao(tituloPublicacao) {
	var methodURL= "publicacao/validar"
	$.ajax({
		type : "POST",
		url : methodURL,
		data : "titulo="+tituloPublicacao,
		success : function(response) {
			if(response == 'true') {
				document.getElementById("mySubmit").disabled = true;
							
							var opts = {
								"closeButton": true,
								"debug": false,
								"positionClass": "toast-top-right",
								"onclick": null,
								"showDuration": "300",
								"hideDuration": "1000",
								"timeOut": "10000",
								"extendedTimeOut": "1000",
								"showEasing": "swing",
								"hideEasing": "linear",
								"showMethod": "fadeIn",
								"hideMethod": "fadeOut"
							};
							
							toastr.error("J� existe uma publica��o com este titulo.", "Puxa! Achamos um erro:", opts);

				$("#retorno").html("Titulo de Publica��o em Uso");
			} else {
				document.getElementById("mySubmit").disabled = false;
				$("#retorno").html("");
			}
		},
		error : function(e) {
			alert("Falha ao validar titulo");
		}
	});
}
function cadastrarPublicacao() {
	var methodURL= "/SmartEditorial/publicacao/cadastro"
	$.ajax({
		type : "POST",
		url : methodURL,
		success : function(response) {
			$("#formularioCadastroPublicacao").html(response);
		},
		error : function(e) {
			alert("Falha ao carregar os Dados");
		}
	});
}

function editarPublicacao(id) {
	var methodURL= "/SmartEditorial/publicacao/editarPublicacao"
	$.ajax({
		type : "POST",
		url : methodURL,
		data : "id="+id,
		success : function(response) {
			$("#formularioCadastroPublicacao").html(response);
		},
		error : function(e) {
			alert("Falha ao carregar os Dados");
		}
	});
}

function removerPublicacao(id) {
	var methodURL= "/SmartEditorial/publicacao/remover"
	$.ajax({
		type : "POST",
		url : methodURL,
		data : "idPublicacao="+id,
		success : function(response) {
			if (response == 'sucesso') {
				window.location = "publicacoes";
			} else {
				var opts = {
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-top-right",
						"onclick": null,
						"showDuration": "300",
						"hideDuration": "1000",
						"timeOut": "10000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut"
					};
					
					toastr.error("Esta Publica��o possui vinculos, remova-os antes de remove-la.", "Puxa! Achamos um erro:", opts);
					return false;
			}
		},
		error : function(e) {
			alert("Falha ao carregar os Dados");
		}
	});
}

function carregarInternaPublicacao(id) {
	var methodURL= "/SmartEditorial/publicacao"
		$.ajax({
			type : "POST",
			url : methodURL,
			data : "idPublicacao="+id,
			success : function(response) {
				$("#main-content").html(response);
				//CHAMADA DO JS QUE CONFIGURA DA DATATABLE
				$(document).ready(function() {
					$('#dataTables-EdicoesDaPublicacao').dataTable();
					$('#dataTables-CategoriasDaPublicacao').dataTable();
			    });
			},
			error : function(e) {
				alert("Falha ao carregar os Dados");
			}
		});
}

function chamarModal(idPublicacao) {
	$('#botaoSim').attr('onclick', 'this.disabled=true, removerPublicacao('+idPublicacao+')');
} 

</script>
</head>
<body>

<body class="page-body">
<div class="page-container horizontal-menu">
	<%@ include file="../componentes/header/menu.jsp" %>
	<div class="main-content" id="main-content">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="row">
						<div class="col-lg-12">
							<h2 class="page-header">
								Publica��es <i class="fa fa-th-list"></i>
							</h2>
						</div>
				        
						<div class="panel-body">
							<div id="mensagem" class="col-lg-12"></div>
			      		</div>
						<!-- /.col-lg-12 -->
					</div>
					<!-- /.row -->
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										 <label class="control-label"><i class="fa fa-pencil-square-o"></i> Publica��es <small>Gerencie suas publica��es</small></label>
									</h4>
									<hr />
								</div>
								
								<!-- /.panel-heading -->
								<div class="panel-body">
									<div class="table-responsive">
										<button type="button" class="btn btn-default btn-icon icon-left" onclick="cadastrarPublicacao()" data-toggle="modal" data-target="#cadastrarPublicacaoModal" data-keyboard="false" data-backdrop="false">
											Criar Publica��o
											<i class="fa fa-plus"></i>
										</button>
										<table class="table table-striped table-bordered table-hover" id="dataTables-ListaDePublicacao">
											<thead>
												<tr>
													<th>Publica��o</th>
													<th>Descri��o</th>
													<th>A��es</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach items="${listaDePublicacoes}" var="publicacao">
													<tr class="even gradeA">
													<td>
														<a href="/SmartEditorial/publicacao?idPublicacao=${publicacao.idPublicacao}">
															<i class="fa fa-folder-open-o"></i>
															${publicacao.titulo}
														</a>
													</td>
													<td>${publicacao.descricao}</td>
													<td>
														<a href="#" onclick="editarPublicacao(${publicacao.idPublicacao})"
															class="btn btn-default btn-sm btn-icon icon-left"
															data-toggle="modal" data-target="#cadastrarPublicacaoModal"
															data-keyboard="false" data-backdrop="false">
															<i class="entypo-pencil"></i>
															Editar
														</a>
														
														<a href="#" onclick="chamarModal(${publicacao.idPublicacao})" class="btn btn-danger btn-sm btn-icon icon-left"  data-toggle="modal" data-target="#modalRemovePublicacao" data-backdrop="false">
															<i class="entypo-cancel"></i>
															Deletar
														</a>
													</td>
												</tr>
												</c:forEach>
											</tbody>
										</table>
										<div id="modalRemovePublicacao" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
											<div class="modal-dialog">
												<div class="modal-content">
												
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
														<h4 class="modal-title" id="myModalLabel">Deseja Remover?</h4>
													</div>
													<div class="modal-body">
														<p>Voc� est� prestes a remover uma Edi��o desta Publica��o, confirma isso?</p>
													</div>
													<div class="modal-footer">
														<button type="button" class="btn btn-default" data-dismiss="modal">N�o</button>
														<button type="button" class="btn btn-primary" onclick="" id="botaoSim">Sim</button>
													</div>
												
												</div><!-- /.modal-content -->
											</div><!-- /.modal-dialog -->
										</div>
									</div>
									<!-- /.table-responsive -->
								</div>
								<!-- /.panel-body -->
							</div>
							<!-- /.panel -->
						</div>
						<!-- /.col-lg-12 -->
						<div id="cadastrarPublicacaoModal" class="modal fade" tabindex="-1"
									role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">
											<span aria-hidden="true"><i class="fa fa-times fa-2x"></i></span>
										</button>
									</div>
									<div class="modal-body">
										<div id="formularioCadastroPublicacao"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /.row -->
				</div>
			</div>
			<%@ include file="../componentes/footer/footer.jsp" %>
		</div>
	</div>
</div>
</body>
</html>