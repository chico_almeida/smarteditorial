	<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page session="false" %>
<!DOCTYPE html>
<html>
<head>
<sec:authorize access="isAnonymous()">
	<script type="text/javascript">
		window.location = "/SmartEditorial/"
	</script>
</sec:authorize>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Publica��es</title>
<%@ include file="../componentes/header/head.jsp" %>
<script type="text/javascript">

function carregarFormulariEdicao(id) {
    var methodURL= "/SmartEditorial/edicoes/cadastro"
    $.ajax({
        type : "POST",
        url : methodURL,
        data : "idPublicacao="+id,
        success : function(response) {
            $("#formularioCadastroEdicao").html(response);
        },
        error : function(e) {
            alert("Falha ao carregar os Dados");
        }
    });
}

function removerEdicao(idEdicao) {
	var methodURL= "/SmartEditorial/edicoes/remover"
	    $.ajax({
	        type : "POST",
	        url : methodURL,
	        data : "idEdicao="+idEdicao,
	        success : function(response) {
	        	if (response == 'vinculo') {
	        		var opts = {
							"closeButton": true,
							"debug": false,
							"positionClass": "toast-top-right",
							"onclick": null,
							"showDuration": "300",
							"hideDuration": "1000",
							"timeOut": "10000",
							"extendedTimeOut": "1000",
							"showEasing": "swing",
							"hideEasing": "linear",
							"showMethod": "fadeIn",
							"hideMethod": "fadeOut"
						};
						
						toastr.error("Existem arquivos vinculados a esta edi��o. Remova-os antes de remove-la.", "Puxa! Achamos um erro:", opts);
				} else {
					location.reload();
				}
	        },
	        error : function(e) {
	        	
	        }
	    });
}
function editarEdicao(idEdicao, idPublicacao) {
	var methodURL= "/SmartEditorial/edicoes/editar"
	    $.ajax({
	        type : "POST",
	        url : methodURL,
	        data : "idEdicao="+idEdicao+"&idPublicacao="+idPublicacao,
	        success : function(response) {
	        	 $("#formularioCadastroEdicao").html(response);
	        },
	        error : function(e) {
	            alert("Falha ao carregar os Dados");
	        }
	    });
}

function carregarFormularioCategoria(id) {
    var methodURL= "/SmartEditorial/categorias/cadastro"
    $.ajax({
        type : "POST",
        url : methodURL,
        data : "idPublicacao="+id,
        success : function(response) {
            $("#formularioCadastroCategoria").html(response);
        },
        error : function(e) {
            alert("Falha ao carregar os Dados");
        }
    });
}
function carregarFormularWorkFlow(titulo, idPublicacao) {
    var methodURL= "/SmartEditorial/fluxos/workflow"
    $.ajax({
        type : "POST",
        url : methodURL,
        data : "tipoElemento="+titulo+"&idPublicacao="+idPublicacao,
        success : function(response) {
            $("#formularioCadastroWorkFlow").html(response);
        },
        error : function(e) {
            alert("Falha ao carregar os Dados");
        }
    });
}

function removerCategoria(idCategoria) {
	
	var methodURL= "/SmartEditorial/categorias/remover"
	    $.ajax({
	        type : "POST",
	        url : methodURL,
	        data : "idCategoria="+idCategoria,
	        success : function(response) {
	        	if (response == 'vinculos') {
	        		var opts = {
							"closeButton": true,
							"debug": false,
							"positionClass": "toast-top-right",
							"onclick": null,
							"showDuration": "300",
							"hideDuration": "1000",
							"timeOut": "10000",
							"extendedTimeOut": "1000",
							"showEasing": "swing",
							"hideEasing": "linear",
							"showMethod": "fadeIn",
							"hideMethod": "fadeOut"
						};
						
						toastr.error("N�o pudemos remover esta Categoria pois existem conte�dos vinculado � ela. ", "Puxa! Achamos um erro:", opts);
				} else if(response == 'sucesso') {
					location.reload();
				} else {
					$("#retornoExcluir").html("<p style='color: red;'>Erro interno, contate o administrador do Sistema</p>");
				}
	        },
	        error : function(e) {
	            alert("Falha ao carregar os Dados");
	        }
	    });
}

function chamarModalEdicao(id) {
	$('#botaoSimEdicao').attr('onclick', 'removerEdicao('+id+')');
} 
function chamarModalCategoria(id) {
	$('#botaoSimCategoria').attr('onclick', 'removerCategoria('+id+')');
} 
</script>
</head>
<body>

<body class="page-body">
<div class="page-container horizontal-menu">
	<%@ include file="../componentes/header/menu.jsp" %>
	<div class="main-content" id="main-content">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="row">
						<div class="col-lg-12">
							<h2 class="page-header">
								${publicacao.titulo} <i class="fa fa-th-list"></i>
							</h2>
							<div class="panel-body">
								<div id="retornoExcluir" class="col-lg-12"></div>
								<div id="editarExcluir" class="col-lg-12"></div>
							</div>
						</div>
					</div>
				</div>
			
				
			</div>
			<!-- /.row -->
		
			<div class="row">
				<div class="col-lg-12">
					
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								 <label class="control-label"><i class="fa fa-pencil-square-o"></i> Edi��es <small>Gerencie suas edi��es</small></label>
							</h4>
							<hr />
						</div>
						
						<div class="panel-body">
							<button type="button" class="btn btn-default btn-icon icon-left" 
							onclick="carregarFormulariEdicao('${publicacao.idPublicacao}')" data-toggle="modal" data-target="#cadastrarEdicaoModal" data-keyboard="false" data-backdrop="false">
								Criar Edi��o  
								<i class="fa fa-plus"></i>
							</button>
							<table class="table table-bordered"
								id="dataTables-EdicoesDaPublicacao">
								<thead>
									<tr>
										<th>Edi��o</th>
										<th>Edit�vel at�</th>
										<th>Data Publica��o</th>
										<th>A��es</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${publicacao.edicoes}" var="edicao">
										<tr class="even gradeA">
										
											<td><b> <a href="publicacao/edicao?idEdicao=${edicao.idEdicao}"><i class="fa fa-folder-open-o"></i> ${edicao.titulo}</a> </b></td>
											<td><fmt:formatDate value="${edicao.dataLimiteEdicao}"
													pattern="dd/MM/yyyy" /></td>
											<td><fmt:formatDate value="${edicao.dataPublicacao}"
													pattern="dd/MM/yyyy" /></td>
											<td><a href="#" onclick="editarEdicao('${edicao.idEdicao}', '${publicacao.idPublicacao}')"
												class="btn btn-default btn-sm btn-icon icon-left"
												data-toggle="modal" data-target="#cadastrarEdicaoModal" data-keyboard="false" data-backdrop="false">
												<i class="entypo-pencil"></i> Editar
											</a> <a href="#" onclick="chamarModalEdicao(${edicao.idEdicao})"
												class="btn btn-danger btn-sm btn-icon icon-left" data-toggle="modal" data-target="#modalRemoveEdicao" data-backdrop="false">
												<i class="entypo-cancel"></i> Deletar
											</a>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
							
							<div id="modalRemoveEdicao" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
									<div class="modal-dialog">
										<div class="modal-content">
										
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
												<h4 class="modal-title" id="myModalLabel">Deseja Remover?</h4>
											</div>
											<div class="modal-body">
												<p>Voc� est� prestes a remover uma Edi��o desta Publica��o, confirma isso?</p>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-default" data-dismiss="modal">N�o</button>
												<button type="button" id="botaoSimEdicao" class="btn btn-primary" onclick="">Sim</button>
											</div>
										
										</div><!-- /.modal-content -->
									</div><!-- /.modal-dialog -->
								</div>  
						</div>
					</div>
					<div id="cadastrarEdicaoModal" class="modal fade" tabindex="-1"
						role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">
										<span aria-hidden="true"><i class="fa fa-times fa-2x"></i></span>
									</button>
								</div>
								<div class="modal-body">
									<div id="formularioCadastroEdicao"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<%@ include file="include-row2.jsp" %>
			<%@ include file="../componentes/footer/footer.jsp" %>
		</div>
	</div>
</div>
</body>
</html>