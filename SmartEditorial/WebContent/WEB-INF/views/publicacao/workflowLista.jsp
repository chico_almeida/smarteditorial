<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				Fluxos para : <label style="text-transform: capitalize; padding: 0px 0px 0px 0px;margin-bottom: 0px">${elemento}</label>
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover" id="dataTablesFluxos">
						<thead>
							<tr>
								<th>Ordem</th>
								<th>Origem</th>
								<th>Destino</th>
								<th>A��es</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${listaFluxoImagem}" var="fluxo">
								<tr class="even gradeA">
									<td>
										<a href="#" onclick="removerFluxo('${fluxo.idFluxo}')">
											<i class="fa fa-times"></i>
										</a>
										${fluxo.ordem}
									</td>
									<td>
									${fluxo.grupoOrigem.nome}
									</td>
									<td>${fluxo.grupoDestino.nome}</td>
									<td>
										Pode Interromper: 
											<img src="
												<c:if test="${fluxo.podeInterromper == true}">
													/SmartEditorial/resources/images/check-green.png
												</c:if>
												<c:if test="${fluxo.podeInterromper == false}">
													/SmartEditorial/resources/images/check-red.png
												</c:if>
											" width="15" height="15">
										Pode Finalizar: 
											<img src="
												<c:if test="${fluxo.podeFinalizar == true}">
													/SmartEditorial/resources/images/check-green.png
												</c:if>
												<c:if test="${fluxo.podeFinalizar == false}">
													/SmartEditorial/resources/images/check-red.png
												</c:if>
											" width="15" height="15">
									</td>
									
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				<!-- /.table-responsive -->
				<div id="retornoExcluir" class="validocoes"></div>
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
	<script type="text/javascript">
	var atualValue = "${lastValue}";
	var grupo = "${grupo.idGrupo}";
	if(atualValue == "") {
		document.getElementById("ordem").value = "1";
		document.getElementById("ordem").readOnly = true;
		document.getElementById("origem").value = "";
		document.getElementById("destino").value = "";
		$('#origem').css({'pointer-events':'all','cursor':'auto','background-color':'#fff'})
	} else {
		var nextValue = ++atualValue;
		document.getElementById("ordem").value = nextValue;
		document.getElementById("ordem").readOnly = true;
		document.getElementById("origem").value = grupo;
		document.getElementById("destino").value = "";
		$('#origem').css({'pointer-events':'none','cursor':'not-allowed','background-color':'#eee'})
	}
</script>
