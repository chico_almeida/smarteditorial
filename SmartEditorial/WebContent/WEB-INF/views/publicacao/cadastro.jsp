<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ page session="false" %>
<c:url var="addAction" value="/publicacao/cadastro/criar"></c:url>
<c:url var="updateAction" value="/publicacao/cadastro/update"></c:url>
<c:if test="${empty publicacao.titulo}">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header">
					Criar Publicação
				</h2>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<!-- /.panel-heading -->
					<div class="panel-body">
						<div class="table-responsive">
						<form action="${addAction}" method="POST" onsubmit="existePublicacao(this.value)"> 
							<label class="control-label" for="formGroupInputLarge">
								Nome
							</label>
							<br>
							<input type="text" class="form-control" placeholder="Nome da Publicação" name="titulo" id="tit" onchange="existePublicacao(this.value)" required="required">
							<div id="retorno" class="validacoes"></div>
							<br>
							<label class="control-label" for="formGroupInputLarge">
								Descrição
							</label>
							<br>
							<textarea class="form-control" rows="5" cols="25" name="descricao"></textarea>
							
							<br>
							<div class="form-group">
								<button type="submit" class="btn btn-green btn-icon icon-left" onsubmit="this.disabled=true;" id="mySubmit">
	                            	Adicionar
	                            	<i class="fa fa-floppy-o"></i>
	                            </button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</c:if>
<c:if test="${!empty publicacao.titulo}">
	<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header">
					Atualizar Publicação
				</h2>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						Dados da Publicação
					</div>
					<!-- /.panel-heading -->
					<div class="panel-body">
						<div class="table-responsive">
						<form action="${updateAction}" method="POST" onsubmit="return valida_form(this)"> 
							<input type="hidden" name="idPublicacao" value="${publicacao.idPublicacao}" readonly="readonly" />
							<label class="control-label" for="formGroupInputLarge">
								Nome
							</label>
							<br>
							<input type="text" class="form-control" value ="${publicacao.titulo}" name="titulo" required="required">
							<br>
							<label class="control-label" for="formGroupInputLarge">
								Descrição
							</label>
							<br>
							<textarea class="form-control" rows="3" name="descricao">${publicacao.descricao}</textarea>
							<br>
							<div class="control-label">
								<button type="submit" class="btn btn-green btn-icon icon-left" onsubmit="this.disabled=true;" id="mySubmit">
	                            	Atualizar
	                            	<i class="fa fa-floppy-o"></i>
	                            </button>
							</div>
							
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</c:if>
