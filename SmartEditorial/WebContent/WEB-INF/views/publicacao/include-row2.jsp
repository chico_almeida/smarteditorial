<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ page session="false"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<div class="row">
	<div class="col-lg-12">
		<div class="col-lg-6">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						 <label class="control-label"><i class="fa fa-pencil-square-o"></i> Fluxos de Trabalho <small>Gerencie seus Fluxos de Trabalho</small></label>
					</h4>
					<hr />
				</div>
				<div class="panel-body">
					<table class="table"
						id="dataTables-publicacaoWorkFlow">
						<thead>
							<tr>
								<th>Elemento</th>
							</tr>
						</thead>
						<tbody>
							<tr class="even gradeA">
								<td>
									<a href="#" onclick="carregarFormularWorkFlow('artigo', ${publicacao.idPublicacao})"
										data-toggle="modal" data-target="#workflowModal"
										data-keyboard="false" data-backdrop="false">
										<i class="fa fa-plus"></i>
										Artigo
									</a>
								</td>
							</tr>
							<tr class="even gradeA">
								<td>
									<a href="#" onclick="carregarFormularWorkFlow('anuncio', ${publicacao.idPublicacao})"
										data-toggle="modal" data-target="#workflowModal"
										data-keyboard="false" data-backdrop="false">
										<i class="fa fa-plus"></i>
										An�ncio
									</a>
								</td>
							</tr>
							<tr class="even gradeA">
								<td>
									<a href="#" onclick="carregarFormularWorkFlow('imagem', ${publicacao.idPublicacao})"
										data-toggle="modal" data-target="#workflowModal"
										data-keyboard="false" data-backdrop="false">
										<i class="fa fa-plus"></i>
										Imagem
									</a>
								</td>
							</tr>
							<tr class="even gradeA">
								<td>
									<a href="#" onclick="carregarFormularWorkFlow('pagina', ${publicacao.idPublicacao})"
										data-toggle="modal" data-target="#workflowModal"
										data-keyboard="false" data-backdrop="false">
										<i class="fa fa-plus"></i>
										P�gina
									</a>
								</td>
							</tr>
						</tbody>
					</table>  
				</div>
			</div>
			<div id="workflowModal" class="modal fade" tabindex="-1"
				role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog" style="width: 70%!important">
					<div class="modal-content" >
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">
								<span aria-hidden="true"><i class="fa fa-times fa-2x"></i></span>
							</button>
						</div>
						<div class="modal-body">
							<div id="formularioCadastroWorkFlow"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END DIV COLUMN WORKFLOW -->
		<!-- START DIV COLUMN CATEGORIA -->
		<div class="col-lg-6">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						 <label class="control-label"><i class="fa fa-pencil-square-o"></i> Categorias <small>Gerencie suas categorias</small></label>
					</h4>
					<hr />
				</div>
				<div class="panel-body">
					<button type="button" class="btn btn-default btn-icon icon-left" 
					onclick="carregarFormularioCategoria('${publicacao.idPublicacao}')" data-toggle="modal" data-target="#CategoriaModal" data-keyboard="false" data-backdrop="false">
						Criar Categoria
						<i class="fa fa-plus"></i>
					</button>
					<table class="table table-bordered" id="dataTables-CategoriasDaPublicacao">
						<thead>
							<tr>
								<th>Titulo</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${publicacao.categorias}" var="categoria">
								<tr class="even gradeA">
									<td>
										<a href="#" onclick="chamarModalCategoria(${categoria.idCategoria})" data-toggle="modal" data-target="#myModal" data-backdrop="false">
											<i class="fa fa-times"></i>
										</a>
										<b> ${categoria.titulo} </b>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
						<div class="modal-dialog">
							<div class="modal-content">
							
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
									<h4 class="modal-title" id="myModalLabel">Deseja Remover?</h4>
								</div>
								<div class="modal-body">
									<p>Voc� est� prestes a remover uma Categoria desta Publica��o, confirma isso?</p>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">N�o</button>
									<button type="button" id="botaoSimCategoria" class="btn btn-primary" onclick="">Sim</button>
								</div>
							
							</div><!-- /.modal-content -->
						</div><!-- /.modal-dialog -->
					</div>
				</div>
			</div>
			<div id="CategoriaModal" class="modal fade" tabindex="-1"
				role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">
								<span aria-hidden="true"><i class="fa fa-times fa-2x"></i></span>
							</button>
						</div>
						<div class="modal-body">
							<div id="formularioCadastroCategoria"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END DIV COLUMN CATEGORIA -->
	</div>
</div>
	
