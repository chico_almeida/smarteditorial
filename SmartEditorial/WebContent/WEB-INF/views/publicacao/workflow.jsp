<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ page session="false" %>

<script type="text/javascript">

$("#formCadastro").submit(function() {
    var $form = $(this);
    $.ajax({
        url: "/SmartEditorial/fluxos/criar",
        data: $form.serialize(),
        type: "POST",
        /* statusCode: {
            404: function() {
                alert("");
            },
            500: function() {
                alert("");
            }
        }, */
        success: function() {
			reloadList();
		},
		error : function(e) {
			alert(e);
		}
    });
    return false;
});

function reloadList() {
	var elemento = "${elemento}"
	var idPublicacao = "${idPublicacao}"
	var methodURL = "/SmartEditorial/fluxos/recarregarLista"
	$.ajax({
		type : "POST",
		url : methodURL,
		data : "elemento="+elemento+"&idPublicacao="+idPublicacao,
		success : function(response) {
			$("#rowlist").html(response);
		},
		error : function(e) {
			alert("Falha ao validar titulo");
		}
	});
}

function teste() {
	var numero = document.getElementById("valueOrder").value;
	
	alert(numero);
}


function validarDestino() {
	var origem = document.getElementById("origem").value;
	var destino = document.getElementById("destino").value;

	if (origem == destino) {
		$("#mensagem").html("O destino n�o pode ser o mesmo que a origem");
		document.getElementById("mySubmit").disabled = true;
		document.getElementById("destino").focus();
		return false;
	} else {
		document.getElementById("mySubmit").disabled = false;
		$("#mensagem").html("");
	}
}

function removerFluxo(idFluxo) {
	var methodURL = "/SmartEditorial/fluxos/remover"
	$.ajax({
		type : "POST",
		url : methodURL,
		data : "idFluxo=" + idFluxo,
		success : function(response) {
			if (response == 'vinculos') {
				$("#retornoExcluir").html("Existe conteudo vinculado � esta categoria.");
			} else if (response == 'sucesso') {
				reloadList();
			} else {
				$("#retornoExcluir").html("Erro interno, contate o administrador do Sistema");
			}
		},
		error : function(e) {
			alert("Falha ao carregar os Dados");
		}
	});
}
</script>

<c:url var="addAction" value="/fluxos/imagem/criar"></c:url>
<c:if test="${empty fluxoImagem.idFluxo}">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header">
					Gerenciar Fluxo : <label style="text-transform: capitalize; padding: 0px 0px 0px 0px;margin-bottom: 0px">${elemento}</label>
				</h2>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						Dados do Fluxo
					</div>
					<!-- /.panel-heading -->
					<div class="panel-body">
						<div class="table-responsive">
						<form method="POST" id="formCadastro">
				 			<input type="hidden" value="${elemento}" name="tipoElemento">
				 			<input type="hidden" value="${idPublicacao}" name="idPublicacao">
							<div class="col-lg-1">
								<label class="control-label" for="formGroupInputLarge">
									Ordem
								</label>
								<input type="text" id="ordem" class="form-control" name="ordem" required="required" value="1" readonly="readonly">
							</div>
							<div class="col-lg-3">
								<label class="control-label" for="formGroupInputLarge" onchange="teste()">
									Origem
								</label>
								
								<select name="origem" required class="form-control" id="origem">
									<option id="grupoOrigem" value="">
										--Selecione--
									</option>
									<c:forEach var="grupo" items="${listaGrupo}">
										<option id="grupoOrigem" value="${grupo.idGrupo}">
											${grupo.nome}
										</option>
									</c:forEach>
								</select>							
							</div>
							<div class="col-lg-3">
								<label class="control-label" for="formGroupInputLarge">
									Destino
								</label>
								
								<select name="destino" required class="form-control" id="destino" onchange="validarDestino()">
									<option id="grupoDestino" value="">
										--Selecione--
									</option>
									<c:forEach var="grupo" items="${listaGrupo}">
										<option id="grupoDestino" value="${grupo.idGrupo}">
											${grupo.nome}
										</option>
									</c:forEach>
								</select>
							</div> 
							<div class="col-lg-5">
								<label class="control-label" for="formGroupInputLarge">
									A��es
								</label>
								<div class="col-lg-12">
								<label class="control-label" for="formGroupInputLarge">
									Interromper <input type="checkbox" name="podeInterromper" >
									Finalizar <input type="checkbox" name="podeFinalizar" >
									</label>
									<button type="submit" class="btn btn-green btn-icon icon-left" onsubmit="this.disabled=true;" id="mySubmit">
		                            	Adicionar
		                            	<i class="fa fa-floppy-o"></i>
		                            </button>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div id="mensagem" class="validacoes"></div>
				
			</div>
		</div>
	</div>
</c:if>
<div class="row" id="rowlist">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				Fluxos para : <label style="text-transform: capitalize; padding: 0px 0px 0px 0px;margin-bottom: 0px">${elemento}</label>
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover" id="dataTablesFluxos">
						<thead>
							<tr>
								<th>Ordem</th>
								<th>Origem</th>
								<th>Destino</th>
								<th>A��es</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${listaFluxoImagem}" var="fluxo">
								<tr class="even gradeA">
									<td>
										<a href="#" onclick="removerFluxo('${fluxo.idFluxo}')">
											<i class="fa fa-times"></i>
										</a>
										${fluxo.ordem}
									</td>
									<td>
										${fluxo.grupoOrigem.nome}
									</td>
									<td>
										${fluxo.grupoDestino.nome}
									</td>
									<td>
										Pode Interromper: 
											<img src="
												<c:if test="${fluxo.podeInterromper == true}">
													/SmartEditorial/resources/images/check-green.png
												</c:if>
												<c:if test="${fluxo.podeInterromper == false}">
													/SmartEditorial/resources/images/check-red.png
												</c:if>
											" width="15" height="15">
										Pode Finalizar: 
											<img src="
												<c:if test="${fluxo.podeFinalizar == true}">
													/SmartEditorial/resources/images/check-green.png
												</c:if>
												<c:if test="${fluxo.podeFinalizar == false}">
													/SmartEditorial/resources/images/check-red.png
												</c:if>
											" width="15" height="15">
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				<!-- /.table-responsive -->
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<script type="text/javascript">
	var atualValue = "${lastValue}";
	var grupo = "${grupo.idGrupo}";
	
	if(atualValue == "") {
		document.getElementById("ordem").value = "1";
		document.getElementById("ordem").readOnly = true;
		document.getElementById("origem").value = "";
		document.getElementById("destino").value = "";
		$('#origem').css({'pointer-events':'all','cursor':'auto','background-color':'#fff'})
	} else {
		var nextValue = ++atualValue;
		document.getElementById("ordem").value = nextValue;
		document.getElementById("ordem").readOnly = true;
		document.getElementById("origem").value = grupo;
		document.getElementById("destino").value = "";
		$('#origem').css({'pointer-events':'none','cursor':'not-allowed','background-color':'#eee'})
	}
</script>
<!-- /.row -->