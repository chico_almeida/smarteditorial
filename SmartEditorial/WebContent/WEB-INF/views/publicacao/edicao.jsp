<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<label class="col-sm-3 control-label">Edi��o</label>
	
	<div class="col-sm-8">
		<select id="idEdicao" required name="idEdicao"  class="form-control">
			<option id="publicacao" value="" >--Selecione--</option>
			<c:forEach var="item" items="${listaEdicoes}">
				<option id="publicacao" value="${item.idEdicao}" >${item.titulo} </option>
			</c:forEach>
		</select>
	</div>
