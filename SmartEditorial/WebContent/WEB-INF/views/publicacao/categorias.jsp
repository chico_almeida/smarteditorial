<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

	<label class="col-sm-3 control-label">Categorias</label>
	
	<div class="col-sm-8">
		<select id="idCategoria" required name="idCategoria"  class="form-control">
			<option id="publicacao" value="" >--Selecione--</option>
			<c:forEach var="item" items="${listaCategorias}">
				<option id="publicacao" value="${item.idCategoria}" >${item.titulo} </option>
			</c:forEach>
		</select>
	</div>
