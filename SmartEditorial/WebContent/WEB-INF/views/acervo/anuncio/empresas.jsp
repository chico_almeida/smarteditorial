<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script type="text/javascript">
$("#formCadastroEmpresa").submit(function() {
    var $form = $(this);
    $.ajax({
        url: "/SmartEditorial/anuncios/empresas/add",
        data: $form.serialize(),
        type: "POST",
        /* statusCode: {
            404: function() {
                alert("");
            },
            500: function() {
                alert("");
            }
        }, */
        success: function() {
        	carregarEmpresas();
		},
		error : function(e) {
			alert(e);
		}
    });
    return false;
});

</script>

<div class="panel-body">
	<ul class="nav nav-tabs" role="tablist" id="myTab">
	  <li role="presentation" class="active"><a href="#empresas" role="tab" data-toggle="tab">Empresas</a></li>
	  <li role="presentation"><a href="#cadastrarEmpresa" role="tab" data-toggle="tab">Cadastrar Empresa</a></li>
	</ul>
	<br />
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="empresas">
			<table class="table table-bordered datatable" id="dataTables-listaDeEmpresas">
				<thead>
					<tr>
						<th>Empresa</th>
						<th>Contato</th>
						<th>Email</th>
						<th>Telefone</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${listaDeEmpresas}" var="empresa">
						<tr class="odd gradeX">
							<td>
								<a href="#" onclick="setEmpresa('${empresa.empresa}')">
									${empresa.empresa}
								</a>
							</td>
							<td>${empresa.contatoComercial}</td>
							<td>${empresa.email}</td>
							<td>${empresa.telefone}</td>
						</tr>
					</c:forEach>	
				</tbody>
			</table>  
		</div>								
		<div role="tabpanel" class="tab-pane" id="cadastrarEmpresa">
			<c:url var="addAction" value="/anuncios/empresas/add"></c:url>
			<form id="formCadastroEmpresa" method="POST" class="form-horizontal form-groups-bordered" role="form">

				<div class="form-group">
					<label class="col-sm-3 control-label">Empresa</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="empresas" name="empresa" required="required"/>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Contato Comercial</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="contatoComercial" name="contatoComercial" />
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label">E-mail</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="email" name="email" />
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Telefone</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="telefone" name="telefone" data-mask="phone" />
					</div>
				</div>
				
				<div class="form-group">
					<button type="submit" class="btn btn-green btn-icon icon-left" onsubmit="this.disabled=true;">
                    	Adicionar
                    	<i class="fa fa-floppy-o"></i>
                    </button>
				</div>
		</form>
		</div>
	</div>
	<script>
	 $(function () {
	   $('#myTab a:first').tab('show')
	 })
	</script>	
</div>


