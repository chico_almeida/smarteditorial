<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ page session="false" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>An�ncios - Cadastrar </title>
<%@ include file="../../componentes/header/head.jsp" %>
<script type="text/javascript">
function getEdicoes() {
	var methodURL= "/SmartEditorial/publicacao/pegaedicao"
	$.ajax({ 
		type : "POST",
		url : methodURL,
		data : "idPublicacao="+idPublicacao.value,
		success : function(response) {
			$("#edicao").html(response);
		},
		error : function(e) {
			alert("Falha ao carregar os Dados");
		}
	});
}

function getCategorias() {
	var methodURL= "/SmartEditorial/publicacao/pegacategorias"
	$.ajax({
		type : "POST",
		url : methodURL,
		data : "idPublicacao="+idPublicacao.value,
		success : function(response) {
			$("#categorias").html(response);
		},
		error : function(e) {
			alert("Falha ao carregar os Dados");
		}
	});
}

function showimagepreview(input) {
	if (input.files && input.files[0]) {
		var filerdr = new FileReader();
		filerdr.onload = function(e) {
			$('#imgprvw').attr('src', e.target.result);
		};
		filerdr.readAsDataURL(input.files[0]);
	}
}

function setEmpresa(empresa) {
	$('#campoEmpresa').attr('value', empresa);
	$('#carregarEmpresas').modal('hide');
}

function carregarEmpresas() {
	var methodURL= "/SmartEditorial/anuncios/empresas"
	$.ajax({
		type : "POST",
		url : methodURL,
		success : function(response) {
		    $('#dataTables-listaDeEmpresas').dataTable();
			$("#listaEmpresas").html(response);
			$(document).ready(function() {
				$('#dataTables-listaDeEmpresas').dataTable();
		    });
		},
		error : function(e) {
			alert("Falha ao carregar os Dados");
		}
	});
}

$("#formCadastroEmpresa").submit(function() {
    var $form = $(this);
    $.ajax({
        url: "/SmartEditorial/anuncios/empresas/add",
        data: $form.serialize(),
        type: "POST",
        /* statusCode: {
            404: function() {
                alert("");
            },
            500: function() {
                alert("");
            }
        }, */
        success: function() {
        	carregarEmpresas();
		},
		error : function(e) {
			alert(e);
		}
    });
    return false;
});

</script>
</head>
<body class="page-body">
<div class="page-container horizontal-menu">
	<%@ include file="../../componentes/header/menu.jsp" %>
	<div class="main-content" id="main-content">
		<div class="container">
			<div class="row"> 
				<div class="col-md-12">
					<div class="row"> 
						<div class="col-md-12">
							<div class="panel-heading">
								<h4 class="panel-title">
									 <label class="control-label"><i class="fa fa-pencil-square-o"></i> An�ncio <small>Cadastre ou edite seus anuncios</small></label>
								</h4>
								<hr />
							</div>
						</div>
					</div>
					<div class="row">
						<c:url var="addAction" value="/anuncios/cadastro/add"></c:url>
						<c:url var="updateAction" value="/anuncios/cadastro/update"></c:url>
						
						<c:if test="${empty anuncio.titulo}">
							<div class="row">
								<div class="row text-center" id="containerFoto">
								<div class="col-md-6">
									<div class="panel panel-primary">
										<div class="panel-heading">
											<h3 class="panel-title">
												Dados do An�ncio
												<span class="glyphicon glyphicon-edit">
												</span>
											</h3>
										</div>
										<div class="panel-body">
												<form action="${addAction}" method="POST" enctype="multipart/form-data" id="myform" class="form-horizontal form-groups-bordered" role="form">
												
												
													<div class="form-group">
														<label for="titulo" class="col-sm-3 control-label">Titulo</label>
														<div class="col-sm-8">
															<input type="text" class="form-control" id="titulo" name="titulo" required="required"/>
														</div>
													</div>
													<div class="form-group">
														<label class="col-sm-3 control-label">Arquivo</label>
														
														<div class="col-sm-8">
														
															<div class="fileinput fileinput-new" data-provides="fileinput">
																<div class="input-group">
																	<div class="form-control uneditable-input" data-trigger="fileinput">
																		<i class="glyphicon glyphicon-file fileinput-exists"></i>
																		<span class="fileinput-filename"></span>
																	</div>
																	<span class="input-group-addon btn btn-default btn-file">
																		<span class="fileinput-new">Selecione</span>
																		<span class="fileinput-exists">Mudar</span>
																		<input type="file" name="arquivo" accept="image/x-png, image/gif, image/jpeg" required="required"onchange="showimagepreview(this)">
																	</span>
																	<a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remover</a>
																</div>
															</div>
															
														</div>
													</div>
													
													<div class="form-group">
														<label class="col-sm-3 control-label">Publica��o</label>
														
														<div class="col-sm-8">
															<select id="idPublicacao" class="form-control" name="idPublicacao" required onchange="getEdicoes(this.value), getCategorias(this.value)">
																<option id="publicacao" value="" >--Selecione--</option>
																<c:forEach var="item" items="${listaDePublicacoes}">
																	<option id="publicacao" value="${item.idPublicacao}" >${item.titulo} </option>
																</c:forEach>
															</select>
														</div>
													</div>

													<div class="form-group" id="edicao">
													</div>
													
													<div class="form-group" id="categorias">
													</div>
													
													<div class="form-group">
														<label class="col-sm-3 control-label">Empresa</label>
														
														<div class="col-sm-8">
														
															<div class="fileinput fileinput-new" data-provides="fileinput">
																<div class="input-group">
																	<div class="form-control uneditable-input" data-trigger="fileinput">
																	<input id="campoEmpresa" value="" readonly="readonly" type="text" name="nomeEmpresa" required="required" style="border: 0px">
																	</div>
																	<span class="input-group-addon btn btn-default btn-file">
																		<a href="#" onclick="carregarEmpresas()" data-toggle="modal" data-target="#carregarEmpresas" data-keyboard="false" data-backdrop="false">
																			<span class="fileinput-new">Selecione</span>
																		</a>
																	</span>
																</div>
															</div>
															
														</div>
													</div>
													
													
													<div class="form-group">
														<label class="col-sm-3 control-label">Palavras Chaves</label>
														<div class="col-sm-8">
															<input type="text" id="pchaves" name="pchaves" class="form-control tagsinput" />
														</div>
													</div>

													<div class="form-group">
														<button type="submit" class="btn btn-green btn-icon icon-left" onsubmit="this.disabled=true;">
							                            	Adicionar
							                            	<i class="fa fa-floppy-o"></i>
							                            </button>
													</div>
											</form>
										</div>
									</div>
								</div>
								<div class="col-md-6" style="" draggable="true">
										<div class="hidden-sm hidden-xs panel panel-primary" id="panelFoto">
											<div class="panel-heading">
												<h3 class="panel-title">
													Preview do An�ncio &nbsp; &nbsp;&nbsp;
													<span class="glyphicon glyphicon-picture">
													</span>
												</h3>
											</div>
											<div class="panel-body">
												<img id="imgprvw" class="col-md-12"	/>
											</div>
										</div>
									</div>
								</div>
							</div>
						</c:if>
						<c:if test="${!empty anuncio.titulo}">
							
							<div class="row">
								<div class="row text-center" id="containerFoto">
								<div class="col-md-6">
									<div class="panel panel-primary">
										<div class="panel-heading">
											<h3 class="panel-title">
												Dados do An�ncio
												<span class="glyphicon glyphicon-edit">
												</span>
											</h3>
										</div>
										<div class="panel-body">
												<form action="${updateAction}" method="POST" enctype="multipart/form-data" id="myform" class="form-horizontal form-groups-bordered" role="form">
													<input type="hidden" value="${anuncio.idAnuncio}" name="idAnuncio">
													<input type="hidden" value="${anuncio.altura}" name="altura">
													<input type="hidden" value="${anuncio.largura}" name="largura">
													<input type="hidden" value="${anuncio.estado}" name="estado">
													<input type="hidden" value="${anuncio.caminho}" name="caminho">
													<div class="form-group">
														<label for="titulo" class="col-sm-3 control-label">Titulo</label>
														<div class="col-sm-8">
															<input type="text" class="form-control" id="titulo" name="titulo" value="${anuncio.titulo}" required="required"/>
														</div>
													</div>
													<div class="form-group">
														<label class="col-sm-3 control-label">Arquivo</label>
														<div class="col-sm-8">
															<div class="fileinput fileinput-new" data-provides="fileinput">
																<div class="input-group">
																	<div class="form-control uneditable-input" data-trigger="fileinput">
																		<i class="glyphicon glyphicon-file fileinput-exists"></i>
																		<span class="fileinput-filename">${anuncio.titulo}</span>
																	</div>
																	<span class="input-group-addon btn btn-default btn-file">
																		<span class="fileinput-new">Selecione</span>
																		<span class="fileinput-exists">Mudar</span>
																		<input type="file" name="arquivo" accept="image/x-png, image/gif, image/jpeg" required="required" onchange="showimagepreview(this)" value="${anuncio.caminho}" disabled="disabled">
																	</span>
																	<a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remover</a>
																</div>
															</div>
														</div>
													</div>
													
													<div class="form-group">
														<label class="col-sm-3 control-label">Publica��o</label>
														<div class="col-sm-8">
															<select id="idPublicacao" class="form-control" name="idPublicacao" required onchange="getEdicoes(this.value)">
																<option id="publicacao" value="${anuncio.edicao.publicacao.idPublicacao}" style="background: rgba(0,0,0,0.3);">
																	${anuncio.edicao.publicacao.titulo}
																</option>
																<c:forEach var="item" items="${listaDePublicacoes}">
																	<option id="publicacao" value="${item.idPublicacao}">
																		${item.titulo}
																	</option>
																</c:forEach>
															</select>
														</div>
													</div>
													<div class="form-group" draggable="true" id="edicao">
														<label class="col-sm-3 control-label">Edi��o</label>
														<div class="col-sm-8">
															<select id="idEdicao" name="idEdicao" required class="form-control">
																<option id="publicacao" value="${anuncio.edicao.idEdicao}" style="background: rgba(0,0,0,0.3);">
																	${anuncio.edicao.titulo}
																</option>
																<c:forEach var="item" items="${listaDeEdicoes}">
																	<option id="edicao" value="${item.idEdicao}" >${item.titulo} </option>
																</c:forEach>
															</select>
														</div>
													</div>
													<div class="form-group" draggable="true" id="categorias">
														<label class="col-sm-3 control-label">Categoria</label>
														<div class="col-sm-8">
															<select id="idCategoria" name="idCategoria" required class="form-control">
																<option id="publicacao" value="${anuncio.categoria.idCategoria}" style="background: rgba(0,0,0,0.3);">
																	${anuncio.categoria.titulo}
																</option>
																<c:forEach var="item" items="${listaDeCategorias}">
																	<option id="categoria" value="${item.idCategoria}" >${item.titulo} </option>
																</c:forEach>
															</select>
														</div>
													</div>

													
													
													<div class="form-group">
														<label class="col-sm-3 control-label">Empresa</label>
														
														<div class="col-sm-8">
														
															<div class="fileinput fileinput-new" data-provides="fileinput">
																<div class="input-group">
																	<div class="form-control uneditable-input" data-trigger="fileinput">
																	<input id="campoEmpresa" value="${anuncio.empresa.empresa}" readonly="readonly" type="text" name="nomeEmpresa" required="required" style="border: 0px">
																	</div>
																	<span class="input-group-addon btn btn-default btn-file">
																		<a href="#" onclick="carregarEmpresas()" data-toggle="modal" data-target="#carregarEmpresas" data-keyboard="false" data-backdrop="false">
																			<span class="fileinput-new">Selecione</span>
																		</a>
																	</span>
																</div>
															</div>
															
														</div>
													</div>
													
													
													<div class="form-group">
														<label class="col-sm-3 control-label">Palavras Chaves</label>
														<div class="col-sm-8">
															<input type="text" id="pchaves" name="pchaves" value='<c:forEach var='tag' items='${anuncio.tags}' >${tag.tags},</c:forEach>' class="form-control tagsinput" />
														</div>
													</div>

													<div class="form-group">
														<button type="submit" class="btn btn-green btn-icon icon-left" onsubmit="this.disabled=true;">
							                            	Adicionar
							                            	<i class="fa fa-floppy-o"></i>
							                            </button>
													</div>
											</form>
										</div>
									</div>
								</div>
								<div class="col-md-6" style="" draggable="true">
										<div class="hidden-sm hidden-xs panel panel-primary" id="panelFoto">
											<div class="panel-heading">
												<h3 class="panel-title">
													Preview do An�ncio &nbsp; &nbsp;&nbsp;
													<span class="glyphicon glyphicon-picture">
													</span>
												</h3>
											</div>
											<div class="panel-body">
												<img id="imgprvw1" class="col-md-12"src='${contextPath}${anuncio.caminho}'/>
											</div>
										</div>
									</div>
								</div>
							</div>
						</c:if>
					</div>
					<div class="row">
						<div class="row text-center" id="containerFoto">
							<div class="col-md-12">
								<div class="panel panel-primary">
									<div id="carregarEmpresas" class="modal fade" tabindex="-1"
											role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">
														<span><i class="fa fa-times fa-2x"></i></span>
													</button>
												</div>
												<div class="modal-body">
													<div id="listaEmpresas">
													
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<%@ include file="../../componentes/footer/footer.jsp" %>
			</div>
		</div>
	</div>
</div>

</body>
</html>