<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ page session="false" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Imagens</title>
<%@ include file="../../componentes/header/head.jsp" %>
<script type="text/javascript">
window.onload = function(){
	$(document).ready(function() {
		$('#dataTables-minhasImagensForaDoFluxo').dataTable();
		$('#dataTables-minhasImagensEmFluxo').dataTable();
		$('#dataTables-minhasImagensFinalizadas').dataTable();
   });
};

function chamarModalIniciarFluxo(id) {
	$('#botaoSim').attr('onclick', 'this.disabled=true, iniciarFluxo('+id+')');
}
function iniciarFluxo(id) {
	var methodURL= "/SmartEditorial/imagem/iniciar-fluxo";
	var comentario = document.getElementById("comentarioFluxo").value;
	if (comentario == "") {
		comentario = " ";
	} 
	$.ajax({
		type : "POST",
		url : methodURL,
		data: "idImagem="+id+"&comentario="+comentario,
		success : function(response) {
			if (response == "sucesso") {
				var opts = {
					"closeButton": true,
					"debug": false,
					"positionClass": "toast-top-right",
					"onclick": null,
					"showDuration": "300",
					"hideDuration": "1000",
					"timeOut": "10000",
					"extendedTimeOut": "1000",
					"showEasing": "swing",
					"hideEasing": "linear",
					"showMethod": "fadeIn",
					"hideMethod": "fadeOut"
				};
					
				toastr.success("Elemento enviado para o nivel seguinte do Fluxo", 
						"Deu tudo certo", opts);
				
			} else if (response == "vazio") {
				var opts = {
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-top-right",
						"onclick": null,
						"showDuration": "300",
						"hideDuration": "1000",
						"timeOut": "10000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut"
					};
						
					toastr.warning("N�o existem fluxos disponiveis.", 
							"N�o temos para onde enviar", opts);
			} else if (response == "falha") {
				var opts = {
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-top-right",
						"onclick": null,
						"showDuration": "300",
						"hideDuration": "1000",
						"timeOut": "10000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut"
					};
						
					toastr.error("Ocorreu um erro interno, contate o administrador do Sistema.", 
							"Puxa! Algo deu errado", opts);
			}
		},
		error : function(e) {
			alert("Falha ao carregar os Dados");
		}
	});
}

function removerImagem(id) {
	var methodURL= "/SmartEditorial/imagens/minhasimagens/remover";
	$.ajax({
		type : "POST",
		url : methodURL,
		data: "id="+id,
		success : function(response) {
			if (response == "sucesso") {
				window.location = "/SmartEditorial/imagens/minhasimagens";	
			} else {
				var opts = {
					"closeButton": true,
					"debug": false,
					"positionClass": "toast-top-right",
					"onclick": null,
					"showDuration": "300",
					"hideDuration": "1000",
					"timeOut": "10000",
					"extendedTimeOut": "1000",
					"showEasing": "swing",
					"hideEasing": "linear",
					"showMethod": "fadeIn",
					"hideMethod": "fadeOut"
				};
					
				toastr.error("Ocorreu um erro interno, contate o administrador do sistema.", "Puxa! Achamos um erro:", opts);
				$('#botaoSimRemover').removeAttr("disabled");
				return false;
			}
			
		},
		error : function(e) {
			alert("Falha ao carregar os Dados");
		}
	});
}



function centerModal() {
    $(this).css('display', 'block');
    var $dialog = $(this).find(".modal-dialog");
    var offset = ($(window).height() - $dialog.height()) / 2;
    // Center modal vertically in window
    $dialog.css("margin-top", offset);
}

$('.modal').on('show.bs.modal', centerModal);
$(window).on("resize", function () {
    $('.modal:visible').each(centerModal);
});

function loadModalImage(url) {
	document.getElementById("imgID").src=url;
}

function chamarModalRemover(id) {
	$('#botaoSimRemover').attr('onclick', 'this.disabled=true, removerImagem('+id+')');
}
$(document).ready(function() {
    $('body').tooltip({
        selector: "[data-tooltip=tooltip]",
        container: "body"
    });
});


function chamarModalInterromperFluxo(idElemento, idFluxo) {
	$('#botaoSimInterromper').attr('onclick', 'this.disabled=true, interromperFluxo('+idElemento+', '+idFluxo+')');
}

function interromperFluxo(idElemento, idFluxo) {
	var methodURL= "/SmartEditorial/imagens/interromper-fluxo"
		$.ajax({
			type : "POST",
			url : methodURL,
			data : "idElemento=" +idElemento+"&idFluxo="+idFluxo,
			success : function(response) {
				if (response == "sucesso") {
					var opts = {
							"closeButton": true,
							"debug": false,
							"positionClass": "toast-top-right",
							"onclick": null,
							"showDuration": "300",
							"hideDuration": "1000",
							"timeOut": "10000",
							"extendedTimeOut": "1000",
							"showEasing": "swing",
							"hideEasing": "linear",
							"showMethod": "fadeIn",
							"hideMethod": "fadeOut"
						};
						
						toastr.success("Fluxo cancelado com sucesso.",
								"Deu tudo certo!:", opts);
				} else {
					var opts = {
							"closeButton": true,
							"debug": false,
							"positionClass": "toast-top-right",
							"onclick": null,
							"showDuration": "300",
							"hideDuration": "1000",
							"timeOut": "10000",
							"extendedTimeOut": "1000",
							"showEasing": "swing",
							"hideEasing": "linear",
							"showMethod": "fadeIn",
							"hideMethod": "fadeOut"
						};
						
						toastr.error("Encontramos um erro, contate o administrador do sistema.", 
								"Puxa! Achamos um erro:", opts);
				}
				
			},
			error : function(e) {
				alert("Falha ao carregar os Dados");
			}
		});
}
</script>
</head>
<body class="page-body">
<div class="page-container horizontal-menu">
	<%@ include file="../../componentes/header/menu.jsp" %>
	<div class="main-content" id="main-content">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h2 class="page-header">
						Galeria de Imagens
					</h2>
					<div class="panel panel-default">
						<div class="panel-body">
							<ul class="nav nav-tabs" role="tablist" id="myTab">
							  <li role="presentation" class="active"><a href="#aguardando" role="tab" data-toggle="tab">Aguardando</a></li>
							  <li role="presentation"><a href="#emFluxo" role="tab" data-toggle="tab">Em Fluxo</a></li>
							  <li role="presentation"><a href="#finalizadas" role="tab" data-toggle="tab">Finalizadas</a></li>
							</ul>
							<br />
							<div class="tab-content">
							  <div role="tabpanel" class="tab-pane active" id="aguardando">
										<table class="table table-bordered" id="dataTables-minhasImagensForaDoFluxo">
											<thead>
												<tr>
													<th>Imagem</th>
													<th>Estado</th>
													<th>Fotografo</th>
													<th>Cr�ditos</th>
													<th>A��es</th>
												</tr>
											</thead>
											
											<tbody>
											<c:forEach items="${imagensForaDoFluxo}" var="imagem">
												<tr>
													<td>
														<a data-toggle="modal" data-target="#myModal" onclick="loadModalImage('${contextPath}${imagem.caminho}')"  data-keyboard="false" data-backdrop="false">
															<i class="fa fa-folder-open-o"></i>
															${imagem.titulo}
														</a>
														
													</td>
													<td>${imagem.estado}</td>
													<td>${imagem.fotografo}</td>
													<td>${imagem.creditos}</td>
													<td>
														<a href="/SmartEditorial/imagens/minhasimagens/editar?id=${imagem.idImagem}&publicacao=${imagem.edicao.publicacao.idPublicacao}" type="submit" class="btn btn-default"
														data-tooltip="tooltip" data-placement="top" 
														title="" data-original-title="Editar Elemento">
															<i class="entypo-pencil"></i>
														</a>
														
														<a href="" type="submit" class="btn btn-red" onclick="chamarModalRemover(${imagem.idImagem})" data-toggle="modal" data-target="#modalRemoverImagem" data-backdrop="false"
														data-tooltip="tooltip" data-placement="top" 
														title="" data-original-title="Remover Elemento">
															<i class="entypo-trash"></i>
														</a>
														
														<button type="button" onclick="chamarModalIniciarFluxo(${imagem.idImagem})" 
														class="btn btn-green" data-toggle="modal" data-target="#modalIniciarFluxo" data-backdrop="false"
														data-tooltip="tooltip" data-placement="top" 
														title="" data-original-title="Iniciar FLuxo">
															<i class="fa fa-arrow-right"></i>
														</button>
													</td>
												</tr>
												</c:forEach>
											</tbody>
										</table>
									<div id="modalIniciarFluxo" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
										<div class="modal-dialog">
											<div class="modal-content">
													<div class="modal-header">
														<button type="button" onclick="location.reload()" class="close" data-dismiss="modal" aria-hidden="true">�</button>
														<h4 class="modal-title" id="myModalLabel">Iniciar Fluxo</h4>
													</div>
													<div class="modal-body">
														<label for="comentario" class="col-sm-3 control-label">Comentario</label>
														<div class="col-sm-8">
															<textarea class="form-control autogrow" id="comentarioFluxo" required="required"></textarea>
														</div>
													</div>
													<div class="modal-footer">
														<button type="button" class="btn btn-default" onclick="location.reload()" data-dismiss="modal">N�o</button>
														<input type="submit" class="btn btn-primary" onclick="" id="botaoSim" value="Sim">
													</div>
											</div><!-- /.modal-content -->
										</div><!-- /.modal-dialog -->
									</div>
									<div id="modalRemoverImagem" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
										<div class="modal-dialog">
											<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
														<h4 class="modal-title" id="myModalLabel">Confirmar Exclus�o</h4>
													</div>
													<div class="modal-body">
														<div class="col-sm-8">
															<p>Voc� est� prestes a remover uma Imagem da sua galeria, confirma isso?</p>
														</div>
													</div>
													<div class="modal-footer">
														<button type="button" class="btn btn-default" data-dismiss="modal">N�o</button>
														<input type="submit" class="btn btn-primary" onclick="" id="botaoSimRemover" value="Sim">
													</div>
											</div><!-- /.modal-content -->
										</div><!-- /.modal-dialog -->
									</div>
							  </div>
							  
							  <div role="tabpanel" class="tab-pane" id="emFluxo">
								  	<table class="table table-bordered" id="dataTables-minhasImagensEmFluxo">
											<thead>
												<tr>
													<th>Imagem</th>
													<th>Estado</th>
													<th>De</th>
													<th>Para</th>
													<th>A��es</th>
												</tr>
											</thead>
											
											<tbody>
											<c:forEach items="${imagensEmFluxo}" var="imagemEF">
												<tr>
													<td>
														<a data-toggle="modal" data-target="#myModal" onclick="loadModalImage('${contextPath}${imagemEF.imagem.caminho}')"  data-keyboard="false" data-backdrop="false">
															<i class="fa fa-folder-open-o"></i>
															${imagemEF.imagem.titulo}
														</a>
														
													</td>
													<td>${imagemEF.imagem.estado}</td>
													<td>${imagemEF.fluxoDeTrabalho.grupoOrigem.nome}</td>
													<td>${imagemEF.fluxoDeTrabalho.grupoDestino.nome}</td>
													<td>
														<a href="" type="submit" class="btn btn-red" onclick="chamarModalInterromperFluxo(${imagemEF.imagem.idImagem}, ${imagemEF.fluxoDeTrabalho.idFluxo})" 
														data-toggle="modal" data-target="#modalInterromperFluxo" data-backdrop="false"
															data-tooltip="tooltip" data-placement="top" 
															title="" data-original-title="Interromper Fluxo">
															<i class="fa fa-times"></i>
														</a>
													
													</td>
												</tr>
												</c:forEach>
											</tbody>
										</table>
										<div id="modalInterromperFluxo" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
											<div class="modal-dialog">
												<div class="modal-content">
												
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="location.reload()">�</button>
														<h4 class="modal-title" id="myModalLabel">Interromper Fluxo?</h4>
													</div>
													<div class="modal-body">
														<p>Deseja Interromper o tratamento deste Elemento?</p>
													</div>
													<div class="modal-footer">
														<button type="button" class="btn btn-default" onclick="location.reload()" data-dismiss="modal">N�o</button>
														<button type="button" class="btn btn-primary" onclick="" id="botaoSimInterromper">Sim</button>
													</div>
												
												</div><!-- /.modal-content -->
											</div><!-- /.modal-dialog -->
										</div>
								</div>
								
							  
							  <div role="tabpanel" class="tab-pane" id="finalizadas">
							  	<table class="table table-bordered" id="dataTables-minhasImagensFinalizadas">
										<thead>
											<tr>
												<th>Imagem</th>
												<th>Estado</th>
												<th>Publica��o</th>
												<th>Edi��o</th>
												<th>Categoria</th>
											</tr>
										</thead>
										
										<tbody>
										<c:forEach items="${imagensFinalizadas}" var="imagemF">
											<tr>
												<td>
													<a data-toggle="modal" data-target="#myModal" onclick="loadModalImage('${contextPath}${imagemF.caminho}')"  data-keyboard="false" data-backdrop="false">
														<i class="fa fa-folder-open-o"></i>
														${imagemF.titulo}
													</a>
													
												</td>
												<td>${imagemF.estado}</td>
												<td>${imagemF.edicao.publicacao.titulo}</td>
												<td>${imagemF.edicao.titulo}</td>
												<td>${imagemF.categoria.titulo}</td>
											</tr>
											</c:forEach>
										</tbody>
									</table>
							  </div>
							</div>
							
							<script>
							  $(function () {
							    $('#myTab a:first').tab('show')
							  })
							</script>
							
							<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">
												<span aria-hidden="true"><i class="fa fa-times fa-2x"></i></span>
											</button>
										</div>
										<div class="modal-body">
											<img class="col-md-12" src="" id="imgID">
										</div>
									</div>
								</div>
							</div>
							
						</div>
					</div>
					<%@ include file="../../componentes/footer/footer.jsp" %>
				</div>
			</div>
		</div>
	</div>
</div>
	
</body>
</html>