<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ page session="false" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Imagens - Cadastrar</title>
<%@ include file="../../componentes/header/head.jsp" %>
<script type="text/javascript">
function getEdicoes() {
	var methodURL= "/SmartEditorial/publicacao/pegaedicao"
	$.ajax({
		type : "POST",
		url : methodURL,
		data : "idPublicacao="+idPublicacao.value,
		success : function(response) {
			$("#edicao").html(response);
		},
		error : function(e) {
			alert("Falha ao carregar os Dados");
		}
	});
}
function getCategorias() {
	var methodURL= "/SmartEditorial/publicacao/pegacategorias"
	$.ajax({
		type : "POST",
		url : methodURL,
		data : "idPublicacao="+idPublicacao.value,
		success : function(response) {
			$("#categorias").html(response);
		},
		error : function(e) {
			alert("Falha ao carregar os Dados");
		}
	});
}
function showimagepreview(input) {
	if (input.files && input.files[0]) {
		var filerdr = new FileReader();
		filerdr.onload = function(e) {
			$('#imgprvw').attr('src', e.target.result);
		};
		filerdr.readAsDataURL(input.files[0]);
	}
}
</script>
</head>
<body class="page-body">
<div class="page-container horizontal-menu">
	<%@ include file="../../componentes/header/menu.jsp" %>
	<div class="main-content" id="main-content">
		<div class="container">
			<div class="row"> 
				<div class="col-md-12">
					<div class="row"> 
						<div class="col-md-12">
							<div class="panel-heading">
								<h4 class="panel-title">
									 <label class="control-label"><i class="fa fa-pencil-square-o"></i> Imagem <small>Cadastre ou edite suas imagens</small></label>
									</h4>
									<hr />
								</div>
							</div>
						</div>
						<div class="row">
							<c:url var="addAction" value="/imagens/cadastro/add"></c:url>
							<c:url var="updateAction" value="/imagens/cadastro/update"></c:url>
							<c:if test="${empty imagem.idImagem}">
								<div class="row">
									<div class="col-md-6">
										<div class="panel panel-primary">
											<div class="panel-heading">
												<h3 class="panel-title">
													Dados da Imagem
													<span class="glyphicon glyphicon-edit">
													</span>
												</h3>
											</div>
											<div class="panel-body">
												<form action="${addAction}" method="POST" enctype="multipart/form-data"
												id="myform" class="form-horizontal form-groups-bordered" role="form">
													<div class="form-group">
														<label for="titulo" class="col-sm-3 control-label">Titulo</label>
														<div class="col-sm-8">
															<input type="text" class="form-control" id="titulo" name="titulo" required="required"/>
														</div>
													</div>
													<div class="form-group">
														<label class="col-sm-3 control-label">Arquivo</label>
														
														<div class="col-sm-8">
														
															<div class="fileinput fileinput-new" data-provides="fileinput">
																<div class="input-group">
																	<div class="form-control uneditable-input" data-trigger="fileinput">
																		<i class="glyphicon glyphicon-file fileinput-exists"></i>
																		<span class="fileinput-filename"></span>
																	</div>
																	<span class="input-group-addon btn btn-default btn-file">
																		<span class="fileinput-new">Selecione</span>
																		<span class="fileinput-exists">Mudar</span>
																		<input type="file" name="arquivo" accept="image/x-png, image/gif, image/jpeg" required="required"onchange="showimagepreview(this)">
																	</span>
																	<a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remover</a>
																</div>
															</div>
															
														</div>
													</div>
													
													<div class="form-group">
														<label for="titulo" class="col-sm-3 control-label">Fotografo</label>
														<div class="col-sm-8">
															<input type="text" class="form-control" id="fotografo" name="fotografo" required="required"/>
														</div>
													</div>
													
													<div class="form-group">
														<label for="titulo" class="col-sm-3 control-label">Cr�ditos</label>
														<div class="col-sm-8">
															<input type="text" class="form-control" id="creditos" name="creditos" required="required"/>
														</div>
													</div>
													
													<div class="form-group">
														<label class="col-sm-3 control-label">Publica��o</label>
														
														<div class="col-sm-8">
															<select id="idPublicacao" class="form-control" name="idPublicacao" required onchange="getEdicoes(this.value), getCategorias(this.value)">
																<option id="publicacao" value="" >--Selecione--</option>
																<c:forEach var="item" items="${listaDePublicacoes}">
																	<option id="publicacao" value="${item.idPublicacao}" >${item.titulo} </option>
																</c:forEach>
															</select>
														</div>
													</div>
	
													<div class="form-group" id="edicao">
													</div>
													
													<div class="form-group" id="categorias">
													</div>
													
													<div class="form-group">
														<label class="col-sm-3 control-label">Palavras Chaves</label>
														<div class="col-sm-8">
															<input type="text" id="pchaves" name="pchaves" class="form-control tagsinput" />
														</div>
													</div>
	
													<div class="form-group">
														<button type="submit" class="btn btn-green btn-icon icon-left" onsubmit="this.disabled=true;">
							                            	Adicionar
							                            	<i class="fa fa-floppy-o"></i>
							                            </button>
													</div>
												</form>
											</div>
										</div>
									</div>
									<div class="col-md-6" style="" draggable="true">
										<div class="hidden-sm hidden-xs panel panel-primary" id="panelFoto">
											<div class="panel-heading">
												<h3 class="panel-title">
													Preview da Imagem &nbsp; &nbsp;&nbsp;
													<span class="glyphicon glyphicon-picture">
													</span>
												</h3>
											</div>
											<div class="panel-body">
												<img id="imgprvw" class="col-md-12"	/>
											</div>
										</div>
									</div>
								</div>
							</c:if>
							<c:if test="${!empty imagem.idImagem}">
							<div class="row text-center" id="containerFoto">
								<div class="col-md-6">
									<div class="panel panel-primary">
										<div class="panel-heading">
											<h3 class="panel-title">
												Dados da Imagem
											</h3>
										</div>
										<div class="panel-body">
										<c:if test="${!empty imagem}">
											<script type="text/javascript">
												$('#imgprvw').attr('src', '${contextPath}${imagem.caminho}');
											</script>
										</c:if>
											<form action="${updateAction}" method="POST" enctype="multipart/form-data"
												id="myform" class="form-horizontal form-groups-bordered" role="form">
													<input type="hidden" name="idImagem" value="${imagem.idImagem}">
													<input type="hidden" value="${imagem.altura}" name="altura">
													<input type="hidden" value="${imagem.largura}" name="largura">
													<input type="hidden" value="${imagem.estado}" name="estado">
													<input type="hidden" value="${imagem.caminho}" name="caminho"> 
													<div class="form-group">
														<label for="titulo" class="col-sm-3 control-label">Titulo</label>
														<div class="col-sm-8">
															<input type="text" class="form-control" value="${imagem.titulo}" id="titulo" name="titulo" required="required"/>
														</div>
													</div>
													<div class="form-group">
														<label class="col-sm-3 control-label">Arquivo</label>
														<div class="col-sm-8">
															<div class="fileinput fileinput-new" data-provides="fileinput">
																<div class="input-group">
																	<div class="form-control uneditable-input" data-trigger="fileinput">
																		<i class="glyphicon glyphicon-file fileinput-exists"></i>
																		<span class="fileinput-filename">${imagem.titulo}</span>
																	</div>
																	<span class="input-group-addon btn btn-default btn-file">
																		<span class="fileinput-new">Selecione</span>
																		<span class="fileinput-exists">Mudar</span>
																		<input type="file" name="arquivo" accept="image/x-png, image/gif, image/jpeg" required="required" onchange="showimagepreview(this)" value="${imagem.caminho}" disabled="disabled">
																	</span>
																	<a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remover</a>
																</div>
															</div>
														</div>
													</div>
													
													<div class="form-group">
														<label for="titulo" class="col-sm-3 control-label">Fotografo</label>
														<div class="col-sm-8">
														<input type="text" value="${imagem.fotografo}" class="form-control" id="fotografo" name="fotografo" required="required"/>
														</div>
													</div>
													
													<div class="form-group">
														<label for="titulo" class="col-sm-3 control-label">Cr�ditos</label>
														<div class="col-sm-8">
															<input type="text" value="${imagem.creditos}" class="form-control" id="creditos" name="creditos" required="required"/>
														</div>
													</div>
													
													<div class="form-group">
														<label class="col-sm-3 control-label">Publica��o</label>
														<div class="col-sm-8">
															<select id="idPublicacao" class="form-control" name="idPublicacao" required onchange="getEdicoes(this.value)">
																<option id="publicacao" value="${imagem.edicao.publicacao.idPublicacao}" style="background: rgba(0,0,0,0.3);">
																	${imagem.edicao.publicacao.titulo}
																</option>
																<c:forEach var="item" items="${listaDePublicacoes}">
																	<option id="publicacao" value="${item.idPublicacao}">
																		${item.titulo}
																	</option>
																</c:forEach>
															</select>
														</div>
													</div>
													<div class="form-group" draggable="true" id="edicao">
														<label class="col-sm-3 control-label">Edi��o</label>
														<div class="col-sm-8">
															<select id="idEdicao" name="idEdicao" required class="form-control">
																<option id="publicacao" value="${imagem.edicao.idEdicao}" style="background: rgba(0,0,0,0.3);">
																	${imagem.edicao.titulo}
																</option>
																<c:forEach var="item" items="${listaDeEdicoes}">
																	<option id="edicao" value="${item.idEdicao}" >${item.titulo} </option>
																</c:forEach>
															</select>
														</div>
													</div>
													<div class="form-group" draggable="true" id="categorias">
														<label class="col-sm-3 control-label">Categoria</label>
														<div class="col-sm-8">
															<select id="idCategoria" name="idCategoria" required class="form-control">
																<option id="publicacao" value="${imagem.categoria.idCategoria}" style="background: rgba(0,0,0,0.3);">
																	${imagem.categoria.titulo}
																</option>
																<c:forEach var="item" items="${listaDeCategorias}">
																	<option id="categoria" value="${item.idCategoria}" >${item.titulo} </option>
																</c:forEach>
															</select>
														</div>
													</div>

													<div class="form-group">
														<label class="col-sm-3 control-label">Palavras Chaves</label>
														<div class="col-sm-8">
															<input type="text" id="pchaves" name="pchaves" value='<c:forEach var='tag' items='${imagem.tags}' >${tag.tags},</c:forEach>' class="form-control tagsinput" />
														</div>
													</div>
	
													<div class="form-group">
														<button type="submit" class="btn btn-green btn-icon icon-left" onsubmit="this.disabled=true;">
							                            	Atualizar
							                            	<i class="fa fa-floppy-o"></i>
							                            </button>
													</div>
												</form>
										</div>
									</div>
								</div>
								<div class="col-md-6" style="" draggable="true">
									<div class="hidden-sm hidden-xs panel panel-primary" id="panelFoto">
										<div class="panel-heading">
											<h3 class="panel-title">
												Preview da Imagem &nbsp; &nbsp;&nbsp;
												<span class="glyphicon glyphicon-picture">
												</span>
											</h3>
										</div>
										<div class="panel-body">
											<img id="imgprvw1" class="col-md-12"src='${contextPath}${imagem.caminho}'/>
										</div>
									</div>
								</div>
							</div>	
							</c:if>
							</div>
						</div>
						<%@ include file="../../componentes/footer/footer.jsp" %>
					</div>
				</div>
			</div>
		</div>
</body>
</html>