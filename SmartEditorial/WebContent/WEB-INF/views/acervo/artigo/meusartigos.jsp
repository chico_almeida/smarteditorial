<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page session="false" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Meus Artigos</title>
<%@ include file="../../componentes/header/head.jsp" %>
<script type="text/javascript">
window.onload = function(){
	$(document).ready(function() {
		$('#dataTables-meusArtigosForaDoFluxo').dataTable();
		$('#dataTables-meusArtigosEmFluxo').dataTable();
		$('#dataTables-meusArtigosFinalizadas').dataTable();
   });
};
$(document).ready(function() {
    $('body').tooltip({
        selector: "[data-tooltip=tooltip]",
        container: "body"
    });
});
function excluirArtigo(id) {
	var methodURL= "/SmartEditorial/artigos/excluir"
	$.ajax({
		type : "POST",
		url : methodURL,
		data : "id="+id,
		success : function(response) {
			if (response == "sucesso") {
				window.location = "/SmartEditorial/artigos/meusartigos"
			} else {
				var opts = {
					"closeButton": true,
					"debug": false,
					"positionClass": "toast-top-right",
					"onclick": null,
					"showDuration": "300",
					"hideDuration": "1000",
					"timeOut": "10000",
					"extendedTimeOut": "1000",
					"showEasing": "swing",
					"hideEasing": "linear",
					"showMethod": "fadeIn",
					"hideMethod": "fadeOut"
				};
				
				toastr.error("Artigo est� em tratamento.", "Puxa! Achamos um erro:", opts);
			}
		},
		error : function(e) {
			alert("Falha ao carregar os Dados")
		}
	});
}

function chamarModal(idArtigo) {
	$('#botaoSim').attr('onclick', 'this.disabled=true, excluirArtigo('+idArtigo+')');
}
function chamarModalIniciarFluxo(id) {
	$('#botaoSimIniciarFluxo').attr('onclick', 'this.disabled=true, iniciarFluxo('+id+')');
}
function iniciarFluxo(id) {
	var methodURL= "/SmartEditorial/artigo/iniciar-fluxo";
	var comentario = document.getElementById("comentarioFluxo").value;
	if (comentario == "") {
		comentario = " ";
	} 
	$.ajax({
		type : "POST",
		url : methodURL,
		data: "idArtigo="+id+"&comentario="+comentario,
		success : function(response) {
			if (response == "sucesso") {
				var opts = {
					"closeButton": true,
					"debug": false,
					"positionClass": "toast-top-right",
					"onclick": null,
					"showDuration": "300",
					"hideDuration": "1000",
					"timeOut": "10000",
					"extendedTimeOut": "1000",
					"showEasing": "swing",
					"hideEasing": "linear",
					"showMethod": "fadeIn",
					"hideMethod": "fadeOut"
				};
					
				toastr.success("Elemento enviado para o nivel seguinte do Fluxo", 
						"Deu tudo certo", opts);
				
			} else if (response == "vazio") {
				var opts = {
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-top-right",
						"onclick": null,
						"showDuration": "300",
						"hideDuration": "1000",
						"timeOut": "10000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut"
					};
						
					toastr.warning("N�o existem fluxos disponiveis.", 
							"N�o temos para onde enviar", opts);
			} else if (response == "falha") {
				var opts = {
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-top-right",
						"onclick": null,
						"showDuration": "300",
						"hideDuration": "1000",
						"timeOut": "10000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut"
					};
						
					toastr.error("Ocorreu um erro interno, contate o administrador do Sistema.", 
							"Puxa! Algo deu errado", opts);
			}
		},
	});
}

function chamarModalInterromperFluxo(idElemento, idFluxo) {
	$('#botaoSimInterromper').attr('onclick', 'this.disabled=true, interromperFluxo('+idElemento+', '+idFluxo+')');
}

function interromperFluxo(idElemento, idFluxo) {
	var methodURL= "/SmartEditorial/artigos/interromper-fluxo"
		$.ajax({
			type : "POST",
			url : methodURL,
			data : "idElemento=" +idElemento+"&idFluxo="+idFluxo,
			success : function(response) {
				if (response == "sucesso") {
					var opts = {
							"closeButton": true,
							"debug": false,
							"positionClass": "toast-top-right",
							"onclick": null,
							"showDuration": "300",
							"hideDuration": "1000",
							"timeOut": "10000",
							"extendedTimeOut": "1000",
							"showEasing": "swing",
							"hideEasing": "linear",
							"showMethod": "fadeIn",
							"hideMethod": "fadeOut"
						};
						
						toastr.success("Fluxo cancelado com sucesso.",
								"Deu tudo certo!:", opts);
				} else {
					var opts = {
							"closeButton": true,
							"debug": false,
							"positionClass": "toast-top-right",
							"onclick": null,
							"showDuration": "300",
							"hideDuration": "1000",
							"timeOut": "10000",
							"extendedTimeOut": "1000",
							"showEasing": "swing",
							"hideEasing": "linear",
							"showMethod": "fadeIn",
							"hideMethod": "fadeOut"
						};
						
						toastr.error("Encontramos um erro, contate o administrador do sistema.", 
								"Puxa! Achamos um erro:", opts);
				}
				
			},
			error : function(e) {
				alert("Falha ao carregar os Dados");
			}
		});
}

</script>
</head>
<body class="page-body">
<div class="page-container horizontal-menu">
	<%@ include file="../../componentes/header/menu.jsp" %>
	<div class="main-content" id="main-content">
		<div class="container">

			<div class="row">
				<div class="col-lg-12">
					<h2 class="page-header">
						Meus Artigos
					</h2>
				</div>
			</div>
		
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-body">
							<ul class="nav nav-tabs" role="tablist" id="myTab">
								<li role="presentation" class="active">
									<a href="#aguardando" role="tab" data-toggle="tab">Aguardando</a>
								</li>
								<li role="presentation">
									<a href="#emFluxo" role="tab" data-toggle="tab">Em Fluxo</a>
								</li>
								<li role="presentation">
									<a href="#finalizadas" role="tab" data-toggle="tab">Finalizadas</a>
								</li>
							</ul>
							<br />
							<div class="tab-content">
								<div role="tabpanel" class="tab-pane active" id="aguardando">
									<table class="table table-bordered" id="dataTables-meusArtigosForaDoFluxo">
										<thead>
											<tr>
												<th>Artigo</th>
												<th>Estado</th>
												<th>Ultima Altera��o</th>
												<th>A��es</th>
											</tr>
										</thead>
										
										<tbody>
										<c:forEach items="${artigosForaDoFluxo}" var="artigo">
											<tr>
												<td>${artigo.titulo}</td>
												<td>${artigo.estado}</td>
												<td><fmt:formatDate value="${artigo.ultimaAlteracao}" pattern="dd/MM/yyyy HH:mm:ss"/></td>
												
											
												<td>
													
													<a href="<c:url value='/artigos/editar?artigo=${artigo.idArtigo}' />" type="submit" class="btn btn-default"
													data-tooltip="tooltip" data-placement="top" 
													title="" data-original-title="Editar Elemento">
														<i class="entypo-pencil"></i>
													</a>
													
													<a href="" type="submit" class="btn btn-red" onclick="chamarModal(${artigo.idArtigo})"data-toggle="modal" data-target="#modalRemoveArtigo" data-backdrop="false"
													data-tooltip="tooltip" data-placement="top" 
													title="" data-original-title="Remover Elemento">
														<i class="entypo-trash"></i>
													</a>
												
													<button type="button" onclick="chamarModalIniciarFluxo(${artigo.idArtigo})" 
													class="btn btn-green" data-toggle="modal" data-target="#modalIniciarFluxo" data-backdrop="false"
													data-tooltip="tooltip" data-placement="top" 
													title="" data-original-title="Iniciar Fluxo">
														<i class="fa fa-arrow-right"></i>
													</button>
															
												</td>
											</tr>
											</c:forEach>
										</tbody>
									</table>
						
						
								</div>
								<div role="tabpanel" class="tab-pane" id="emFluxo">
						
									<table class="table table-bordered" id="dataTables-meusArtigosEmFluxo">
										<thead>
											<tr>
												<th>Artigo</th>
												<th>Estado</th>
												<th>De</th>
												<th>Para</th>
												<th>A��es</th>
											</tr>
										</thead>
										
										<tbody>
										<c:forEach items="${artigosEmFluxo}" var="artigoEF">
											<tr>
												<td>${artigoEF.artigo.titulo}</td>
												<td>${artigoEF.artigo.estado}</td>
												<td>${artigoEF.fluxoDeTrabalho.grupoOrigem.nome}</td>
												<td>${artigoEF.fluxoDeTrabalho.grupoDestino.nome}</td>
												<td>
													<a href="" type="submit" class="btn btn-red" onclick="chamarModalInterromperFluxo(${artigoEF.artigo.idArtigo}, ${artigoEF.fluxoDeTrabalho.idFluxo})" 
													data-toggle="modal" data-target="#modalInterromperFluxo" data-backdrop="false"
														data-tooltip="tooltip" data-placement="top" 
														title="" data-original-title="Interromper Fluxo">
														<i class="fa fa-times"></i>
													</a>
												
												</td>
											</tr>
											</c:forEach>
										</tbody>
									</table>
									<div id="modalInterromperFluxo" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
										<div class="modal-dialog">
											<div class="modal-content">
											
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="location.reload()">�</button>
													<h4 class="modal-title" id="myModalLabel">Interromper Fluxo?</h4>
												</div>
												<div class="modal-body">
													<p>Deseja Interromper o tratamento deste Elemento?</p>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" onclick="location.reload()" data-dismiss="modal">N�o</button>
													<button type="button" class="btn btn-primary" onclick="" id="botaoSimInterromper">Sim</button>
												</div>
											
											</div><!-- /.modal-content -->
										</div><!-- /.modal-dialog -->
									</div>
								</div>
								<div role="tabpanel" class="tab-pane" id="finalizadas">
									<table class="table table-bordered" id="dataTables-meusArtigosFinalizadas">
										<thead>
											<tr>
												<th>Artigo</th>
												<th>Estado</th>
												<th>Publica��o</th>
												<th>Edi��o</th>
												<th>Categoria</th>
											</tr>
										</thead>
										
										<tbody>
										<c:forEach items="${artigosFinalizadas}" var="artigoF">
											<tr>
												<td>${artigoF.titulo}</td>
												<td>${artigoF.estado}</td>
												<td>${artigoF.edicao.publicacao.titulo}</td>
												<td>${artigoF.edicao.titulo}</td>
												<td>${artigoF.categoria.titulo}</td>
											</tr>
											</c:forEach>
										</tbody>
									</table>
						
								</div>
							</div>
							<script>
								$(function() {
									$('#myTab a:first').tab('show')
								})
							</script>
						</div>
						<div id="modalRemoveArtigo" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
							<div class="modal-dialog">
								<div class="modal-content">
								
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
										<h4 class="modal-title" id="myModalLabel">Deseja Remover?</h4>
									</div>
									<div class="modal-body">
										<p>Voc� est� prestes a remover uma Edi��o desta Publica��o, confirma isso?</p>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">N�o</button>
										<button type="button" class="btn btn-primary" onclick="" id="botaoSim">Sim</button>
									</div>
								
								</div><!-- /.modal-content -->
							</div><!-- /.modal-dialog -->
						</div>
						<div id="modalIniciarFluxo" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
								<div class="modal-dialog">
									<div class="modal-content">
											<div class="modal-header">
												<button type="button" onclick="location.reload()" class="close" data-dismiss="modal" aria-hidden="true">�</button>
												<h4 class="modal-title" id="myModalLabel">Iniciar Fluxo</h4>
											</div>
											<div class="modal-body">
												<label for="comentario" class="col-sm-3 control-label">Comentario</label>
												<div class="col-sm-8">
													<textarea class="form-control autogrow" id="comentarioFluxo" required="required"></textarea>
												</div>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-default" onclick="location.reload()" data-dismiss="modal">N�o</button>
												<input type="submit" class="btn btn-primary" onclick="" id="botaoSimIniciarFluxo" value="Sim">
											</div>
									</div><!-- /.modal-content -->
								</div><!-- /.modal-dialog -->
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<%@ include file="../../componentes/footer/footer.jsp" %>
</body>
</html>