<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ page session="false" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Artigos </title>
<%@ include file="../../componentes/header/head.jsp" %>

<script src="${myUrl}/resources/js/nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">bkLib.onDomLoaded(nicEditors.allTextAreas);</script>

<script type="text/javascript">
window.onload = function(){
	if(getQueryVariable("q")== 'meus-artigos') {
		meusArtigos();
	}
	
};
document.onload = function() {
	nicEditors.allTextAreas;
};


function getEdicoes() {
	var methodURL= "/SmartEditorial/publicacao/pegaedicao"
	$.ajax({
		type : "POST",
		url : methodURL,
		data : "idPublicacao="+idPublicacao.value,
		success : function(response) {
			$("#edicao").html(response);
		},
		error : function(e) {
			alert("Falha ao carregar os Dados");
		}
	});
}
function getCategorias() {
	var methodURL= "/SmartEditorial/publicacao/pegacategorias"
	$.ajax({
		type : "POST",
		url : methodURL,
		data : "idPublicacao="+idPublicacao.value,
		success : function(response) {
			$("#categorias").html(response);
		},
		error : function(e) {
			alert("Falha ao carregar os Dados");
		}
	});
}


</script>
</head>
<body class="page-body">
<div class="page-container horizontal-menu">
	<%@ include file="../../componentes/header/menu.jsp" %>
	<div class="main-content" id="main-content">
		<div class="container">
			<div class="row"> 
				<div class="col-md-12">
					<div class="row"> 
						<div class="col-md-8">
							<div class="panel-heading">
								<h4 class="panel-title">
									 <label class="control-label"><i class="fa fa-pencil-square-o"></i> Artigo <small>Crie ou edite seus artigo</small></label>
								</h4>
								<hr />
							</div>
						</div>
						<div class="col-md-4">
							<header>
								<h4><i class="fa fa-cog"></i> Propriedades <small>Publica��o, categoria, fluxo</small></h4>
							</header>
						</div>
					</div>
					<div class="row">
						<c:url var="addAction" value="/artigo/criar"></c:url>
						<c:url var="updateAction" value="/artigo/atualizar"></c:url>
						<c:if test="${empty artigo.idArtigo}">
						<form action="${addAction}" method="POST" class="form-horizontal" role="form">
					        <div class="col-md-8">
					      		<div class="panel panel-primary">
									<div class="panel-body">
										<div class="table-responsive">
											<div class="form-group">
												<label class="control-label">T�tulo<span class="required" >*</span></label>
												<input class="form-control" type="text" name="titulo" required="required">
											</div>
											<div class="form-group">
												<label class="control-label">Artigo</label>
												<textarea id="artigo" rows="10" name="artigo"></textarea> <br />
											</div>
					                    </div>
					                </div>
								</div>
					        </div>
							<div class="col-md-4">
					              <div class="panel-body">
		                            <h4>Publicacao</h4>
		                            <hr/>	
		                            <div class="form-group">
		                            	<div id="publicacao">
											<label class="col-sm-3 control-label">Publica��o</label>
											<div class="col-sm-8">
												<select id="idPublicacao" class="form-control" name="idPublicacao" required onchange="getEdicoes(this.value), getCategorias(this.value)">
													<option id="publicacao" value="" >--Selecione--</option>
													<c:forEach var="item" items="${listaDePublicacoes}">
														<option id="publicacao" value="${item.idPublicacao}" >${item.titulo} </option>
													</c:forEach>
												</select>
											</div>
										</div>
									</div>
									<div class="form-group" id="edicao">
									</div>
									
									<div class="form-group" id="categorias">
									</div>
		                            <h4>Fonte  &amp; Tags</h4>
		                            <hr>
		                            <div class="form-group">
		                                <label class="col-sm-3 control-label">Fonte</label>
		                                <div class="col-sm-8">
		                                	<input type="text" name="fonte" class="form-control" />
		                                </div>
		                            </div>
		                            <div class="form-group">
		                                <label class="col-sm-3 control-label">Tags</label>
		                                <div class="col-sm-8">
		                                	<input type="text" name="pchaves" class="form-control tagsinput" />
		                                	<span><small>Pressione Enter ap�s cada palavra!</small></span>
		                                </div>
		                            </div>
			                        <div class="form-actions">
			                            <button type="submit" class="btn btn-green btn-icon icon-left" onsubmit="this.disabled=true;">
			                            	Salvar
			                            	<i class="fa fa-floppy-o"></i>
			                            </button>
			                        </div>
				                </div>
					        </div>
						</form>
						</c:if>
						<c:if test="${!empty artigo.idArtigo}">
						<form action="${updateAction}" method="POST"  class="form-horizontal" role="form">
							<input type="hidden" name="idArtigo" value="${artigo.idArtigo}">
							<input type="hidden" name="criacao" value="${artigo.dataCriacao}">
							<input type="hidden" name="estado" value="${artigo.estado}">
					        <div class="col-md-8">
					      		<div class="panel panel-primary">
									
									<div class="panel-body">
										<div class="table-responsive">
						                    
												<div class="form-group">
													<label class="control-label">T�tulo<span class="required" >*</span></label>
													<input class="form-control" type="text" name="titulo" value="${artigo.titulo}" required="required">
												</div>
												<div class="form-group">
													<label class="control-label">Artigo</label>
													<textarea id="artigo" rows="10" name="artigo">${artigo.artigo}</textarea> <br />
												</div>
					                    </div>
					                </div>
								</div>
					        </div>
					        
							<div class="col-md-4">
					              <div class="panel-body">
		                            <h4>Publicacao</h4>
		                            <hr/>	
		                            <div class="form-group">
		                            	<div id="publicacao">
											<label class="col-sm-3 control-label">Publica��o</label>
											<div class="col-sm-8">
												<select id="idPublicacao" class="form-control" name="idPublicacao" required onchange="getEdicoes(this.value), getCategorias(this.value)">
													<option id="publicacao" value="${artigo.edicao.publicacao.idPublicacao}" style="background: rgba(0,0,0,0.3);">
														${artigo.edicao.publicacao.titulo}
													</option>
													<c:forEach var="item" items="${listaDePublicacoes}">
														<option id="publicacao" value="${item.idPublicacao}">
															${item.titulo}
														</option>
													</c:forEach>
												</select>
											</div>
										</div>
									</div>
									<div class="form-group" id="edicao">
										<label class="col-sm-3 control-label">Edi��o</label>
										<div class="col-sm-8">
											<select id="idEdicao" class="form-control" name="idEdicao" required>
												<option id="publicacao" value="${artigo.edicao.idEdicao}" style="background: rgba(0,0,0,0.3);">
													${artigo.edicao.titulo}
												</option>
												<c:forEach var="edicao" items="${listaDeEdicoes}">
													<option id="edicao" value="${edicao.idEdicao}">
														${edicao.titulo}
													</option>
												</c:forEach>
											</select>
										</div>
									</div>
									
									<div class="form-group" id="categorias">
										<label class="col-sm-3 control-label">Categoria</label>
										<div class="col-sm-8">
											<select id="idCategoria" class="form-control" name="idCategoria" required>
												<option id="categoria" value="${artigo.categoria.idCategoria}" style="background: rgba(0,0,0,0.3);">
													${artigo.categoria.titulo}
												</option>
												<c:forEach var="categoria" items="${listaDeCategorias}">
													<option id="categoria" value="${categoria.idCategoria}">
														${categoria.titulo}
													</option>
												</c:forEach>
											</select>
										</div>
									</div>
		                            <h4>Fonte  &amp; Tags</h4>
		                            <hr>
		                            <div class="form-group">
		                                <label class="col-sm-3 control-label">Fonte</label>
		                                <div class="col-sm-8">
		                                	<input type="text" name="fonte" class="form-control" value="${artigo.fonte}" />
		                                </div>
		                            </div>
		                            <div class="form-group">
		                                <label class="col-sm-3 control-label">Tags</label>
		                                <div class="col-sm-8">
		                                	<input type="text" id="pchaves" name="pchaves" value='<c:forEach var='tag' items='${anuncio.tags}' >${tag.tags},</c:forEach>' class="form-control tagsinput" />
		                                	<span><small>Pressione Enter ap�s cada palavra!</small></span>
		                                </div>
		                            </div>
			                        <div class="form-actions">
			                            <button type="submit" class="btn btn-green btn-icon icon-left" onsubmit="this.disabled=true;">
			                            	Salvar
			                            	<i class="fa fa-floppy-o"></i>
			                            </button>
			                        </div>
				                </div>
					        </div>
						</form>
						</c:if>
					</div>
				</div>
			<%@ include file="../../componentes/footer/footer.jsp" %>
			</div>
		</div>
	</div>
</div>

</body>
</html>