<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ page session="false" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>P�gina - Cadastrar </title>
<%@ include file="../../componentes/header/head.jsp" %>
<script type="text/javascript">
function getEdicoes() {
	var methodURL= "publicacao/pegaedicao"
	$.ajax({
		type : "POST",
		url : methodURL,
		data : "idPublicacao="+idPublicacao.value,
		success : function(response) {
			$("#edicao").html(response);
		},
		error : function(e) {
			alert("Falha ao carregar os Dados");
		}
	});
}
function getCategorias() {
	var methodURL= "/SmartEditorial/publicacao/pegacategorias"
	$.ajax({
		type : "POST",
		url : methodURL,
		data : "idPublicacao="+idPublicacao.value,
		success : function(response) {
			$("#categorias").html(response);
		},
		error : function(e) {
			alert("Falha ao carregar os Dados");
		}
	});
}
function showimagepreview(input) {
	if (input.files && input.files[0]) {
		var filerdr = new FileReader();
		filerdr.onload = function(e) {
			$('#pdfView').attr('data', e.target.result);
		};
		filerdr.readAsDataURL(input.files[0]);
	}
}
</script>
</head>
<body class="page-body">
<div class="page-container horizontal-menu">
	<%@ include file="../../componentes/header/menu.jsp" %>
	<div class="main-content" id="main-content">
		<div class="container">
			<div class="row"> 
				<div class="col-md-12">
					<div class="row"> 
						<div class="col-md-12">
							<div class="panel-heading">
								<h4 class="panel-title">
									 <label class="control-label"><i class="fa fa-pencil-square-o"></i> P�ginas <small>Cadastre ou edite suas p�ginas</small></label>
								</h4>
								<hr />
							</div>
						</div>
					</div>
					<div class="row">
								<c:url var="addAction" value="/paginas/cadastro/add"></c:url>
								<c:url var="updateAction" value="/paginas/cadastro/update"></c:url>
								<div class="row">
								
								<div class="col-md-6">
									<div class="panel panel-primary">
										<div class="panel-heading">
											<h3 class="panel-title">
												Dados da P�gina
												<span class="glyphicon glyphicon-edit">
												</span>
											</h3>
										</div>
										<div class="panel-body">
											<form action="${addAction}" method="POST" enctype="multipart/form-data" id="myform" class="form-horizontal form-groups-bordered" role="form">
												
												<div class="form-group">
													<label for="titulo" class="col-sm-3 control-label">Titulo</label>
													<div class="col-sm-8">
														<input type="text" class="form-control" id="titulo" name="titulo" required="required"/>
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-sm-3 control-label">PDF para Preview</label>
													
													<div class="col-sm-8">
													
														<div class="fileinput fileinput-new" data-provides="fileinput">
															<div class="input-group">
																<div class="form-control uneditable-input" data-trigger="fileinput">
																	<i class="glyphicon glyphicon-file fileinput-exists"></i>
																	<span class="fileinput-filename"></span>
																</div>
																<span class="input-group-addon btn btn-default btn-file">
																	<span class="fileinput-new">Selecione</span>
																	<span class="fileinput-exists">Mudar</span>
																	<input type="file" name="PDFarquivo" accept="application/pdf" required="required"onchange="showimagepreview(this)">
																</span>
																<a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remover</a>
															</div>
														</div>
														
													</div>
												</div>
												
<!-- Trecho omitido do sistema por n�o estar nos requisitos de desenvolvimento para o TCC, ser� tratado como requisito postergado da aplica��o. -->
												<!-- <div class="form-group">
													<label class="col-sm-3 control-label">InDesign Arquivo</label>
													
													<div class="col-sm-8">
													
														<div class="fileinput fileinput-new" data-provides="fileinput">
															<div class="input-group">
																<div class="form-control uneditable-input" data-trigger="fileinput">
																	<i class="glyphicon glyphicon-file fileinput-exists"></i>
																	<span class="fileinput-filename"></span>
																</div>
																<span class="input-group-addon btn btn-default btn-file">
																	<span class="fileinput-new">Selecione</span>
																	<span class="fileinput-exists">Mudar</span>
																	<input type="file" name="INDDarquivo" accept=".indd" required="required"onchange="showimagepreview(this)">
																</span>
																<a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remover</a>
															</div>
														</div>
														
													</div>
												</div> -->
												
												<div class="form-group">
													<label class="col-sm-3 control-label">Publica��o</label>
													
													<div class="col-sm-8">
														<select id="idPublicacao" class="form-control" name="idPublicacao" required onchange="getEdicoes(this.value), getCategorias(this.value)">
															<option id="publicacao" value="" >--Selecione--</option>
															<c:forEach var="item" items="${listaDePublicacoes}">
																<option id="publicacao" value="${item.idPublicacao}" >${item.titulo} </option>
															</c:forEach>
														</select>
													</div>
												</div>

												<div class="form-group" id="edicao">
												</div>
												
												<div class="form-group" id="categorias">
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label">Palavras Chaves</label>
													<div class="col-sm-8">
														<input type="text" id="pchaves" name="pchaves" class="form-control tagsinput" />
													</div>
												</div>

												<div class="form-group">
													<button type="submit" class="btn btn-green btn-icon icon-left" onsubmit="this.disabled=true;">
						                            	Adicionar
						                            	<i class="fa fa-floppy-o"></i>
						                            </button>
												</div>
											</form>
										</div>
									</div>
								</div>
								<div class="col-md-6" style="" draggable="true">
										<div class="hidden-sm hidden-xs panel panel-primary" id="panelFoto">
											<div class="panel-heading">
												<h3 class="panel-title">
													Preview do An�ncio &nbsp; &nbsp;&nbsp;
													<span class="glyphicon glyphicon-picture">
													</span>
												</h3>
											</div>
											<div class="panel-body" style="height: 600px">
												<object id="pdfView" data="" type="application/pdf" width="100%" height="100%">
												</object>
											</div>
										</div>
									</div>
								</div>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
