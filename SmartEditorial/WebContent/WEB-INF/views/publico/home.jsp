<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ page session="false" %>
<c:set var="req" value="${pageContext.request}" />
<c:set var="baseURL" value="${fn:replace(req.requestURL, fn:substring(req.requestURI, 1, fn:length(req.requestURI)), req.contextPath)}" />
<c:set var="string1" value="${baseURL}/${MyID}"/>
<c:set var="string2" value="${fn:replace(string1, 
                                '8080//', '8080/')}" />
<c:url var="myUrl" value="${string2}" />
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="icon"  type="image/png"  href="${myUrl}resources/images/favicon.ico">
<title>Inicio</title>
<script type="text/javascript">

window.onload = function(){
	if(getQueryVariable("login_error")) {
		document.getElementById("valida").style.display = "block";
	}
	function getQueryVariable(variable)
	{
	       var query = window.location.search.substring(1);
	       var vars = query.split("&");
	       for (var i=0;i<vars.length;i++) {
	               var pair = vars[i].split("=");
	               if(pair[0] == variable){return pair[1];}
	       }
	       return(false);
	}
};

</script>


	<script src="${myUrl}resources/js/jquery-1.11.1.min.js"></script>
	<link rel="stylesheet" href="${myUrl}resources/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css"  id="style-resource-1">
	<link rel="stylesheet" href="${myUrl}resources/css/font-icons/entypo/css/entypo.css"  id="style-resource-2">
	<link rel="stylesheet" href="${myUrl}resources/css/font-icons/entypo/css/animation.css"  id="style-resource-3">
	<link rel="stylesheet" href="${myUrl}resources/css/neon.css"  id="style-resource-5">
	<link rel="stylesheet" href="${myUrl}resources/css/custom.css"  id="style-resource-6">

<!-- Owl Carousel Assets -->
</head>
<body class="page-body login-page login-form-fall">

<sec:authorize access="isAnonymous()">

<div class="login-container">
	
	<div class="login-header login-caret">
		
		<div class="login-content">
			
			<a href="#" class="logo">
				<img src="${myUrl}resources/images/logo.png" alt="" width="320px" height="150px" />
			</a>
			
			<p class="description">Bem-vindo, conecte-se para acessar o Smart Editorial!</p>
			
			<!-- progress bar indicator -->
			<div class="login-progressbar-indicator">
				<h3>43%</h3>
				<span>Carregando</span>
			</div>
		</div>
		
	</div>
	
	<div class="login-progressbar">
		<div></div>
	</div>
	<div class="login-form">
		<div class="login-content">
			<div class="alert alert-danger" id="valida" style="display: none">
				<strong>Puxa :(</strong> N�o encontramos voc� no sistema, confirme seus dados e tente novamente.
			</div>
			<form  method="post"	action="<c:url value="/j_spring_security_check"></c:url>" role="form">
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<i class="entypo-user"></i>
						</div>
						<input type="text" class="form-control" name="email" id="username" placeholder="Email" autocomplete="off" />
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<i class="entypo-key"></i>
						</div>
						<input type="password" class="form-control" name="senha" id="password" placeholder="Senha" autocomplete="off" />
					</div>
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-block btn-login">
						Entrar
						<i class="entypo-login"></i>
					</button>
				</div>
			</form>
			<div class="login-bottom-links">
				<a href="#" class="link">Esqueceu sua senha?</a>
				<br />
			</div>
		</div>
	</div>
	
	
	<script src="${myUrl}resources/js/gsap/main-gsap.js" id="script-resource-1"></script>
	<script src="${myUrl}resources/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js" id="script-resource-2"></script>
	<script src="${myUrl}resources/js/bootstrap.min.js" id="script-resource-3"></script>
	<script src="${myUrl}resources/js/joinable.js" id="script-resource-4"></script>
	<script src="${myUrl}resources/js/resizeable.js" id="script-resource-5"></script>
	<script src="${myUrl}resources/js/neon-api.js" id="script-resource-6"></script>
	<script src="${myUrl}resources/js/jquery.validate.min.js" id="script-resource-7"></script>
	<script src="${myUrl}resources/js/neon-login.js" id="script-resource-8"></script>
	<script src="${myUrl}resources/js/neon-custom.js" id="script-resource-9"></script>
	<script src="${myUrl}resources/js/neon-demo.js" id="script-resource-10"></script>
</div>
</sec:authorize>
<sec:authorize access="isAuthenticated()">
	<script type="text/javascript">
		window.location = "inicio"
	</script>
</sec:authorize>
</body>
</html>