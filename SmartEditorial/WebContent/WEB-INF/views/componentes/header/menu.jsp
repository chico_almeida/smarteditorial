<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<header class="navbar navbar-fixed-top">
	<div class="navbar-inner">
		<div class="navbar-brand">
			<a href="/SmartEditorial/inicio">
				<img src="${myUrl}resources/images/icon.png" width="88px" height="20px" />
			</a>
		</div>
		<ul class="navbar-nav">
			<li>
				<a href='${myUrl}inicio'> <i class="fa fa-home"></i> <span>Inicio</span></a>
			</li>
			<li class="opened active">
				<a href="#"><i class="fa fa-quote-left"></i> <span>Artigos</span></a>
				<ul>
					<li class="active">
						<a href='${myUrl}artigos'><i class="fa fa-terminal"></i> <span> Carregar Artigo</span></a>
					</li>
					<li>
						<a href='${myUrl}artigos/meusartigos'><i class="fa fa-list"></i> <span> Meus Artigos</span></a>
					</li>
				</ul>
			</li>
			<li>
				<a href='#'> <i class="fa fa-bullhorn"></i> <span>An�ncios</span></a>
				<ul>
					<li class="active">
						<a href='${myUrl}anuncios'><i class="glyphicon glyphicon-comment"></i> <span> Carregar An�ncio</span></a>
					</li>
					<li>
						<a href='${myUrl}anuncios/meusanuncios'><i class="fa fa-list"></i> <span> Meus An�ncios</span></a>
					</li>
				</ul>
			</li>
			<li>
				<a href='#'> <i class="fa fa-picture-o"></i> <span>Imagens</span></a>
				<ul>
					<li class="active">
						<a href='${myUrl}imagens'><i class="glyphicon glyphicon-comment"></i> <span> Carregar Imagem</span></a>
					</li>
					<li>
						<a href='${myUrl}imagens/minhasimagens'><i class="fa fa-list"></i> <span> Minhas Imagens</span></a>
					</li>
				</ul>
			</li>
			<li>
				<a href='#'><i class="fa fa-list-alt"></i> <span>P�ginas</span></a>
				<ul>
					<li class="active">
						<a href='${myUrl}paginas'><i class="entypo-docs"></i> <span>Carregar P�ginas</span></a>
					</li>
					<li>
						<a href='${myUrl}paginas/minhaspaginas'><i class="fa fa-list"></i> <span>Minhas P�ginas</span></a>
					</li>
				</ul>
			</li>
			<li> 
				<a href='${myUrl}publicacoes'> <i class="fa fa-th-list"></i> <span>Publica��es</span></a>
			</li>
			<li>
				<a href='#'><i class="fa fa-wrench"></i> <span>Administra��o</span></a>
				<ul>
					<li class="active">
						<a href='${myUrl}admin/grupos'><i class="fa fa-users"></i> <span>Grupos do Sistema</span></a>
					</li>
					<li>
						<a href='${myUrl}admin/usuarios'><i class="fa fa-user"></i> <span>Usu�rios do Sistema</span></a>
					</li>
				</ul>
			</li>
			
			<li id="search" class="search-input-collapsed">
				<form method="get" action="#">
					<input type="text" name="q" class="search-input" placeholder="Busca no acervo..."
					/>
					<button type="submit">
						<i class="entypo-search">
						</i>
					</button>
				</form>
			</li>
		</ul>
		<ul class="nav navbar-right pull-right">
			<li class="sep">
			</li>
			<li>
				<a href="<c:url value="/j_spring_security_logout"/>">
					Sair <i class="entypo-logout right"></i>
				</a>
			</li>
			<!-- mobile only -->
			<li class="visible-xs">
				<div class="horizontal-mobile-menu visible-xs">
					<a href="#" class="with-animation">
						<i class="entypo-menu"></i>
					</a>
				</div>
			</li>
		</ul>
	</div>
</header>