<script type="text/javascript">
window.onload = function(){
	if(getQueryVariable("q")== 'grupos') {
		listarGrupos();
	}
	else if (getQueryVariable("q")== 'usuarios') {
		listarUsuarios();
	}
	$(document).ready(function() {
        $('#dataTables-ListaDeUsuarios').dataTable();
        $('#dataTables-ListaDeGrupos').dataTable();
        $('#dataTables-UsuariosDoGrupo').dataTable();
		$('#dataTables-PermissoesDoGrupo').dataTable();
    });
};

function cadastrarUsuario() {
	var methodURL= "/SmartEditorial/admin/usuario/criar"
	$.ajax({
		type : "POST",
		url : methodURL,
		success : function(response) {
			$("#modalBodyUsuario").html(response);
		},
		error : function(e) {
			alert("Falha ao carregar os Dados");
		}
	});
}
function cadastrarGrupo() {
	var methodURL= "/SmartEditorial/admin/grupos/criar"
	$.ajax({
		type : "POST",
		url : methodURL,
		success : function(response) {
			$("#modalBodyGrupo").html(response);
		},
		error : function(e) {
			alert("Falha ao carregar os Dados");
		}
	});
}

function excluirGrupo(id) {
	var methodURL= "/SmartEditorial/admin/grupo/excluir"
	$.ajax({
		type : "POST",
		url : methodURL,
		data : "id="+id,
		success : function(response) {
			window.location = "/SmartEditorial/admin/grupos";
		},
		error : function(e) {
			alert("Falha ao carregar os Dados");
		}
	});
}

function excluirUsuario(id) {
	var methodURL= "/SmartEditorial/admin/usuario/excluir"
	$.ajax({
		type : "POST",
		url : methodURL,
		data : "id="+id,
		success : function(response) {
			if (response == "falha") {
				var opts = {
					"closeButton": true,
					"debug": false,
					"positionClass": "toast-top-right",
					"onclick": null,
					"showDuration": "800",
					"hideDuration": "2000",
					"timeOut": "10000",
					"extendedTimeOut": "1000",
					"showEasing": "swing",
					"hideEasing": "linear",
					"showMethod": "fadeIn",
					"hideMethod": "fadeOut"
				};
				toastr.error("O usu�rio que voc� est� tentando remover possui vinculos com outros elementos do sistema, n�o podemos prosseguir com esta a��o. Desabilite este usu�rio.",
					"Puxa! Achamos um erro:", opts);
			} else {
				window.location = "/SmartEditorial/admin/usuarios";
				$(document).ready(function() {
			        $('#dataTables-ListaDeUsuarios').dataTable();
			    });
			}
		},
		error : function(e) {
			alert("Falha ao carregar os Dados");
		}
	});
}

function editarUsuario(id) {
	var methodURL= "/SmartEditorial/admin/usuario/editar"
	$.ajax({
		type : "POST",
		url : methodURL,
		data : "id="+id,
		success : function(response) {
			$("#modalBodyUsuario").html(response);
		},
		error : function(e) {
			alert("Falha ao carregar os Dados");
		}
	});
}
function editarGrupo(id) {
	var methodURL= "/SmartEditorial/admin/grupo/editar"
	$.ajax({
		type : "POST",
		url : methodURL,
		data : "id="+id,
		success : function(response) {
			$("#modalBodyGrupo").html(response);
		},
		error : function(e) {
			alert("Falha ao carregar os Dados");
		}
	});
}

function ehVazio(id) {
	var methodURL = "/SmartEditorial/admin/grupo/ehvazio"
	$.ajax({
		type : "POST",
		url : methodURL,
		data : "id="+id,
		success : function(response) {
			if(response == 'true') {
				var opts = {
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-top-right",
						"onclick": null,
						"showDuration": "800",
						"hideDuration": "2000",
						"timeOut": "10000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut"
					};
					
					toastr.error("O Grupo que voc� est� tentando remover possui vinculos com outros elementos do sistema, n�o podemos prosseguir com esta a��o.", "Puxa! Achamos um erro:", opts);
			} else {
				excluirGrupo(id);
			}
		},
		error : function(e) {
			alert("Falha ao validar titulo");
		}
	});
}
function usuarioAtivo(idUsuario) {
	var methodURL = "/SmartEditorial/admin/usuario/ativo"
	$.ajax({
		type : "POST",
		url : methodURL,
		data : "idUsuario="+idUsuario,
		success : function(response) {
			location.reload()
		},
		error : function(e) {
			alert("Falha ao carregar os Dados");
		}
	});
}
function chamarModalGrupo(id) {
	$('#botaoSimGrupo').removeAttr('disabled');
	$('#botaoSimGrupo').attr('onclick', 'this.disabled=true, ehVazio('+id+')');
}


function carregarUsuarios(idGrupo) {
	var methodURL= "/SmartEditorial/admin/grupo/interna/usuario"
	$.ajax({
		type : "POST",
		url : methodURL,
		data :"idGrupo="+idGrupo,
		success : function(response) {
			$("#listaDeUsuarios").html(response);
			$(document).ready(function() {
		        $('#dataTablesUsuarios').dataTable();
		    });
		},
		error : function(e) {
			alert("Falha ao carregar os Dados");
		}
	});
}

function vincularUsuario(idGrupo, idUsuario) {
	var methodURL= "/SmartEditorial/admin/grupo/interna/vincularusuario"
	$.ajax({
		type : "POST",
		url : methodURL,
		data :"idGrupo="+idGrupo+"&idUsuario="+idUsuario,
		success : function(response) {
			if (response == "sucesso") {
				var opts = {
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-top-right",
						"onclick": null,
						"showDuration": "800",
						"hideDuration": "2000",
						"timeOut": "10000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut"
					};
					
					toastr.success("Usu�rio vinculado com sucesso!", 
							"Opa! Deu tudo certo:", opts);	
			} else {
				var opts = {
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-top-right",
						"onclick": null,
						"showDuration": "800",
						"hideDuration": "2000",
						"timeOut": "10000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut"
					};
					
					toastr.warning("Parece que este usu�rio j� esta neste grupo!", 
							"Ihhh!", opts);	
			}
			
		},
		error : function(e) {
			var opts = {
					"closeButton": true,
					"debug": false,
					"positionClass": "toast-top-right",
					"onclick": null,
					"showDuration": "800",
					"hideDuration": "2000",
					"timeOut": "10000",
					"extendedTimeOut": "1000",
					"showEasing": "swing",
					"hideEasing": "linear",
					"showMethod": "fadeIn",
					"hideMethod": "fadeOut"
				};
				
				toastr.error(e, 
						"Puxa! Achamos um erro:", opts);
		}
	});
}

function removerUsuario(idGrupo, idUsuario) {
	var methodURL= "/SmartEditorial/admin/grupo/interna/removerusuario"
	$.ajax({
		type : "POST",
		url : methodURL,
		data :"idGrupo="+idGrupo+"&idUsuario="+idUsuario,
		success : function(response) {
			var opts = {
					"closeButton": true,
					"debug": false,
					"positionClass": "toast-top-right",
					"onclick": null,
					"showDuration": "800",
					"hideDuration": "2000",
					"timeOut": "10000",
					"extendedTimeOut": "1000",
					"showEasing": "swing",
					"hideEasing": "linear",
					"showMethod": "fadeIn",
					"hideMethod": "fadeOut"
				};
				
			toastr.success("Usu�rio removido com sucesso", 
					"Opa! Deu tudo certo:", opts);	
		},
		error : function(e) {
			var opts = {
					"closeButton": true,
					"debug": false,
					"positionClass": "toast-top-right",
					"onclick": null,
					"showDuration": "800",
					"hideDuration": "2000",
					"timeOut": "10000",
					"extendedTimeOut": "1000",
					"showEasing": "swing",
					"hideEasing": "linear",
					"showMethod": "fadeIn",
					"hideMethod": "fadeOut"
				};
				
				toastr.error(e, 
						"Puxa! Achamos um erro:", opts);
		}
	});
}


function carregarPermissoes(idGrupo) {
	var methodURL= "/SmartEditorial/admin/grupo/interna/permissao"
	$.ajax({
		type : "POST",
		url : methodURL,
		data :"idGrupo="+idGrupo,
		success : function(response) {
			$("#listaDePermissoes").html(response);
			$(document).ready(function() {
		        $('#dataTablesPemissoes').dataTable();
		    });
		},
		error : function(e) {
			var opts = {
					"closeButton": true,
					"debug": false,
					"positionClass": "toast-top-right",
					"onclick": null,
					"showDuration": "800",
					"hideDuration": "2000",
					"timeOut": "10000",
					"extendedTimeOut": "1000",
					"showEasing": "swing",
					"hideEasing": "linear",
					"showMethod": "fadeIn",
					"hideMethod": "fadeOut"
				};
				
				toastr.error(e, 
						"Puxa! Achamos um erro:", opts);
		}
	});
}

function vincularPermissao(idGrupo, idPermissao) {
	var methodURL= "/SmartEditorial/admin/grupo/interna/vincularpermissao"
	$.ajax({
		type : "POST",
		url : methodURL,
		data :"idGrupo="+idGrupo+"&idPermissao="+idPermissao,
		success : function(response) {
			if (response == "sucesso") {
				var opts = {
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-top-right",
						"onclick": null,
						"showDuration": "800",
						"hideDuration": "2000",
						"timeOut": "10000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut"
					};
					
					toastr.success("Permiss�o vinculada com sucesso", 
							"Opa! Deu tudo certo:", opts);	
			} else {
				var opts = {
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-top-right",
						"onclick": null,
						"showDuration": "800",
						"hideDuration": "2000",
						"timeOut": "10000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut"
					};
					
					toastr.warning("O grupo j� possui esta permiss�o",
							"Ihhh!", opts);
			}
			
		},
		error : function(e) {
			var opts = {
					"closeButton": true,
					"debug": false,
					"positionClass": "toast-top-right",
					"onclick": null,
					"showDuration": "800",
					"hideDuration": "2000",
					"timeOut": "10000",
					"extendedTimeOut": "1000",
					"showEasing": "swing",
					"hideEasing": "linear",
					"showMethod": "fadeIn",
					"hideMethod": "fadeOut"
				};
				
				toastr.error(e, 
						"Puxa! Achamos um erro:", opts);
		}
	});
}

function removerPermissao(idGrupo, idPermissao) {
	var methodURL= "/SmartEditorial/admin/grupo/interna/removerpermissao"
	$.ajax({
		type : "POST",
		url : methodURL,
		data :"idGrupo="+idGrupo+"&idPermissao="+idPermissao,
		success : function(response) {
				var opts = {
					"closeButton": true,
					"debug": false,
					"positionClass": "toast-top-right",
					"onclick": null,
					"showDuration": "800",
					"hideDuration": "2000",
					"timeOut": "10000",
					"extendedTimeOut": "1000",
					"showEasing": "swing",
					"hideEasing": "linear",
					"showMethod": "fadeIn",
					"hideMethod": "fadeOut"
				};
				
				toastr.success("Permiss�o removida com sucesso", 
						"Opa! Deu tudo certo:", opts);
		},
		error : function(e) {
			var opts = {
					"closeButton": true,
					"debug": false,
					"positionClass": "toast-top-right",
					"onclick": null,
					"showDuration": "800",
					"hideDuration": "2000",
					"timeOut": "10000",
					"extendedTimeOut": "1000",
					"showEasing": "swing",
					"hideEasing": "linear",
					"showMethod": "fadeIn",
					"hideMethod": "fadeOut"
				};
				
				toastr.error(e, 
						"Puxa! Achamos um erro:", opts);
		}
	});
}

function existeEmail(email) {
	var methodURL = "usuario/validar"
	$.ajax({
		type: "POST",
		url: methodURL,
		data: "email=" + email,
		success: function (response) {
			if (response == 'true') {
				document.getElementById("mySubmit").disabled = true;
				var opts = {
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-top-right",
						"onclick": null,
						"showDuration": "800",
						"hideDuration": "2000",
						"timeOut": "10000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut"
					};
					
					toastr.error("E-mail j� cadastrado no sistema.", 
							"Puxa! Achamos um erro:", opts);
					validaEmail(); 
			} else {
				document.getElementById("mySubmit").disabled = false;
				validaEmail(); 
			}
		},
		error: function (e) {
			alert("Falha ao validar titulo");
		}
	});
}

function validaEmail() {
	var email = $("#email").val();
	var filtro = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	if (filtro.test(email)) {
		return true;
	} else {
		document.getElementById("mySubmit").disabled = true;
		var opts = {
				"closeButton": true,
				"debug": false,
				"positionClass": "toast-top-right",
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "10000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
			};
			
			toastr.error("Digite um e-mail v�lido.", 
					"Puxa! Achamos um erro:", opts);
	}
};

</script>