<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" 
    prefix="fn" %> 
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %> 
<sec:authorize access="isAnonymous()">
	<script type="text/javascript">
		window.location = "/SmartEditorial/"
	</script>
</sec:authorize>

<link rel="icon"  type="image/png"  href="${myUrl}resources/images/favicon.ico">


<c:set var="req" value="${pageContext.request}" />
<c:set var="baseURL" value="${fn:replace(req.requestURL, fn:substring(req.requestURI, 1, fn:length(req.requestURI)), req.contextPath)}" />
<c:set var="string1" value="${baseURL}/${MyID}"/>
<c:set var="string2" value="${fn:replace(string1, 
                                '8080//', '8080/')}" />
<c:url var="myUrl" value="${string2}" />

<!-- DATA TABLE -->
<link href="${myUrl}resources/css/plugins/dataTables.bootstrap.css" rel="stylesheet">
<!-- DATA TABLE -->

<link rel="stylesheet" type="text/css" href="${myUrl}resources/fontAwesome/css/font-awesome.css">
<link rel="stylesheet" type="text/css" href="${myUrl}resources/fontAwesome/css/font-awesome.min.css">
<link rel="stylesheet" href="${myUrl}/resources/css/font-icons/entypo/css/entypo.css"  id="style-resource-2">
<link rel="stylesheet" href="${myUrl}/resources/css/font-icons/entypo/css/animation.css"  id="style-resource-3">
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic"  id="style-resource-4">
<link rel="stylesheet" type="text/css" href="${myUrl}/resources/fontAwesome/css/font-awesome.min.css">


	<script src="${myUrl}resources/js/jquery-1.10.2.min.js"></script>
	<link rel="stylesheet" href="${myUrl}resources/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css"  id="style-resource-1">
	<link rel="stylesheet" href="${myUrl}resources/css/font-icons/entypo/css/entypo.css"  id="style-resource-2">
	<link rel="stylesheet" href="${myUrl}resources/css/font-icons/entypo/css/animation.css"  id="style-resource-3">
	<link rel="stylesheet" href="${myUrl}resources/css/neon.css"  id="style-resource-5">
	<link rel="stylesheet" href="${myUrl}resources/css/custom.css"  id="style-resource-6">
	<script src="${myUrl}resources/js/gsap/main-gsap.js" id="script-resource-1"></script>
	<script src="${myUrl}resources/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js" id="script-resource-2"></script>
	<script src="${myUrl}resources/js/bootstrap.min.js" id="script-resource-3"></script>
	<script src="${myUrl}resources/js/joinable.js" id="script-resource-4"></script>
	<script src="${myUrl}resources/js/resizeable.js" id="script-resource-5"></script>
	<script src="${myUrl}resources/js/neon-api.js" id="script-resource-6"></script>
	<script src="${myUrl}resources/js/jquery.validate.min.js" id="script-resource-7"></script>
	<script src="${myUrl}resources/js/neon-login.js" id="script-resource-8"></script>
	<script src="${myUrl}resources/js/neon-custom.js" id="script-resource-9"></script>
	<script src="${myUrl}resources/js/neon-demo.js" id="script-resource-10"></script>
	<script src="${myUrl}resources/js/fileinput.js" id="script-resource-10"></script>
	<script src="${myUrl}resources/js/bootstrap-tagsinput.min.js" id="script-resource-8"></script>
	<script src="${myUrl}resources/js/toastr.js" id="script-resource-7"></script>
	<script src="${myUrl}resources/js/jquery.inputmask.bundle.min.js" id="script-resource-7"></script>

<script type="text/javascript">
function getQueryVariable(variable)
{
       var query = window.location.search.substring(1);
       var vars = query.split("&");
       for (var i=0;i<vars.length;i++) {
               var pair = vars[i].split("=");
               if(pair[0] == variable){return pair[1];}
       }
       return(false);
}
function meusArtigos() {
	var methodURL = "/SmartEditorial/artigos/meusartigos";
	$.ajax({
		type : "POST",
		url : methodURL,
		success : function(response) {
			$("#main-content").html(response);
			$(document).ready(function() {
				$('#dataTables-meusArtigos').dataTable();
		    });
		},
		error : function(e) {
			alert("Falha ao carregar os Dados");
		}
	}) 
}
</script>


