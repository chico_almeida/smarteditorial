<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ page session="false" %>
<!DOCTYPE html>
<html>
<head>
<title>Inicio</title>
<%@ include file="../componentes/header/head.jsp" %>
<%@ include file="../componentes/header/scriptAdm.jsp" %>
<script type="text/javascript">
function chamarModalRemoveEdicao(idG, idP) {
	$('#botaoSimPermissao').attr('onclick', 'this.disabled=true, removerPermissao('+idG+', '+idP+')');
}chamarModalRemoveUsuario

function chamarModalRemoveUsuario(idG, idU) {
	$('#botaoSimUsuario').attr('onclick', 'this.disabled=true, removerUsuario('+idG+', '+idU+')');
}
</script>
</head>
<body class="page-body">
	<div class="page-container horizontal-menu">
		<%@ include file="../componentes/header/menu.jsp" %>
		<div class="main-content" id="main-content">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<h2 class="page-header">
							${grupo.nome} <i class="fa fa-users"></i>
						</h2>
					</div>
				</div>
				<!-- /.row -->
				
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									 <label class="control-label"><i class="fa fa-pencil-square-o"></i> ${grupo.nome}<small> Adicione/remova usu�rios ou permiss�es</small></label>
								</h4>
								<hr />
							</div>
							<div class="panel-body">
								<!-- DIV USUARIOS -->
								<div class="col-lg-6">
									<div class="panel panel-default">
										<div class="panel-body">
											<button type="button" class="btn btn-default btn-icon icon-left" onclick="carregarUsuarios(${grupo.idGrupo})" data-toggle="modal" data-target="#usuarioModal" data-keyboard="false" data-backdrop="false">
												Adicionar Usu�rios
												<i class="fa fa-plus"></i>
											</button>
											<table class="table table-bordered" id="dataTables-UsuariosDoGrupo">
												<thead>
													<tr>
														<th>Nome</th>
													</tr>
												</thead>
												<tbody>
													<c:forEach items="${grupo.usuarios}" var="usuario">
														<tr class="even gradeA">
															<td>
																<a href="#" onclick="chamarModalRemoveUsuario(${grupo.idGrupo}, ${usuario.idUsuario})" 
																data-toggle="modal" data-target="#modalRemoveUsuario" data-backdrop="false">
																<i class="fa fa-times"></i></a><b> ${usuario.email} </b><br/>
															</td>
														</tr>
													</c:forEach>
												</tbody>
											</table>
										</div>
									</div>
									<div id="usuarioModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" onclick="location.reload()"><span aria-hidden="true"><i class="fa fa-times fa-2x"></i></span></button>
												</div>
												<div class="modal-body">
													<div id="listaDeUsuarios">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div id="modalRemoveUsuario" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
										<div class="modal-dialog">
											<div class="modal-content">
											
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="location.reload()">�</button>
													<h4 class="modal-title" id="myModalLabel">Deseja Remover?</h4>
												</div>
												<div class="modal-body">
													<p>Voc� est� prestes a remover um Usu�rio deste Grupo, confirma isso?</p>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal" onclick="location.reload()">N�o</button>
													<button type="button" class="btn btn-primary" onclick="" id="botaoSimUsuario">Sim</button>
												</div>
											
											</div><!-- /.modal-content -->
										</div><!-- /.modal-dialog -->
									</div>
								</div>
								<!-- END DIV USUARIOS -->
								
								<div class="col-lg-6">
									<div class="panel panel-default">
										<div class="panel-body">
											<button type="button" class="btn btn-default btn-icon icon-left" onclick="carregarPermissoes(${grupo.idGrupo})" data-toggle="modal" data-target="#permissaoModal" data-keyboard="false" data-backdrop="false">
												Adicionar Permiss�o
												<i class="fa fa-plus"></i>
											</button>
											<table class="table table-bordered" id="dataTables-PermissoesDoGrupo">
												<thead>
													<tr>
														<th>Nome</th>
													</tr>
												</thead>
												<tbody>
													<c:forEach items="${grupo.permissoes}" var="permissao">
														<tr class="even gradeA">
															<td>
																<a href="#" onclick="chamarModalRemoveEdicao(${grupo.idGrupo}, ${permissao.idPermissao})" 
																data-toggle="modal" data-target="#modalRemovePermissao" data-backdrop="false">
																<i class="fa fa-times"></i></a>
																<b> ${permissao.tipo} </b><br/>
															</td>
														</tr>
													</c:forEach>
												</tbody>
											</table>
										</div>
									</div>
									
									<div id="permissaoModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" onclick="location.reload()"><span aria-hidden="true"><i class="fa fa-times fa-2x"></i></span></button>
												</div>
												<div class="modal-body">
													<div id="listaDePermissoes">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div id="modalRemovePermissao" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
										<div class="modal-dialog">
											<div class="modal-content">
											
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="location.reload()">�</button>
													<h4 class="modal-title" id="myModalLabel">Deseja Remover?</h4>
												</div>
												<div class="modal-body">
													<p>Voc� est� prestes a remover uma Permiss�o deste Grupo, confirma isso?</p>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal" onclick="location.reload()">N�o</button>
													<button type="button" class="btn btn-primary" onclick="" id="botaoSimPermissao">Sim</button>
												</div>
											
											</div><!-- /.modal-content -->
										</div><!-- /.modal-dialog -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<%@ include file="../componentes/footer/footer.jsp" %>
			</div>
		</div>
	</div>
</body>
</html>