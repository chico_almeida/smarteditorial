<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ page session="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Administra��o - Permiss�o</title>
<%@ include file="../componentes/header/head.jsp" %>
</head>
<body>
<%@ include file="../componentes/header/menu.jsp" %>

<c:url var="addAction" value="/admin/permissao/add"></c:url>
<c:url var="addActionUpdate" value="/admin/permissao/update/${tipo}"></c:url>


<c:if test="${empty permissao.tipo}">
	<div id="forms">
		<form:form action="${addAction}" commandName="permissao">
			<form:label path="tipo">
				<spring:message text="Tipo"/>
			</form:label>
			<form:input path="tipo" />
			<c:if test="${param.q == 'Tipo'}">
				<label>Permiss�o j� existente no sistema</label>
			</c:if>
			<form:label path="descricao">
				<spring:message text="Descri��o"/>
			</form:label>
			<form:input path="descricao" />
			<input type="submit" value="<spring:message text="Adicionar"/>" />
		</form:form>
	</div>
</c:if>

<c:if test="${!empty permissao.tipo}">
<h4>Alterar uma permiss�o pode afetar todo o funcionamento do sistema.</h4>

	<div id="forms">
	
		<form action="${addActionUpdate}" method="POST" onsubmit="return valida_form(this)">
			<input type="text" name="idPermissao" value="${permissao.idPermissao}" readonly="readonly"/> <br/>
			Tipo: <input id="tipo" type="text" name="tipo" value="${permissao.tipo}"/> <br />
			<c:if test="${param.q == 'Tipo'}"> 
				<label style="color: #FF0000;"><b>Permissao j� cadastrado</b></label>
			</c:if>
			Descricao: <input type="text" name="descricao" value="${permissao.descricao}"/> <br />
			<input type="submit" value="Editar" />
		</form>
	</div>
</c:if>
</body>
</html>