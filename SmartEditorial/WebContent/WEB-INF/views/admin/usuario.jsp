<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ page session="false" %>
<c:url var="addAction" value="/admin/usuario/add"></c:url>
<c:url var="addActionUpdate" value="/admin/usuario/update"></c:url>
	
	<!-- INICIO DA CRIA��O DE USUARIOS -->
	<c:if test="${empty usuario.nome}">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header">
					Cadastrar Usu�rio
				</h2>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<!-- /.panel-heading -->
					<div class="panel-body">
						<div class="table-responsive">
						<form action="${addAction}" method="POST" class="form-horizontal" role="form">
							<div class="form-group">
								<label for="titulo" class="col-sm-3 control-label">Nome</label>
								<div class="col-sm-8">
									<input class="form-control" id="nome" type="text" name="nome" required="required" />
								</div>
							</div> 
							<div class="form-group">
								<label for="titulo" class="col-sm-3 control-label">Sobrenome</label>
								<div class="col-sm-8">
									<input class="form-control" type="text" name="sobrenome" /> 
								</div>
							</div>
							<div class="form-group">
								<label for="titulo" class="col-sm-3 control-label">E-mail</label>
								<div class="col-sm-8">
									<input class="form-control" id="email" type="text" name="email" onchange="existeEmail(this.value)" required="required" /> 
								</div>
							</div>
							<div class="form-group">
								<label for="titulo" class="col-sm-3 control-label">Senha</label>
								<div class="col-sm-8">
									<input class="form-control" type="password" name="senha" required="required" /> 
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-sm-8">
								</div>
								<div class="col-sm-4">
									<button type="submit" class="btn btn-green btn-icon icon-left " onsubmit="this.disabled=true;" id="mySubmit">
		                            	Adicionar
		                            	<i class="fa fa-floppy-o"></i>
		                            </button>
	                            </div>
							</div>
							
						</form>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	</c:if>
	<!-- FIM DA CRIA��O DE USUARIOS -->
	
	<!-- INICIO DA ATUALIZA��O DE USUARIOS -->
	<c:if test="${!empty usuario.nome}"> 
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header">
					Atualizar Usu�rio
				</h2>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<!-- /.panel-heading -->
					<div class="panel-body">
						<div class="table-responsive">
						<form action="${addActionUpdate}" method="POST" class="form-horizontal" role="form">
							<input type="hidden" name="idUsuario" value="${usuario.idUsuario}" readonly="readonly"/>
							
							<div class="form-group">
								<label for="titulo" class="col-sm-3 control-label">Nome</label>
								<div class="col-sm-8">
									<input class="form-control" id="nome" type="text" name="nome" value="${usuario.nome}" />
								</div>
							</div> 
							<div class="form-group">
								<label for="titulo" class="col-sm-3 control-label">Sobrenome</label>
								<div class="col-sm-8">
									<input class="form-control" type="text" name="sobrenome" value="${usuario.sobrenome}" /> 
								</div>
							</div>
							<div class="form-group">
								<label for="titulo" class="col-sm-3 control-label">E-mail</label>
								<div class="col-sm-8">
									<input class="form-control" id="email" type="text" name="email" onchange="validaEmail()" value="${usuario.email}" /> 
								</div>
							</div>
							<div class="form-group">
								<label for="titulo" class="col-sm-3 control-label">Senha</label>
								<div class="col-sm-8">
									<input class="form-control" type="password" name="senha" value="${usuario.senha}" required="required" /> 
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-sm-8">
								</div>
								<div class="col-sm-4">
									<button type="submit" class="btn btn-green btn-icon icon-left " onsubmit="this.disabled=true;" id="mySubmit">
		                            	Atualizar
		                            	<i class="fa fa-floppy-o"></i>
		                            </button>
	                            </div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	</c:if>
	<!-- FIM DA ATUALIZA��O DE USUARIOS -->
	
