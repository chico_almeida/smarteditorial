<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ page session="false" %>
<script type="text/javascript">
function existeGrupo(tituloGrupo) {
	var methodURL= "grupo/validar"
	$.ajax({
		type : "POST",
		url : methodURL,
		data : "titulo="+tituloGrupo,
		success : function(response) {
			if(response == 'true') {
				document.getElementById("mySubmit").disabled = true;
				var opts = {
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-top-right",
						"onclick": null,
						"showDuration": "300",
						"hideDuration": "1000",
						"timeOut": "10000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut"
					};
					
					toastr.error("Titulo de Grupo em Uso.", "Puxa! Achamos um erro:", opts);
					return false;
			} else {
				document.getElementById("mySubmit").disabled = false;
				$("#retorno").html("");
			}
		},
		error : function(e) {
			alert("Falha ao validar titulo");
		}
	});
}

</script>
<c:url var="addAction" value="/admin/grupo/add"></c:url>
<c:url var="addActionUpdate" value="/admin/grupo/update"></c:url>
	<c:if test="${empty grupo.nome}">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header">
					Cadastrar  Grupo
				</h2>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<!-- /.panel-heading -->
					<div class="panel-body">
						<div class="table-responsive">  
						<form action="${addAction}" method="POST" onsubmit="return valida_form(this)" class="form-horizontal" role="form">
							<div class="form-group">
								<label for="titulo" class="col-sm-3 control-label">Nome</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" placeholder="Nome" name="nome" id="tit" onchange="existeGrupo(this.value)" required="required" />
								</div>
							</div> 

							<div class="fielValidation" id="retorno"></div>

							<div class="form-group">
								<label for="titulo" class="col-sm-3 control-label">Descri��o</label>
								<div class="col-sm-8">
									<textarea class="form-control" rows="5" cols="25" name="descricao"></textarea>
								</div>
							</div> 
							<div class="form-group">
								<div class="col-sm-8">
								</div>
								<div class="col-sm-4">
									<button type="submit" class="btn btn-green btn-icon icon-left " onsubmit="this.disabled=true;" id="mySubmit">
		                            	Adicionar
		                            	<i class="fa fa-floppy-o"></i>
		                            </button>
	                            </div>
							</div>
						</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</c:if>
	
	<c:if test="${!empty grupo.nome}">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header">
					Atualizar Grupo
				</h2>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<!-- /.panel-heading -->
					<div class="panel-body">
						<div class="table-responsive">
						<form action="${addActionUpdate}" method="POST" onsubmit="return valida_form(this)" class="form-horizontal" role="form">
							<input type="hidden" name="idGrupo" value="${grupo.idGrupo}"/>
							
							<div class="form-group">
								<label for="titulo" class="col-sm-3 control-label">Nome</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" value="${grupo.nome}" name="nome" id="tit" onchange="existeGrupo(this.value)" required="required" />
								</div>
							</div> 

							<div class="fielValidation" id="retorno"></div>

							<div class="form-group">
								<label for="titulo" class="col-sm-3 control-label">Descri��o</label>
								<div class="col-sm-8">
									<textarea class="form-control" rows="5" cols="25" name="descricao">${grupo.descricao}</textarea>
								</div>
							</div> 
							<div class="form-group">
								<div class="col-sm-8">
								</div>
								<div class="col-sm-4">
									<button type="submit" class="btn btn-green btn-icon icon-left " onsubmit="this.disabled=true;" id="mySubmit">
		                            	Atualizar
		                            	<i class="fa fa-floppy-o"></i>
		                            </button>
	                            </div>
							</div>
						</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</c:if>
