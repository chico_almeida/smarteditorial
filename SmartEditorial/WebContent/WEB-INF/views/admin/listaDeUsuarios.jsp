<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ page session="false" %>
<c:if test="${!empty usuarios}">
	<div class="row">
		<div class="col-lg-12">
			<h2 class="page-header">
				Usu�rios do Sistema
			</h2>
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					Informa��es do Usu�rio
				</div>
				<!-- /.panel-heading -->
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover" id="dataTablesUsuarios">
							<thead>
								<tr>
									<th>Nome</th>
									<th>Sobrenome</th>
									<th>Email</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${usuarios}" var="usuario">
									<tr class="even gradeA">
										<td>
											<a href="#" onclick="vincularUsuario('${grupo.idGrupo}','${usuario.idUsuario}')" >
												${usuario.nome}
											</a>
										</td>
										<td>${usuario.sobrenome}</td>
										<td>${usuario.email}</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
					<!-- /.table-responsive -->
				</div>
				<!-- /.panel-body -->
			</div>
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
</c:if>