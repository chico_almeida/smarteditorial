<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Administração - Permissões</title>
<%@ include file="../componentes/header/head.jsp" %>

</head>
<body>
<%@ include file="../componentes/header/menu.jsp" %>
<a href="${myUrl}admin/permissoes.html">Criar Permissão</a>
<table class="table table-bordered table-striped datatable" id="table-2" style="width: 60% !important;">
	<thead>
		<tr>
			<th>Tipo</th>
			<th>Descrição</th>
			<th>Ações</th>
		</tr>
	</thead>
	
	<tbody>
		<c:forEach items="${permissao}" var="permissao">
		<tr>
			<td>${permissao.tipo}</td>
			<td>${permissao.descricao}</td>
			<td>
				<a href="<c:url value='/admin/editarPermissao/${permissao.idPermissao}' />" class="btn btn-default btn-sm btn-icon icon-left">
					<i class="entypo-pencil"></i>
					Editar
				</a>
				
				<a href="<c:url value='/admin/removerPermissao/${permissao.idPermissao}' />" class="btn btn-danger btn-sm btn-icon icon-left">
					<i class="entypo-cancel"></i>
					Deletar
				</a>
			</td>
		</tr>
		</c:forEach>
		
	</tbody>
</table>


</body>
</html>