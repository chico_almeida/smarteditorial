<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ page session="false" %>
<!DOCTYPE html>
<html>
<head>
<title>Inicio</title>
<%@ include file="../componentes/header/head.jsp" %>
<%@ include file="../componentes/header/scriptAdm.jsp" %>
</head>
<body class="page-body">
	<div class="page-container horizontal-menu">
		<%@ include file="../componentes/header/menu.jsp" %>
		<div class="main-content" id="main-content">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header">
							Grupos do Sistema
						</h1>
					</div>
					<!-- /.col-lg-12 -->
				</div>
				<!-- /.row -->
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									 <label class="control-label"><i class="fa fa-pencil-square-o"></i> Grupos <small>Gerencie seus grupos</small></label>
								</h4>
								<hr />
							</div>
							<div class="panel-body">
								<div class="table-responsive">
									<button type="button" class="btn btn-default btn-icon icon-left" onclick="cadastrarGrupo()" data-toggle="modal" data-target="#modalGrupo" data-keyboard="false" data-backdrop="false">
										Criar Grupo
										<i class="fa fa-plus"></i>
									</button>
									<table class="table table-striped table-bordered table-hover" id="dataTables-ListaDeGrupos">
										<thead>
											<tr>
												<th>Grupo</th>
												<th>Descri��o</th>
												<th>Permiss�es</th>
												<th>A��es</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${grupos}" var="grupo">
												<tr class="even gradeA">
													<td>
														<a href="grupos/interna?idGrupo=${grupo.idGrupo}">
														<i class="fa fa-angle-right"></i> 
															${grupo.nome}
														</a>
													</td>
													<td>${grupo.descricao}</td>
													<td>
														<c:forEach items="${grupo.permissoes}" var="permissao">
															${permissao.tipo}
															<br />
														</c:forEach>
													</td>
													<td>
														<a href="#" class="btn btn-default btn-sm btn-icon icon-left" onclick="editarGrupo(${grupo.idGrupo})" data-toggle="modal" data-target="#modalGrupo" data-keyboard="false" data-backdrop="false">
															<i class="entypo-pencil"></i>
															Editar
														</a>
														
														<a href="#" onclick="chamarModalGrupo(${grupo.idGrupo})" class="btn btn-danger btn-sm btn-icon icon-left"  data-toggle="modal" data-target="#modalRemoveGrupo" data-backdrop="false">
															<i class="entypo-cancel"></i>
															Deletar
														</a>
													</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
									<div id="modalRemoveGrupo" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
										<div class="modal-dialog">
											<div class="modal-content">
											
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
													<h4 class="modal-title" id="myModalLabel">Deseja Remover?</h4>
												</div>
												<div class="modal-body">
													<p id="modalMessage">Voc� est� prestes a remover um Grupo do sistema, confirma isso?</p>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">N�o</button>
													<button type="button" class="btn btn-primary" onclick="" id="botaoSimGrupo">Sim</button>
												</div>
											
											</div><!-- /.modal-content -->
										</div><!-- /.modal-dialog -->
									</div>
									<div id="modalGrupo" class="modal fade" tabindex="-1"
										role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">
														<span aria-hidden="true"><i class="fa fa-times fa-2x"></i></span>
													</button>
												</div>
												<div class="modal-body" id="modalBodyGrupo">
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- /.table-responsive -->
							</div>
							<!-- /.panel-body -->
						</div>
						<!-- /.panel -->
					</div>
					<!-- /.col-lg-12 -->
				</div>
				<%@ include file="../componentes/footer/footer.jsp" %>
			</div>
		</div>
	</div>
</body>
</html>