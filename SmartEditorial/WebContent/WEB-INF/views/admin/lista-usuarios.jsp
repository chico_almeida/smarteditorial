<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ page session="false" %>
<!DOCTYPE html>
<html>
<head>
<title>Usu�rios</title>
<%@ include file="../componentes/header/head.jsp" %>
<%@ include file="../componentes/header/scriptAdm.jsp" %>
<script type="text/javascript">
function chamarModalDesativar(id, nome, acao) {
	$("#acao").html(acao);
	$("#nomeUsuario").html(nome);
	$('#botaoSimDesativar').attr('onclick', 'this.disabled=true, usuarioAtivo('+id+')');
}
function chamarModalRemoveUsuario(idU) {
	$('#botaoSimUsuario').attr('onclick', 'this.disabled=true, excluirUsuario('+idU+')');
}
</script>
</head>
<body class="page-body">
	<div class="page-container horizontal-menu">
		<%@ include file="../componentes/header/menu.jsp" %>
		<div class="main-content" id="main-content">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header">
							Usuario do Sistema
						</h1>
					</div>
					<!-- /.col-lg-12 -->
				</div>
				<!-- /.row -->
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									 <label class="control-label"><i class="fa fa-pencil-square-o"></i> Usu�rios <small>Gerencie os usu�rios do Sistema</small></label>
								</h4>
								<hr />
							</div>
							<div class="panel-body">
								<div class="table-responsive">
									<button type="button" class="btn btn-default btn-icon icon-left" onclick="cadastrarUsuario()" 
									data-toggle="modal" data-target="#modalUsuario" data-keyboard="false" data-backdrop="false">
										Criar Usu�rio
										<i class="fa fa-plus"></i>
									</button>
									<table class="table table-striped table-bordered table-hover" id="dataTables-ListaDeUsuarios">
										<thead>
											<tr>
												<th>Nome</th>
												<th>Sobrenome</th>
												<th>Email</th>
												<th>Estado</th>
												<th>A��es</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${listarUsuarios}" var="usuario">
												<tr class="even gradeA">
													<td>${usuario.nome}</td>
													<td>${usuario.sobrenome}</td>
													<td>${usuario.email}</td>
													<td>
														<c:if test="${usuario.ativo == true }">
															<a href="#" onclick="chamarModalDesativar('${usuario.idUsuario}', '${usuario.nome}', 'desativar')"
															data-toggle="modal" data-target="#modalDesativar" data-backdrop="false">
																<img src="/SmartEditorial/resources/images/check-green.png" width="20" height="20">
															</a>
														</c:if>
														<c:if test="${usuario.ativo == false }">
															<a href="#" onclick="chamarModalDesativar('${usuario.idUsuario}', '${usuario.nome}', 'ativar')" 
															data-toggle="modal" data-target="#modalDesativar" data-backdrop="false">
																<img src="/SmartEditorial/resources/images/check-red.png" width="20" height="20">
															</a>
														</c:if>
													</td>
													<td>
														<a href="#" onclick="editarUsuario(${usuario.idUsuario})" class="btn btn-default btn-sm btn-icon icon-left"
														data-toggle="modal" data-target="#modalUsuario" data-keyboard="false" data-backdrop="false">
															<i class="entypo-pencil"></i>
															Editar
														</a>
														<a href="#" onclick="chamarModalRemoveUsuario(${usuario.idUsuario})" class="btn btn-danger btn-sm btn-icon icon-left"
														data-toggle="modal" data-target="#modalRemoveUsuario" data-backdrop="false">
															<i class="entypo-cancel"></i>
															Deletar
														</a>
													</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
									<div id="modalUsuario" class="modal fade" tabindex="-1"
										role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">
														<span aria-hidden="true"><i class="fa fa-times fa-2x"></i></span>
													</button>
												</div>
												<div class="modal-body" id="modalBodyUsuario">
												</div>
											</div>
										</div>
									</div>
									<div id="modalDesativar" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-body">
													<h4 id="modalMessage" align="center">Voc� deseja <b id="acao"></b> o usu�rio <b id="nomeUsuario"></b></h4>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">N�o</button>
													<button type="button" class="btn btn-primary" onclick="" id="botaoSimDesativar">Sim</button>
												</div>
											
											</div><!-- /.modal-content -->
										</div><!-- /.modal-dialog -->
									</div>
									<div id="modalRemoveUsuario" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
										<div class="modal-dialog">
											<div class="modal-content">
											
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="location.reload()">�</button>
													<h4 class="modal-title" id="myModalLabel">Deseja Remover?</h4>
												</div>
												<div class="modal-body">
													<p>Voc� est� prestes a remover um Usu�rio do sistema', confirma isso?</p>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal" onclick="location.reload()">N�o</button>
													<button type="button" class="btn btn-primary" onclick="" id="botaoSimUsuario">Sim</button>
												</div>
											
											</div><!-- /.modal-content -->
										</div><!-- /.modal-dialog -->
									</div>
								</div>
								<!-- /.table-responsive -->
							</div>
							<!-- /.panel-body -->
						</div>
						<!-- /.panel -->
					</div>
					<!-- /.col-lg-12 -->
				</div>
				<!-- /.row -->
				<%@ include file="../componentes/footer/footer.jsp" %>
			</div>
		</div>
	</div>
</body>
</html>