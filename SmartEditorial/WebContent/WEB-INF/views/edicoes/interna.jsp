<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page session="false" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Publica��es</title>
<%@ include file="../componentes/header/head.jsp" %>
<script type="text/javascript">
 window.onload = function(){
	$(document).ready(function() {
        $('#dataTables-ArtigosFinalizados').dataTable();
        $('#dataTables-ImagensFinalizadas').dataTable();
        $('#dataTables-AnunciosFinalizados').dataTable();
        $('#dataTables-PaginasFinalizadas').dataTable();
    });
};

function loadModalPreview(e, t, n) {
	if (t == "artigo") {
		var r = "/SmartEditorial/artigo/preview";
		$.ajax({
			type: "POST",
			url: r,
			data: "id=" + e,
			success: function (e) {
				$("#divRetornoPreview").html(e)
			},
			error: function (e) {
				alert("Falha ao carregar os Dados")
			}
		})
	} else if (t == "imagem") {
		var r = "/SmartEditorial/imagem/preview";
		$.ajax({
			type: "POST",
			url: r,
			data: "id=" + e,
			success: function (e) {
				$("#divRetornoPreview").html(e)
			},
			error: function (e) {
				alert("Falha ao carregar os Dados")
			}
		})
	} else if (t == "anuncio") {
		var r = "/SmartEditorial/anuncio/preview";
		$.ajax({
			type: "POST",
			url: r,
			data: "id=" + e,
			success: function (e) {
				$("#divRetornoPreview").html(e)
			},
			error: function (e) {
				alert("Falha ao carregar os Dados")
			}
		})
	}
}
</script>
</head>

<body class="page-body">
<div class="page-container horizontal-menu">
	<%@ include file="../componentes/header/menu.jsp" %>
	<div class="main-content" id="main-content">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="row">
						<div class="col-lg-12">
							<h2 class="page-header">
								Elementos da Edi��o <i class="fa fa-th-list"></i>
							</h2>
							<div class="panel-body">
								<ul class="nav nav-tabs" role="tablist" id="myTab">
								  <li role="presentation" class="active"><a href="#home" role="tab" data-toggle="tab">P�ginas</a></li>
								  <li role="presentation"><a href="#profile" role="tab" data-toggle="tab">Imagens</a></li>
								  <li role="presentation"><a href="#messages" role="tab" data-toggle="tab">Anuncios</a></li>
								  <li role="presentation"><a href="#settings" role="tab" data-toggle="tab">Artigos</a></li>
								</ul>
								
								<div class="tab-content">
								  <div role="tabpanel" class="tab-pane active" id="home">
								  	<table class="table table-bordered"
										id="dataTables-PaginasFinalizadas">
										<thead>
											<tr>
												<th>Titulo</th>
												<th>Autor</th>
												<th>Estado</th>
												<th>Publica��o</th>
												<th>Edi��o</th>
												<th>Categoria</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${listaPaginasFinalizadas}" var="pagina">
												<tr class="even gradeA">
												
													<td><b><a data-toggle="modal" data-target="#modalPreviewElemento" 
													onclick="loadModalPreview(${pagina.idPagina}, 'pagina', '${contextPath}${pagina.caminho}')"  
													data-keyboard="false" data-backdrop="false">
													<i class="fa fa-folder-open-o"></i> ${pagina.titulo}</a> </b></td>
													<td>${pagina.usuario.nome}</td>
													<td>${pagina.estado}</td>
													<td>${pagina.edicao.publicacao.titulo}</td>
													<td>${pagina.edicao.titulo}</td>
													<td>${pagina.categoria.titulo}</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								  </div>
								  <div role="tabpanel" class="tab-pane" id="profile">
									<table class="table table-bordered"
										id="dataTables-ImagensFinalizadas">
										<thead>
											<tr>
												<th>Titulo</th>
												<th>Autor</th>
												<th>Estado</th>
												<th>Publica��o</th>
												<th>Edi��o</th>
												<th>Categoria</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${listaImagensFinalizadas}" var="imagem">
												<tr class="even gradeA">
													<td><b> <a data-toggle="modal" data-target="#modalPreviewElemento" 
													onclick="loadModalPreview(${imagem.idImagem}, 'imagem', '${contextPath}${imagem.caminho}')"  
													data-keyboard="false" data-backdrop="false">
													<i class="fa fa-folder-open-o"></i> ${imagem.titulo}</a> </b></td>
													<td>${imagem.usuario.nome}</td>
													<td>${imagem.estado}</td>
													<td>${imagem.edicao.publicacao.titulo}</td>
													<td>${imagem.edicao.titulo}</td>
													<td>${imagem.categoria.titulo}</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								  </div>
								  <div role="tabpanel" class="tab-pane" id="messages">
								  	<table class="table table-bordered"
										id="dataTables-AnunciosFinalizados">
										<thead>
											<tr>
												<th>Titulo</th>
												<th>Autor</th>
												<th>Estado</th>
												<th>Publica��o</th>
												<th>Edi��o</th>
												<th>Categoria</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${listaAnunciosFinalizadas}" var="anuncio">
												<tr class="even gradeA">
													<td><b> 
													<a data-toggle="modal" data-target="#modalPreviewElemento" 
													onclick="loadModalPreview(${anuncio.idAnuncio}, 'anuncio', '${contextPath}${anuncio.caminho}')"  
													data-keyboard="false" data-backdrop="false">
														<i class="fa fa-folder-open-o"></i> ${anuncio.titulo}</a> </b></td>
													<td>${anuncio.usuario.nome}</td>													
													<td>${anuncio.estado}</td>
													<td>${anuncio.edicao.publicacao.titulo}</td>
													<td>${anuncio.edicao.titulo}</td>
													<td>${anuncio.categoria.titulo}</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								  </div>
								  <div role="tabpanel" class="tab-pane" id="settings">
								  	<table class="table table-bordered"
										id="dataTables-ArtigosFinalizados">
										<thead>
											<tr>
												<th>Titulo</th>
												<th>Artigo</th>
												<th>Estado</th>
												<th>Publica��o</th>
												<th>Edi��o</th>
												<th>Categoria</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${listaArtigosFinalizadas}" var="artigo">
												<tr class="even gradeA">
													<td><b>
													<a data-toggle="modal" data-target="#modalPreviewElemento" 
													onclick="loadModalPreview(${artigo.idArtigo}, 'artigo', '${contextPath}${artigo.caminho}')"  
													data-keyboard="false" data-backdrop="false">
													
													<i class="fa fa-folder-open-o"></i> ${artigo.titulo}</a> </b></td>
													<td>${artigo.usuario.nome}</td>
													<td>${artigo.estado}</td>
													<td>${artigo.edicao.publicacao.titulo}</td>
													<td>${artigo.edicao.titulo}</td>
													<td>${artigo.categoria.titulo}</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								  </div>
								</div>
								
								<script>
								  $(function () {
								    $('#myTab a:first').tab('show')
								  })
								</script>
								<div id="modalPreviewElemento" class="modal fade" tabindex="-1" role="dialog" 
									onclick="location.reload()" aria-labelledby="myModalLabel" aria-hidden="true">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" onclick="location.reload()" class="close" data-dismiss="modal">
												<span aria-hidden="true"><i class="fa fa-times fa-2x"></i></span>
												</button>
											</div>
											<div class="modal-body" id="divRetornoPreview">
												<img id="elemento" class="col-md-12" src="">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<%@ include file="../componentes/footer/footer.jsp" %>
		</div>
	</div>
</div>
</body>
</html>
