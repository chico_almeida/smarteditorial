<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ page session="false" %>
<script>

	function existeEdicao(tituloEdicao, publicacao) {
		var methodURL = "/SmartEditorial/edicoes/validar"
		$.ajax({
			type : "POST",
			url : methodURL,
			data : "titulo=" + tituloEdicao + "&publicacao=" + publicacao,
			success : function(response) {
				if (response == 'true') {
					$("#retorno").html(
							"Ops! Edi��o ja cadastrada nesta Publica��o");
					var opts = {
							"closeButton": true,
							"debug": false,
							"positionClass": "toast-top-right",
							"onclick": null,
							"showDuration": "300",
							"hideDuration": "1000",
							"timeOut": "10000",
							"extendedTimeOut": "1000",
							"showEasing": "swing",
							"hideEasing": "linear",
							"showMethod": "fadeIn",
							"hideMethod": "fadeOut"
						};
						
						toastr.error("Uma edi��o com este titulo j� existe nesta Publica��o.", "Puxa! Achamos um erro:", opts);

					
					document.getElementById("mySubmit").disabled = true;

				} else {
					document.getElementById("mySubmit").disabled = false;
					$("#retorno").html("");
				}
			},
			error : function(e) {
				alert("Falha ao validar titulo");
			}
		});
	}
	
	function ehDiferente(tituloEdicao, publicacao) {
		/* Valida se a Edi��o, ao ser atulizada, possui titulo diferente da versao atual*/
		var titulo = document.getElementById("tituloUpdate").value;
		if (titulo != tituloEdicao) {
			existeEdicao(titulo, publicacao);
		} else {
			alert(titulo + tituloEdicao)
			return true;
		}
	}

	function resetTit() {
		$('#tit').val(''), $("#retorno").html("");
	}
	
	function validarData() {
		var dataLimite = document.getElementById("datepicker1").value;
		var dataPublicacao = document.getElementById("datepicker2").value;

		if (dataLimite >= dataPublicacao) {
			
			var opts = {
					"closeButton": true,
					"debug": false,
					"positionClass": "toast-top-right",
					"onclick": null,
					"showDuration": "300",
					"hideDuration": "1000",
					"timeOut": "10000",
					"extendedTimeOut": "1000",
					"showEasing": "swing",
					"hideEasing": "linear",
					"showMethod": "fadeIn",
					"hideMethod": "fadeOut"
				};
				
				toastr.error("A Data Limite para edi��o deve ser igual ou inferior a Data de Publica��o.", "Puxa! Achamos um erro:", opts);

			document.getElementById("datepicker1").focus();
			return false;
		} else {
			$("#mensagem").html("");
		}
	}
</script>

<c:url var="addAction" value="/edicoes/cadastro/criar"></c:url>
<c:url var="updateAction" value="/edicoes/cadastro/update"></c:url>
	<c:if test="${empty edicao.titulo}">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header">
					Criar Edi��o
				</h2>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="table-responsive">
							<form action="${addAction}" method="POST" class="form-horizontal" role="form">
								<input type="hidden" name="idPub" value="${idPub}" />
								<div class="form-group">
									<label for="titulo" class="col-sm-3 control-label">Titulo</label>
									<div class="col-sm-8">
										<input type="text" required="required" class="form-control" placeholder="Titulo da Edi��o" id="tit" name="titulo" onchange="existeEdicao(this.value,'${idPub}')">
									</div>
								</div>
								
								<div class="form-group">
									<label for="titulo" class="col-sm-3 control-label">Descri��o</label>
									<div class="col-sm-8">
										<textarea class="form-control" rows="3" cols="25" name="descricao"></textarea>
									</div>
								</div>
								
								<div class="form-group">
									<label for="titulo" class="col-sm-3 control-label">Edit�vel at�:</label>
									<div class="col-sm-8">
										<input type="date" class="form-control" name="dataLimiteEdicao" id="datepicker1" required="required"/>
									</div>
								</div>
								
								<div class="form-group">
									<label for="titulo" class="col-sm-3 control-label">Data Publica��o:</label>
									<div class="col-sm-8">
										<input type="date" class="form-control" name="dataPublicacao" id="datepicker2" required="required" />
									</div>
								</div>
								
								<div id="mensagem" style="color: red;" class="col-lg-12"> </div>
								<div class="col-md-12">
									<p class="bs-example">
										<button type="submit" class="btn btn-green btn-icon icon-left" onsubmit="this.disabled=true;" id="mySubmit" onclick="return valuelidarData()">
									    	Adicionar
									    	<i class="fa fa-floppy-o"></i>
									    </button>
									</p>
								</div>	
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</c:if>
	
	<c:if test="${!empty edicao.titulo}">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header">
					Atualizar Edi��o
				</h2>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="table-responsive">
							<form action="${updateAction}" method="POST" class="form-horizontal" role="form">
								<input type="hidden" name="idPublicacao" value="${idPublicacao}" />
								<input type="hidden" value="${edicao.idEdicao}" name="idEdicao" />
								
								<div class="form-group">
									<label for="titulo" class="col-sm-3 control-label">Titulo</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" value="${edicao.titulo}" name="titulo" id="tituloUpdate" onchange="ehDiferente(${edicao.titulo}, ${idPublicacao})" required="required">
									</div>
								</div>
								
								<div class="form-group">
									<label for="titulo" class="col-sm-3 control-label">Descri��o</label>
									<div class="col-sm-8">
										<textarea class="form-control" rows="3" cols="25" name="descricao">${edicao.descricao}</textarea>
									</div>
								</div>
								
								<div class="form-group">
									<label for="titulo" class="col-sm-3 control-label">Edit�vel at�:</label>
									<div class="col-sm-8">
										<input type="date" class="form-control" name="dataLimiteEdicao" id="datepicker1" value="${edicao.dataLimiteEdicao}" />
									</div>
								</div>
								
								<div class="form-group">
									<label for="titulo" class="col-sm-3 control-label">Data Publica��o:</label>
									<div class="col-sm-8">
										<input type="date" class="form-control" name="dataPublicacao" id="datepicker2" value="${edicao.dataPublicacao}" />
									</div>
								</div>
							
								<div id="mensagem" style="color: red;" class="col-lg-12"> </div>
								<div class="col-md-12">
									<button type="submit" class="btn btn-green btn-icon icon-left" onsubmit="this.disabled=true;" id="mySubmit">
		                            	Atualizar
		                            	<i class="fa fa-floppy-o"></i>
		                            </button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</c:if>
