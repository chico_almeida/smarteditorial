<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page session="false"%>
<table class="table table-bordered" id="dataTables-EdicoesDaPublicacao">
	<thead>
		<tr>
			<th>Titulo</th>
			<th>Estado</th>
			<th>Publica��o</th>
			<th>Edi��o</th>
			<th>Categoria</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${listaAnunciosFinalizadas}" var="anuncio">
			<tr class="even gradeA">
				<td><b> <a href="#"><i class="fa fa-folder-open-o"></i>
							${anuncio.titulo}</a>
				</b></td>
				<td>${anuncio.estado}</td>
				<td>${anuncio.edicao.publlicacao.titulo}</td>
				<td>${anuncio.edicao.titulo}</td>
				<td>${anuncio.categoria.titulo}</td>
			</tr>
		</c:forEach>
	</tbody>
</table>